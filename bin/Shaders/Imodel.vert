#version 430

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 tangent;
layout (location = 3) in vec2 texCoords;

//and matrix columns are in location 3,4,5,6
layout (location = 4) in mat4 instanceMatrix;

#pragma sar include("Shaders/common.include")

#ifdef SHADER_SHADOW
out GS_IN{
	vec2 TexCoords;
	flat uint DrawID;
	} outData;
#else
out VertexData{
	vec2 TexCoords;
	flat uint DrawID;
	vec3 Normal;
	vec3 Tangent;
	vec3 Position; 
}outData;
#endif





uniform mat4 VP;
uniform mat4 BaseMat;

void main()
{
	mat4 model = instanceMatrix *  BaseMat;
	vec4 pos = model * vec4(position, 1.0f);
#ifdef SHADER_SHADOW
	gl_Position = pos;
#else
    gl_Position = BasicVP * pos;
	outData.Position = pos.xyz;	
	outData.Normal = (model * vec4(normal, 0.0)).xyz;
	outData.Tangent = (model * vec4(tangent, 0.0)).xyz;
#endif	
	outData.DrawID = gl_InstanceID;	

    outData.TexCoords = texCoords;

}
