// shadertype=glsl

#version 420

uniform ivec2 viewport;


//
// Inputs
//
layout(triangles) in;
in vec2 tes_terrainTexCoord[];
in vec2 tes_coordsTriangle[];
in float tes_tessLevel[];


//
// Outputs
//
layout(triangle_strip, max_vertices = 4) out;

out vec4 gs_wireColor;
noperspective out vec3 gs_edgeDist;
out vec2 gs_terrainTexCoord;

in TES_OUT { 
	vec2 coordsTriangle; 	
	vec2 terrainTexCoord;
	vec3 position; } 
gs_in[]; 
out GS_OUT { vec2 coordsTriangle; vec3 position;} gs_out; 


vec4 wireframeColor()
{
	if (tes_tessLevel[0] == 64.0)
		return vec4(1.0, 0.0, 0.0, 1.0);
	else if (tes_tessLevel[0] >= 32.0)
		return vec4(1.0, 0.5, 0.0, 1.0);
	else if (tes_tessLevel[0] >= 16.0)
		return vec4(0.0, 1.0, 0.0, 1.0);
	else if (tes_tessLevel[0] >= 8.0)
		return vec4(0.0, 0.0, 1.0, 1.0);
	else
		return vec4(0.58039, 0.0, 0.82745, 1.0);
}

void main()
{
	vec4 wireColor = wireframeColor();

	// Calculate edge distances for wireframe
	float ha, hb, hc;
	
		vec2 p0 = vec2(viewport * (gl_in[0].gl_Position.xy / gl_in[0].gl_Position.w));
		vec2 p1 = vec2(viewport * (gl_in[1].gl_Position.xy / gl_in[1].gl_Position.w));
		vec2 p2 = vec2(viewport * (gl_in[2].gl_Position.xy / gl_in[2].gl_Position.w));

		float a = length(p1 - p2);
		float b = length(p2 - p0);
		float c = length(p1 - p0);
		float alpha = acos( (b*b + c*c - a*a) / (2.0*b*c) );
		float beta = acos( (a*a + c*c - b*b) / (2.0*a*c) );
		ha = abs( c * sin( beta ) );
		hb = abs( c * sin( alpha ) );
		hc = abs( b * sin( alpha ) );
	

	// Output verts
	for(int i = 0; i < gl_in.length(); ++i)
	{
		gl_Position = gl_in[i].gl_Position;
		gs_terrainTexCoord = gs_in[i].terrainTexCoord;
		gs_out.coordsTriangle = gs_in[i].coordsTriangle;
		gs_out.position = gs_in[i].position;
		gs_wireColor = wireColor;

		if (i == 0)
			gs_edgeDist = vec3(ha, 0, 0);
		else if (i == 1)
			gs_edgeDist = vec3(0, hb, 0);
		else
			gs_edgeDist = vec3(0, 0, hc);

		EmitVertex();
	}

	// This closes the the triangle
	gl_Position = gl_in[0].gl_Position;
	gs_edgeDist = vec3(ha, 0, 0);
	gs_terrainTexCoord = gs_in[0].terrainTexCoord;	
	gs_out.coordsTriangle = gs_in[0].coordsTriangle;
	gs_out.position = gs_in[0].position;

	gs_wireColor = wireColor;
	EmitVertex();
	
	EndPrimitive();
}