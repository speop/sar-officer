#version 430

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

out vec2 TexCoords;

uniform mat4 VP;
uniform mat4 BaseMat;

void main()
{
    gl_Position = VP * BaseMat* vec4(position, 1.0f);
    TexCoords = texCoords;
}
