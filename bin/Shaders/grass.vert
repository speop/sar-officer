#version 440

struct Grass {
	vec4 normal;
	vec4 seed;
};

layout (binding = 10, std430 ) buffer GrassSeeds{
	Grass grassSeeds[];
};

out vertex{
	uint drawID;
} vertex_o;

void main(){
	vertex_o.drawID = gl_InstanceID;
	gl_Position = grassSeeds[gl_InstanceID].seed;
}