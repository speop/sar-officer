#version 440
#pragma sar include("Shaders/common.include")
layout(binding=0) uniform sampler2D DiffuseAndSpecular;
layout(binding=1) uniform sampler2D Normal;
layout(binding=2) uniform sampler2D Postion;
layout(binding=3) uniform usampler2D ID;
layout(binding=4) uniform sampler2D TexNoise;
layout(binding=5) uniform sampler2DRect  Depth;

layout(location = 0) uniform vec3 kernels[16];

in vec2 vCoord;
layout (location = 0) out float o_occlusion;

int kernelSize = 16;
float radius =50;
float radiusV =0.5;
float bias = 0.03;

void main(){


	vec2 coords = (vCoord +1) / 2.f;
	vec2 noiseScale = vec2(ScreenDim.x/4.f, ScreenDim.y/4.f); 
	ivec2 fullCoords = ivec2(coords * vec2(textureSize(Depth, 0)));

	uint objId = texture(ID, coords).r;


	vec3 objColor =  texture(DiffuseAndSpecular, coords).rgb;
	vec3 objNormal = texture(Normal, coords).rgb;
	vec3 objPos = texture(Postion, coords).rgb;
	vec3 randomVec = texture(TexNoise, coords * noiseScale).rgb;
	float dwrwrwerffvasddf =  texelFetch(Depth, fullCoords).r;


	if(objId == SKY_ID){
		o_occlusion = 1.f;
	}
	else{
		objNormal = objNormal *2.f - 1.f;

		vec3 tangent = normalize(randomVec - objNormal * dot(randomVec, objNormal));
		vec3 bitangent = cross(objNormal, tangent);
		mat3 TBN = mat3(tangent, bitangent, objNormal);

		float objDepth =  (BasicV *vec4(objPos, 1.f)).z;
		
		// Iterate over the sample kernel and calculate occlusion factor
		float occlusion = 0.0;		
		int sampledPixels = 0;

		for(int i = 0; i < kernelSize; ++i)
		{
			// get sample position
			vec3 kernelSample = TBN * kernels[i]; // From tangent to world-space
			kernelSample = objPos + kernelSample * radius; 
        
			// project sample position
			vec4 ssaoCords = vec4(kernelSample, 1.0);
			ssaoCords = BasicVP * ssaoCords; // from world to clip-space
			ssaoCords.xy /= ssaoCords.w; // perspective divide
			ssaoCords.xy = ssaoCords.xy * 0.5 + 0.5; // transform to range 0.0 - 1.0
			
			if(ssaoCords.x < 0.f) ssaoCords.x = abs(ssaoCords.x);
			else if(ssaoCords.x > 1.f) ssaoCords.x = 2 - ssaoCords.x;
			
			if(ssaoCords.y < 0.f) ssaoCords.y = abs(ssaoCords.y);
			else if(ssaoCords.y > 1.f) ssaoCords.y = 2 - ssaoCords.y;
			     
				// get sample depth
				uint sampleId = texture(ID, ssaoCords.xy).r;
				if(sampleId == SKY_ID) continue;
				vec3 sampleNormal =normalize(texture(Normal, ssaoCords.xy).rgb *2.f - 1.f);
			
				vec3 samplePos =texture(Postion, ssaoCords.xy).rgb;
				float sampleDepth = (BasicV*vec4(samplePos,1)).z; // Get depth value of kernel sample
        
				// range check & accumulate 
				float rangeCheck = smoothstep(0.0, 1.0, radiusV / abs(objDepth - sampleDepth));
				float angle = abs(dot(sampleNormal,objNormal));
				//if(!isnan(angle) && angle > 0.f && angle < 1.f){
				//		occlusion += (sampleDepth >= objDepth +bias ? ((1.0f- abs(dot(sampleNormal,objNormal)))/4.f +0.75f ): 0.0) * rangeCheck; 
				//} 
				//else {
					occlusion += (sampleDepth >= objDepth +bias ?1.f : 0.0) * rangeCheck;				
				//}
				
				sampledPixels++;
			
		
		
		}
		occlusion = 1.0f - (occlusion / sampledPixels);	
		o_occlusion =  occlusion;
	}
	
}


