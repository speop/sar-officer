// shadertype=glsl
#version 420

layout(triangles) in;
layout(triangle_strip, max_vertices = 12) out;

#pragma sar include("Shaders/common.include")

#ifndef CAMERAS
#define CAMERAS csmVP
#endif

#ifndef LAYERS
#define LAYERS 4
#endif

in GS_IN { 
	vec2 TexCoords;
	#ifdef DRAWID
	flat uint DrawID;
	#endif
	} 
gs_in[]; 

out GS_OUT{ 
	vec2 TexCoords;
	#ifdef DRAWID
	flat uint DrawID;
	#endif
} gs_out; 


void main()
{
	mat4 VP;
	mat4 tfMatrix;
 

	for(int layer = 0; layer < LAYERS; layer++)
	{
		gl_Layer = layer;
		VP = CAMERAS[layer];
		
		for(int i=0 ; i<3 ; i++)
		{
			gl_Position = VP*gl_in[i].gl_Position;
			gs_out.TexCoords = gs_in[i].TexCoords;
			#ifdef DRAWID
				gs_out.DrawID =  gs_in[i].DrawID;
			#endif
			EmitVertex();
		}
	
		EndPrimitive();
	}

}