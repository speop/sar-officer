#version 440
layout(binding=0) uniform sampler2D SSAO;

in vec2 vCoord;
out float color;

#define KERNEL_SIZE 16

// Gaussian kernel
// 1 2 2 1
// 2 4 4 2
// 2 4 4 2
// 1 2 2 1	
const float kernel[KERNEL_SIZE] = { 
	1.0f/36.0f, 2.0f/36.0f, 2.0f/36.0f, 1.0f/36.0f,
	2.0f/36.0f, 4.0f/36.0f, 4.0f/36.0f, 2.0f/36.0f,
	2.0f/36.0f, 4.0f/36.0f, 4.0f/36.0f, 2.0f/36.0f,
	1.0f/36.0f, 2.0f/36.0f, 2.0f/36.0f, 1.0f/36.0f 
};


void main(){
	vec2 coords = (vCoord +1) / 2.f;
    vec2 texelSize = 1.0f / vec2(textureSize(SSAO, 0));
    float result = 0.0;


    for (int x = 0; x < 4; x++) 
    {
        for (int y = 0; y < 4; ++y) 
        {
            vec2 offset = vec2(float(x), float(y)) * texelSize;
            result += texture(SSAO, coords + offset).r * kernel[y*4 +x];
        }
    }
    color = result ;
  
	
}

