#version 440
#pragma sar include("Shaders/common.include")
#pragma sar include("Shaders/commonGeometryFrag.include")

struct Grass {
	vec4 normal;
	vec4 seed;
};

layout (binding = 10, std430 ) buffer GrassSeeds{
	Grass grassSeeds[];
};

in geometry{
	vec3 position;
	vec3 normal;
	vec3 color;	
	flat uint drawID;
} frag_in;

void main(){
	vec4 color;
	color = vec4(1,0,1,1);

	o_diffuseAndSpecular = vec4(frag_in.color,0.0);
	o_normal = normalize(grassSeeds[frag_in.drawID].normal.xyz - vec3(frag_in.normal.xyz)/10.f);//vec3(0,1,0);
	o_position = frag_in.position;
	o_ID = GRASS_ID;
}