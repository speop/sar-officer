#version 440

//in vec2 TexCoords;
//out vec4 color;

layout(binding=0) uniform sampler2DArray  Diffuse;
layout(binding=1) uniform sampler2DArray  Specular;
layout(binding=2) uniform sampler2DArray  Normal;

#pragma sar include("Shaders/IImodel.include")
#pragma sar include("Shaders/common.include")

#ifdef SHADER_SHADOW
in GS_OUT {
    vec2 TexCoords;
	//vec3 position;
	flat uint DrawID;
} vertexData;
#else
#pragma sar include("Shaders/commonGeometryFrag.include")
in VertexData{
	vec2 TexCoords;
	flat uint DrawID;
	vec3 Normal;
	vec3 Tangent;
	vec3 Position;
}vertexData;
#endif

#pragma sar include("Shaders/BumpedNormal.include");

void main()
{  
	vec4 color = texture(Diffuse, vec3(vertexData.TexCoords,perDraw[vertexData.DrawID].diffuseTextureIndex) );	
	if(perDraw[vertexData.DrawID].alphaDiscard > 0.0001 && color.a <= perDraw[vertexData.DrawID].alphaDiscard){
		discard;
	}
	
#ifdef SHADER_SHADOW
#else
	float specular = texture(Specular, vec3(vertexData.TexCoords,perDraw[vertexData.DrawID].specularTextureIndex) ).r;	
	vec4 normal = texture(Normal, vec3(vertexData.TexCoords,perDraw[vertexData.DrawID].normalTextureIndex) );

	uint extraData = perDraw[vertexData.DrawID].extraData;
	
	//special color	
	uint val =  extraData & 0xFF000000;
	if(bool(val)){
		val >>= 24;
		float a = 1.f/255 * val;

		val = extraData & 0x00FF0000;
		val >>= 16;
		float r = 1.f/255 * val;

		val = extraData & 0x0000FF00;
		val >>= 8;
		float g = 1.f/255 * val;

		val = extraData & 0x000000FF;
		float b = 1.f/255 * val;

		vec3 pomColor = vec3(r,g,b);
		color =  vec4(mix(color.rgb,pomColor, a), color.a);
	}

	o_diffuseAndSpecular = vec4(color.r,color.g,color.b,specular);
	o_normal = CalcBumpedNormal(vertexData.Normal, vertexData.Tangent, normal.xyz);
	o_position = vertexData.Position;
	o_ID = BASIC_OBJECT_ID;
#endif
}
