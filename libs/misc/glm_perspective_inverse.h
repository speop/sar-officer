#include <glm/glm.hpp>

template <typename T>
inline glm::detail::tmat4x4<T, glm::defaultp> perspectiveInverse(
    T const & fovy,
    T const & aspect,
    T const & zNear, 
    T const & zFar)
{
    glm::detail::tmat4x4<T, glm::defaultp> m(static_cast<T>(0));

    const T tanHalfFovy = tan(fovy / static_cast<T>(2));
    m[0][0] = tanHalfFovy * aspect;
    m[1][1] = tanHalfFovy;
    m[3][2] = static_cast<T>(-1);

    const T d = static_cast<T>(2) * far * near;
    m[2][3] = (near - far) / d;
    m[3][3] = (far + near) / d;

    return m;
}