#include "ConfigStore.h"
#include "UIElement.h"


ConfigStore::ConfigStore()
{
	LoadConfig(this->defaultConfig);
	m_UserData = m_rootNode.child("UserData");
	m_ApplicationData = m_rootNode.child("ApplicationData");
}


ConfigStore::~ConfigStore()
{
}

ConfigStore * ConfigStore::PushSetConfig(const std::string & data)
{
	m_configFilesStack.push(m_currentConfig);
	LoadConfig(data);

	return this;
}

ConfigStore * ConfigStore::PopSetConfig()
{
	if (!m_configFilesStack.empty()) {
		LoadConfig(m_configFilesStack.top());
		m_configFilesStack.pop();
	}
	return this;
}

void ConfigStore::LoadConfig(const std::string & filename)
{
	m_currentConfig = filename;
	pugi::xml_parse_result result = m_configData.load_file(m_currentConfig.c_str());

	if (result.status != pugi::xml_parse_status::status_ok) {
		throw ConfigCoruptedException("Error code: "+std::to_string(result.status)+", message: "+result.description());
	}

	m_rootNode = m_configData.child("Config");
}

ConfigStore * ConfigStore::Save()
{
	m_configData.save_file(m_currentConfig.c_str());
	return this;
}

#pragma region Config getters and setters
//ApplicationData section
std::vector<std::string> ConfigStore::GetTerrainKeyPath(const std::string & key, uint maxChildren)
{
	std::vector<std::string> data;

	auto node = m_ApplicationData.child("Terrain").child(key.c_str());
	if (maxChildren == 0) {
		data.push_back(node.attribute("path").value());
	}
	else {
		pugi::xml_node_iterator it;
		uint i;
		for (it = node.children().begin(),i = 0; it != node.children().end() && i < maxChildren; ++it, ++i) {
			data.push_back(it->attribute("path").value());
		}
	}

	return data;
}


std::vector<Model::ModelSaveRecord> ConfigStore::GetModels(const std::string & key)
{
	std::vector<Model::ModelSaveRecord> models;
	auto modelsNode = m_ApplicationData.child("Models").child(key.c_str());

	for (auto &it : modelsNode.children()) {
		Model::ModelSaveRecord record;
		
		record.name = it.attribute("name").value();
		record.path = it.attribute("path").value();
		record.positions = it.attribute("positions").value();
		record.hasY = it.attribute("hasY").as_bool();
		record.indirect = it.attribute("indirect").as_bool();
		record.alphaDiscard = it.attribute("alphaDiscard").as_float();

		models.push_back(record);
	}

	return models;
}

std::vector<std::string> ConfigStore::GetModels()
{
	std::vector<std::string> models;
	auto modelsNode = m_ApplicationData.child("Models").children();

	for (auto &modelType : modelsNode) {
		for(auto &model : modelType) {
			models.push_back(model.attribute("name").as_string());
		}
	}
	
	return models;
}

std::string ConfigStore::GetMainMenuBackground()
{
	std::string val = m_ApplicationData.child("MainMenu").attribute("background").as_string();
	/*
	if (val <= 0) {
		throw ConfigCoruptedException("Corrupted config \"" + m_currentConfig + "\", please reinstal game to resolve this error.");
	}*/
	return val;
}

std::string ConfigStore::GetMainMenuSound()
{
	std::string val = m_ApplicationData.child("MainMenu").attribute("sound").as_string();
	return val;
}

std::string ConfigStore::GetLanguage()
{
	std::string val = m_ApplicationData.child("Languages").attribute("set").as_string();
	return val;
}

ConfigStore* ConfigStore::SetLanguage(std::string key)
{
	m_ApplicationData.child("Languages").attribute("set").set_value(key.c_str());
	return this;
}

std::vector<ConfigStore::LangRec> ConfigStore::GetLanguages()
{
	std::vector<LangRec> languages;
	auto modelsNode = m_ApplicationData.child("Languages");

	for (auto &lang : modelsNode.children()) {
		LangRec rec;
		rec.name = lang.child_value();
		rec.path = lang.attribute("path").as_string();
		rec.key = lang.attribute("key").as_string();

		languages.push_back(rec);
	}
	return languages;
}

bool ConfigStore::EditorEnabled()
{
	auto node = m_ApplicationData.child("Editor"); 
	if(node == nullptr) {
		return false;
	}
	return node.attribute("enabled").as_bool();
}

std::vector<ConfigStore::ResolutionRec> ConfigStore::GetResolutions()
{
	std::vector<ConfigStore::ResolutionRec> resolutions;
	auto resolutionsNode = m_ApplicationData.child("Resolutions");

	for (auto &resolution : resolutionsNode.children()) {
		ResolutionRec record;
	
		record.x = resolution.attribute("x").as_int();
		record.y = resolution.attribute("y").as_int();			
		resolutions.push_back(record);
	}

	return resolutions;
}

glm::vec3 ConfigStore::GetSunColor(const std::string& name)
{
	return UIElement::GLmColor(m_ApplicationData.child("Sun").find_child_by_attribute("Color", "name", name.c_str()).attribute("val").as_string());
}

float ConfigStore::GetSunFloat(const std::string& nodeName, const std::string& name)
{
	return m_ApplicationData.child("Sun").find_child_by_attribute(nodeName.c_str(), "name", name.c_str()).attribute("val").as_float();
}

std::vector<ConfigStore::SoundRec> ConfigStore::GetSounds(const std::string& category, const std::string& section)
{
	std::vector<ConfigStore::SoundRec> sounds;
	auto soundsNode = m_ApplicationData.child("Sounds").child(category.c_str()).child(section.c_str());

	for (auto &sound : soundsNode.children()) {
		SoundRec record;

		record.name = sound.attribute("name").as_string();
		record.path = sound.attribute("path").as_string();
		record.volume= sound.attribute("volume").as_float();
		sounds.push_back(record);
	}

	return sounds;
}

//UserData section
int ConfigStore::GetXRes()
{
	int val = m_UserData.child("Window").attribute("xRes").as_int();
	if (val <= 0) {
		throw ConfigCoruptedException("Corrupted config \"" + m_currentConfig + "\", please reinstal game to resolve this error.");
	}
	return val;
}

ConfigStore * ConfigStore::SetXres(int xRes)
{
	m_UserData.child("Window").attribute("xRes").set_value(xRes);
	return this;
}

int ConfigStore::GetYRes()
{
	int val = m_UserData.child("Window").attribute("yRes").as_int();
	if (val <= 0) {
		throw ConfigCoruptedException("Corrupted config \"" + m_currentConfig + "\", please reinstall game to resolve this error.");
	}
	return val;
}

ConfigStore * ConfigStore::SetYres(int yRes)
{
	m_UserData.child("Window").attribute("yRes").set_value(yRes);
	return this;
}

bool ConfigStore::GetFullscreen()
{
	return m_UserData.child("Window").attribute("fullscreen").as_bool();
}

ConfigStore * ConfigStore::SetFullscreen(bool fs)
{
	m_UserData.child("Window").attribute("fullscreen").set_value(fs);
	return this;
}

float ConfigStore::GetMusicLevel()
{
	return m_UserData.child("SoundLevels").attribute("music").as_float();
}

ConfigStore * ConfigStore::SetMusicLevel(float level)
{
	m_UserData.child("SoundLevels").attribute("music").set_value(level);
	return this;
}

float ConfigStore::GetAudioFXlevel()
{
	return m_UserData.child("SoundLevels").attribute("effects").as_float();
}

ConfigStore * ConfigStore::SetAudioFXlevel(float level)
{
	m_UserData.child("SoundLevels").attribute("effects").set_value(level);
	return this;
}


#pragma endregion