// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 12-09-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <set>
#include <iostream>
#include <GL\glew.h>
#include <vector>
#include "Exceptions.h"


struct ShaUN {
	GLuint type = 0;
	std::string filePath = "";
	std::string additionalCode = "";

	/// <summary>
	/// Initializes a new instance of the <see cref="ShaUN"/> struct.
	/// </summary>
	ShaUN() {};

	/// <summary>
	/// Initializes a new instance of the <see cref="ShaUN"/> struct.
	/// </summary>
	/// <param name="Type">The type.</param>
	/// <param name="FilePath">The file path.</param>
	ShaUN(GLuint Type, std::string FilePath) :type(Type), filePath(FilePath) {};

	/// <summary>
	/// Initializes a new instance of the <see cref="ShaUN"/> struct.
	/// </summary>
	/// <param name="Type">The type.</param>
	/// <param name="FilePath">The file path.</param>
	/// <param name="AdditionalCode">The additional code.</param>
	ShaUN(GLuint Type, std::string FilePath, const std::string &AdditionalCode) :type(Type), filePath(FilePath), additionalCode(AdditionalCode) {};
};

/// <summary>
/// Class Shader.
/// </summary>
class Shader
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="Shader"/> class.
	/// </summary>
	Shader();
	/// <summary>
	/// Finalizes an instance of the <see cref="Shader"/> class.
	/// </summary>
	~Shader();

	/// <summary>
	/// Builds the shaders.
	/// </summary>
	/// <param name="shaders">The shaders.</param>
	void BuildShaders(std::initializer_list<ShaUN> shaders);

	/// <summary>
	/// Adds the atribute.
	/// </summary>
	/// <param name="attributeName">Name of the attribute.</param>
	void AddAtribute(const std::string& attributeName);

	/// <summary>
	/// Uses shader.
	/// </summary>
	void Use();

	/// <summary>
	/// Unuses this shader.
	/// </summary>
	void Unuse();

	/// <summary>
	/// Gets the shader ID.
	/// </summary>
	/// <returns>GLuint.</returns>
	GLuint GetID();


	#pragma region GL data wrapper
	
	Shader* Set1f(const std::string &name, float val);
	/// <summary>
	/// Sets 1 int, specified by name.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="val">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* Set1i(const std::string &name, int val);

	/// <summary>
	/// Sets 1 int, specified by location.
	/// </summary>
	/// <param name="location">The location.</param>
	/// <param name="val">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* Set1i(uint location, int val);
	
	/// <summary>
	/// Sets the bool, specified by name.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="val">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* SetBool(const std::string &name, bool val);

	/// <summary>
	/// Sets the bool, specified by location.
	/// </summary>
	/// <param name="location">The location.</param>
	/// <param name="val">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* SetBool(uint location, bool val);

	/// <summary>
	/// Sets 1 float, specified by name.
	/// </summary>
	/// <param name="location">The location.</param>
	/// <param name="val">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* Set1f(uint location, float val);
	/// <summary>
	/// Sets 3 floats, specified by name.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="val">The value.</param>
	/// <param name="val2">The val2.</param>
	/// <param name="val3">The val3.</param>
	/// <returns>Shader *.</returns>
	Shader* Set3f(const std::string &name, float val, float val2, float val3);

	/// <summary>
	/// Set2is the specified name.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="count">The count.</param>
	/// <param name="value">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* Set2i(const std::string &name, GLsizei count, const GLint *value);

	/// <summary>
	/// Sets the vec2f, specified by name.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="count">The count.</param>
	/// <param name="value">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* SetVec2f(const std::string &name, GLsizei count, const GLfloat *value);

	/// <summary>
	/// Sets the vec2f, specified by location.
	/// </summary>
	/// <param name="location">The location.</param>
	/// <param name="count">The count.</param>
	/// <param name="value">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* SetVec2f(uint location, GLsizei count, const GLfloat *value);

	/// <summary>
	/// Sets the vec3f, specified by name.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="count">The count.</param>
	/// <param name="value">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* SetVec3f(const std::string &name, GLsizei count, const GLfloat *value);

	/// <summary>
	/// Sets the vec3f, specified by location
	/// </summary>
	/// <param name="location">The location.</param>
	/// <param name="count">The count.</param>
	/// <param name="value">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* SetVec3f(uint location, GLsizei count, const GLfloat *value);

	/// <summary>
	/// Sets the mat4f, specified by name.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="count">The count.</param>
	/// <param name="transpose">The transpose.</param>
	/// <param name="value">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* SetMat4f(const std::string &name, GLsizei count, GLboolean transpose, const GLfloat *value);

	/// <summary>
	/// Sets the mat4f, specified by location.
	/// </summary>
	/// <param name="location">The location.</param>
	/// <param name="count">The count.</param>
	/// <param name="transpose">The transpose.</param>
	/// <param name="value">The value.</param>
	/// <returns>Shader *.</returns>
	Shader* SetMat4f(uint location, GLsizei count, GLboolean transpose, const GLfloat *value);


	#pragma endregion

private:
	/// <summary>
	/// Compiles the shader.
	/// </summary>
	/// <param name="shaderID">The shader identifier.</param>
	void CompileShader(GLuint shaderID);

	/// <summary>
	/// Loads the shader code.
	/// </summary>
	/// <param name="filePath">The file path.</param>
	/// <param name="sourceCode">The source code.</param>
	/// <param name="inludeStack">The inlude stack.</param>
	void LoadShaderCode(const std::string &filePath, std::string &sourceCode, std::vector<std::string> &inludeStack);

	/// <summary>
	/// Prints the error if cyclical includes.
	/// </summary>
	/// <param name="inludeStack">The inlude stack.</param>
	void PrintErrorCyclicInclude(std::set<std::string> &inludeStack);

	
	GLuint _programID;
	int _numAttributes;
	ShaUN m_currentShaUN;
	bool m_codeAdded = false;
};

