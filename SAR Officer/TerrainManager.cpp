#include "TerrainManager.h"
#include "Logger.h"
#include <bitset>
#include "CollisionManager.h"


TerrainManager::TerrainManager(int width, int length, int hOffset): m_terrainWidth(width), m_terrainLength(length), heightOffset(hOffset)
{
}


TerrainManager::~TerrainManager()
{
	Release();
}

void TerrainManager::Init()
{
	float quadData[] = {
		// Vert 1
		-0.5f, 0.0f, -0.5f, 1.0,	// Position	
		// Vert 2
		0.5f, 0.0f, -0.5f, 1.0,		// Position		
		// Vert 3
		0.5f, 0.0f, 0.5f, 1.0,		// Position
		// Vert 4
		-0.5f, 0.0f, 0.5f, 1.0,		// Position

	};

	//grass geometry cull distance
	auto cam = PerspectiveCamera::GetCameraInstance("Basic");
	auto fov = cam->initialFoV;
	auto grassLod2 = c_grassHeight / c_grassEpsilon / glm::tan(fov / 2);
	auto grassLod0 = grassLod2 / 18.f;
	auto grassLod1 = grassLod2 / 18.f + grassLod0;
	this->m_terrainProgram = std::make_shared<Shader>();
	this->m_terrainProgram->BuildShaders({
		ShaUN(GL_VERTEX_SHADER, "Shaders/terrain.vert"),
		ShaUN(GL_TESS_CONTROL_SHADER, "Shaders/terrain.tesc"),
		ShaUN(GL_TESS_EVALUATION_SHADER, "Shaders/terrain.tese"),
		//ShaUN(GL_GEOMETRY_SHADER, "Shaders/terrain.geom"),
		ShaUN(GL_FRAGMENT_SHADER, "Shaders/terrain.frag")
	});

	std::string additionalCode = "#define SHADER_SHADOW \n";

	m_terrainShaddowsShader = std::make_shared<Shader>();
	m_terrainShaddowsShader->BuildShaders({
		ShaUN(GL_VERTEX_SHADER, "Shaders/terrain.vert"),
		ShaUN(GL_TESS_CONTROL_SHADER, "Shaders/terrain.tesc"),
		ShaUN(GL_TESS_EVALUATION_SHADER, "Shaders/terrain.tese", additionalCode),
		ShaUN(GL_GEOMETRY_SHADER, "Shaders/CSM.geom"),
		ShaUN(GL_FRAGMENT_SHADER, "Shaders/terrain.frag", additionalCode)
	});
    
	
	m_grassShader = std::make_shared<Shader>();

	m_grassShader->BuildShaders({
		ShaUN(GL_VERTEX_SHADER, "Shaders/grass.vert"),	
		ShaUN(GL_GEOMETRY_SHADER, "Shaders/grass.geom"),
		ShaUN(GL_FRAGMENT_SHADER, "Shaders/grass.frag")
	});

	m_grassComp = std::make_shared<Shader>();
	m_grassComp->BuildShaders({ ShaUN(GL_COMPUTE_SHADER,  "Shaders/grass.comp") });

	auto programID = this->m_terrainProgram->GetID();

	glGenVertexArrays(1, &vaoQuad);
	glGenVertexArrays(1, &m_grassVAO);
	glGenBuffers(1, &vboQuad);
	glGenBuffers(1, &m_SSBO);	
	glGenBuffers(1, &m_grassCommandBuffer);
	glGenBuffers(1, &m_grassSeeds);

	glBindVertexArray(m_grassVAO);
	DrawAraysIndirectCommand drawCMD;
	drawCMD.instanceCount = 0;
	drawCMD.baseVertex = 0;
	drawCMD.vertexCount = 1;
	drawCMD.firstIndex = 0;
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_grassCommandBuffer);
	glBufferData(GL_DRAW_INDIRECT_BUFFER, sizeof(DrawAraysIndirectCommand), &drawCMD, GL_DYNAMIC_DRAW);


	glBindVertexArray(vaoQuad);

	//grass buffers
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_grassCommandBuffer);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 11, m_grassCommandBuffer);

	auto wDim = cam->GetViewport();
	m_maxGrassSeedsNo =  wDim.x*wDim.y;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_grassSeeds);
	glBufferData(GL_SHADER_STORAGE_BUFFER,2* sizeof(glm::vec4) * m_maxGrassSeedsNo, nullptr, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 10, m_grassSeeds);
   
	
	// Setup data	
	glBindBuffer(GL_ARRAY_BUFFER, vboQuad);
	glBufferData(GL_ARRAY_BUFFER, 4 * 4 * sizeof(float), quadData, GL_STATIC_DRAW);

	auto vertexAttributeLoc = glGetAttribLocation(programID, "a_vertex");
	glEnableVertexAttribArray(vertexAttributeLoc);
	glVertexAttribPointer(vertexAttributeLoc, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);

	
	this->m_terrainProgram->Use();
	this->m_terrainProgram
		->Set1f("TerrainLength", float(m_terrainLength))
		->Set1f("TerrainWidth", float(m_terrainWidth))
		->Set3f("TerrainOrigin", -m_terrainWidth / 2.0f, 0.0f, -m_terrainLength / 2.0f)
		->Set1f("TerrainHeightOffset", float(heightOffset))
		//->Set1f(6, grassLod0)
		//->Set1f(7, grassLod1)
		->Set1f(8, grassLod2)
		->Set1i(9, m_maxGrassSeedsNo)
		//->SetVec2f(10, 1,glm::value_ptr(glm::vec2(1, 0)))
		//->SetVec2f(11, 1, glm::value_ptr(glm::vec2(0, 1)))
	;
	 
	//we work with 4 verticies per patch
	glPatchParameteri(GL_PATCH_VERTICES, 4);

	this->m_terrainShaddowsShader->Use();
	this->m_terrainShaddowsShader
		->Set1f("TerrainLength", float(m_terrainLength))
		->Set1f("TerrainWidth", float(m_terrainWidth))
		->Set3f("TerrainOrigin", -m_terrainWidth / 2.0f, 0.0f, -m_terrainLength / 2.0f)
		->Set1f("TerrainHeightOffset", float(heightOffset))
		
	;

	//we work with 4 verticies per patch
	glPatchParameteri(GL_PATCH_VERTICES, 4);

	this->m_terrainShaddowsShader->Unuse();
  
	try {
		m_heightmap = std::make_unique<Texture>(m_terrainProgram);
		m_heightmap->Load(0, ConfigStore::Instance()->GetTerrainKeyPath("HeightMap"));


		//setting m_heightmap data for collision detection
		CollisionManager::Instance().SetHeightMap(ConfigStore::Instance()->GetTerrainKeyPath("HeightMap").at(0)).SetHOffset(static_cast<float>(heightOffset)).SetHMultiplier(3000.f);

		//base color texture, this texture is also used as material map
		m_baseColor = std::make_unique<Texture>(m_terrainProgram);
		m_baseColor->Load(2, ConfigStore::Instance()->GetTerrainKeyPath("BaseColor"));

		//loading textures (all materials) this loading is order dependend
		m_texture = std::make_unique<Texture>(m_terrainProgram);
		m_texture->Load(3, ConfigStore::Instance()->GetTerrainKeyPath("Textures", 3));
		
		m_normalMap = std::make_unique<Texture>(m_terrainProgram);
		m_normalMap->Load(6, ConfigStore::Instance()->GetTerrainKeyPath("NormalMap"));

		m_normals = std::make_unique<Texture>(m_terrainProgram);
		m_normals->Load(7, ConfigStore::Instance()->GetTerrainKeyPath("Normals", 3));

		auto tree = new TerrainTree();
		tree->CreateTree(Coords(0, 0, 0), float(m_terrainWidth), float(m_terrainLength), false);

		//getting render data from tree
		GenerateRenderData(tree, m_ssboData);

		//we dont need this tree anymore
		tree->ShutdownTree();
	
	}
	catch (TextureException &ex) {
		Logger::GetLogger()->Log(Logger::Critical, "Terrain textures error: " + ex.GetMsg());
		throw BasicException(ex.GetMsg());
	}
	glBindVertexArray(0);
  m_initialized = true;
    
	
}

void TerrainManager::Release()
{
	if (!m_initialized) return;
	m_initialized = false;

	
	m_terrainProgram.reset();
	m_terrainShaddowsShader.reset();
	m_grassShader.reset();
	
	glDeleteVertexArrays(1, &vaoQuad);
	glDeleteVertexArrays(1, &m_grassVAO);
	glDeleteBuffers(1, &vboQuad);
	glDeleteBuffers(1, &m_SSBO);
	glDeleteBuffers(1, &m_grassCommandBuffer);
	glDeleteBuffers(1, &m_grassSeeds);

	m_heightmap.reset();
	m_baseColor.reset();
	m_texture.reset();
	m_normalMap.reset();
	m_normals.reset();
	material.reset();
	
}

void TerrainManager::Run(RenderPass renderPass)
{
	if (!m_initialized) throw NotInitializedException("Terrain manager not initialized!");

	glBindVertexArray(vaoQuad);
	std::shared_ptr<Shader> shader;

	if (renderPass == CLASSIC) {

		//resetin grass seed no
		DrawAraysIndirectCommand* ssboPtr;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_grassCommandBuffer);
		ssboPtr = (DrawAraysIndirectCommand*)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
		ssboPtr->instanceCount = 0;		
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

		shader = m_terrainProgram;
	}
	else if (renderPass == SHADOW) {
		shader = m_terrainShaddowsShader;
	}

	shader->Use();
	


	//put on GPU some datas which are same for all nodes

	this->m_heightmap->Use(0);
	this->m_baseColor->Use(1);
	this->m_texture->Use("materialsTextures", shader);
	m_normalMap->Use(2);
	m_normals->Use("materialsNormal", shader);

	
	if (renderPass == CLASSIC) {
		// Calculate and render the terrain	
		Render();		
	}
	else if(renderPass == SHADOW) {	
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_SSBO);
		glDrawArraysInstanced(GL_PATCHES, 0, 4, m_ssboData.size());
	}

	shader->Unuse();

	//rendering grass
	if(renderPass == CLASSIC) {
    //*
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		m_grassComp->Use();		
		m_grassComp->Set1i(0, m_maxGrassSeedsNo);
		glDispatchCompute(1, 1, 1);
		m_grassComp->Unuse();
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		m_grassShader->Use();
		glBindVertexArray(m_grassVAO);
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_grassCommandBuffer);
		glMultiDrawArraysIndirect(GL_POINTS, nullptr, 1, 0);
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, 0);
		m_grassShader->Unuse();
    //*/

	}

	glBindVertexArray(0);

	
}

void TerrainManager::Render()
{
	
	//synchronizing tree from worker thread
	if (m_calculatingTerrain) {
		m_terrainWorker.wait();
		m_ssboData.swap(m_ssboDataSecondBuffer);
		m_calculatingTerrain = false;	
	}



	m_calculatingTerrain = true;
	m_terrainWorker = std::async(std::launch::async, [this]() {
		auto tree = new TerrainTree();
		tree->CreateTree(Coords(0, 0, 0), float(m_terrainWidth), float(m_terrainLength), false);
		
		//getting render data from tree
		GenerateRenderData(tree, m_ssboDataSecondBuffer);

		//we dont need this tree anymore
		std::async(std::launch::async, [tree]() { tree->ShutdownTree(); });	
	});

		

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_SSBO);
	if(m_ssboSize < m_ssboData.size()) {
		m_ssboSize = static_cast<int>(m_ssboData.size());
		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(SSBOStruct) * m_ssboSize, &m_ssboData[0], GL_DYNAMIC_DRAW);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, m_SSBO);
	}else {
		glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(SSBOStruct) * m_ssboData.size(), &m_ssboData[0]);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, m_SSBO);
	}
  	
	glDrawArraysInstanced(GL_PATCHES, 0, 4, m_ssboData.size());
	
}

void TerrainManager::GenerateRenderData(TerrainTree* tree, std::vector<SSBOStruct>& returnVec)
{
	std::stack<TerrainNode*> nodes;
	nodes.push(tree->GetRoot());
	m_ssboDataSecondBuffer.clear();

	while (!nodes.empty()) {
		auto node = nodes.top();
		nodes.pop();
		if (node == nullptr) continue;

		//if node has no child, render this node
		if (!node->HasChildren()) RenderNode(node, tree, returnVec);
		else {
			if (node->child1 != nullptr) nodes.push(node->child1);
			if (node->child2 != nullptr) nodes.push(node->child2);
			if (node->child3 != nullptr) nodes.push(node->child3);
			if (node->child4 != nullptr) nodes.push(node->child4);
		}
	}
}

void TerrainManager::RenderNode(TerrainNode * node, TerrainTree* tree, std::vector<SSBOStruct> &returnVec)
{
	if (node == nullptr) return;
	tree->CalcTessScale(node);

	SSBOStruct ssboRec;

	ssboRec.modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(node->origin.x, node->origin.y, node->origin.z));
	auto camera = PerspectiveCamera::GetCameraInstance("Basic");
				
	ssboRec.tileScale =  node->width;

	// Send patch neighbor edge tess scale factors
	ssboRec.tscaleNegX = node->tscaleNegX;
	ssboRec.tscaleNegZ = node->tscaleNegZ;
	ssboRec.tscalePosX = node->tscalePosX;
	ssboRec.tscalePosZ = node->tscalePosZ;

	returnVec.push_back(ssboRec);

}


