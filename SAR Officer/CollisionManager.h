// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 12-28-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <string>
#include <memory>
#include "glm/glm.hpp"
#include <FreeImagePlus.h>
#include "Exceptions.h"

/// <summary>
/// CollisionManager - singleton
/// </summary>
class CollisionManager
{

public:
	
	/// <summary>
	/// Instances this instance.
	/// </summary>
	/// <returns>CollisionManager &.</returns>
	static CollisionManager & Instance()
	{
		// Since it's a static variable, if the class has already been created,
		// It won't be created again.
		// And it **is** thread-safe in C++11.

		static CollisionManager myInstance;

		// Return a reference to our instance.
		return myInstance;
	}

	// delete copy and move constructors and assign operators
	CollisionManager(CollisionManager const&) = delete;             // Copy construct
	CollisionManager(CollisionManager&&) = delete;                  // Move construct
	CollisionManager& operator=(CollisionManager const&) = delete;  // Copy assign
	CollisionManager& operator=(CollisionManager &&) = delete;      // Move assign

	// Any other public methods
	/// <summary>
	/// Sets the height map.
	/// </summary>
	/// <param name="path">The path.</param>
	/// <returns>CollisionManager &.</returns>
	CollisionManager& SetHeightMap(std::string path);

	/// <summary>
	/// Sets the height multiplier.
	/// </summary>
	/// <param name="mult">The mult.</param>
	/// <returns>CollisionManager &.</returns>
	CollisionManager& SetHMultiplier(float mult) { m_hMultiplier = mult; return *this; }

	/// <summary>
	/// Sets the height offset.
	/// </summary>
	/// <param name="offset">The offset.</param>
	/// <returns>CollisionManager &.</returns>
	CollisionManager& SetHOffset(float offset) { m_hOffset = offset; return *this; }

	/// <summary>
	/// Gets the height of point specified by x and z.
	/// </summary>
	/// <param name="coords">The coords.</param>
	/// <param name="centered">The centered.</param>
	/// <returns>float.</returns>
	float GetHeight(glm::vec2 coords, bool centered = false);

	/// <summary>
	/// Gets the height interpolated height of point specified by x and z and slope in this point.
	/// </summary>
	/// <param name="coords">The coords.</param>
	/// <param name="centered">The centered.</param>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	std::pair<float, glm::vec2>GetHeightInterpolatedAndSlope(glm::vec2 coords, bool centered = false);

	/// <summary>
	/// Gets the height interpolated height of point specified by x and z.
	/// </summary>
	/// <param name="coords">The coords.</param>
	/// <param name="centered">The centered.</param>
	/// <returns>float.</returns>
	float GetHeightInterpolated(glm::vec2 coords, bool centered = false);

	/// <summary>
	/// Gets the aabb of terrain.
	/// </summary>
	/// <param name="centered">The centered.</param>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	std::pair<glm::vec3, glm::vec3> GetAABB(bool centered = false);

	 


	

protected:
	/// <summary>
	/// Initializes a new instance of the <see cref="CollisionManager"/> class.
	/// </summary>
	CollisionManager() : m_hMultiplier(1), m_hOffset(0)
	{
		// Constructor code goes here.
	}

	/// <summary>
	/// Finalizes an instance of the <see cref="CollisionManager"/> class.
	/// </summary>
	~CollisionManager()
	{
		m_HeightMap.reset();
	}

private:
	/// <summary>
	/// The m height map
	/// </summary>
	std::unique_ptr<fipImage> m_HeightMap;
	/// <summary>
	/// The m h multiplier
	/// </summary>
	float m_hMultiplier;
	/// <summary>
	/// The m h offset
	/// </summary>
	float m_hOffset;
	/// <summary>
	/// The m minimum height
	/// </summary>
	float m_minHeight;
	/// <summary>
	/// The m maximum height
	/// </summary>
	float m_maxHeight;
};


#pragma region Exceptions
/// <summary>
/// Class CollisionBasicException.
/// </summary>
/// <seealso cref="BasicException" />
class CollisionBasicException : public BasicException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="CollisionBasicException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	CollisionBasicException(std::string msg) : BasicException(msg) {};
};

/// <summary>
/// Class CollisionNoDataException.
/// </summary>
/// <seealso cref="CollisionBasicException" />
class CollisionNoDataException : public CollisionBasicException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="CollisionNoDataException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	CollisionNoDataException(std::string msg) : CollisionBasicException(msg) {};
};
#pragma endregion 