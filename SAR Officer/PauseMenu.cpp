#include "PauseMenu.h"
#include "Translator.h"
#include "MainGame.h"


PauseMenu::PauseMenu()
{
}


PauseMenu::~PauseMenu()
{
}

void PauseMenu::Wake() {}

void PauseMenu::Run() {}
void PauseMenu::RunImgui()
{
	bool open;
	auto TR = Translator::Get();
	auto display = ImGui::GetIO().DisplaySize;
	
	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::SetNextWindowSize(ImVec2(display.x, display.y));
	ImGui::PushStyleColor(ImGuiCol_WindowBg,Color("#35454688"));

	ImGui::Begin("overlay", &open,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoScrollbar |
		ImGuiWindowFlags_NoCollapse |
		ImGuiWindowFlags_NoInputs
	);
	ImGui::End();
	ImGui::PopStyleColor();

	auto x = display.x * 0.5f - 100;
	auto y = display.y*0.5f - 100;
	ImGui::SetNextWindowPos(ImVec2(x, y));
	ImGui::SetNextWindowSize(ImVec2(200, 200));
	ImGui::PushStyleColor(ImGuiCol_WindowBg, Color("#0000"));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.f);

	ImGui::Begin("Pause menu", &open,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoScrollbar |
		ImGuiWindowFlags_NoCollapse
	);


	ImGui::PushStyleColor(ImGuiCol_Button, cBtnBlue);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cBtnBlueHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cBtnBlueActive);

	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(17, 6));
	ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.f);
	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 11));
	
	auto btnSize = ImVec2(175,40);

	if (ImGui::Button(TR->Trans("Continue", "PauseMenu").c_str(), btnSize)) ContinueBtn_click();
	if (ImGui::Button(TR->Trans("ExitToMainMenu", "PauseMenu").c_str(), btnSize)) MainMenuBtn_click();
	if (ImGui::Button(TR->Trans("ExitToWindows", "PauseMenu").c_str(), btnSize)) ExitBtn_click();
	

	ImGui::PopStyleColor(3);
	ImGui::PopStyleVar(3);
	ImGui::End();

	ImGui::PopStyleColor(); 
	ImGui::PopStyleVar();

}
void PauseMenu::Stop() {}
void PauseMenu::Burn() {}
void PauseMenu::Init() {}


#pragma region Buttons events
void PauseMenu::ContinueBtn_click()
{
	MainGame::Get()->Continue();
}
void PauseMenu::MainMenuBtn_click()
{
	MainGame::Get()->Menu();
}
void PauseMenu::ExitBtn_click()
{
	MainGame::Get()->Exit();
}
#pragma endregion 
