#pragma once
#include "Player.h"

namespace GO {
	class Animal :
		public Player
	{
	public:
		Animal();
		//Animal(const Animal &obj): Player(nullptr){};
		Animal& operator=(Animal const&obj) { return *this; };
		~Animal();
	};
}

