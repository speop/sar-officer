#include "GBuffer.h"
#include <algorithm>
#include <thread>
#include "Camera.h"
GBuffer::GBuffer()
{
	m_baked = false;
	m_textureDiffuse = std::make_unique<TextureA>();
	m_textureSpecular = std::make_unique<TextureA>();
	m_textureNormal = std::make_unique<TextureA>();
	m_indirectShader = std::make_shared<Shader>();
	m_indirectShader->BuildShaders({ ShaUN(GL_VERTEX_SHADER, "Shaders/IImodel.vert"), ShaUN(GL_FRAGMENT_SHADER, "Shaders/IImodel.frag") });

	std::string additionalCode = "#define SHADER_SHADOW \n";
	m_shadowShader = std::make_shared<Shader>();
	m_shadowShader->BuildShaders({ 
		ShaUN(GL_VERTEX_SHADER, "Shaders/IImodel.vert",additionalCode),
		ShaUN(GL_GEOMETRY_SHADER, "Shaders/CSM.geom", " #define DRAWID \n"),
		ShaUN(GL_FRAGMENT_SHADER, "Shaders/IImodel.frag",additionalCode) });

	m_cullingShader = std::make_shared<Shader>();
	m_cullingShader->BuildShaders({ ShaUN(GL_COMPUTE_SHADER,  "Shaders/IImodel.comp")});
}

GBuffer::~GBuffer()
{
	glDeleteVertexArrays(1, &m_VAO);
	glDeleteBuffers(1, &m_VBO);
	glDeleteBuffers(1, &m_IBO);
	glDeleteVertexArrays(1, &m_AABBVAO);
	glDeleteBuffers(1, &m_commandBuffer);
	glDeleteBuffers(1, &m_SSBO);
	glDeleteBuffers(1, &m_drawIdBO);
}


void GBuffer::Load(const GBufferStruct& data, const std::string& name)
{
	if (m_baked) throw GBufferAlreadyBakedException("You cannot add meshes file if GBuffer was already baked.");
	if (name == "") throw GBufferModelInvalidNameException("Model must have name!");

	auto modelRec = m_objects.find(name);


	if (modelRec == m_objects.end()) {
		ModelRecord rec;
		rec.modelName = name;
		std::pair<std::map<std::string, ModelRecord>::iterator, bool> pair = m_objects.insert(std::make_pair(name, rec));
		modelRec = pair.first;
	}

	modelRec->second.meshes.push_back(data);
}


GBuffer* GBuffer::Bake()
{
	

	// if buffers are 0 nothing happens and no error will be omitted from opengl
	//glDeleteBuffers(1, &m_VAO);
	glDeleteBuffers(1, &m_VBO);
	glDeleteBuffers(1, &m_IBO);
	//glDeleteBuffers(1, &m_instanceVBO);
	glDeleteBuffers(1, &m_commandBuffer);
	glDeleteBuffers(1, &m_SSBO);
	glDeleteBuffers(1, &m_drawIdBO);

	glGenVertexArrays(1, &m_VAO);
	glGenVertexArrays(1, &m_AABBVAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_IBO);
	//glGenBuffers(1, &m_instanceVBO);
	glGenBuffers(1, &m_commandBuffer);
	glGenBuffers(1, &m_SSBO);
	glGenBuffers(1, &m_drawIdBO);

	if (!m_VAO || !m_VBO || !m_IBO || !m_AABBVAO || !m_commandBuffer || !m_SSBO || !m_drawIdBO) {
		throw CannotCreateOGLBufferException(
			"VAO: " + std::to_string(m_VAO) +
			", VBO: " + std::to_string(m_VBO) +
			", IBO: " + std::to_string(m_IBO) +
			", SSBO: " + std::to_string(m_SSBO) +
			", AABBVAO: " + std::to_string(m_AABBVAO) +
			", Command buffer: " + std::to_string(m_commandBuffer) +
			", Draw IDs buffer: " + std::to_string(m_drawIdBO)
		);
	}
	m_baked = true;
	if (m_objects.size() < 1) return this;

	//baking textures
	m_textureDiffuse->Bake();
	m_textureSpecular->Bake();
	m_textureNormal->Bake();

	//merging all meshes and creating draw command for each mesh
	std::vector<Mesh::Vertex> VBOdata;
	std::vector<GLuint> indices;

	GLuint drawId = 0;
	DrawElementsIndirectCommand command;
	SSBOStruct ssboRecord;


	for (auto& object : m_objects) {

		auto commandVector = std::vector<DrawElementsIndirectCommand>();
		auto ssboVector = std::vector<SSBOStruct>();

		// shared data per instance
		for (auto& mesh : object.second.meshes) {
			auto VBOoffset = VBOdata.size();
			auto indicesOffset = indices.size();

			//merging meshes data 
			VBOdata.insert(VBOdata.end(), mesh.vertices->begin(), mesh.vertices->end());
			indices.insert(indices.end(), mesh.indices->begin(), mesh.indices->end());


			command.firstIndex = static_cast<GLuint>(indicesOffset);
			command.baseVertex = static_cast<GLuint>(VBOoffset);
			command.vertexCount = static_cast<GLuint>(mesh.indices->size());
			command.instanceCount = 1;

			commandVector.push_back(command);

			mesh.firstIndex = command.firstIndex;
			mesh.baseVertex = command.baseVertex;

			//setting per Draw data
			ssboRecord.transformation = mesh.transformation;

			// per Draw textures
			auto it = mesh.textures.find(Mesh::TextTypes::Diffuse);
			ssboRecord.diffuseTextureIndex = it != mesh.textures.end() ? m_textureDiffuse->GetTextureIndex(it->second) : -1;
			it = mesh.textures.find(Mesh::TextTypes::Specular);
			ssboRecord.specularTextureIndex = it != mesh.textures.end() ? m_textureSpecular->GetTextureIndex(it->second) : -1;
			it = mesh.textures.find(Mesh::TextTypes::Normal);
			ssboRecord.normalTextureIndex = it != mesh.textures.end() ? m_textureNormal->GetTextureIndex(it->second) : -1;

			ssboRecord.alphaDiscard = mesh.alphaDiscard;
			ssboVector.push_back(ssboRecord);
		}

		//now for each instance
		for (auto dator = object.second.meshDataPointers.begin(); dator != object.second.meshDataPointers.end();) {
			bool validPointer = false;

			//this is the core loop where we create are draw commands
			for (auto i = 0; i < (*dator)->m_dataPositionsPtrs.size(); i++) {
				auto ptrVal = *((*dator)->m_dataPositionsPtrs[i]);
				if (ptrVal < 0) continue;
				validPointer = true;;
				commandVector[i].baseInstance = ptrVal;

				m_ssboData[ptrVal].alphaDiscard = ssboVector[i].alphaDiscard;
				m_ssboData[ptrVal].diffuseTextureIndex = ssboVector[i].diffuseTextureIndex;
				m_ssboData[ptrVal].specularTextureIndex = ssboVector[i].specularTextureIndex;
				m_ssboData[ptrVal].transformation = ssboVector[i].transformation;

				m_commands.push_back(commandVector[i]);
				m_drawIds.push_back(drawId++);
			}

			//all dataPtr's items are nor valid pointer, so we consider that this means are dataPtr is not valid so we remove it.
			if (!validPointer) {
				dator = object.second.meshDataPointers.erase(dator);
			}
			else {
				++dator;
			}
		}

	}


	//copy data to OpenGL

	//our vao
	glBindVertexArray(m_VAO);

	//seting up vertices
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Mesh::Vertex) * VBOdata.size(), VBOdata.data(), GL_STATIC_DRAW);

	//Vertex positions
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Mesh::Vertex), (GLvoid*)0);
	glVertexAttribDivisor(4, 0);

	//Vertex normals 
	glEnableVertexAttribArray(5);
	glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(Mesh::Vertex), (GLvoid*)offsetof(Mesh::Vertex, Normal));
	glVertexAttribDivisor(5, 0);

	//Vertex tangents 
	glEnableVertexAttribArray(6);
	glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, sizeof(Mesh::Vertex), (GLvoid*)offsetof(Mesh::Vertex, Normal));
	glVertexAttribDivisor(6, 0);

	//Texture coords
	glEnableVertexAttribArray(7);
	glVertexAttribPointer(7, 2, GL_FLOAT, GL_FALSE, sizeof(Mesh::Vertex), (GLvoid*)offsetof(Mesh::Vertex, TexCoords));
	glVertexAttribDivisor(7, 0);


	//drawing index 
	glBindBuffer(GL_ARRAY_BUFFER, m_drawIdBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(GLuint) * m_drawIds.size(),m_drawIds.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(8);
	glVertexAttribIPointer(8, 1, GL_UNSIGNED_INT, sizeof(GLuint), (GLvoid*)0);
	glVertexAttribDivisor(8, 1); //maybe set for GLuint max and then it'll be once per mesh?

	//setting up indices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

	//setting up per draw data stored in SSBO (textures select index, basic transform, etc..)

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_SSBO);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(SSBOStruct) * m_ssboData.size(), m_ssboData.data() , GL_STATIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_SSBO);

	/*
	//setting up instance transformations	
	glBindBuffer(GL_ARRAY_BUFFER, m_instanceVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * m_objectsMatrices.size(), &m_objectsMatrices[0], GL_STATIC_DRAW);

	for (auto i = 0; i < 4; i++) {
		glEnableVertexAttribArray(3 + i);
		glVertexAttribPointer(3 + i, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(glm::vec4), (GLvoid*)(i * sizeof(glm::vec4)));
		glVertexAttribDivisor(3 + i, 1);
	}*/


	//setting up commands
	
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_commandBuffer);
	glBufferData(GL_DRAW_INDIRECT_BUFFER, sizeof(DrawElementsIndirectCommand) * m_commands.size(), m_commands.data(), GL_STATIC_DRAW);

	/*
	glBindVertexArray(m_AABBVAO);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_SSBO);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_SSBO);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_drawIdBO);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, m_drawIdBO);

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_commandBuffer);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, m_commandBuffer);
	*/
	glBindVertexArray(0);

	return this;
}

void GBuffer::Draw(RenderPass renderPass) const
{
	if (m_commands.size() < 1 || m_drawIds.size() < 1) return;
	if (renderPass == SHADOW)
	{/*
		glBindVertexArray(m_AABBVAO);

		m_cullingShader->Use();

		auto frustrum = PerspectiveCamera::GetCameraInstance("Basic")->GetFrustrum();
		glUniform4fv(1, 6, reinterpret_cast<GLfloat*>(frustrum));

		glDispatchCompute(static_cast<GLsizei>(m_commands.size()), 1, 1);
		m_cullingShader->Unuse();*/
	}
	glBindVertexArray(m_VAO);

	std::shared_ptr<Shader> shader;
	if (renderPass == CLASSIC) {
		shader = m_indirectShader;
	}
	else if (renderPass == SHADOW) {
		shader = m_shadowShader;
	}
	shader->Use();

	m_textureDiffuse->Draw(0);
	m_textureSpecular->Draw(1);
	m_textureNormal->Draw(2);
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_commandBuffer);
	glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, (GLvoid*)0, static_cast<GLsizei>(m_commands.size()), 0);
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, 0);

	shader->Unuse();
			
	
	glBindVertexArray(0);
}

std::shared_ptr<std::vector<std::shared_ptr<DataPtr>>> GBuffer::AddInstances(std::vector<ModelLocation>& modelLocations, std::string& objectName)
{
	auto retValue = std::make_shared<std::vector<std::shared_ptr<DataPtr>>>();
	auto instances = static_cast<int>(modelLocations.size());
	retValue->reserve(instances);


	auto object = m_objects.find(objectName);
	if (object == m_objects.end()) return nullptr;


	for (int i = 0; i < instances; i++) {

		//create new Object record
		auto objectPtr = std::make_shared<DataPtr>();

		//push it to the current object...
		object->second.meshDataPointers.push_back(objectPtr);

		//and store the pointer for return
		retValue->push_back(objectPtr);

		//now update pointer value for each mesh
		for (auto& mesh : object->second.meshes) {
			SSBOStruct ssborec;
			ssborec.position = modelLocations.at(i).GetPosition();
			ssborec.minAABB = glm::vec4(modelLocations.at(i).GetMinCorner(), 1.f);
			ssborec.maxAABB = glm::vec4(modelLocations.at(i).GetMaxCorner(), 1.f);

			auto pos = m_ssboData.size();
			auto dataPointer = std::make_shared<int>();
			*dataPointer = static_cast<int>(pos);
			m_ssboData.push_back(ssborec);
			m_objectsDataPointer.push_back(dataPointer);
			objectPtr->m_dataPositionsPtrs.push_back(dataPointer);
		}
	}


	return retValue;
}

glm::mat4 GBuffer::GetMatrix(std::shared_ptr<DataPtr> ptr)
{
	auto index = *(ptr->m_dataPositionsPtrs[0]);
	if (index < 0) throw GBufferInvalidDataPtrException("Suplied pointer is not valid DataPtr. Are you sure you are not using pointer on already deleted data?");

	return m_ssboData.at(index).position;
}

void GBuffer::UpdateMatrix(std::shared_ptr<DataPtr> dator, const glm::mat4& position)
{
	if (!m_baked) throw GBufferNotBakedException("");
	auto mat = m_ssboData.at(*(dator->m_dataPositionsPtrs[0])).position;
	mat = mat * position;
	std::vector<ModelLocation> loc;
	ModelLocation::Create({ mat }, loc, m_ssboData.at(*(dator->m_dataPositionsPtrs[0])).minAABB, m_ssboData.at(*(dator->m_dataPositionsPtrs[0])).maxAABB);

	SSBOStruct* ssboPtr;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_SSBO);
	ssboPtr = (SSBOStruct*) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);

	//update data
	for (auto& index : dator->m_dataPositionsPtrs) {
		m_ssboData.at(*index).position = loc[0].GetPosition();
		ssboPtr[*index].position = loc[0].GetPosition();

		m_ssboData.at(*index).minAABB = glm::vec4(loc[0].GetMinCorner(), 1.f);
		ssboPtr[*index].minAABB = glm::vec4(loc[0].GetMinCorner(), 1.f);
		m_ssboData.at(*index).maxAABB = glm::vec4(loc[0].GetMaxCorner(), 1.f);
		ssboPtr[*index].maxAABB = glm::vec4(loc[0].GetMaxCorner(), 1.f);
	}

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void GBuffer::UpdateData(std::shared_ptr<DataPtr> dator, uint data)
{
	if (!m_baked) throw GBufferNotBakedException("");
	SSBOStruct* ssboPtr;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_SSBO);
	ssboPtr = (SSBOStruct*)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);

	//update data
	for (auto& index : dator->m_dataPositionsPtrs) {

		ssboPtr[*index].extraData = data;
	}

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void GBuffer::DeleteFromBaked(std::shared_ptr<DataPtr> dator)
{
	if (!m_baked) throw GBufferNotBakedException("");

	std::thread worker([&]() {
		for (auto& ptr : dator->m_dataPositionsPtrs) {
			m_bufferFreeSpace.push_back(*ptr);
		}

		std::sort(m_bufferFreeSpace.rbegin(), m_bufferFreeSpace.rend());
	});

	//remove comands
	m_commands.erase(std::remove_if(m_commands.begin(), m_commands.end(), [&](const DrawElementsIndirectCommand& comm) {
		for (auto& ptr : dator->m_dataPositionsPtrs) {
			if (comm.baseInstance == *ptr) {
				m_commandsBufferFreeSpace++;
				return true;
			}
		}
		return false;
	}), m_commands.end());


	//update commands
	//setting up commands
	if (m_commands.size() >= 1) {
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_commandBuffer);
		glBufferSubData(GL_DRAW_INDIRECT_BUFFER, 0, sizeof(DrawElementsIndirectCommand) * m_commands.size(), &m_commands[0]);
	}

	worker.join();
}

std::shared_ptr<std::vector<std::shared_ptr<DataPtr>>> GBuffer::AddToBaked(const std::string& name, const std::vector<ModelLocation>& modelLocations)
{
	glMemoryBarrier(GL_ALL_BARRIER_BITS);
	if (!m_baked) throw GBufferNotBakedException("");
	auto object = m_objects.find(name);

	if (object == m_objects.end()) {
		return nullptr;
	}
	//our vao
	glBindVertexArray(m_VAO);

	auto retValue = std::make_shared<std::vector<std::shared_ptr<DataPtr>>>();
	auto instances = static_cast<int>(modelLocations.size());
	retValue->reserve(instances);

	int numMesh = static_cast<int>(object->second.meshes.size());
	int size = static_cast<int>(modelLocations.size()) * numMesh;
	if (size < numMesh) throw FatalErrorException("Size cannot be smaller than number of meshes!");

	DrawElementsIndirectCommand command;
	SSBOStruct ssboRecord;
	auto commandVector = std::vector<DrawElementsIndirectCommand>();
	auto ssboVector = std::vector<SSBOStruct>();


	for (auto &mesh : object->second.meshes) {

		command.firstIndex = mesh.firstIndex;
		command.baseVertex = mesh.baseVertex;
		command.vertexCount = static_cast<GLuint>(mesh.indices->size());
		command.instanceCount = 1;
		commandVector.push_back(command);


		//setting per Draw data
		ssboRecord.transformation = mesh.transformation;
		ssboRecord.alphaDiscard = mesh.alphaDiscard;

		// per Draw textures
		auto it = mesh.textures.find(Mesh::TextTypes::Diffuse);
		ssboRecord.diffuseTextureIndex = it != mesh.textures.end() ? m_textureDiffuse->GetTextureIndex(it->second) : -1;
		it = mesh.textures.find(Mesh::TextTypes::Specular);
		ssboRecord.specularTextureIndex = it != mesh.textures.end() ? m_textureSpecular->GetTextureIndex(it->second) : -1;
		it = mesh.textures.find(Mesh::TextTypes::Normal);
		ssboRecord.normalTextureIndex = it != mesh.textures.end() ? m_textureNormal->GetTextureIndex(it->second) : 0;



		ssboVector.push_back(ssboRecord);
	}

	bool mapped = false;
	SSBOStruct* ssboPtr = nullptr;

	if (m_bufferFreeSpace.size() >= size) {
		mapped = true;
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_SSBO);
		ssboPtr = (SSBOStruct*)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
	}



	for (auto &location : modelLocations) {
		
		//create new Object record
		auto objectPtr = std::make_shared<DataPtr>();

		//push it to the current object...
		object->second.meshDataPointers.push_back(objectPtr);

		//and store the pointer for return
		retValue->push_back(objectPtr);
		
		//copying data to some free space
		for(int i = 0; i < numMesh; i++) {
			int index;

			if (m_bufferFreeSpace.size() <= 0) {
				index = static_cast<int>(m_ssboData.size());
				m_ssboData.push_back(ssboVector[i]);
				m_drawIds.push_back(m_drawIds.size() > 0 ? m_drawIds.back() + 1 : 0);
			}
			else {
				index = m_bufferFreeSpace.back();
				m_bufferFreeSpace.pop_back();
				m_ssboData[index] = ssboVector[i];
			}

			m_ssboData[index].minAABB = glm::vec4(location.GetMinCorner(), 1.f);
			m_ssboData[index].maxAABB = glm::vec4(location.GetMaxCorner(), 1.f);
			m_ssboData[index].position = location.GetPosition();

			if (mapped) {
				ssboPtr[index] = m_ssboData[index];
			}
			
			

			commandVector[i].baseInstance = index;
			m_commands.push_back(commandVector[i]);
			if (m_commandsBufferFreeSpace > 0) m_commandsBufferFreeSpace--;
			auto pointer = std::make_shared<int>(index);
			objectPtr->m_dataPositionsPtrs.push_back(pointer);
		}


	}

	if(mapped) {
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}

	//there is no space so we are generating new bigger buffers
	else {
		auto ptr = m_ssboData.size();
		m_ssboData.resize(ptr + m_bufferPlusGrow);		
		auto drId = m_drawIds.back() + 1;
		

		for(auto i = 0; i < m_bufferPlusGrow; i++) {
			m_drawIds.push_back(drId++);
			m_bufferFreeSpace.push_back(ptr++);
			std::sort(m_bufferFreeSpace.rbegin(), m_bufferFreeSpace.rend());
		}

		glBindBuffer(GL_ARRAY_BUFFER, m_drawIdBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLuint) * m_drawIds.size() , &m_drawIds[0], GL_STATIC_DRAW);
		//glBufferSubData(GL_DRAW_INDIRECT_BUFFER, 0, sizeof(GLuint) * m_drawIds.size(), &m_drawIds[0]);
		
		//setting up per draw data stored in SSBO (textures select index, basic transform, etc..)
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_SSBO);
		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(SSBOStruct) * m_ssboData.size(), &m_ssboData[0], GL_STATIC_DRAW);
		//glBufferSubData(GL_SHADER_STORAGE_BUFFER,0, sizeof(SSBOStruct) * m_ssboData.size(), &m_ssboData[0]);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_SSBO);
		
	}


	//setting up commands
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_commandBuffer);
	//not enoughs space in command buffer
	if(m_commandsBufferFreeSpace < size) {			
		glBufferData(GL_DRAW_INDIRECT_BUFFER, sizeof(DrawElementsIndirectCommand) * (m_commands.size() + m_bufferPlusGrow), &m_commands[0], GL_STATIC_DRAW);
	}	
	glBufferSubData(GL_DRAW_INDIRECT_BUFFER, 0, sizeof(DrawElementsIndirectCommand) * m_commands.size(), &m_commands[0]);

	if (!mapped && m_bufferPlusGrow < m_bufferPlusGrowLimit) m_bufferPlusGrow <<= 1;

	glBindVertexArray(0);

	return retValue;
}
