#pragma once

#define M_PI 3.14159265358979323846  /* pi */
#include <string>
#include <map>
#include "WindowManager.h"
#include "KeyboardLayoutManager.h"
#include <glm\gtc\matrix_transform.hpp>
#include <glm\glm.hpp>
#include <glm\matrix.hpp>
#include <glm\geometric.hpp>
#include <glm\trigonometric.hpp>


class Camera {
public:
	
	enum MoveEnum { UP, LEFT, DOWN, RIGHT, NONE };
	void virtual Recalc() = 0;

	/// <summary>
	/// Gets the MVP.
	/// </summary>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetMVP();

	/// <summary>
	/// Gets the view matrix.
	/// </summary>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetViewMatrix();

	/// <summary>
	/// Gets the projection matrix.
	/// </summary>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetProjectionMatrix();

	/// <summary>
	/// Get window resolution for window, where is our camera.
	/// </summary>
	/// <returns>vec2(width, height)</returns>
	glm::ivec2 GetViewport();

	/// <summary>
	/// Gets the inverse projection matrix.
	/// </summary>
	/// <returns>glm.mat4.</returns>
	virtual glm::mat4 GetInverseProjectionMatrix() { return glm::inverse(projectionMatrix); };

	/// <summary>
	/// Gets the window manager.
	/// </summary>
	/// <returns>WindowManager *.</returns>
	WindowManager* GetWM() { return m_wManager; }

protected:
	
	WindowManager* m_wManager = nullptr;
	glm::mat4 projectionMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 MVP;

	/// <summary>
	/// Finalizes an instance of the <see cref="Camera"/> class.
	/// </summary>
	virtual ~Camera() {};
};

/// <summary>
/// Class PerspectiveCamera.
/// </summary>
/// <seealso cref="Camera" />
class PerspectiveCamera : public Camera
{
public:
	


	/// <summary>
	/// Returns or creates camera. If camera with given name exist, it will return it instance. If camera doesn't exist method creates new instance and returns this new instance.
	/// </summary>
	/// <param name="cameraName">Name of camera</param>
	/// <param name="wm">The wm.</param>
	/// <returns>PerspectiveCamera *.</returns>
	static PerspectiveCamera* GetCamera(std::string cameraName, WindowManager* wm);

	/// <summary>
	/// Gets the camera instance. If camera with specified name doesn't exists nullptr is returned.
	/// </summary>
	/// <param name="cameraName">Name of the camera.</param>
	/// <returns>PerspectiveCamera *.</returns>
	static PerspectiveCamera* GetCameraInstance(std::string cameraName);

	/// <summary>
	/// Recalcs this instance.
	/// </summary>
	void Recalc() override;

	/// <summary>
	/// Gets the inverse projection matrix.
	/// </summary>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetInverseProjectionMatrix() override;

	/// <summary>
	/// Processes the move.
	/// </summary>
	/// <param name="dir">The dir.</param>
	/// <returns>glm.vec3.</returns>
	glm::vec3 ProcessMove(PerspectiveCamera::MoveEnum dir);

	/// <summary>
	/// Moves the specified movement direction.
	/// </summary>
	/// <param name="movementDirection">The movement direction.</param>
	void Move(const glm::vec3& movementDirection);

	/// <summary>
	/// Processes the move.
	/// </summary>
	/// <param name="hAngle">The h angle.</param>
	/// <param name="vAngle">The v angle.</param>
	void ProcessMove(float hAngle, float vAngle);
	
	using Camera::GetMVP;
	/// <summary>
	/// Gets the MVP.
	/// </summary>
	/// <param name="model">The model.</param>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetMVP(glm::mat4 model);

	/// <summary>
	/// Gets the position.
	/// </summary>
	/// <returns>glm.vec3.</returns>
	glm::vec3 GetPosition();

	/// <summary>
	/// Sets the position.
	/// </summary>
	/// <param name="pos">The position.</param>
	void SetPosition(const glm::vec3 &pos);

	/// <summary>
	/// Gets the frustrum planes.
	/// </summary>
	/// <returns>glm.vec4 *.</returns>
	glm::vec4* GetFrustrum();
	

	/// <summary>
	/// Finalizes an instance of the <see cref="PerspectiveCamera"/> class.
	/// </summary>
	~PerspectiveCamera();

private:
	static std::map<std::string, PerspectiveCamera*> _instances;

	/// <summary>
	/// Private constructor, which will create new instance of camera.
	/// </summary>
	/// <param name="wm">The wm.</param>
	PerspectiveCamera(WindowManager* wm);

	/// <summary>
	/// Method which recalculates direction vector. For computation it uses current value of vertical and horizontal angle.
	/// </summary>
	void CountDirection();

	/// <summary>
	/// Method which recalculates right vector. For computation it uses current value of horizontal angle.
	/// </summary>
	void CountRight();

	/// <summary>
	/// Method which counts UP vector. UP vector is  perpendicular to both direction and right, so it uses current right and direction vectors.
	/// </summary>
	void CountUp();

	/// <summary>
	/// Method which recalculate all vectors in right order.
	/// </summary>
	void CountVectors();

	/// <summary>
	/// Calculates the frustrum.
	/// </summary>
	void CalculateFrustrum();

public:

	float initialFoV = glm::half_pi<float>();

	/// <summary>
	/// Gets the right vector.
	/// </summary>
	/// <returns>glm.vec3.</returns>
	glm::vec3 GetRight() { return rightVector; }

	/// <summary>
	/// Gets the look vector.
	/// </summary>
	/// <returns>glm.vec3.</returns>
	glm::vec3 GetLook() { return m_directionVector; }
private:
	
	
	glm::vec3 position = glm::vec3(-0, 30, -0);//glm::vec3(-1627, 10, -1552);
	// horizontal angle : toward -Z
	float horizontalAngle = float(M_PI);
	// vertical angle : 0, look at the horizon
	float verticalAngle = 0.0f;
	float speed = 120.f;//120.0f;//1.4f; // 0.3 units / second
	float mouseSpeed = 0.1f;
	
	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 m_directionVector;
	glm::vec3 rightVector;
	glm::vec3 upVector;
	glm::vec4 m_frustrum[6];
};

/// <summary>
/// Class DirectCamera. This camera is used to orthogonal projection.
/// </summary>
/// <seealso cref="Camera" />
class DirectCamera : public Camera {
public:
	
	/// <summary>
	/// Returns or creates camera. If camera with given name exist, it will return it instance. If camera doesn't exist method creates new instance and returns this new instance.
	/// </summary>
	/// <param name="cameraName">Name of the camera.</param>
	/// <param name="wm">The wm.</param>
	/// <param name="nearPlane">The near plane.</param>
	/// <param name="farPlane">The far plane.</param>
	/// <param name="fov">The fov.</param>
	/// <returns>DirectCamera *.</returns>
	static DirectCamera* GetCamera(std::string cameraName, WindowManager* wm, float nearPlane, float farPlane, float fov);
	
	/// <summary>
	/// Gets the camera instance. If camera with specified name doesn't exists nullptr is returned.
	/// </summary>
	/// <param name="cameraName">Name of the camera.</param>
	/// <returns>DirectCamera *.</returns>
	static DirectCamera* GetCameraInstance(std::string cameraName);
	
	/// <summary>
	/// Recalcs the specified light position.
	/// </summary>
	/// <param name="lightPos">The light position.</param>
	/// <param name="inverseView">The inverse view.</param>
	/// <param name="margin">The margin.</param>
	void Recalc(const glm::vec3 &lightPos, const glm::mat4 &inverseView, glm::vec2 margin = glm::vec2(0, 0));
	
	/// <summary>
	/// Get worlds BB.
	/// </summary>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	std::pair<glm::vec4, glm::vec4> WorldBB();
	
private:
	/// <summary>
	/// Recalcs this instance.
	/// </summary>
	void Recalc();
	static std::map<std::string, DirectCamera*> m_instances;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="DirectCamera"/> class.
	/// </summary>
	/// <param name="wm">The wm.</param>
	/// <param name="nearPlane">The near plane.</param>
	/// <param name="farPlane">The far plane.</param>
	/// <param name="fov">The fov.</param>
	DirectCamera(WindowManager* wm, float nearPlane, float farPlane, float fov);

	/// <summary>
	/// Computes the near and far by triangle clipping by frustrum view frustrum planes.
	/// </summary>
	/// <param name="xmin">The xmin.</param>
	/// <param name="xmax">The xmax.</param>
	/// <param name="ymin">The ymin.</param>
	/// <param name="ymax">The ymax.</param>
	/// <returns>std.tuple&lt;_This, _Rest...&gt;.</returns>
	std::tuple<float, float> ComputeNearAndFar(float xmin, float xmax, float ymin, float ymax);

	
	float m_near;
	float m_far;
	float m_tanHFov;
	float m_tanVFov;
	glm::vec4 m_viewMinCorner;
	glm::vec4 m_viewMaxCorner;

	glm::vec4 m_frustrumCorners[8];

	/// <summary>
	/// Struct Triangle used when computing near and far plane.
	/// </summary>
	struct Triangle {		
		glm::vec3 vertices[3];
		bool culled;
	};

	
};



#ifndef __NAMESPACE_MATRIX
#define __NAMESPACE_MATRIX

/// <summary>
/// The Matrix namespace.
/// </summary>
namespace Matrix {
	
	template <typename T>
	/// <summary>
	/// Creates perpestcive iversion matrix.
	/// </summary>
	/// <param name="fovy">The fovy.</param>
	/// <param name="aspect">The aspect.</param>
	/// <param name="zNear">The z near.</param>
	/// <param name="zFar">The z far.</param>
	/// <returns>glm.tmat4x4&lt;T, P&gt;.</returns>
	extern inline glm::tmat4x4<T, glm::defaultp> perspectiveInverse(
		T const & fovy,
		T const & aspect,
		T const & zNear,
		T const & zFar);
};

#endif // !__NAMESPACE_MATRIX
