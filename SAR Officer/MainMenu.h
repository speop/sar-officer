// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-01-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <memory>
#include "Shader.h"
#include "Texture.h"
#include "UIElement.h"
#include "ConfigStore.h"

/// <summary>
/// Class MainMenu.
/// </summary>
/// <seealso cref="UIElement" />
class MainMenu : public UIElement
{
	/// <summary>
	/// Enum for views.
	/// </summary>
	enum Screen {
		ScrMain = 0,
		ScrSettings,
		ScrKeyboard,
		ScrLoading,
		END_STOP
	};

public:
	
	/// <summary>
	/// Initializes a new instance of the <see cref="MainMenu"/> class.
	/// </summary>
	MainMenu();
	/// <summary>
	/// Finalizes an instance of the <see cref="MainMenu"/> class.
	/// </summary>
	virtual ~MainMenu();
	
	/// <summary>
	/// Wake up method <see cref="UIElement.Wake" />. This method chooses menu background.
	/// </summary>
	void Wake() override;

	/// <summary>
	/// Run method <see cref="UIElement.Run" />. 
	/// </summary>
	void Run() override;

	/// <summary>
	/// ImGui run method <see cref="UIElement.RunImgui" />. Draw UI elements.
	/// </summary>
	void RunImgui() override;

	/// <summary>
	/// Stops method <see cref="UIElement.Stop" />. Releases alocated memory.
	/// </summary>
	void Stop() override;

	/// <summary>
	/// Burns this instance <see cref="UIElement.Burn" />.
	/// </summary>
	void Burn() override;

private:
	/// <summary>
	/// Initializes this instance and creates background shader.
	/// </summary>
	void Init() override;

	/// <summary>
	/// Refrehes the setting values to match config file.
	/// </summary>
	void RefrehSettingValues();

	/// <summary>
	/// Refrehes the keyboard values to match keyboard config file.
	/// </summary>
	void RefrehKeyboardValues();
	
	
	typedef void (MainMenu::*MemFn)(void);
	
	MemFn m_Screens[Screen::END_STOP] = { nullptr };
	Screen m_currentScreen = ScrMain;
	std::vector<ConfigStore::ResolutionRec> m_resolutions;
	std::vector<ConfigStore::LangRec> m_languages;
	bool m_Swindowed = false;
	int m_SselectedRes = 0, m_SselectedLang = 0;
	float m_SaudioFXLevel = 0.f, m_SmusicLevel = 0.f;
	int* m_keys = nullptr;
	int m_key = 0;
	bool m_modalW = false;

	std::shared_ptr<Shader> m_backgroundShader;
	std::vector<std::unique_ptr<Texture>> m_backgroundS;
	int m_bgIndex = 0;
	GLuint m_VAO = 0;

#pragma region Screens
	
	/// <summary>
	/// Draws and manages main menu screen.
	/// </summary>
	void Main();

	/// <summary>
	/// Draws and manages loading screen.
	/// </summary>
	void Loading();

	/// <summary>
	/// Draws and manages settings screen,
	/// </summary>
	void Settings();

	/// <summary>
	/// Draws and manages keys bindings screen.
	/// </summary>
	void Keyboard();
#pragma endregion 

#pragma region Buttons events
	
	/// <summary>
	/// Callback for New game button click. This method calls <see cref="MainGame.LoadLevel" />
	/// </summary>
	void NewGameBtn_click();
	/// <summary>
	/// Callback for settings button click. This method switches screen to settings screen.
	/// </summary>
	void SettingsBtn_click();

	/// <summary>
	/// Callback for keyboard button click. This method switches screen to key binding screen.
	/// </summary>
	void KeyboardBtn_click();

	/// <summary>
	/// Callback for editor button click. This method switches to editor UI.
	/// </summary>
	void EditorBtn_click();

	/// <summary>
	/// Exits to windows BTN click.
	/// </summary>
	void ExitToWindowsBtn_click();

	/// <summary>
	/// Callback for settings save button.
	/// </summary>
	/// 
	void SettingsSaveBtn_click();
	/// <summary>
	/// Callback for settings cancel button.
	/// </summary>
	void SettingsCancelBtn_click();

	/// <summary> 
	/// Callback for key binding save button.
	/// </summary>
	void KeyboardSaveBtn_click();

	/// <summary>
	/// Callback for key binding cancel button.
	/// </summary>
	void KeyboardCancelBtn_click();

#pragma endregion 

#pragma region Helpers
	
	/// <summary>
	/// Makes name from key value.
	/// </summary>
	/// <param name="GLFW_code">The GLFW code.</param>
	/// <param name="tag">The tag.</param>
	/// <returns>std.string.</returns>
	std::string KeyboardBtnName(int GLFW_code, int tag = 0);
#pragma endregion 

};

