// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-16-2016
//
// Last Modified By : David Buchta
// Last Modified On : 03-05-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include "Camera.h"
#define MAX_NODES 500

// The size of a patch in meters at which point to
// stop subdividing a terrain patch once it's width is
// less than the cutoff.. because we have highamp with precision 1m and we want terrain precision 25cm we are going to use tesselation high randomizer.. and because max tesselation level is 64 we and 64 *0.25 is 16 we have to set cutoff to 16
#define TERRAIN_REC_CUTOFF 64


struct Coords
{
	float x, y, z;
	Coords(float X, float Y, float Z) : x(X), y(Y), z(Z) {};
};


/// <summary>
/// Class TerrainNode.
/// </summary>
class TerrainNode {
public:
	Coords origin;
	float width;
	float height;
	int type; // child # relative to its parent. (0 == root)

	//Tesselation scale
	int tscaleNegX = 1;
	int tscalePosX = 1;
	int tscaleNegZ = 1;
	int tscalePosZ = 1;

	TerrainNode* parent = nullptr;
	TerrainNode* child1 = nullptr;
	TerrainNode* child2 = nullptr;
	TerrainNode* child3 = nullptr;
	TerrainNode* child4 = nullptr;

	//And neighbours... Because everybody wants to know his neighbour. What if his neighbour is cereal killer?
	TerrainNode* neihbourNorth = nullptr;
	TerrainNode* neihbourSouth = nullptr;
	TerrainNode* neihbourEast = nullptr;
	TerrainNode* neihbourWest = nullptr;

	/// <summary>
	/// Initializes a new instance of the <see cref="TerrainNode"/> class.
	/// </summary>
	/// <param name="Parent">The parent.</param>
	/// <param name="Type">The type.</param>
	/// <param name="C">The c.</param>
	/// <param name="Width">The width.</param>
	/// <param name="Height">The height.</param>
	TerrainNode(TerrainNode *Parent, int Type, Coords C, float Width, float Height) : parent(Parent), type(Type), origin(C), width(Width), height(Height) {};
	
	/// <summary>
	/// Divides the node.
	/// </summary>
	/// <returns>bool.</returns>
	bool DivideNode();
	/// <summary>
	/// Checks if node have to be divided. 
	/// </summary>
	/// <returns>bool.</returns>
	bool CheckDivideNode();

	/// <summary>
	/// Determines whether this node has children.
	/// </summary>
	/// <returns>bool.</returns>
	bool HasChildren();

};

/// <summary>
/// Class TerrainTree. Implementaion of QuadTree for terrain.
/// </summary>
class TerrainTree
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="TerrainTree"/> class.
	/// </summary>
	TerrainTree();
	/// <summary>
	/// Finalizes an instance of the <see cref="TerrainTree"/> class.
	/// </summary>
	~TerrainTree();

	/// <summary>
	/// Creates the tree.
	/// </summary>
	/// <param name="origin">The origin.</param>
	/// <param name="width">The width.</param>
	/// <param name="height">The height.</param>
	/// <param name="deleteOld">Tells if root node contains tree whichs must be deleted.</param>
	void CreateTree(Coords origin, float width, float height, bool deleteOld = true);

	/// <summary>
	/// Deletes tree and free its memory
	/// </summary>
	void ShutdownTree();

	/// <summary>
	/// Gets the root node of tree.
	/// </summary>
	/// <returns>TerrainNode *.</returns>
	TerrainNode* GetRoot();

	/// <summary>
	/// Calculates the tess scale.
	/// </summary>
	void CalcTessScale();

	/// <summary>
	/// NOT RECURSIVE
	/// Calculate tesselation scale. This is necessary for situation when are two nodes with different LOD next to each other. If we don't calculate this there could
	/// originate some holes in terrain
	/// </summary>
	/// <param name="node">Node of Terrain Quad tree</param>
	void CalcTessScale(TerrainNode* node);

private:
	
	/// <summary>
	/// Clears the tree.
	/// </summary>
	void ClearTree();
	/// <summary>
	/// Clears the tree.
	/// </summary>
	/// <param name="root">The root.</param>
	void ClearTree(TerrainNode* root);

	/// <summary>
	/// Finds the node.
	/// </summary>
	/// <param name="x">The x.</param>
	/// <param name="z">The z.</param>
	/// <returns>TerrainNode *.</returns>
	TerrainNode* FindNode(float x, float z);

	/// <summary>
	/// Finds the node.
	/// </summary>
	/// <param name="root">The root.</param>
	/// <param name="x">The x.</param>
	/// <param name="z">The z.</param>
	/// <returns>TerrainNode *.</returns>
	TerrainNode* FindNode(TerrainNode* root, float x, float z);

private:
	TerrainNode* treeRoot = nullptr;
};

