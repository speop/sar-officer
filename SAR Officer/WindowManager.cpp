#include "WindowManager.h"
#include "Exceptions.h"


/// <summary>
/// The m last key{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
int WindowManager::m_lastKey = 0;
/// <summary>
/// The m keys{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
/// / The m fullscreen
bool WindowManager::m_keys[1024];

WindowManager::WindowManager(int w, int h , bool fs): m_width(w), m_height(h), m_fullscreen(fs){}


/**/
void WindowManager::Init()
{
	/*
		Creating game window because... you know... we don't want imposibru difficulty which is when you play and don't see anything..
	*/
	if (!glfwInit()) {
		throw new BasicException("Failed to initialize GLFW");
	}


	//glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // We want OpenGL 4.x because some says it's better
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,4);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed.. so why did I add it here? I am not developing for MacOS.. I only support Visual Studio
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL.. Yeah everything old is bad.. Like old wine, old whisky, old art... But old OpenGL is like old socks 
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// Open a window and create its OpenGL context
	std::string windowTag = "SAR Officer - v." + std::string(VERSION);
	m_window = glfwCreateWindow(this->m_width, this->m_height,windowTag.c_str(), m_fullscreen ? glfwGetPrimaryMonitor() : nullptr, nullptr);
	
	// And now we should have window pointer.. If we don't have it something is wrong.. you know.. difficulty imposibru	
	if (m_window == nullptr) {
		glfwTerminate();
		throw BasicException("Failed to open GLFW window. Try bing to get answer why you can't play this super game");
	}
	glfwMakeContextCurrent(m_window); // Initialize GLEW
	glewExperimental = true; // Needed in core profile
	if (glewInit() != GLEW_OK) {
		throw BasicException("Failed to initialize GLEW");
	}

	//voila we have window.. isn't it great?
	
	//center cursor to the screen
	glfwSetCursorPos(m_window, 1024 / 2.0, 768 / 2.0);

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(m_window, GLFW_STICKY_KEYS, GL_TRUE);
	

	glfwSetFramebufferSizeCallback(m_window, framebuffer_size_callback);
	glfwSetKeyCallback(m_window, key_press_callback);
	
}


void WindowManager::Run()
{
	glfwSwapBuffers(m_window);
	glfwPollEvents();
}

bool WindowManager::KeyPressed(int key)
{
	return glfwGetKey(m_window, key) == GLFW_PRESS;
	
}

bool WindowManager::KeyPressedMulitple(int key)
{
	return m_keys[key];
}

bool WindowManager::WindowClosed()
{
	return glfwWindowShouldClose(m_window) != 0;
}

bool WindowManager::Focus()
{
	return glfwGetWindowAttrib(this->m_window, GLFW_FOCUSED) != 0;

}

int WindowManager::GetWidth()
{
	return this->m_width;
}

int WindowManager::GetHeight()
{
	return this->m_height;
}



GLFWwindow * WindowManager::GetWindow()
{
	return m_window;
}

void WindowManager::SetResolution(ConfigStore::ResolutionRec res)
{
	glfwSetWindowSize(m_window, res.x, res.y);
}


WindowManager::~WindowManager()
{
	glfwTerminate();
}


int WindowManager::GetLastKey()
{
	auto ret = m_lastKey;
	m_lastKey = 0;
	return ret;
}
#pragma  region Callbacks
void WindowManager::framebuffer_size_callback(GLFWwindow * window, int width, int height)
{
	glViewport(0, 0, width, height);
}
void WindowManager::key_press_callback(GLFWwindow * windows, int key, int scancode, int action, int mode)
{
	if (action == GLFW_PRESS)
	{
		m_lastKey = key;
		m_keys[key] = true;
	}
	else if (action == GLFW_RELEASE)
	{
		m_keys[key] = false;
	}
}

#pragma endregion

