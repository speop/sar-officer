// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-15-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <memory>
#include <stack>
#include "WindowManager.h"
#include "Defines.h"
#include "GraphicsEngine.h"
#include "Camera.h"
#include "UIManager.h"
#include "TimeManager.h"
#include "ConfigStore.h"
#include "Exceptions.h"
#include "Logger.h"
#include <chrono>
#include "Player.h"

/// <summary>
/// Class MainGame.
/// </summary>
class MainGame
{

public:
	/// <summary>
	/// Gets this instance.
	/// </summary>
	/// <returns>MainGame *.</returns>
	static MainGame* Get();
	/// <summary>
	/// Finalizes an instance of the <see cref="MainGame"/> class.
	/// </summary>
	~MainGame();

	/// <summary>
	/// Starts this instance.
	/// </summary>
	void Start();

	/// <summary>
	/// Gets current game state.
	/// </summary>
	/// <returns>GameState.</returns>
	GameState State();

	/// <summary>
	/// Gets current loadings the progress
	/// </summary>
	/// <returns>float.</returns>
	float LoadingProgress();

	/// <summary>
	/// Sets current loadings the progress.
	/// </summary>
	/// <param name="value">The value.</param>
	void LoadingProgress(float value);

	/// <summary>
	/// Increment loading progres by stack top inc value.
	/// </summary>
	void LoadingProgressStackInc();

	/// <summary>
	/// Erase loading progress increment values stack.
	/// </summary>
	void LoadingProgressStackErase();

	/// <summary>
	/// Push increment value to loading progress stack.
	/// </summary>
	/// <param name="value">The value.</param>
	void LoadingProgressStackPushV(float value);

	/// <summary>
	/// Push increment percentage to loading progress stack.
	/// </summary>
	/// <param name="percentage">The percentage.</param>
	void LoadingProgressStackPushP(float percentage);

	/// <summary>
	/// Pops value from loading progress stack pop.
	/// </summary>
	void LoadingProgressStackPop();

	/// <summary>
	/// Return if loaded.
	/// </summary>
	/// <returns>bool.</returns>
	bool Loaded();

	/// <summary>
	/// Loads the level.
	/// </summary>
	/// <param name="level">The level.</param>
	void LoadLevel(std::string level);

	/// <summary>
	/// Loads the editor.
	/// </summary>
	void LoadEditor();

	/// <summary>
	/// Continues this instance.
	/// </summary>
	void Continue();

	/// <summary>
	/// Shows menu.
	/// </summary>
	void Menu();

	/// <summary>
	/// Exits game.
	/// </summary>
	void Exit();

	/// <summary>
	/// Set if lighting should be calculated.
	/// </summary>
	/// <param name="value">The value.</param>
	void DrawLighting(bool value);

	/// <summary>
	/// Gets the video modes.
	/// </summary>
	/// <returns>std.vector&lt;_Ty, _Alloc&gt;.</returns>
	std::vector<ConfigStore::ResolutionRec> GetVideoModes();

	/// <summary>
	/// Sets the resolution.
	/// </summary>
	/// <param name="res">The resource.</param>
	void SetResolution(ConfigStore::ResolutionRec res);

	/// <summary>
	/// Gets the pressed safe key.
	/// </summary>
	/// <returns>int.</returns>
	int GetSafeKey();

	/// <summary>
	/// Gets the HWND.
	/// </summary>
	/// <returns>HWND.</returns>
	HWND GetHWND();

	/// <summary>
	/// Gets the mouse position.
	/// </summary>
	/// <returns>glm.vec2.</returns>
	glm::vec2 GetMouse();

	/// <summary>
	/// Saves the screenshot.
	/// </summary>
	void SaveScreenshot();

private:
	/// <summary>
	/// Prevents a default instance of the <see cref="MainGame"/> class from being created.
	/// </summary>
	MainGame();

	/// <summary>
	/// Loads the level.
	/// </summary>
	/// <param name="level">The level.</param>
	/// <param name="afterLoadState">State of the after load.</param>
	void LoadLevel(std::string level, GameState afterLoadState);

	/// <summary>
	/// Runs this instance.
	/// </summary>
	void Drive();

	/// <summary>
	/// Manages the input.
	/// </summary>
	void ManageInput();

private:
	static MainGame* instance;

	
	WindowManager* m_wManager = nullptr;
	GameState m_gameState;
	GameState m_previousGameState;
	GameState m_afterLoadGameState;
	GraphicsEngine* m_gEngine = nullptr;
	PerspectiveCamera* m_camera = nullptr;
	std::shared_ptr<UIManager> m_uiManager;
	bool m_started = false, m_levelLoaded = false;

	float m_loadingProgress = 0.f;
	std::stack<float> m_loadingProgressIncrement;
	glm::vec2 m_mousePos{ 0 };
	glm::vec3 m_movementVector;
	bool m_mouseCursor = true;
	bool m_drawLighting = true;
	std::unique_ptr<GO::Player> m_player;

	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;

	/// <summary>
	/// Profiles the fucntion specified by tag.
	/// </summary>
	/// <param name="tag">The tag.</param>
	void Profile(std::string tag);
};

