// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-16-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-05-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <glm\gtc\type_ptr.hpp>
#include <memory>
// Include GLEW. Always include it before gl.h and glfw.h, since it's a bit magic.
#include <GL/glew.h>


#include "TerrainTree.h"
#include "Shader.h"
#include "FileManager.h"
#include "Texture.h"
#include <future>


/// <summary>
/// Class TerrainManage. This class create, maintain and draw terratin and grass
/// </summary>
class TerrainManager {
	
	typedef struct alignas(16) {
		glm::mat4 modelMatrix;
		GLint tscaleNegX;
		GLint tscaleNegZ;
		GLint tscalePosX;
		GLint tscalePosZ;
		GLfloat tileScale;
	} SSBOStruct;

public:
	/// <summary>
	/// Initializes a new instance of the <see cref="TerrainManager"/> class.
	/// </summary>
	/// <param name="length">The length.</param>
	/// <param name="width">The width.</param>
	/// <param name="hOffset">The h offset.</param>
	TerrainManager(int length, int width, int hOffset);
	
	/// <summary>
	/// Releases all data of this instance.
	/// </summary>
	void Release();

	/// <summary>
	/// Finalizes an instance of the <see cref="TerrainManager"/> class.
	/// </summary>
	~TerrainManager();

	/// <summary>
	/// Initializes this instance.
	/// </summary>
	/// <exception cref="BasicException">If cannot load textures for terrain</exception>
	void Init();

	/// <summary>
	/// Runs the specified render pass.
	/// </summary>
	/// <param name="renderPass">The render pass.</param>
	/// <exception cref="NotInitializedException">If terrain wasn't initialized</exception>
	void Run(RenderPass renderPass = CLASSIC);
private:
	/// <summary>
	/// Recalculate and render terrain.
	/// </summary>
	void Render();

	/// <summary>
	/// Render Node to screen. This methods call OpenGl functions and display one node on screen
	/// </summary>
	/// <param name="node">node to be rendered</param>
	/// <param name="tree">The tree.</param>
	/// <param name="returnVec">The return vec.</param>
	void RenderNode(TerrainNode* node, TerrainTree* tree, std::vector<SSBOStruct> &returnVec);
	
	/// <summary>
	/// Generates OpenGL data for making renering terrain possible
	/// </summary>
	/// <param name="tree">The tree.</param>
	/// <param name="returnVec">The return vec.</param>
	void GenerateRenderData(TerrainTree* tree, std::vector<SSBOStruct> &returnVec);
	
	const float c_grassHeight = 1.5f;
	const float c_grassEpsilon = 0.008f;


	bool m_calculatingTerrain = false;
	std::future<void> m_terrainWorker;

	int m_terrainLength;
	int m_terrainWidth;
	int m_maxGrassSeedsNo;
	int heightOffset;
	bool m_initialized = false;
	
	std::shared_ptr<Shader> m_terrainProgram;
	std::shared_ptr<Shader> m_terrainShaddowsShader;
	std::shared_ptr<Shader> m_grassShader;
	std::shared_ptr<Shader> m_grassComp;
	std::unique_ptr<Texture> m_heightmap;
	std::unique_ptr<Texture> material;
	std::unique_ptr<Texture> m_texture;
	std::unique_ptr<Texture> m_baseColor;
	std::unique_ptr<Texture> m_normalMap;
	std::unique_ptr<Texture> m_normals;
	GLuint vboQuad;
	GLuint vaoQuad;
	GLuint m_SSBO;
	std::vector<SSBOStruct> m_ssboData;
	std::vector<SSBOStruct> m_ssboDataSecondBuffer;
	int m_ssboSize = 0;
	int count = 0;

	GLuint m_grassCommandBuffer = 0, m_grassSeeds = 0, m_grassVAO = 0;
	
	
	
	typedef struct {
		GLuint  vertexCount;
		GLuint  instanceCount;
		GLuint  firstIndex;
		GLuint  baseVertex;;
	} DrawAraysIndirectCommand;

};
