#pragma once
#include <glm/gtc/type_ptr.hpp>
#include "Texture.h"
#include <GL\glew.h>
#include <vector>

/// <summary>
/// One mesh of model.
/// </summary>
class Mesh {
public:
	struct Vertex {
		glm::vec3 Postion;
		glm::vec3 Normal;
		glm::vec3 Tangent;
		glm::vec2 TexCoords;

		float weight[4] = {0};
		uint id[4] = {0};
	};

	enum TextTypes { Diffuse, Specular, Normal };

public:
	Mesh();
	~Mesh();

	/*Mesh Data*/
	std::shared_ptr<std::vector<Vertex>> m_vertices;
	std::shared_ptr<std::vector<GLuint>> m_indices;

	/* Functions */

	/// <summary>
	/// Initializes a new instance of the <see cref="Mesh"/> class.
	/// </summary>
	/// <param name="vertices">The vertices.</param>
	/// <param name="indices">The indices.</param>	
	Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices);
	
	/// <summary>
	/// Sets the texture for mesh.
	/// </summary>
	/// <param name="id">The identifier.</param>
	/// <param name="type">The type.</param>
	/// <returns>Mesh *.</returns>
	Mesh* SetTexture(GLuint id, TextTypes type);
	
	/// <summary>
	/// Setups the base transformation matrix.
	/// </summary>
	/// <param name="matrix">The matrix.</param>
	/// <returns>Mesh *.</returns>
	Mesh* SetupBaseMatrix(glm::mat4 matrix);
	
	/// <summary>
	/// Adds the instances matrices.
	/// </summary>
	/// <param name="trans">The trans.</param>
	/// <returns>std.vector&lt;_Ty, _Alloc&gt;.</returns>
	std::vector<std::shared_ptr<int>> AddInstances(const std::vector<glm::mat4>& trans);
	
	/// <summary>
	/// Updates the position of mesh.
	/// </summary>
	/// <param name="dataPtr">The data PTR.</param>
	/// <param name="position">The position.</param>
	/// <returns>Mesh *.</returns>
	Mesh * UpdatePosition(std::shared_ptr<int> dataPtr, const glm::mat4 &position);

	/// <summary>
	/// Delete mesh defined by the position.
	/// </summary>
	/// <param name="dataPtr">The data PTR.</param>
	/// <returns>Mesh *.</returns>
	Mesh * DeletePosition(std::shared_ptr<int> dataPtr);

	/// <summary>
	/// Sets the alpha discard. If current fetched pixel from texture will have alpha lower than this value, fragment will be discard
	/// </summary>
	/// <param name="val">The value.</param>
	/// <returns>Mesh *.</returns>
	Mesh* SetAlphaDiscard(float val);

	/// <summary>
	/// Sets the aditional data.
	/// </summary>
	/// <param name="dataPtr">The data PTR.</param>
	/// <param name="val">The value.</param>
	/// <returns>Mesh *.</returns>
	Mesh* SetAditionalData(std::shared_ptr<int> dataPtr, uint val);
	glm::mat4 GetPosition(std::shared_ptr<int> dataPtr);

	/// <summary>
	/// Returns const pointer do vertices
	/// </summary>
	/// <returns></returns>
	std::shared_ptr<const std::vector<Vertex>> GetVertices();

	/// <summary>
	/// Returns const pointer to indices
	/// </summary>
	/// <returns></returns>
	std::shared_ptr<const std::vector<GLuint>> GetIndices();

	/// <summary>
	/// Gets the basic model transformation.
	/// </summary>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetTransformation();
	void Draw(std::shared_ptr<Shader> shader, bool instanced = false);

  /// <summary>
	/// Setups the mesh.
	/// </summary>
	void SetupMesh();

private:

	std::vector<glm::mat4> m_positions;
	std::vector<std::shared_ptr<int>> m_dataPtrs;

	int m_IBOsize = 0;
	int m_bufferPlusGrow = 8;
	const int m_bufferPlusGrowLimit = 1024;

	/*  Render data  */
	GLuint m_VAO =0, m_VBO =0, m_EBO = 0, m_instanceVBO = 0;
	glm::mat4 m_baseMatrix;
	GLuint m_textureDiffuse = 0;
	GLuint m_textureSpecular = 0;
	GLuint m_textureNormal = 0;

	uint m_additionalData;
	GLint m_drawId;
	float m_alphaDiscard;


	/*  Functions    */



	/// <summary>
	/// Generates the instance buffer.
	/// </summary>
	void GenerateInstanceBO();

};
