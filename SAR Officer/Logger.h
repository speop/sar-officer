// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 12-09-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <string>
#include <iostream>
#ifdef _WIN32
#include <windows.h> 
#undef APIENTRY
#endif
#include <streambuf>
#include <fstream>




/// <summary>
/// Class Logger.
/// </summary>
class Logger
{
public:
	
	enum Level
	{		
		Info = 8,
		Warning = 14,
		Critical = 12,
	};


public:
	/// <summary>
	/// Gets the logger.
	/// </summary>
	/// <returns>Logger *.</returns>
	static Logger* GetLogger();
	/// <summary>
	/// Hides the console.
	/// </summary>
	/// <returns>Logger *.</returns>
	Logger* HideConsole();
	
	/// <summary>
	/// Logs the specified level.
	/// </summary>
	/// <param name="level">The level.</param>
	/// <param name="msg">The MSG.</param>
	/// <returns>Logger *.</returns>
	Logger* Log(Logger::Level level, std::string msg);

private:
	/// <summary>
	/// Prevents a default instance of the <see cref="Logger"/> class from being created.
	/// </summary>
	Logger();
	
	/// <summary>
	/// Changes the output color.
	/// </summary>
	/// <param name="level">The level.</param>
	void ChangeColor(Logger::Level level);

	/// <summary>
	/// Resets the output color.
	/// </summary>
	void ResetColor();

	/// <summary>
	/// Redirects the io.
	/// </summary>
	void RedirectIO();

	/// <summary>
	/// Resets the io.
	/// </summary>
	void ResetIO();

	/// <summary>
	/// Finalizes an instance of the <see cref="Logger"/> class.
	/// </summary>
	~Logger();

	
	bool m_console = true;
	std::streambuf *CinBuffer, *CoutBuffer, *CerrBuffer;
	std::fstream ConsoleInput, ConsoleOutput, ConsoleError;

#ifdef _WIN32
	DWORD m_winpid;
#endif


private:
	static Logger* LoggerInstance;
};
