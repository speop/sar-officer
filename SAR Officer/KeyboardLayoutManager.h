#pragma once
#include "WindowManager.h"
#include <string>
#include "pugixml.hpp"
#include "Exceptions.h"

/*
	SINGLETON
*/
class KeyboardLayoutManager
{

public:
	~KeyboardLayoutManager();
	
	enum Events { FORWARD, BACKWARD, LEFT, RIGHT, JUMP, STOP_END };

private:
	/// <summary>
	/// Initializes a new instance of the <see cref="KeyboardLayoutManager"/> class.
	/// </summary>
	/// <param name="wm">The wm.</param>
	KeyboardLayoutManager(WindowManager *wm);

public:
	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <param name="wm">The wm.</param>
	/// <returns>KeyboardLayoutManager *.</returns>
	static KeyboardLayoutManager* GetInstance(WindowManager *wm);
	/// <summary>
	/// Instances this instance.
	/// </summary>
	/// <returns>KeyboardLayoutManager *.</returns>
	static KeyboardLayoutManager* Instance();
	/// <summary>
	/// Check if direction key pressed.
	/// </summary>
	/// <param name="dir">The dir.</param>
	/// <returns>bool.</returns>
	bool DirectionKeyPressed(Events dir);
	
	/// <summary>
	/// Gets the current keys binding.
	/// </summary>
	/// <returns>int *.</returns>
	int* GetKeys();

	/// <summary>
	/// Saves the keys binding.
	/// </summary>
	/// <param name="keys">The keys.</param>
	void SaveKeys(int* keys);

private:
	static KeyboardLayoutManager* instance;
	WindowManager *wManager = nullptr;
	pugi::xml_document  m_configData;
	pugi::xml_node m_rootNode;
	std::string m_currentConfig;
	int m_keys[STOP_END];
};

