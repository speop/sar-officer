#include "GraphicElement.h"
#include "Model.h"
#include "glm/gtc/matrix_access.hpp"


GraphicElement::~GraphicElement()
{
	m_program.reset();
	glDeleteVertexArrays(1, &m_VAO);
	glDeleteBuffers(1, &m_VBO);
}

void GraphicElement::LoadPositions(std::string filename, std::vector<glm::mat4>& positions, bool hasY)
{
	std::ifstream ifs(filename);
	std::string line;
	if (!ifs.good()) {
		throw ModelParamsFileErrorException(filename);
	}

	while (std::getline(ifs, line)) {
		if (line.empty()) continue;
		
		glm::mat4 mat;
		auto matPtr = glm::value_ptr(mat);
		std::istringstream in(line);      //make a stream for the line itself

		//opengl is column major
		/*
			0  4  8  12
			1  5  9  13
			2  6 10  14
			3  7 11  15
		 */
		 
		//now read the whitespace-separated floats
		for(auto i = 0; i < 16; i++) {
			float val;
			in >> val;

			// 
			if(i == 14 && !hasY) {
				try {
					matPtr[13] = CollisionManager::Instance().GetHeightInterpolated(glm::vec2(matPtr[12], val), true);
				}
				catch (CollisionNoDataException &ex) {
					matPtr[13] = 0;
				}
			}
			
			matPtr[i] = val;
		}
		
		positions.push_back(mat);
	}
}