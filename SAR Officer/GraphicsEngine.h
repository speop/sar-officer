// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-15-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <memory>
// Include GLEW. Always include it before gl.h and glfw.h, since it's a bit magic.
#include <GL/glew.h>
// Include GLFW
#include <GLFW\glfw3.h>
// Include GLM
#include <glm/glm.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "TerrainManager.h"
#include "Sky.h"
#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "ForrestManager.h"
#include "GBuffer.h"
#include "RBuffer.h"
#include "NPCManager.h"

/// <summary>
/// Class GraphicsEngine.
/// </summary>
class GraphicsEngine
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="GraphicsEngine"/> class.
	/// </summary>
	GraphicsEngine();
	/// <summary>
	/// Finalizes an instance of the <see cref="GraphicsEngine"/> class.
	/// </summary>
	~GraphicsEngine();

	/// <summary>
	/// Initializes with specified resolution.
	/// </summary>
	/// <param name="xres">The xres.</param>
	/// <param name="yres">The yres.</param>
	void Init(uint xres, uint yres);

	/// <summary>
	/// Releases this instance.
	/// </summary>
	void Release();

	/// <summary>
	/// Runs this instance.
	/// </summary>
	void Run();

	/// <summary>
	/// Sets if lighting is calculated.
	/// </summary>
	/// <param name="val">The value.</param>
	void Ligthing(bool val) { m_ligthing = val; }

	/// <summary>
	/// Gets if lighting si calculated.
	/// </summary>
	/// <returns>bool.</returns>
	bool Ligthing() { return m_ligthing; };


	
	struct alignas(16) UBOstruct {
		glm::mat4 BasicVP;
		glm::mat4 BasicP;
		glm::mat4 BasicV;
		glm::mat4 BasicInvVP;
		glm::mat4 BasicInvP;
		glm::mat4 BasicInvV;
		glm::mat4 CurrentVP;
		glm::mat4 CurrentP;
		glm::mat4 CurrentV;

		
		glm::vec4 Wind;
		glm::ivec4 ScreenDim;
		GLfloat Time;
		GLfloat CloudsTime;
		GLfloat CloudsIntensity;

	};


	
	struct alignas(16) UBOcsm {
		glm::mat4 csmVP[4];
		glm::mat4 csmCLOUDS[4];
	};

private:
	
	Shader* _program = nullptr;

	/// <summary>
	/// Renders this instance.
	/// </summary>
	void Render();

	/// <summary>
	/// Initilaizes SSAO objects.
	/// </summary>
	/// <param name="xres">The xres.</param>
	/// <param name="yres">The yres.</param>
	void SSAOInit(uint xres, uint yres);

	/// <summary>
	/// Initilaizes CSM objects.
	/// </summary>
	/// <param name="xres">The xres.</param>
	/// <param name="yres">The yres.</param>
	/// <param name="parts">The parts.</param>
	void CSMInit(uint xres, uint yres, int parts);

	/// <summary>
	/// Creates the CSM cameras.
	/// </summary>
	/// <param name="nearPlane">The near plane.</param>
	/// <param name="farPlane">The far plane.</param>
	/// <param name="parts">The parts.</param>
	/// <param name="base">The base.</param>
	/// <param name="name">The name.</param>
	void CreateCSMCameras(float nearPlane, float farPlane, int parts, int base, const std::string &name = "CSM");
	
	
	std::shared_ptr<GBuffer> m_GBuffer;
	std::unique_ptr<TerrainManager> m_terrainManager;
	std::unique_ptr<Sky> m_skyManager;
	std::unique_ptr<ForrestManager> m_forrestManager;
	std::unique_ptr<NPCManager> m_npcManager;
	std::unique_ptr<RBuffer> m_shadowFBO;
	std::unique_ptr<RBuffer> m_cloudsShaddowFBO;
	std::unique_ptr<RBuffer> m_deferredFBO;
	std::unique_ptr<RBuffer> m_ssaoFBO;
	std::unique_ptr<RBuffer> m_ssaoBlurFBO;
	std::unique_ptr<Shader> m_lightingShader;
	std::unique_ptr<Shader> m_ssaoShader;
	std::unique_ptr<Shader> m_ssaoBlurShader;

	
	GLuint m_ubo = 0;
	GLuint m_csmUBO = 0;
	GLuint m_lightingVAO = 0;

	glm::vec3 m_sunMinColor;
	glm::vec3 m_sunColorMultplier;
	float m_sunMinAmbient = 0;
	float m_sunAmbientMultiplier = 0;
	float m_fogIntensityMapper = 0;
	std::vector<glm::vec3> m_ssaoKernel;
	GLuint m_noiseTexture = 0;
	bool m_initialized = false;
	bool m_ligthing = true;



};

