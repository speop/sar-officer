// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 01-30-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <memory>
#include "imgui.h"
#include "WindowManager.h"
#include "UIElement.h"
#include "IconsFontAwesome.h"


static const ImWchar s_FontAwesomeRange[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
static const ImWchar s_latin[] = { 0x0020, 0x00FF, 0x0100,0x017F, 0 };

/// <summary>
/// Class UIManager.
/// </summary>
class UIManager
{
public:

	
	enum UIStates {
		StMainMenu = 0,
		StPauseMenu,
		StEditor,
		END_STOP
	};

	/// <summary>
	/// Initializes a new instance of the <see cref="UIManager"/> class.
	/// </summary>
	UIManager();
	/// <summary>
	/// Finalizes an instance of the <see cref="UIManager"/> class.
	/// </summary>
	~UIManager();

	/// <summary>
	/// Runs the specified state.
	/// </summary>
	/// <param name="state">The state.</param>
	/// <param name="mouseCursor">The mouse cursor.</param>
	void Run(UIStates state, bool mouseCursor = true);

	/// <summary>
	/// Initializes the specified w manager.
	/// </summary>
	/// <param name="wManager">The w manager.</param>
	void Init(WindowManager* wManager);

	/// <summary>
	/// Stops this instance.
	/// </summary>
	void Stop();

	/// <summary>
	/// Burns this instance.
	/// </summary>
	void Burn();

	/// <summary>
	/// Sets the font.
	/// </summary>
	/// <param name="font">The font.</param>
	/// <param name="size">The size.</param>
	void SetFont(std::string font, float size);

	/// <summary>
	/// Chekcs if mouse is the over GUI.
	/// </summary>
	/// <returns>bool.</returns>
	bool MouseOverGUI();

	/// <summary>
	/// Redraws this instance.
	/// </summary>
	static void Redraw();
	/// <summary>
	/// ImGUI renderer.
	/// </summary>
	/// <param name="draw_data">The draw data.</param>
	static void ImGuiRenderer(ImDrawData* draw_data);

	/// <summary>
	/// ImGUI scroll callback.
	/// </summary>
	/// <param name="">The .</param>
	/// <param name="xoffset">The xoffset.</param>
	/// <param name="yoffset">The yoffset.</param>
	static void ImGuiScrollCallback(GLFWwindow*, double xoffset, double yoffset);

private:
	/// <summary>
	/// ImGUI create device objects.
	/// </summary>
	void ImGuiCreateDeviceObjects();

	/// <summary>
	/// ImGUI create fonts texture.
	/// </summary>
	void ImGuiCreateFontsTexture();

	/// <summary>
	/// ImGUI invalidate device objects.
	/// </summary>
	void ImGuiInvalidateDeviceObjects();

	/// <summary>
	/// ImguI initialize.
	/// </summary>
	/// <param name="install_callbacks">The install callbacks.</param>
	void ImGuInit(bool install_callbacks);

	/// <summary>
	/// ImGUI shutdown.
	/// </summary>
	void ImGuiShutdown();

	/// <summary>
	/// ImGUI new frame.
	/// </summary>
	/// <param name="mouseCursor">The mouse cursor.</param>
	void ImGuiNewFrame(bool mouseCursor);

	static UIManager* m_instance;
	static UIStates m_lastState;

	WindowManager* m_wManager = nullptr;
	UIElement* m_actualUI = nullptr;
	UIElement* m_UIelements[END_STOP] = { nullptr };
	bool m_mouseCursor;
	bool m_mouseOverGui = false;
	
	
	GLuint m_FontTexture = 0;
	static int          s_ShaderHandle, s_AttribLocationTex, s_AttribLocationProjMtx;
	int m_VertHandle = 0, m_FragHandle = 0;
	int          m_AttribLocationPosition = 0, m_AttribLocationUV = 0, m_AttribLocationColor = 0;
	static unsigned int s_VboHandle, s_VaoHandle, s_ElementsHandle;
	static double s_currentTime;
	bool         m_MousePressed[3] = { false, false, false };
	static float        m_MouseWheel;
	
};

