#include "ModelLocation.h"
#include "Defines.h"



ModelLocation::ModelLocation()
{
}

ModelLocation::ModelLocation(glm::vec3 minCorner, glm::vec3 maxCorner, glm::mat4 position)
{
	m_minCorner = minCorner;
	m_maxCorner = maxCorner;
	m_position = position;
}

void ModelLocation::RecalcAABB(glm::vec3& min, glm::vec3& max, const glm::mat4& transform)
{
	auto bMin = min;
	auto bMax = max;
	auto maxFloat = std::numeric_limits<float>::max();
	min = glm::vec3(maxFloat);
	max = glm::vec3(-maxFloat);

	bool fx = true;
	bool fy = true;
	bool fz = true;	

	for (uint i = 0; i < 8; i++) {

		fz = !fz;
		if (i % 2 == 0) fy = !fy;
		if (i % 4 == 0) fx = !fx;

		auto z = fz ? bMin.z : bMax.z;
		auto y = fy ? bMin.y : bMax.y;
		auto x = fx ? bMin.x : bMax.x;

		auto transVec = transform * glm::vec4(x, y, z, 1.f);

		min.x = transVec.x < min.x ? transVec.x : min.x;
		min.y = transVec.y < min.y ? transVec.y : min.y;
		min.z = transVec.z < min.z ? transVec.z : min.z;

		max.x = transVec.x > max.x ? transVec.x : max.x;
		max.y = transVec.y > max.y ? transVec.y : max.y;
		max.z = transVec.z > max.z ? transVec.z : max.z;
	}
}

void ModelLocation::Create(const std::vector<glm::mat4> &transforms, std::vector<ModelLocation>& modelLocations, glm::vec3 minCorner, glm::vec3 maxCorner)
{
	for(const auto &mat : transforms) {
		ModelLocation mloc;
		mloc.m_minCorner = minCorner;
		mloc.m_maxCorner = maxCorner;
		mloc.m_position = mat;
		RecalcAABB(mloc.m_minCorner, mloc.m_maxCorner, mloc.m_position);
		modelLocations.push_back(mloc);
	}
}

void ModelLocation::Create(const std::vector<glm::mat4> &transforms, std::vector<ModelLocation>& modelLocations, glm::vec4 minCorner, glm::vec4 maxCorner)
{
	auto min = glm::vec3(minCorner.x, minCorner.y, minCorner.z);
	auto max = glm::vec3(maxCorner.x, maxCorner.y, maxCorner.z);
	Create(transforms, modelLocations, min, max);
}

void ModelLocation::RecalcAABB(const glm::mat4& transform)
{
	m_position = transform;
	RecalcAABB(m_minCorner, m_maxCorner, m_position);
}

glm::vec3 ModelLocation::GetMinCorner()
{
	return m_minCorner;
}

glm::vec3 ModelLocation::GetMaxCorner()
{
	return m_maxCorner;

}

glm::mat4 ModelLocation::GetPosition()
{
	return m_position;
}

glm::vec3 ModelLocation::GetMinCorner() const
{
	return m_minCorner;
}


glm::vec3 ModelLocation::GetMaxCorner() const
{
	return m_maxCorner;

}

glm::mat4 ModelLocation::GetPosition() const
{
	return m_position;
}



ModelLocation::~ModelLocation()
{
}
