#pragma once
#include <memory>
#include <vector>
#include <stack>
#include "glm\glm.hpp"
#include "Defines.h"
#include "Ray.h"

namespace SAR {

	template <typename T>
	class OctTree {

	public: class Record; //just forward declaration

	private:
		class Node : std::enable_shared_from_this<Node> {

		public:


			struct AxisBool {
				bool x;
				bool y;
				bool z;

				AxisBool(bool val)
				{
					x = val;
					y = val;
					z = val;
				}

				AxisBool()
				{
					x = false;
					y = false;
					z = false;
				}


				AxisBool& operator =(const AxisBool& a)
				{
					x = x && a.x;
					y = y && a.y;
					z = z && a.z;
					return *this;
				}

				bool Any(bool val)
				{
					return x == val || y == val || z == val;
				}

				bool operator==(const bool val)
				{
					return x == y == z == val;
				}
			};


			std::shared_ptr<Node> m_parent;
			std::vector<std::shared_ptr<typename OctTree<T>::Node::Record>> m_data;
			std::vector<std::shared_ptr<Record>> m_notMovableData;
			bool m_hasChildren = false;
			

			const uint childrenCount = 8;
			std::shared_ptr<Node> m_children[8];
			glm::vec3 m_minCorner;
			glm::vec3 m_maxCorner;

			static int IncX(int index) { return index + 4; }
			static int DecX(int index) { return index - 4; }
			static int IncY(int index) { return index + 2; }
			static int DecY(int index) { return index - 2; }
			static int IncZ(int index) { return index + 1; }
			static int DecZ(int index) { return index - 1; }


			/// <summary>
			/// Basic node constructor
			/// </summary>
			/// <param name="minCorner">left bottom  near corner of world part cube</param>
			/// <param name="maxCorner">right top far corner of world part cube</param>
			Node(const glm::vec3& minCorner, const glm::vec3& maxCorner, std::shared_ptr<Node> parent)
			{
				m_parent = parent;
				m_minCorner = minCorner;
				m_maxCorner = maxCorner;
			}


			/// <summary>
			/// Make children for current cube. This function subdived cube space into 8 smaller cubes. Each new cube is stored in m_childrens array and it is node with this smaller dimension and no children.
			/// </summary>
			void MakeChildren(std::shared_ptr<Node> parent)
			{
				glm::vec3 min;
				glm::vec3 max;

				m_hasChildren = true;

				for (uint i = 0; i < childrenCount; i++) {
					min.x = i & IncX(0) ? (m_minCorner.x + m_maxCorner.x)/2.f : m_minCorner.x;
					min.y = i & IncY(0) ? (m_minCorner.y + m_maxCorner.y)/2.f : m_minCorner.y;
					min.z = i & IncZ(0) ? (m_minCorner.z + m_maxCorner.z)/2.f : m_minCorner.z;

					max.x = i & IncX(0) ? m_maxCorner.x : (m_minCorner.x + m_maxCorner.x)/2.f;
					max.y = i & IncY(0) ? m_maxCorner.y : (m_minCorner.y + m_maxCorner.y)/2.f;
					max.z = i & IncZ(0) ? m_maxCorner.z : (m_minCorner.z + m_maxCorner.z)/2.f;

					m_children[i] = std::make_shared<Node>(min, max, parent);
				}
			}

			/// <summary>
			/// Find cube in which we can store data. Second pair element contains cube index if any is found otherwise  first index contains logical or of axis which are intersecting item's box.
			/// </summary>
			/// <param name="minCorner">left bottom  near corner of item cube</param>
			/// <param name="maxCorner">right top far corner of item cube</param>
			/// <returns>pair &lt; axis intersect, index &gt;</returns>
			std::pair<int, int> FindDataSlot(const glm::vec3& minCorner, const glm::vec3& maxCorner)
			{
				int notFitAxisFlag = 0;
				int fitIndex = 0;


				//x fit
				if (minCorner.x >= this->m_children[0]->m_minCorner.x && maxCorner.x <= this->m_children[0]->m_maxCorner.x) {
					fitIndex |= 0;
				}
				else if (minCorner.x >= this->m_children[IncX(0)]->m_minCorner.x && maxCorner.x <= this->m_children[IncX(0)]->m_maxCorner.x) {
					fitIndex |= IncX(0);
				}
				else {
					notFitAxisFlag |= IncX(0);
				}

				//y fit
				if (minCorner.y >= this->m_children[0]->m_minCorner.y && maxCorner.y <= this->m_children[0]->m_maxCorner.y) {
					fitIndex |= 0;
				}
				else if (minCorner.y >= this->m_children[IncY(0)]->m_minCorner.y && maxCorner.y <= this->m_children[IncY(0)]->m_maxCorner.y) {
					fitIndex |= IncY(0);
				}
				else {
					notFitAxisFlag |= IncY(0);
				}

				//z fit
				if (minCorner.z >= this->m_children[0]->m_minCorner.z && maxCorner.z <= this->m_children[0]->m_maxCorner.z) {
					fitIndex |= 0;
				}
				else if (minCorner.z >= this->m_children[IncZ(0)]->m_minCorner.z && maxCorner.z <= this->m_children[IncZ(0)]->m_maxCorner.z) {
					fitIndex |= IncZ(0);
				}
				else {
					notFitAxisFlag |= IncZ(0);
				}

				return std::make_pair(notFitAxisFlag, fitIndex);
			}

			/// <summary>
			/// Check if we can resize this node (move axis). If new axis positions will not intersect any stored item we can resize otherwise not. This means we can evaluate this only if we are working with leave nodes. Also if we hit max check depth we cannot resize.
			/// </summary>
			/// <param name="rootNode">Current working node (this)</param>
			/// <param name="resizeFlag">In which axis we want resize</param>
			/// <param name="resizeAmount">Amount of which we want resize (negative value means shrink)</param>
			/// <param name="currDepth">Current search depth</param>
			/// <param name="maxResizeCheckDepth"></param>
			/// <returns>True for axis which could be moved</returns>
			AxisBool CanResize(std::shared_ptr<Node> rootNode, int resizeFlag, const glm::vec3& resizeAmount, uint currDepth,uint maxResizeCheckDepth)
			{
				AxisBool canResize(true);
				glm::vec3 newMinPos = resizeAmount + rootNode->m_minCorner;
				glm::vec3 newMaxPox = resizeAmount + rootNode->m_maxCorner;

				// only the most bottom nodes contains data (we are ignoring notMovableData) so we check if resizing will not interfer with the datas
				if (!rootNode->m_hasChildren) {
					for (auto& element : rootNode->m_data) {

						//check for every axis flag if we can resize to specified dimension
						if (resizeFlag & IncX(0) && (resizeAmount.x > 0 && element->minCorner.x < newMinPos.x || resizeAmount.x < 0 && element->maxCorner.x > newMaxPox.x)) canResize.x = false;
						if (resizeFlag & IncY(0) && (resizeAmount.y > 0 && element->minCorner.y < newMinPos.y || resizeAmount.y < 0 && element->maxCorner.y > newMaxPox.y)) canResize.y = false;
						if (resizeFlag & IncZ(0) && (resizeAmount.z > 0 && element->minCorner.z < newMinPos.z || resizeAmount.z < 0 && element->maxCorner.z > newMaxPox.z)) canResize.z = false;
					}

				}
				else {
					if (currDepth == maxResizeCheckDepth) return AxisBool(false);

					//false because  0 % N will switch this values
					auto leftX = false;
					auto leftY = false;
					auto leftZ = false;

					for (uint i = 0; i < childrenCount; i++) {
						bool checking = false;

						if (!(i % IncX(0))) leftX = !leftX;
						if (!(i % IncY(0))) leftY = !leftY;
						if (!(i % IncZ(0))) leftZ = !leftZ;

						if (resizeFlag & IncX(0) && (resizeAmount.x < 0 && !leftX || resizeAmount.x > 0 && leftX)) checking = true;
						if (resizeFlag & IncY(0) && (resizeAmount.y < 0 && !leftY || resizeAmount.y > 0 && leftY)) checking = true;
						if (resizeFlag & IncZ(0) && (resizeAmount.z < 0 && !leftZ || resizeAmount.z > 0 && leftZ)) checking = true;

						if (!checking) continue;

						canResize = CanResize(rootNode->m_children[i], resizeFlag, resizeAmount, ++currDepth, maxResizeCheckDepth);
						if (canResize.Any(false)) break;

					}
				}

				return canResize;
			}

			/// <summary>
			/// Resize nodes. This function can be called only if we have previously called <see cref="CanResize"/>
			/// </summary>
			/// <param name="rootNode">Current working node</param>
			/// <param name="resizeFlag"></param>
			/// <param name="resizeAmount"></param>
			void Resize(std::shared_ptr<Node> rootNode, int resizeFlag, const glm::vec3& resizeAmount)
			{
				if (rootNode->m_hasChildren == false) return;

				// false because  0 % N will switch this values
				auto leftX = false;
				auto leftY = false;
				auto leftZ = false;

				for (uint i = 0; i < childrenCount; i++) {
					if (!(i % IncX(0))) leftX = !leftX;
					if (!(i % IncY(0))) leftY = !leftY;
					if (!(i % IncZ(0))) leftZ = !leftZ;

					if (resizeFlag & IncX(0)) {
						if (leftX) {
							rootNode->m_children[i]->m_maxCorner.x += resizeAmount.x;
						}
						else {
							rootNode->m_children[i]->m_minCorner.x += resizeAmount.x;
						}
					}

					if (resizeFlag & IncY(0)) {
						if (leftY) {
							rootNode->m_children[i]->m_maxCorner.y += resizeAmount.y;
						}
						else {
							rootNode->m_children[i]->m_minCorner.y += resizeAmount.y;
						}
					}

					if (resizeFlag & IncZ(0)) {
						if (leftZ) {
							rootNode->m_children[i]->m_maxCorner.z += resizeAmount.z;
						}
						else {
							rootNode->m_children[i]->m_minCorner.z += resizeAmount.z;
						}
					}

					bool resizing = false;
					if (resizeFlag & IncX(0) && (resizeAmount.x < 0 && !leftX || resizeAmount.x > 0 && leftX)) resizing = true;
					if (resizeFlag & IncY(0) && (resizeAmount.y < 0 && !leftY || resizeAmount.y > 0 && leftY)) resizing = true;
					if (resizeFlag & IncZ(0) && (resizeAmount.z < 0 && !leftZ || resizeAmount.z > 0 && leftZ)) resizing = true;
					if (!resizing) continue;

					Resize(rootNode->m_children[i], resizeFlag, resizeAmount);
				}
			}

			/// <summary>
			/// Add new data to tree.
			/// </summary>
			/// <param name="rootNode">Current working node (this)</param>
			/// <param name="data"></param>
			/// <param name="maxNodeItem">Max number of items in node</param>
			/// <param name="maxResizeSearchDepth">Max depth for search if resizing is needed</param>
			/// <param name="removeMode">True if we hit maxNodeItem and moving data to child nodes</param>
			void AddData(std::shared_ptr<Node> rootNode, std::shared_ptr<Record> data, uint maxNodeItem, uint maxResizeSearchDepth, bool removeMode = false)
			{
				auto node = rootNode;

				while (node != nullptr) {


					//we can store data in current node
					if (node->m_data.size() < maxNodeItem && !node->m_hasChildren) {
						data->node = node;
						node->m_data.push_back(data);
						break;
					}
					else if (!removeMode) {
						if (!node->m_hasChildren) {
							node->MakeChildren(node);
						}

						//moving data to child nodes
						for (auto& element : node->m_data) {
							AddData(node, element, maxNodeItem, maxResizeSearchDepth, true);
						}

						//emptying 
						std::vector<std::shared_ptr<Record>> dat;
						node->m_data.swap(dat);

					}

					//check if item can exactly fit in some node slot
					auto findFlags = node->FindDataSlot(data->minCorner, data->maxCorner);


					//we found branch where we should go
					if (findFlags.first == 0) {
						node = node->m_children[findFlags.second];
						continue;
					}


					//because node does not fit anywhere we check if we can resize some slot	
					glm::vec3 minusResize = node->m_children[0]->m_maxCorner;
					minusResize = data->minCorner - minusResize;

					glm::vec3 plusResize = node->m_children[0]->m_maxCorner;
					plusResize = data->maxCorner - plusResize;


					AxisBool plus(true);
					AxisBool minus(true);

					auto leftX = false;
					auto leftY = false;
					auto leftZ = false;

					for (uint i = 0; i < childrenCount; i++) {
						if (!(i % IncX(0))) leftX = !leftX;
						if (!(i % IncY(0))) leftY = !leftY;
						if (!(i % IncZ(0))) leftZ = !leftZ;

						glm::vec3 resizeVec;
						resizeVec.x = leftX ? minusResize.x : plusResize.x;
						resizeVec.y = leftY ? minusResize.y : plusResize.y;
						resizeVec.z = leftZ ? minusResize.z : plusResize.z;

						auto canResize = CanResize(node->m_children[i], findFlags.first, resizeVec, 0, maxResizeSearchDepth);

						if (leftX) minus.x = minus.x && canResize.x;
						else plus.x = plus.x && canResize.x;

						if (leftY) minus.y = minus.y && canResize.y;
						else plus.y = plus.y && canResize.y;

						if (leftZ) minus.z = minus.z && canResize.z;
						else plus.z = plus.z && canResize.z;

					}


					//resizing - always resize as smallest as possible
					glm::vec3 resizeVec;

					bool resize = true;
					if (findFlags.first & IncX(0)) {
						if (minus.x && plus.x) {
							resizeVec.x = abs(minusResize.x) < abs(plusResize.x) ? minusResize.x : plusResize.x;
						}
						else if (minus.x) resizeVec.x = minusResize.x;
						else if (plus.x) resizeVec.x = plusResize.x;
						else resize = false;
					}
					if (findFlags.first & IncY(0)) {
						if (minus.y && plus.y) {
							resizeVec.y = abs(minusResize.y) < abs(plusResize.y) ? minusResize.y : plusResize.y;
						}
						else if (minus.y) resizeVec.y = minusResize.y;
						else if (plus.y) resizeVec.y = plusResize.y;
						else resize = false;
					}
					if (findFlags.first & IncZ(0)) {
						if (minus.z && plus.z) {
							resizeVec.z = abs(minusResize.z) < abs(plusResize.z) ? minusResize.z : plusResize.z;
						}
						else if (minus.z) resizeVec.z = minusResize.z;
						else if (plus.z) resizeVec.z = plusResize.z;
						else resize = false;
					}

					if (resize) {
						Resize(node, findFlags.first, resizeVec);

						//find slot after resizing
						findFlags = node->FindDataSlot(data->minCorner, data->maxCorner);

						//we found branch where we should go
						if (findFlags.first == 0) {
							node = node->m_children[findFlags.second];
							continue;
						}
					}

					//item not fit and we can not resize anything => we are saving item in closet fitting parrent this mean this node
					data->node = node;
					data->notMovable = true;
					node->m_notMovableData.push_back(data);

					break;
				}
			}


			bool CheckDataFits(const glm::vec3 &minCorner, const glm::vec3 &maxCorner)
			{
				return  m_minCorner.x < minCorner.x && m_minCorner.y < minCorner.y && m_minCorner.z < minCorner.z
					&& m_maxCorner.x > maxCorner.x && m_maxCorner.y > maxCorner.y && m_maxCorner.z > maxCorner.z;
			}


			std::pair<float, std::shared_ptr<Record>> FindElement(std::shared_ptr<Node> rootNode, const Ray &ray)
			{
				std::pair<float, std::shared_ptr<Record>> object;
				std::shared_ptr<Node> node;
				std::stack<std::shared_ptr<Node>> nodeStack;
				nodeStack.push(rootNode);

				while(!nodeStack.empty()) {
					node = nodeStack.top();
					nodeStack.pop();

					if (node == nullptr) continue;

					//check if user clicked on some not movable data
					for(auto &nmData : node->m_notMovableData) {
						auto intersect = ray.IntersectAABB(nmData->minCorner, nmData->maxCorner);
						StoreFoundElement(object, intersect, nmData);
					}

					//now check all children.. we are performing DFS with leftiest way
					if (node->m_hasChildren) {
						for (auto i = childrenCount - 1; i >= 0 && i <childrenCount; i--) {
							auto pomNode = node->m_children[i];
							auto intersect = ray.IntersectAABB(pomNode->m_minCorner, pomNode->m_maxCorner);

							//we would work with node only if there is intersection or all objects are in front of currently found object
							if (!intersect.first || object.second != nullptr && object.first <= intersect.second) continue;
							nodeStack.push(pomNode);
						}
					}
					//node doesn't have any children -> we check its data
					else {
						for (auto &data : node->m_data) {
							auto intersect = ray.IntersectAABB(data->minCorner, data->maxCorner);
							StoreFoundElement(object, intersect, data);
						}
					}
				}

				return object;
			}
			

			void StoreFoundElement(std::pair<float, std::shared_ptr<Record>> &object, const std::pair<bool, float> &findResult, std::shared_ptr<Record> rec)
			{
				if (findResult.first == false) return;

				if (object.second == nullptr || object.first > findResult.second) {
					object.first = findResult.second;
					object.second = rec;
				}

			}
			
		};

		

		uint m_MaxDataInNode = 1;
		uint m_MinDataInNode = 1;
		uint m_maxResizeCheckDepth = 3;

		std::shared_ptr<Node> m_rootNode;
		std::shared_ptr<Node> m_cacheNode;


	public:
		class Record {
			friend OctTree;
			friend Node;
			bool notMovable = false;
			std::shared_ptr<Node> node;
			glm::vec3 minCorner;
			glm::vec3 maxCorner;

		public:
			T data;
		};


		OctTree(float max)
		{
			Construct(0.f, max, 0.f, max, 0.f, max);
		}

		OctTree(float min, float max)
		{
			Construct(min, max, min, max, min, max);
		}

		OctTree(float maxX, float maxY, float maxZ)
		{
			Construct(0.f, maxX, 0.f, maxY, 0.f, maxZ);
		}

		OctTree(float minX, float maxX, float minY, float maxY, float minZ, float maxZ)
		{
			Construct(minX, maxX, minY, maxY,  minZ, maxZ);
		}

		/// <summary>
		/// Add data to tree
		/// </summary>
		/// <param name="data">Our data</param>
		/// <param name="minCorner">Min corner of aabb</param>
		/// <param name="maxCorner">Max corner of aabb</param>
		std::shared_ptr<Record> AddData(T data, glm::vec3 minCorner, glm::vec3 maxCorner)
		{
			auto rec = std::make_shared<Record>();
			rec->minCorner = minCorner;
			rec->maxCorner = maxCorner;
			rec->data = data;

			m_rootNode->AddData(m_rootNode, rec, m_MaxDataInNode, m_maxResizeCheckDepth);
			return rec;
		}

		void UpdateDataPos(std::shared_ptr<Record> rec, const glm::vec3 &minCorner, const glm::vec3 &maxCorner)
		{
			bool fits = rec->node->CheckDataFits(minCorner, maxCorner);

			//update values
			rec->minCorner = minCorner;
			rec->maxCorner = maxCorner;

			//if data still fits to their node we don't have to do anything
			if (fits) return;

			//data not fits we are removing them 
			DeleteElement(rec);
		
			//and insert them as new data
			m_rootNode->AddData(m_rootNode, rec, m_MaxDataInNode, m_maxResizeCheckDepth);			
		}

		std::shared_ptr<Record> FindElement(const Ray &ray)
		{
			auto object = m_rootNode->FindElement(m_rootNode, ray);
			return object.second;
		}

		void DeleteElement(std::shared_ptr<Record> rec)
		{
			auto  ourDataVector = rec->node->m_hasChildren ? &(rec->node->m_notMovableData) : &(rec->node->m_data);	
						
			for (uint i = 0; i < ourDataVector->size(); i++) {
				auto pomRec = ourDataVector->at(i);

				if (pomRec == rec) {
					ourDataVector->erase(ourDataVector->begin() + i);
					break;
				}
			}			
		}

		~OctTree() { }


	private:
		/// <summary>
		/// Something like constructor. This function is called from all constructors and creates tree.
		/// </summary>
		/// <param name="minX"></param>
		/// <param name="maxX"></param>
		/// <param name="minY"></param>
		/// <param name="maxY"></param>
		/// <param name="minZ"></param>
		/// <param name="maxZ"></param>
		void Construct(float minX, float maxX, float minY, float maxY, float minZ, float maxZ)
		{
			auto minCorner = glm::vec3(minX, minY, minZ);
			auto maxCorner = glm::vec3(maxX, maxY, maxZ);

			m_rootNode = std::make_shared<Node>(minCorner, maxCorner, nullptr);

				m_rootNode->MakeChildren(m_rootNode);
		}
	};

}

