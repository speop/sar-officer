// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-16-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-16-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include "WindowManager.h"

/// <summary>
/// Class TimeManager.
/// </summary>
class TimeManager
{
public:
	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <returns>TimeManager *.</returns>
	static TimeManager* GetInstance();
	/// <summary>
	/// Runs this instance.
	/// </summary>
	void Run();

	/// <summary>
	/// Starts the measurement.
	/// </summary>
	void StartMeasurement();

	/// <summary>
	/// Stops the measurement.
	/// </summary>
	void StopMeasurement();

	/// <summary>
	/// Gets the delta time.
	/// </summary>
	/// <returns>float.</returns>
	float GetDeltaTime();

	/// <summary>
	/// Gets the day time.
	/// </summary>
	/// <returns>float.</returns>
	float GetDayTime();

	/// <summary>
	/// Gets the game time.
	/// </summary>
	/// <returns>float.</returns>
	float GetGameTime();

	/// <summary>
	/// Finalizes an instance of the <see cref="TimeManager"/> class.
	/// </summary>
	~TimeManager();
private:

	/// <summary>
	/// Prevents a default instance of the <see cref="TimeManager"/> class from being created.
	/// </summary>
	TimeManager();
private:
	static TimeManager* _instance;
	float startTime;
	float lastTime;
	float currentTime;
	float deltaTime;
	float m_gameTime;
	float m_dayTime;
	float m_fpsLastTime;
	int numFrames = 0;
	bool m_measuring = false;
	float m_minFps;
	float m_maxFps;
	float m_avgFps;
	int m_Fps;

};

