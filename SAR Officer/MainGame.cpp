#include <GL/glew.h>
#include "MainGame.h"
#include <thread>
#include "OctTree.h"
#include "SceneManager.h"
#include "Ray.h"
#include <chrono>

#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>
#include "Editor.h"
#include "AudioManager.h"


void MainGame::SaveScreenshot() {
	/*
	if (m_wManager->KeyPressed(GLFW_KEY_PRINT_SCREEN)) {


		auto width = m_wManager->GetWidth();
		auto height = m_wManager->GetWidth();

		BYTE* pixels = new BYTE[3 * width*height];
		glReadPixels(0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE, pixels);

		FIBITMAP* image = FreeImage_ConvertFromRawBits(pixels, width, height, 3 * width, 24, 0x0000FF, 0xFF0000, 0x00FF00, false);
		FreeImage_Save(FIF_BMP, image, "test.bmp", 0);

		delete[] pixels;
		
	}//*/
}

/// <summary>
/// The instance{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
MainGame* MainGame::instance = nullptr;

MainGame * MainGame::Get()
{
	if(instance ==  nullptr) instance = new MainGame();
	return instance;
}

MainGame::MainGame()
{
}


MainGame::~MainGame()
{
}

void MainGame::Start()
{	
		
	if (m_started) return;
	m_started = true;
	auto logger = Logger::GetLogger();
	//this is the first place where config is load or tried to be load.. config is singleton so here we realize if config file exists and if its format is ok
	ConfigStore* config;
	try {
		config = ConfigStore::Instance();
	}
	catch (ConfigCoruptedException &ex) {
		logger->Log(Logger::Critical, "Config loader exception, "+ex.GetMsg());
		throw BasicException("Corrupted config file, please reinstall game.");
	}

	m_wManager = new WindowManager(config->GetXRes(), config->GetYRes(), config->GetFullscreen());
	m_wManager->Init(); 

	try {
		KeyboardLayoutManager::GetInstance(m_wManager);
	}
	catch (ConfigCoruptedException &ex) {
		logger->Log(Logger::Critical, "Config loader exception, " + ex.GetMsg());
		throw BasicException("Corrupted config file, please reinstall game.");
	}


	m_camera = PerspectiveCamera::GetCamera("Basic", m_wManager);

	m_gEngine = new GraphicsEngine();		

	m_uiManager = std::make_shared<UIManager>();
	m_uiManager->Init(m_wManager);

	m_previousGameState = m_gameState = GameState::MENU;
	

	Drive();

}


void MainGame::Drive()
{
	
	
	glEnable(GL_DEBUG_OUTPUT);	
	
	while (true)
	{
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		auto tManager = TimeManager::GetInstance();
		auto camera = PerspectiveCamera::GetCamera("Basic", m_wManager);
		
		tManager->Run();		
		ManageInput();
		
		//shuting down the system... last one, please turn off lights
		if (m_gameState == GameState::EXIT) {
			delete m_wManager;
			break;
		}
		

		int xres, yres;
		auto window = this->m_wManager->GetWindow();

		/*
		 * Main game loop
		 *
		 */
		switch (m_gameState)
		{
		case GameState::FOCUS_OUT:
			break;
		
		case GameState::LOADING:
			UIManager::Redraw();
			Editor::Reset();
			AudioManager::Get()->MainMenu(false);
			m_levelLoaded = true;
			SceneManager::Get()->MakeScene();
			glfwGetWindowSize(window, &xres, &yres);
			m_gEngine->Init(static_cast<uint>(xres), static_cast<uint>(yres));
			m_uiManager->Stop();
			
			glfwSetCursorPos(window, xres / 2.0, yres / 2.0);
			camera->ProcessMove(0, 0);
			m_gameState = m_afterLoadGameState;
			m_gEngine->Ligthing(true);
			break;

		case GameState::PLAY:
			if (!m_levelLoaded) break;			
			m_player->ProcessMove({ m_movementVector.x, m_movementVector.z });
			camera->Recalc();
			AudioManager::Get()->UpdatePlayerPos(camera->GetPosition(), camera->GetLook())->Game(true);
			m_gEngine->Run();
			break;

		case GameState::EDITOR:
			if (!m_levelLoaded) break;
			camera->Move(m_movementVector);
			camera->Recalc();
			m_gEngine->Run();
			m_uiManager->Run(UIManager::StEditor);
			break;

		case GameState::PAUSE:
			m_gEngine->Run();
			m_uiManager->Run(UIManager::StPauseMenu);
			break;

		case GameState::MENU:
			m_uiManager->Run(UIManager::StMainMenu);
			break;
		
		default:
			break;
		}
		if (m_wManager->KeyPressed(GLFW_KEY_PRINT_SCREEN)) {
			SaveScreenshot();
		}
		
		m_wManager->Run();

	
		// check OpenGL error
		GLenum err;
		while ((err = glGetError()) != GL_NO_ERROR) {
			std::cerr << "OpenGL error: " << err << std::endl;
		}
		
	}
	
	//end is here.. we spent lot of time together.. so saying farewell will be tough for both of us. But we both knew that this moment will come.. don't be affraid to cry before me.. Goodbye my beloved one...
	//after this bracket comes endless nothing
}

void MainGame::ManageInput()
{
	auto camera = PerspectiveCamera::GetCameraInstance("Basic");
	auto window = this->m_wManager->GetWindow();
	bool mouseCursor = true;
	
	
	if(m_wManager->WindowClosed()) {
		m_gameState = GameState::EXIT;
	}

	if (m_wManager->KeyPressed(GLFW_KEY_ESCAPE)) {	
		switch(m_gameState) {
		case GameState::PLAY:
			m_gameState = GameState::PAUSE;
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

		}
		return;
	}

	if (!this->m_wManager->Focus()) {
		if (m_gameState != GameState::FOCUS_OUT) m_previousGameState = m_gameState;
		this->m_gameState = GameState::FOCUS_OUT;
		return;
	 }
	else {
		if (m_gameState == GameState::FOCUS_OUT) m_gameState = m_previousGameState;
	}



	// Get mouse position
	double xMousePos, yMousePos;
	glfwGetCursorPos(window, &xMousePos, &yMousePos);
	
	auto tManager = TimeManager::GetInstance();
	auto deltaTime = tManager->GetDeltaTime();
	//Window Resolution
	int xres, yres;
	// Reset mouse position for next frame
	glfwGetWindowSize(window, &xres, &yres);

	//checking if user wants to turn camera
	//in game cursor is hiden and mouse centered so every mous move turns camera
	if (m_gameState == GameState::PLAY) {
		mouseCursor = false;
		
		glfwSetCursorPos(window, xres / 2.0, yres / 2.0);
	
		// Compute new orientation
		auto horizontalAngle =  float(xres / 2.0 - xMousePos);
		auto verticalAngle =  float(yres / 2.0 - yMousePos);
		camera->ProcessMove(horizontalAngle, verticalAngle);
	}

	//in editor mode camer can move only if user hold right mouse button. And camera moves only by delta from previous position
	else if(m_gameState == GameState::EDITOR && (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)) {
		mouseCursor = false;
		// Compute new orientation
		auto xOffset = m_mousePos.x - xres / 2.0;
		auto yOffset = m_mousePos.y - yres / 2.0;		
		xMousePos = xMousePos - xOffset;
		yMousePos = yMousePos - yOffset;

		
		//move camera
		auto horizontalAngle = float(xres / 2.0 -  xMousePos);
		auto verticalAngle = float(yres / 2.0 -  yMousePos);
		camera->ProcessMove(horizontalAngle, verticalAngle);
		glfwSetCursorPos(window, m_mousePos.x, m_mousePos.y);
	}
	else if (m_gameState == GameState::EDITOR && (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)) {
		m_mousePos.x = static_cast<float>(xMousePos);
		m_mousePos.y = static_cast<float>(yMousePos);
		mouseCursor = true;

		//we procced click to editor only if click event wasn't triggered for some gui element
		if (!m_uiManager->MouseOverGUI()) {
			auto ray = SAR::Ray(m_camera, m_mousePos.x, m_mousePos.y);
			ray.Print();
			Editor::Get()->ProcessRay(ray);
		}
	}
	//camera does not move, we store mouse coords
	else {
		m_mousePos.x = static_cast<float>(xMousePos);
		m_mousePos.y = static_cast<float>(yMousePos);
		mouseCursor = true;
	}
	
	auto kblManager = KeyboardLayoutManager::GetInstance(this->m_wManager);
	
	m_movementVector = { 0,0,0 };

	//jump
	if (kblManager->DirectionKeyPressed(KeyboardLayoutManager::JUMP) && m_gameState == GameState::PLAY) {
		m_player->Jump();
	}

	// Move forward
	if (kblManager->DirectionKeyPressed(KeyboardLayoutManager::FORWARD)) {
		m_movementVector += camera->ProcessMove(PerspectiveCamera::MoveEnum::UP);
	}
	// Move backward
	if (kblManager->DirectionKeyPressed(KeyboardLayoutManager::BACKWARD)) {
		m_movementVector += camera->ProcessMove(PerspectiveCamera::MoveEnum::DOWN);
	}
	// Strafe right
	if (kblManager->DirectionKeyPressed(KeyboardLayoutManager::RIGHT)) {
		m_movementVector += camera->ProcessMove(PerspectiveCamera::MoveEnum::RIGHT);
	}
	// Strafe left
	if (kblManager->DirectionKeyPressed(KeyboardLayoutManager::LEFT)) {
		m_movementVector += camera->ProcessMove(PerspectiveCamera::MoveEnum::LEFT);
	}

	//cursor
	if(mouseCursor != m_mouseCursor) {
		m_mouseCursor = mouseCursor;
		if (m_mouseCursor) glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		else glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);		
	}

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) TimeManager::GetInstance()->StartMeasurement();
	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) TimeManager::GetInstance()->StopMeasurement();
}

void MainGame::Profile(std::string tag)
{
	t2 = std::chrono::high_resolution_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	
//	std::cout << tag << ": " <<duration << std::endl;;
	t1 = std::chrono::high_resolution_clock::now();
}


GameState MainGame::State()
{
	return m_gameState;
}

void MainGame::LoadLevel(std::string level)
{
	m_player = std::make_unique<GO::Player>(PerspectiveCamera::GetCamera("Basic", m_wManager));
	LoadLevel(level, GameState::PLAY);
}

void MainGame::Exit()
{
	m_gameState = GameState::EXIT;
}

void MainGame::DrawLighting(bool value)
{
	m_drawLighting = value;
	m_gEngine->Ligthing(value);
}

void MainGame::Continue()
{
	int xres, yres;
	auto window = this->m_wManager->GetWindow();

	glfwGetWindowSize(window, &xres, &yres);
	glfwSetCursorPos(window, xres / 2.0, yres / 2.0);

	m_gameState = GameState::PLAY;
}

void MainGame::Menu()
{
	SceneManager::Get()->DeleteScene();
	m_gEngine->Release();
	m_gameState = GameState::MENU;
	AudioManager::Get()->Game(false);
}

float MainGame::LoadingProgress()
{
	return m_loadingProgress;
}

void MainGame::LoadingProgress(float value)
{
	m_loadingProgress = value;
}

bool MainGame::Loaded()
{
	return m_levelLoaded;
}

void  MainGame::LoadingProgressStackErase()
{
	std::stack<float> st;
	m_loadingProgressIncrement.swap(st);
}


void MainGame::LoadingProgressStackInc()
{
	auto val = 1.f;

	if (!m_loadingProgressIncrement.empty()) {
		val = m_loadingProgressIncrement.top();
	}
	m_loadingProgress += val;
	UIManager::Redraw();
}


void MainGame::LoadingProgressStackPushV(float value)
{
	m_loadingProgressIncrement.push(value);
}

void MainGame::LoadingProgressStackPushP(float percentage)
{
	auto base = 1.f;

	if (!m_loadingProgressIncrement.empty()) {
		base = m_loadingProgressIncrement.top();
	}

	percentage = base * percentage;
	m_loadingProgressIncrement.push(percentage);
}

void MainGame::LoadingProgressStackPop()
{
	if (m_loadingProgressIncrement.empty()) return;
	m_loadingProgressIncrement.pop();
}

std::vector<ConfigStore::ResolutionRec> MainGame::GetVideoModes()
{
	std::vector<ConfigStore::ResolutionRec> modesV;
	int count;
	const GLFWvidmode* modes = glfwGetVideoModes(glfwGetPrimaryMonitor(), &count);

	for(auto i =0; i <count; i++) {
		ConfigStore::ResolutionRec rec;
		rec.x = modes[i].width;
		rec.y = modes[i].height;
		modesV.push_back(rec);
	}
	
	return modesV;
}

void MainGame::SetResolution(ConfigStore::ResolutionRec res)
{
	m_wManager->SetResolution(res);
}

int MainGame::GetSafeKey()
{
	int key = m_wManager->GetLastKey();
	
	return key;
}

HWND MainGame::GetHWND()
{
	return  glfwGetWin32Window(m_wManager->GetWindow());
}

void MainGame::LoadEditor()
{
	LoadLevel("", GameState::EDITOR);
}

void MainGame::LoadLevel(std::string level, GameState afterLoadState)
{
	LoadingProgressStackErase();
	m_loadingProgress = 0;

	m_gameState = GameState::LOADING;
	m_afterLoadGameState = afterLoadState;
}

glm::vec2 MainGame::GetMouse()
{
	return m_mousePos;
}

