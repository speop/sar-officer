#include "KeyboardLayoutManager.h"
#include "Logger.h"
KeyboardLayoutManager* KeyboardLayoutManager::instance = nullptr;


KeyboardLayoutManager::KeyboardLayoutManager(WindowManager* wm)
{
	wManager = wm;
	m_currentConfig = "../resources/keyboard.cfg";
	pugi::xml_parse_result result = m_configData.load_file(m_currentConfig.c_str());

	if (result.status != pugi::xml_parse_status::status_ok) {
		Logger::GetLogger()->Log(Logger::Critical, "Error code: " + std::to_string(result.status) + ", message: " + result.description());
		throw ConfigCoruptedException("Keyboard layout config not found. Please reinstall game to fix this issue.");
	}

	m_rootNode = m_configData.child("Keyboard");

	m_keys[FORWARD] = m_rootNode.child("Forward").attribute("val").as_int();
	m_keys[BACKWARD] = m_rootNode.child("Backward").attribute("val").as_int();
	m_keys[LEFT] = m_rootNode.child("Left").attribute("val").as_int();
	m_keys[RIGHT] = m_rootNode.child("Right").attribute("val").as_int();
	m_keys[JUMP] = m_rootNode.child("Jump").attribute("val").as_int();
}


KeyboardLayoutManager::~KeyboardLayoutManager()
{
}

KeyboardLayoutManager* KeyboardLayoutManager::GetInstance(WindowManager * wm)
{
	if(instance == nullptr) instance = new KeyboardLayoutManager(wm);
	return instance;
}
KeyboardLayoutManager * KeyboardLayoutManager::Instance()
{
	return instance;
}

bool KeyboardLayoutManager::DirectionKeyPressed(Events dir)
{
	bool retValue = false;
	//auto window = wManager->GetWindow();

	switch (dir)
	{
	case KeyboardLayoutManager::FORWARD:
		retValue =  wManager->KeyPressedMulitple(m_keys[FORWARD]);
		break;
	case KeyboardLayoutManager::BACKWARD:
		retValue = wManager->KeyPressedMulitple(m_keys[BACKWARD]);
		break;
	case KeyboardLayoutManager::LEFT:
		retValue = wManager->KeyPressedMulitple(m_keys[LEFT]);
		break;
	case KeyboardLayoutManager::RIGHT:
		retValue = wManager->KeyPressedMulitple(m_keys[RIGHT]);
		break;
	case KeyboardLayoutManager::JUMP:
		retValue = wManager->KeyPressedMulitple(m_keys[JUMP]);
		break;
	default:
		break;
	}

	return retValue;
}

int * KeyboardLayoutManager::GetKeys()
{
	int* keys = new int[STOP_END];
	for(int i = 0; i < STOP_END; i++) {
		keys[i] = m_keys[i];
	}
	
	return keys;
}


void KeyboardLayoutManager::SaveKeys(int* keys)
{
	m_rootNode.child("Forward").attribute("val").set_value(keys[FORWARD]); 
	m_rootNode.child("Backward").attribute("val").set_value(keys[BACKWARD]);
	m_rootNode.child("Left").attribute("val").set_value(keys[LEFT]);
	m_rootNode.child("Right").attribute("val").set_value(keys[RIGHT]);
	m_rootNode.child("Jump").attribute("val").set_value(keys[JUMP]);

	m_configData.save_file(m_currentConfig.c_str());
}