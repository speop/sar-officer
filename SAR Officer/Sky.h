// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 11-11-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-05-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once

#include "GraphicElement.h"
#include "Camera.h"
#include "TimeManager.h"



/// <summary>
/// Class Sky.
/// </summary>
/// <seealso cref="GraphicElement" />
class Sky :
	public GraphicElement
{
public:
	/// <summary>
	/// Initializes this instance.
	/// </summary>
	virtual void Init();
	
	/// <summary>
	/// Runs and renders sky with default CLASSIC parameter.
	/// </summary>
	void Run() override { Run(CLASSIC); };
	
	/// <summary>
	/// Runs the specified render pass.
	/// </summary>
	/// <param name="renderPass">The render pass.</param>
	void Run(RenderPass renderPass);

	

#pragma region Getters and setters
	
	/// <summary>
	/// Set suns the positon.
	/// </summary>
	/// <param name="position">The position.</param>
	void SunPositon(glm::vec3 position);
	
	/// <summary>
	/// Getposition of sun
	/// </summary>
	/// <returns>glm.vec3.</returns>
	glm::vec3 SunPosition();
#pragma endregion 
	
	/// <summary>
	/// Initializes a new instance of the <see cref="Sky"/> class.
	/// </summary>
	Sky();
	/// <summary>
	/// Finalizes an instance of the <see cref="Sky"/> class.
	/// </summary>
	~Sky();

private:

	/// <summary>
	/// Updates the sun position.
	/// </summary>
	void UpdateSun();

	
	glm::vec3 m_sunPosition;
	std::shared_ptr<Shader> m_clouds;
	std::unique_ptr<Shader> m_cloudsShadow;
	float m_sunAngle = 0;
	const float c_sunIncPerDaySec = 0.003125f;
	const float c_sunRise = 18000.f;
	const float c_sunset = 75600.f;


};

