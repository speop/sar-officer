#include "AudioManager.h"
#include "ConfigStore.h"
#include <string>
#include "Model.h"
#include <complex.h>
#include <ctime>
#include "TimeManager.h"

/// <summary>
/// The instance{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
AudioManager* AudioManager::instance = nullptr;

AudioManager * AudioManager::Get()
{
	if(instance== nullptr) instance = new AudioManager();
	return instance;	
}

AudioManager::AudioManager()
{
	m_sEngine = irrklang::createIrrKlangDevice();
	m_gameFxs = ConfigStore::Instance()->ConfigStore::Instance()->GetSounds("Game", "FX");
	m_random = std::mt19937(time(nullptr));
	m_fxDist = std::uniform_int_distribution<>(0, m_gameFxs.size()-1);
}



AudioManager::~AudioManager()
{
	m_sEngine->drop();
}


void AudioManager::MainMenu(bool play)
{
	auto menuP = Model::MapFind(m_playingMAP, std::string("mainMenu"));
	if(play == *menuP) return;

	*menuP = play;	

	if(!play && m_mainMenuSound) {
		m_mainMenuSound->stop();
		m_mainMenuSound->drop();
	}
	else {
		std::string menuSound = ConfigStore::Instance()->GetMainMenuSound();
		m_mainMenuSound = m_sEngine->play2D(menuSound.c_str(),true,false, true);
		m_mainMenuSound->setVolume(ConfigStore::Instance()->GetMusicLevel() *0.01f);
	}
}

void AudioManager::Game(bool play)
{
	GameAmbient(play);
	if (!play) return;
	GameFX();
}

AudioManager* AudioManager::UpdatePlayerPos(const glm::vec3 &pos, const glm::vec3 &lookDir)
{
	m_sEngine->setListenerPosition({ pos.x,pos.y,pos.z }, { lookDir.x, lookDir.y, lookDir.z });
	return this;
}


void AudioManager::GameAmbient(bool play)
{
	auto gameP = Model::MapFind(m_playingMAP, std::string("gameAmbient"));
	if (play == *gameP) return;

	*gameP = play;

	if (!play) {
		while(!m_ambient.empty()) {
			auto sound = m_ambient.back();
			m_ambient.pop_back();
			sound->stop();
			sound->drop();
		}
	
	
	}
	else {
		auto ambientSounds = ConfigStore::Instance()->GetSounds("Game", "Ambient");
		auto volume = ConfigStore::Instance()->GetAudioFXlevel();
		auto audioVolume = ConfigStore::Instance()->GetMusicLevel();

		for(const auto &sound : ambientSounds) {
			auto sInterface = m_sEngine->play2D(sound.path.c_str(), true, false, true);
			if(!sInterface) continue;
			auto vol = sound.name == "music" ? audioVolume : volume;
			sInterface->setVolume(vol *0.01f*sound.volume);
			m_ambient.push_back(sInterface);
		}
	}
}

void AudioManager::GameFX()
{
	auto time = TimeManager::GetInstance()->GetGameTime();
	if(!m_scheduledPlay) {
		m_nextFxTime = m_timeDist(m_random) + time;
		m_scheduledPlay = true;
		return;
	}
	
	
	if (m_nextFxTime > time) return;

	m_scheduledPlay = false;
	auto index = m_fxDist(m_random);

	std::uniform_real_distribution<float> dist1(-10.f, 10.f);
	std::uniform_real_distribution<float> dist0(0.f, 5.f);

	irrklang::vec3df fxPos(dist1(m_random), dist0(m_random), dist1(m_random));
	
	auto  inter = m_sEngine->play3D(m_gameFxs[index].path.c_str(), fxPos, false, false, true);
	inter->setVolume(ConfigStore::Instance()->GetAudioFXlevel() *0.01f*m_gameFxs[index].volume);

}
