#include "Mesh.h"
#include <limits>



Mesh::Mesh()
{
	
}


Mesh::~Mesh()
{
	
	glDeleteVertexArrays(1, &m_VAO);
	glDeleteBuffers(1, &m_VBO);
	glDeleteBuffers(1, &m_EBO);
	
}

Mesh::Mesh(const std::vector<Vertex> &vertices,const std::vector<GLuint> &indices)
{
	m_vertices = std::make_shared<std::vector<Vertex>>();
	m_indices = std::make_shared<std::vector<GLuint>>();

	m_vertices->insert(m_vertices->end(), vertices.begin(), vertices.end());
	m_indices->insert(m_indices->end(), indices.begin(), indices.end());
	
	
	this->m_baseMatrix = glm::mat4(1.0);
	
}

Mesh * Mesh::SetupBaseMatrix(glm::mat4 matrix)
{
	m_baseMatrix *= matrix;
	return this;
}

void Mesh::Draw(std::shared_ptr<Shader> shader, bool instanced)
{
	::Texture *text = new ::Texture(shader);
	
	text->Use(0, m_textureDiffuse);
	text->Use(1, m_textureSpecular);
	text->Use(2,  m_textureNormal);
	
	glBindVertexArray(m_VAO);
	shader->SetMat4f("BaseMat", 1, GL_FALSE, glm::value_ptr(m_baseMatrix));
	glUniform1f(7, m_alphaDiscard);
	glUniform1i(8, m_drawId);
	glUniform1ui(9, m_additionalData);

  
	
	glDrawElementsInstanced(GL_TRIANGLES, static_cast<GLsizei>(m_indices->size()), GL_UNSIGNED_INT, nullptr, m_positions.size());
	

	glBindVertexArray(0);
	delete(text);
}

void Mesh::SetupMesh()
{
	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);


	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);	
	glBufferData(GL_ARRAY_BUFFER, m_vertices->size() * sizeof(Vertex), m_vertices->data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices->size() * sizeof(GLuint), m_indices->data(), GL_STATIC_DRAW);

	//Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
	glVertexAttribDivisor(0, 0);

	//Vertex normals 
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));
	glVertexAttribDivisor(1, 0);

	//Vertex tangents
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Tangent));
	glVertexAttribDivisor(2, 0);

	//Texture coords
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));
	glVertexAttribDivisor(3, 0);

  
	//and unbind
	glBindVertexArray(0);
}

void Mesh::GenerateInstanceBO()
{
	glBindVertexArray(m_VAO);
	glGenBuffers(1, &m_instanceVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_instanceVBO);
	

	//seting up transformation
	for (auto i = 0; i < 4; i++) {
		glEnableVertexAttribArray(4 + i);
		glVertexAttribPointer(4 + i, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(glm::vec4), (GLvoid*)(i * sizeof(glm::vec4)));
		glVertexAttribDivisor(4 + i, 1);
	}

	glBindVertexArray(0);
  
}


std::vector<std::shared_ptr<int>> Mesh::AddInstances(const std::vector<glm::mat4>& trans)
{

	int count = static_cast<int>(trans.size());
	int current = static_cast<int>(m_positions.size());
  
	if (count < 0) throw FatalErrorException("Mesh int casting error");
	else if (count == 0) return std::vector<std::shared_ptr<int>>();
  
	if (m_positions.size() + trans.size() > (std::numeric_limits<int>::max)()) throw FatalErrorException("Int out of range");
	int ptrVal = static_cast<int>(m_positions.size());
  
	for(const auto &mat: trans) {
		m_positions.push_back(mat);
		auto ptr = std::make_shared<int>(ptrVal++);
		m_dataPtrs.push_back(ptr);
	}

	
	if (m_instanceVBO == 0) {
		GenerateInstanceBO();
	}
  
	glBindBuffer(GL_ARRAY_BUFFER, m_instanceVBO);
  
	if (m_IBOsize < m_positions.size())
	{
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * m_positions.size(), &m_positions[0], GL_STATIC_DRAW);
		m_IBOsize = static_cast<int>(m_positions.size());
	}
	else {
		glBufferSubData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * current, sizeof(glm::mat4) *count, &trans[0]);
	}
	
	
	return std::vector<std::shared_ptr<int>>(m_dataPtrs.begin() + current, m_dataPtrs.end());
}

Mesh *  Mesh::UpdatePosition(std::shared_ptr<int> dataPtr, const glm::mat4& position)
{
	auto index = *dataPtr;
	if (index < 0 || index >= m_positions.size()) return this;

	auto trans  = m_positions[index] * position;
	m_positions[index] = trans;

	glm::mat4* vboPtr;

	glBindBuffer(GL_ARRAY_BUFFER, m_instanceVBO);
	vboPtr = (glm::mat4*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	
	vboPtr[index] = trans;

	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return this;
}

Mesh* Mesh::DeletePosition(std::shared_ptr<int> dataPtr)
{
	auto index = *dataPtr;
	if (index < 0 || index >= m_positions.size()) return this;

	m_positions.erase(m_positions.begin() + index);
	m_dataPtrs.erase(m_dataPtrs.begin() + index);

	for(int i = 0; i < static_cast<int>(m_dataPtrs.size()); i++) {
		*(m_dataPtrs[i]) = i;
	}

	if (m_positions.size() < 1) return this;
	m_drawId = -1;

	glBindBuffer(GL_ARRAY_BUFFER, m_instanceVBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(glm::mat4) * m_positions.size(), &m_positions[0]);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return this;
}

Mesh* Mesh::SetAlphaDiscard(float val)
{
	m_alphaDiscard = val; 
	return this;
}

Mesh* Mesh::SetAditionalData(std::shared_ptr<int> dataPtr, uint val)
{
	m_drawId = *dataPtr;
	m_additionalData = val;
	return this;
}

glm::mat4 Mesh::GetPosition(std::shared_ptr<int> dataPtr)
{
	auto index = *dataPtr;
	if (index < 0 || index >= m_positions.size()) return glm::mat4(1.0f);
	return m_positions.at(index);
}

std::shared_ptr<const std::vector<Mesh::Vertex>> Mesh::GetVertices()
{
	return m_vertices;
}

std::shared_ptr<const std::vector<GLuint>> Mesh::GetIndices()
{
	return m_indices;
}

Mesh * Mesh::SetTexture(GLuint id, TextTypes type)
{
	switch(type) {

	case TextTypes::Specular:
		m_textureSpecular = id;
		break;

	case TextTypes::Diffuse:
		m_textureDiffuse = id;
		break;

	case TextTypes::Normal:
		m_textureNormal = id;
		break;
	}
	return this;
}

glm::mat4 Mesh::GetTransformation()
{
	return m_baseMatrix;
}