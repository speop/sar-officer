// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 03-11-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include "Defines.h"
#include <GL/glew.h>
#include "Exceptions.h"

/// <summary>
/// Class RBuffer.
/// </summary>
class RBuffer
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="RBuffer"/> class.
	/// </summary>
	RBuffer();
	/// <summary>
	/// Finalizes an instance of the <see cref="RBuffer"/> class.
	/// </summary>
	~RBuffer();

	/// <summary>
	/// Initializes with specified dimension. If lite is true, there will be only one color output.
	/// </summary>
	/// <param name="width">The width.</param>
	/// <param name="height">The height.</param>
	/// <param name="lite">The lite.</param>
	void Init(uint width, uint height, bool lite = false);

	/// <summary>
	/// Initializes the layered FBO.
	/// </summary>
	/// <param name="texture">The texture.</param>
	/// <param name="width">The width.</param>
	/// <param name="height">The height.</param>
	/// <param name="layers">The layers.</param>
	void InitLayered(uint texture, uint width, uint height, uint layers);

	/// <summary>
	/// Initializes with the specified texture identifier.
	/// </summary>
	/// <param name="textureID">The texture identifier.</param>
	/// <param name="width">The width.</param>
	/// <param name="height">The height.</param>
	void Init(GLuint textureID, uint width, uint height);

	/// <summary>
	/// Binds for writing.
	/// </summary>
	void BindForWriting();

	/// <summary>
	/// Binds for reading.
	/// </summary>
	/// <param name="firstTextureUnit">The first texture unit.</param>
	void BindForReading(uint firstTextureUnit = 0);

	/// <summary>
	/// Binds the depth for reading.
	/// </summary>
	/// <param name="firstTextureUnit">The first texture unit.</param>
	void BindDepthForReading(uint firstTextureUnit = 0);
private:

	/// <summary>
	/// Finalizes the fbo.
	/// </summary>
	/// <param name="width">The width.</param>
	/// <param name="height">The height.</param>
	void FinalizeFBO(uint width, uint height);

	/// <summary>
	/// Generates the textures.
	/// </summary>
	/// <param name="width">The width.</param>
	/// <param name="height">The height.</param>
	void GenerateTextures(uint width, uint height);

	
	GLuint m_fbo = 0;
	GLuint m_basicTexture = 0;
	GLuint m_normalTexture = 0;
	GLuint m_depthTexture = 0;
	GLuint m_positionTexture = 0;
	GLuint m_idTexture = 0;

	GLuint* m_depthTextureViews = nullptr;
	int m_layers = 0;

	bool m_lite = false;
	bool m_layered = false;
};

#pragma region exceptions
/// <summary>
/// Class RBufferException.
/// </summary>
/// <seealso cref="BasicException" />
class RBufferException : public BasicException {
	/// <summary>
	/// Initializes a new instance of the <see cref="RBufferException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
public: RBufferException(std::string msg) : BasicException(msg) {}
};
#pragma endregion 



