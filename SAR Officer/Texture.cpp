#include "Texture.h"
#include <fstream>


Texture::Texture(std::shared_ptr<Shader> program)
{
	this->m_shader = program;
}

Texture * Texture::Load(uint textureUnit, const std::vector<std::string> &filePaths,  bool managed)
{
	//number of first texture unit
	this->m_textureUnit = textureUnit;
	uint textureID;
  m_managed = managed;
	for (auto itFilepath = filePaths.begin(); itFilepath != filePaths.end(); itFilepath++)
	{

		auto fimg = new fipImage();
		bool loaded = fimg->load((*itFilepath).c_str()) != 0;
		if (!loaded) {
			throw TextureException("Texture cannot be loaded, file not found or unknown format", *itFilepath);
		}

		auto bits = fimg->getBitsPerPixel();
		auto colorType = fimg->getColorType();
		//auto pom =fimg->

		
		glGenTextures(1, &textureID);
		if (textureID == 0) {
			throw BasicException("Cannot create texture unit no.: " + textureUnit);
		}
		this->m_textureIDs.push_back(textureID);

		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, textureID);


		//we want only some types.. so we are going to use if hell for this and if image is not one of desired type...  exception will be thrown

		//first is heightmap.. 16bit grayscale
		if (bits == 16 && colorType == FIC_MINISBLACK) {
			glTexStorage2D(GL_TEXTURE_2D, 1, GL_R16, fimg->getWidth(), fimg->getHeight());
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, fimg->getWidth(), fimg->getHeight(), GL_RED, GL_UNSIGNED_SHORT, fimg->accessPixels()); 
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		}

		// 8 bit grayscale
		else if (bits == 8 && colorType == FIC_MINISBLACK) {
			glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, fimg->getWidth(), fimg->getHeight());
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, fimg->getWidth(), fimg->getHeight(), GL_RED, GL_UNSIGNED_BYTE, fimg->accessPixels());
		}

		// 8 bit RGB
		else if (bits == 24 && colorType == FIC_RGB) {
			glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, fimg->getWidth(), fimg->getHeight());
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, fimg->getWidth(), fimg->getHeight(), GL_BGR, GL_UNSIGNED_BYTE, fimg->accessPixels());
			
		}
		// 8 bit RGBA
		else if (bits == 32 && (colorType == FIC_RGB || colorType == FIC_RGBALPHA)) {
			glTexStorage2D(GL_TEXTURE_2D, 5, GL_RGBA8, fimg->getWidth(), fimg->getHeight());
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, fimg->getWidth(), fimg->getHeight(), GL_BGRA, GL_UNSIGNED_BYTE, fimg->accessPixels());
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}
		// 16 bit textures because we want quality
		// 16 bit RGBA
		else if (bits == 64 && colorType == FIC_RGBALPHA) {
			glTexStorage2D(GL_TEXTURE_2D, 5, GL_RGBA16, fimg->getWidth(), fimg->getHeight());
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, fimg->getWidth(), fimg->getHeight(), GL_RGBA, GL_UNSIGNED_SHORT, fimg->accessPixels());
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}

		// not supported format
		else{
			throw TextureException("Unsupported texture format!", (*itFilepath).c_str());
		}


		//incrementing texture unit
		textureUnit++;
		delete fimg;
	}


	//glActiveTexture(0);
	return this;
}

void Texture::SetShader(std::shared_ptr<Shader> program)
{
	this->m_shader = program;
}


void Texture::Use(std::string name)
{
	Use(name, m_textureIDs, m_textureUnit);
}

void Texture::Use(GLuint binding)
{
	glActiveTexture(GL_TEXTURE0 + binding);___
	glBindTexture(GL_TEXTURE_2D, m_textureIDs.at(0));___
}

void Texture::Use(GLuint binding, GLuint textureId)
{
	glActiveTexture(GL_TEXTURE0 + binding);___
	glBindTexture(GL_TEXTURE_2D, textureId);___
}

void Texture::Use(std::string name, std::shared_ptr<Shader> shader)
{
	auto tempShader = m_shader;
	m_shader = shader;
	Use(name, m_textureIDs, m_textureUnit);
	m_shader = tempShader;
}

void Texture::Use(std::string name, const std::vector<GLuint> &textures, uint textureUnit, bool asArray /* false */)
{
	if (m_shader == Texture::ShaderNull) return;

	if (textures.size() == 1 && !asArray)
	{	
		GLint baseImageLoc = glGetUniformLocation(m_shader->GetID(), name.c_str());
		glUniform1i(baseImageLoc, textureUnit);
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, textures.at(0));
	}
	else {
		std::string pomName;
		std::vector<uint>::const_iterator textIt;
		int i;

		for (textIt = textures.begin(), i = 0; textIt != textures.end(); textIt++, i++) {
			
			pomName = name + "[" + std::to_string(i) + "]";
			GLint loc = glGetUniformLocation(m_shader->GetID(), pomName.c_str());
			glUniform1i(loc, textureUnit +  i);
			glActiveTexture(GL_TEXTURE0 + textureUnit + i);
			glBindTexture(GL_TEXTURE_2D, (*textIt));
		}
	}
}

GLuint Texture::GetID(int index /* 0 */)
{ 
	index = index < 0 ? 0 : index;
	int textures = static_cast<int>(m_textureIDs.size());
	index = index < textures ? index : textures - 1;
	return this->m_textureIDs[index]; 
}

Texture::~Texture()
{
	if (m_textureIDs.size() > 0 && m_managed) {
		glDeleteTextures(static_cast<GLsizei>(m_textureIDs.size()), &m_textureIDs[0]);
		m_textureIDs.clear();
	}
}
