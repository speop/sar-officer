#include "Shader.h"
#include <fstream>
#include <vector>
#include "FileManager.h"
#include <regex>
#include <sstream>
#include <iostream>



Shader::Shader() : _programID(0),  _numAttributes(0){}
Shader::~Shader()
{
	glDeleteProgram(_programID);
}

void Shader::BuildShaders(std::initializer_list<ShaUN> shaders)
{
	std::vector<GLint> shaderIDs;

	this->_programID  = glCreateProgram();
	if (this->_programID == 0) {
		throw BasicException("Program failed to be created");
	}
	
	
	//compiling shaders
	for (auto &shaun : shaders)
	{
		auto id = glCreateShader(shaun.type);
		if (id == 0) {
			throw BasicException(shaun.type + " shader failed to be created");
		}
		m_currentShaUN = shaun;
		m_codeAdded = false;
		CompileShader(id);
		shaderIDs.push_back(id);
	}

	//Attach our shaders to our program
	for (auto it = shaderIDs.begin(); it != shaderIDs.end(); it++) {
		glAttachShader(this->_programID, *it);
	}

	//Link our program
	glLinkProgram(this->_programID);


	//Note the different functions here: glGetProgram* instead of glGetShader*.
	GLint isLinked = 0;
	glGetProgramiv(_programID, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(_programID, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		std::vector<char> errorLog(maxLength);
		glGetProgramInfoLog(_programID, maxLength, &maxLength, &errorLog[0]);

		//We don't need the program anymore.
		glDeleteProgram(_programID);

		//Don't leak shaders either.
		for (auto it = shaderIDs.begin(); it != shaderIDs.end(); it++) {
			glDeleteShader(*it);
		}

		//Use the infoLog as you see fit.
		std::printf("%s \n", &errorLog[0]);
		throw BasicException("Shader failed to link!");

	}

	//Always detach shaders after a successful link.
	for (auto it = shaderIDs.begin(); it != shaderIDs.end(); it++) {
		glDetachShader(this->_programID, *it);
		glDeleteShader(*it);
	}


}


void Shader::CompileShader(GLuint shaderID) {
	std::string sourceCode = "";
	
	GLint success = 0;

	//loading vertex shader code
	std::vector<std::string> includeFiles;
	includeFiles.push_back(m_currentShaUN.filePath);
	LoadShaderCode(m_currentShaUN.filePath, sourceCode, includeFiles);

	//compiling shader

	//get a pointer to our file contents c string;
	const char*  fcPointer = sourceCode.c_str();
	glShaderSource(shaderID, 1, &fcPointer, nullptr);
	glCompileShader(shaderID);
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);

	if (success == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::string errorLog;
		errorLog.resize(maxLength);

		glGetShaderInfoLog(shaderID, maxLength, &maxLength, &errorLog[0]);

		// Provide the infolog in whatever manor you deem best.
		// Exit with failure.
		glDeleteShader(shaderID); // Don't leak the shader.

		std::cout << m_currentShaUN.filePath << "\n---------------\n" << std::endl;
		//auto errorString = std::string(errorLog.begin(), errorLog.end());
	
		std::istringstream f(errorLog);
		std::string line;
		while (std::getline(f, line)) {
			std::regex rError(R"(^ *error: \d+:\d+)", std::regex_constants::icase);
			std::regex rErrorNv(R"(^ *\d+\(\d+\) *: *error)", std::regex_constants::icase);
			std::regex rNumber(R"(\d+)");
			std::smatch match;			
			//parse error
			if (std::regex_search(line, match, rError) || std::regex_search(line, match, rErrorNv)) {
				std::string pom = match[0];
				std::regex_search(pom, match, rNumber);
				auto index = std::stoi(match[0]);
				std::string filename = "Filename";

				if(index >= 0 && index < includeFiles.size()) {
					filename = includeFiles[index];
				}
				line = std::regex_replace(line, rNumber, filename, std::regex_constants::format_first_only);
			}
			std::cerr << line << std::endl;			
		}
		throw BasicException("Shader " + m_currentShaUN.filePath + " failed to compile");
	}

}


void Shader::LoadShaderCode(const std::string& filePath, std::string& sourceCode, std::vector<std::string>& includedFiles)
{
	FileManager file(filePath);
	std::regex rInclude(R"(^ *#pragma +sar +include *\( *"[\w\-\d\/\\]+\.[\w\d]+"\))");
	std::regex rHeader(R"([\w\-\d\/\\]+\.[\w\d]+)");
	std::regex rVersion(R"(^ *#version *\d+)");
	std::smatch match;
	int currFilename = includedFiles.size() - 1;
	std::string filename;
	int numRow = 1;

	std::pair<bool, std::string> line;
	do {
		line = file.GetLine();
		numRow++;

		//checking for #version tag and addign additional code to shader
		if(!m_codeAdded && m_currentShaUN.additionalCode != "" && std::regex_search(line.second, match, rVersion)) {
			sourceCode += line.second;
			sourceCode += m_currentShaUN.additionalCode;
			sourceCode += "#line 0 " + std::to_string(currFilename) + " \n";
			m_codeAdded = true;
		}

		//checking for include directive
		else if (std::regex_search(line.second, match, rInclude)) {
			std::string include = match[0];
			std::regex_search(include, match, rHeader);
			filename = match[0];
			
			//we have already include this file
			if(std::find(includedFiles.begin(), includedFiles.end(), filename) != includedFiles.end()) continue;
			
			sourceCode += "#line 1 " + std::to_string(includedFiles.size()) + " \n";
			includedFiles.push_back(filename);
			LoadShaderCode(filename, sourceCode, includedFiles);
			sourceCode += "#line " + std::to_string(numRow) + "  " + std::to_string(currFilename) + " \n";
		}
		else {
			sourceCode += line.second;
		}

	} while (line.first);
}

void Shader::PrintErrorCyclicInclude(std::set<std::string>& inludeStack)
{
	//printf("Cyclic file include")
}


void Shader::AddAtribute(const std::string& attributeName) {
	glBindAttribLocation(_programID, _numAttributes++, attributeName.c_str());
}

void Shader::Use(){
	glUseProgram(_programID);
}

void Shader::Unuse(){
	glUseProgram(0);
	
}

GLuint Shader::GetID(){
	return _programID;
}

#pragma region GL data wrapper

Shader * Shader::Set2i(const std::string & name, GLsizei count, const GLint *value)
{
	auto temp = glGetUniformLocation(_programID, name.c_str());
	glUniform2iv(temp,count, value);
	return this;
}

Shader * Shader::SetVec2f(const std::string & name, GLsizei count, const GLfloat * value)
{
	auto temp = glGetUniformLocation(_programID, name.c_str());
	glUniform2fv(temp, count, value);
	return this;
}

Shader* Shader::SetVec2f(uint location, GLsizei count, const GLfloat* value)
{
	glUniform2fv(location, count, value);
	return this;
}

Shader * Shader::SetVec3f(const std::string & name, GLsizei count, const GLfloat * value)
{
	auto temp = glGetUniformLocation(_programID, name.c_str());
	glUniform3fv(temp, count, value);
	return this;
}

Shader* Shader::SetVec3f(uint location, GLsizei count, const GLfloat* value)
{
	glUniform3fv(location, count, value);
	return this;
}

Shader* Shader::Set1f(const std::string &name, float val)
{
	auto temp = glGetUniformLocation(_programID, name.c_str());
	glUniform1f(temp, val);
	return this;
}

Shader* Shader::Set1i(const std::string& name, int val)
{
	auto temp = glGetUniformLocation(_programID, name.c_str());
	Set1i(temp, val);
	return this;
}

Shader* Shader::Set1i(uint location, int val)
{
	glUniform1i(location, val);
	return this;
}

Shader* Shader::SetBool(const std::string& name, bool val)
{
	Set1i(name, val);
	return this;
}

Shader* Shader::SetBool(uint location, bool val)
{
	Set1i(location, val);
	return this;
}

Shader* Shader::Set1f(uint location, float val)
{
	glUniform1f(location, val);
	return this;
}

Shader* Shader::Set3f(const std::string & name, float val, float val2, float val3)
{
	auto temp = glGetUniformLocation(_programID, name.c_str());
	glUniform3f(temp, val, val2, val3);
	return this;
}

Shader * Shader::SetMat4f(const std::string &name, GLsizei count, GLboolean transpose, const GLfloat * value)
{
	auto location = glGetUniformLocation(this->_programID, name.c_str());
	glUniformMatrix4fv(location, count, transpose, value);

	return this;
}
Shader * Shader::SetMat4f(uint location, GLsizei count, GLboolean transpose, const GLfloat * value)
{
	glUniformMatrix4fv(location, count, transpose, value);
	return this;
}

#pragma endregion
