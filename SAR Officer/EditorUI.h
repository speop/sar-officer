// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-04-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-16-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include "UIElement.h"
#include <vector>
#include  "Editor.h"
/// <summary>
/// Class EditorUI. Front end class for <see cref="Editor"/> class.
/// </summary>
/// <seealso cref="UIElement" />
class EditorUI :
	public UIElement
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="EditorUI"/> class.
	/// </summary>
	EditorUI();
	/// <summary>
	/// Finalizes an instance of the <see cref="EditorUI"/> class.
	/// </summary>
	~EditorUI();

	/// <summary>
	/// Wake up method <see cref="UIElement.Wake" />. This method resets UI, resets selected states and gets current loaded models.
	/// </summary>
	void Wake() override;
	
	/// <summary>
	/// Run method <see cref="UIElement.Run" />. 
	/// </summary>
	void Run() override;
	
	/// <summary>
	/// ImGui run method <see cref="UIElement.RunImgui" />. Draw UI elements.
	/// </summary>
	void RunImgui() override;
	
	/// <summary>
	/// Stops method <see cref="UIElement.Stop" />. Releases alocated memory.
	/// </summary>
	void Stop() override;
	
	/// <summary>
	/// Burns this instance <see cref="UIElement.Burn" />.
	/// </summary>
	void Burn() override;

private:
	/// <summary>
	/// Initializes this instance.
	/// </summary>
	void Init() override;

	/// <summary>
	/// This method manages tools panel.
	/// </summary>
	void ToolsPanel();

	/// <summary>
	/// This method manages main panel.
	/// </summary>
	void MainPanel();

	/// <summary>
	/// Modal window which occurs when saving scene. Purpose for this is to show progress to user. Saving animation runs in this window.
	/// </summary>
	void SavingModal();


	bool m_saving = false;
	std::chrono::high_resolution_clock::time_point m_timerPoint;
	std::vector<std::string> m_models;
	char** m_modelsChars;
	float m_speed = 1.f;
	int m_selectedModel = 0;
	bool m_lighting = false;
	bool m_snapToY = true;
	bool m_addMode = false;
	Editor::Axis m_axis;


#pragma region buttons callbacks
	
	/// <summary>
	/// This is callback method for alternation buttons click. This method call editor function which transforms object.
	/// </summary>
	/// <param name="alt">The alt.</param>
	void AlterBtn_Click(Editor::Alteration alt);
	
	/// <summary>
	/// This is callback method for delete button click. This method call editor function which deletes object.
	/// </summary>
	void DeleteBtn_Click();
	
	/// <summary>
	/// This is callback method for save buttonb click. This method call editor function which saves the scene.
	/// </summary>
	void SaveBtn_Click();
	
	/// <summary>
	/// Exit button callback. This method shuts down editor and exits to main menu. All not save changes will be lost.
	/// </summary>
	void ExitBtn_Click();

	/// <summary>
	/// Callback for lighting toggle switch. If it's true scene in editor will be lit.
	/// </summary>
	void LightingBtn_Click();

	/// <summary>
	/// Callback for adding mode toggle switch
	/// </summary>
	void AddBtn_Click();

	/// <summary>
	/// Callback for snap to y toggle switch.
	/// </summary>
	void SnapToYBtn_Click();

	/// <summary>
	/// Callback for selecting model from drop down list. This function tell editor which model is selected for adding to scene.
	/// </summary>
	void ModelBtn_Click();
#pragma endregion 
};

