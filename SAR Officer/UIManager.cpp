#define GLFW_EXPOSE_NATIVE_WIN32
#include "UIManager.h"
#include "GLFW/glfw3native.h"
#include "MainMenu.h"
#include "PauseMenu.h"
#include "EditorUI.h"

/// <summary>
/// The s attribute location tex{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
int   UIManager::s_ShaderHandle = 0, UIManager::s_AttribLocationTex = 0, UIManager::s_AttribLocationProjMtx = 0;
/// <summary>
/// The s vao handle{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
unsigned int UIManager::s_VboHandle = 0, UIManager::s_VaoHandle = 0, UIManager::s_ElementsHandle = 0;
/// <summary>
/// The s current time{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
double UIManager::s_currentTime = 0;
/// <summary>
/// The m mouse pressed
/// </summary>
float UIManager::m_MouseWheel = 0;
/// <summary>
/// The m instance{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
UIManager* UIManager::m_instance = nullptr;
/// <summary>
/// The m last state{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
UIManager::UIStates UIManager::m_lastState = UIManager::StMainMenu;

UIManager::UIManager()
{
	
}


UIManager::~UIManager()
{
}


void UIManager::Init(WindowManager* wManager)
{
	m_wManager = wManager;

	m_UIelements[StMainMenu] = new MainMenu();
	m_UIelements[StPauseMenu] = new PauseMenu();
	m_UIelements[StEditor] = new EditorUI();

	ImGuInit(true);

}

void UIManager::Run(UIStates state, bool mouseCursor)
{
	if (state < 0 || state >= END_STOP) return;

	m_instance = this;	
	m_lastState = state;

	UIElement* ptr;
	ptr = m_UIelements[state];

	if (ptr == nullptr) return;

	if(m_actualUI == nullptr) {
		m_actualUI = ptr;
		m_actualUI->Wake();
	}else if(m_actualUI != ptr) {
		m_actualUI->Stop();
		m_actualUI = ptr;
		m_actualUI->Wake();
	}

	m_actualUI->Run();


	// Use ImGui functions between here and Render()
	ImGuiNewFrame(mouseCursor);		
	m_actualUI->RunImgui();
	//m_UIelements[StPauseMenu]->RunImgui();
	// ImGui functions end here

	m_mouseOverGui = ImGui::IsMouseHoveringAnyWindow();
	ImGui::Render();
	
}


void UIManager::Stop()
{
	m_actualUI->Stop();
}

void UIManager::Burn()
{
	for (auto i = 0; i < END_STOP; i++) {
		if(m_UIelements[i] != nullptr) {
			delete m_UIelements[i];
			m_UIelements[i] = nullptr;
		}
	}
}

void UIManager::ImGuiRenderer(ImDrawData * draw_data)
{
	// Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
	ImGuiIO& io = ImGui::GetIO();
	int fb_width = static_cast<int>(io.DisplaySize.x * io.DisplayFramebufferScale.x);
	int fb_height = static_cast<int>(io.DisplaySize.y * io.DisplayFramebufferScale.y);
	if (fb_width == 0 || fb_height == 0)
		return;
	draw_data->ScaleClipRects(io.DisplayFramebufferScale);

#pragma region Backup GL state
	GLint last_program; glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
	GLint last_texture; glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
	GLint last_active_texture; glGetIntegerv(GL_ACTIVE_TEXTURE, &last_active_texture);
	GLint last_array_buffer; glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
	GLint last_element_array_buffer; glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
	GLint last_vertex_array; glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
	GLint last_blend_src; glGetIntegerv(GL_BLEND_SRC, &last_blend_src);
	GLint last_blend_dst; glGetIntegerv(GL_BLEND_DST, &last_blend_dst);
	GLint last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, &last_blend_equation_rgb);
	GLint last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &last_blend_equation_alpha);
	GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
	GLint last_scissor_box[4]; glGetIntegerv(GL_SCISSOR_BOX, last_scissor_box);
	GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
	GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
	GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
	GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);
#pragma endregion 


	// Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);
	glActiveTexture(GL_TEXTURE0);

	// Setup viewport, orthographic projection matrix
	glViewport(0, 0, static_cast<GLsizei>(fb_width), static_cast<GLsizei>(fb_height));
	const float ortho_projection[4][4] =
	{
		{ 2.0f / io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
		{ 0.0f,                  2.0f / -io.DisplaySize.y, 0.0f, 0.0f },
		{ 0.0f,                  0.0f,                  -1.0f, 0.0f },
		{ -1.0f,                  1.0f,                   0.0f, 1.0f },
	};


	glUseProgram(UIManager::s_ShaderHandle);
	glUniform1i(UIManager::s_AttribLocationTex, 0);
	glUniformMatrix4fv(UIManager::s_AttribLocationProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);
	glBindVertexArray(UIManager::s_VaoHandle);

	for (int n = 0; n < draw_data->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = draw_data->CmdLists[n];
		const ImDrawIdx* idx_buffer_offset = 0;

		glBindBuffer(GL_ARRAY_BUFFER, UIManager::s_VboHandle);
		glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert), (const GLvoid*)cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, UIManager::s_ElementsHandle);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx), (const GLvoid*)cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);

		for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
			if (pcmd->UserCallback)
			{
				pcmd->UserCallback(cmd_list, pcmd);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
				glScissor((int)pcmd->ClipRect.x, (int)(fb_height - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
				glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
			}
			idx_buffer_offset += pcmd->ElemCount;
		}
	}

#pragma region Restore modified GL state
	glUseProgram(last_program);
	glActiveTexture(last_active_texture);
	glBindTexture(GL_TEXTURE_2D, last_texture);
	glBindVertexArray(last_vertex_array);
	glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
	glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
	glBlendFunc(last_blend_src, last_blend_dst);
	if (last_enable_blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
	if (last_enable_cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
	if (last_enable_depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
	if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST); else glDisable(GL_SCISSOR_TEST);
	glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
	glScissor(last_scissor_box[0], last_scissor_box[1], (GLsizei)last_scissor_box[2], (GLsizei)last_scissor_box[3]);
#pragma endregion 
}


void UIManager::ImGuiCreateDeviceObjects()
{
	// Backup GL state
	GLint last_texture, last_array_buffer, last_vertex_array;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
	glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);

	const GLchar *vertex_shader =
		"#version 330\n"
		"uniform mat4 ProjMtx;\n"
		"in vec2 Position;\n"
		"in vec2 UV;\n"
		"in vec4 Color;\n"
		"out vec2 Frag_UV;\n"
		"out vec4 Frag_Color;\n"
		"void main()\n"
		"{\n"
		"	Frag_UV = UV;\n"
		"	Frag_Color = Color;\n"
		"	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
		"}\n";

	const GLchar* fragment_shader =
		"#version 330\n"
		"uniform sampler2D Texture;\n"
		"in vec2 Frag_UV;\n"
		"in vec4 Frag_Color;\n"
		"out vec4 Out_Color;\n"
		"void main()\n"
		"{\n"
		"	Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
		"}\n";

	UIManager::s_ShaderHandle = glCreateProgram();
	m_VertHandle = glCreateShader(GL_VERTEX_SHADER);
	m_FragHandle = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(m_VertHandle, 1, &vertex_shader, 0);
	glShaderSource(m_FragHandle, 1, &fragment_shader, 0);
	glCompileShader(m_VertHandle);
	glCompileShader(m_FragHandle);
	glAttachShader(UIManager::s_ShaderHandle, m_VertHandle);
	glAttachShader(UIManager::s_ShaderHandle, m_FragHandle);
	glLinkProgram(UIManager::s_ShaderHandle);

	UIManager::s_AttribLocationTex = glGetUniformLocation(UIManager::s_ShaderHandle, "Texture");
	UIManager::s_AttribLocationProjMtx = glGetUniformLocation(UIManager::s_ShaderHandle, "ProjMtx");
	m_AttribLocationPosition = glGetAttribLocation(UIManager::s_ShaderHandle, "Position");
	m_AttribLocationUV = glGetAttribLocation(UIManager::s_ShaderHandle, "UV");
	m_AttribLocationColor = glGetAttribLocation(UIManager::s_ShaderHandle, "Color");

	glGenBuffers(1, &UIManager::s_VboHandle);
	glGenBuffers(1, &UIManager::s_ElementsHandle);

	glGenVertexArrays(1, &UIManager::s_VaoHandle);
	glBindVertexArray(UIManager::s_VaoHandle);
	glBindBuffer(GL_ARRAY_BUFFER, UIManager::s_VboHandle);
	glEnableVertexAttribArray(m_AttribLocationPosition);
	glEnableVertexAttribArray(m_AttribLocationUV);
	glEnableVertexAttribArray(m_AttribLocationColor);

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
	glVertexAttribPointer(m_AttribLocationPosition, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
	glVertexAttribPointer(m_AttribLocationUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
	glVertexAttribPointer(m_AttribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
#undef OFFSETOF

	ImGuiCreateFontsTexture();

	// Restore modified GL state
	glBindTexture(GL_TEXTURE_2D, last_texture);
	glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
	glBindVertexArray(last_vertex_array);
}

void UIManager::ImGuiCreateFontsTexture()
{
	// Build texture atlas
	ImGuiIO& io = ImGui::GetIO();
	unsigned char* pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);   // Load as RGBA 32-bits (75% of the memory is wasted, but default font is so small) because it is more likely to be compatible with user's existing shaders. If your ImTextureId represent a higher-level concept than just a GL texture id, consider calling GetTexDataAsAlpha8() instead to save on GPU memory.
	
															  // Upload texture to graphics system
	GLint last_texture;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
	glGenTextures(1, &m_FontTexture);
	glBindTexture(GL_TEXTURE_2D, m_FontTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

	// Store our identifier
	io.Fonts->TexID = (void *)(intptr_t)m_FontTexture;

	// Restore state
	glBindTexture(GL_TEXTURE_2D, last_texture);
}

void UIManager::ImGuiInvalidateDeviceObjects()
{
	if (UIManager::s_VaoHandle) glDeleteVertexArrays(1, &UIManager::s_VaoHandle);
	if (UIManager::s_VboHandle) glDeleteBuffers(1, &UIManager::s_VboHandle);
	if (UIManager::s_ElementsHandle) glDeleteBuffers(1, &UIManager::s_ElementsHandle);
	UIManager::s_VaoHandle = UIManager::s_VboHandle = UIManager::s_ElementsHandle = 0;

	if (UIManager::s_ShaderHandle && m_VertHandle) glDetachShader(UIManager::s_ShaderHandle, m_VertHandle);
	if (m_VertHandle) glDeleteShader(m_VertHandle);
	m_VertHandle = 0;

	if (UIManager::s_ShaderHandle && m_FragHandle) glDetachShader(UIManager::s_ShaderHandle, m_FragHandle);
	if (m_FragHandle) glDeleteShader(m_FragHandle);
	m_FragHandle = 0;

	if (UIManager::s_ShaderHandle) glDeleteProgram(UIManager::s_ShaderHandle);
	UIManager::s_ShaderHandle = 0;

	if (m_FontTexture)
	{
		glDeleteTextures(1, &m_FontTexture);
		ImGui::GetIO().Fonts->TexID = 0;
		m_FontTexture = 0;
	}
}

void UIManager::ImGuInit(bool install_callbacks)
{
	SetFont("../resources/ui/PoiretOne-Regular.ttf", 24.f);

	ImGuiIO& io = ImGui::GetIO();
	io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;                         // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
	io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
	io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
	io.KeyMap[ImGuiKey_PageUp] = GLFW_KEY_PAGE_UP;
	io.KeyMap[ImGuiKey_PageDown] = GLFW_KEY_PAGE_DOWN;
	io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
	io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
	io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
	io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
	io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
	io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
	io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
	io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
	io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
	io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
	io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

	io.RenderDrawListsFn = ImGuiRenderer;       // Alternatively you can set this to NULL and call ImGui::GetDrawData() after ImGui::Render() to get the same ImDrawData pointer.
	//io.SetClipboardTextFn = ImGui_ImplGlfwGL3_SetClipboardText;
	//io.GetClipboardTextFn = ImGui_ImplGlfwGL3_GetClipboardText;
	io.ClipboardUserData = m_wManager->GetWindow();
#ifdef _WIN32
	io.ImeWindowHandle = glfwGetWin32Window(m_wManager->GetWindow());
#endif

	
	if (install_callbacks)
	{
		//glfwSetMouseButtonCallback(window, ImGui_ImplGlfwGL3_MouseButtonCallback);
		glfwSetScrollCallback(m_wManager->GetWindow(), ImGuiScrollCallback);
		//glfwSetKeyCallback(window, ImGui_ImplGlfwGL3_KeyCallback);
		//glfwSetCharCallback(window, ImGui_ImplGlfwGL3_CharCallback);
	}

}

void UIManager::ImGuiShutdown()
{
	this->ImGuiInvalidateDeviceObjects();
	ImGui::Shutdown();
}

void UIManager::ImGuiNewFrame(bool mouseCursor)
{
	if (!m_FontTexture)
		ImGuiCreateDeviceObjects();

	ImGuiIO& io = ImGui::GetIO();

	// Setup display size (every frame to accommodate for window resizing)
	int w, h;
	int display_w, display_h;
	auto window = m_wManager->GetWindow();
	glfwGetWindowSize(window, &w, &h);
	glfwGetFramebufferSize(window, &display_w, &display_h);
	io.DisplaySize = ImVec2(static_cast<float>(w), static_cast<float>(h));
	io.DisplayFramebufferScale = ImVec2(w > 0 ? ((float)display_w / w) : 0, h > 0 ? ((float)display_h / h) : 0);

	// Setup time step
	double time = glfwGetTime();
	io.DeltaTime = UIManager::s_currentTime > 0.0 ? static_cast<float>(time - UIManager::s_currentTime) : static_cast<float>(1.0f / 60.0f);
	UIManager::s_currentTime = time;

	// Setup inputs
	// (we already got mouse wheel, keyboard keys & characters from glfw callbacks polled in glfwPollEvents())
	
	if (glfwGetWindowAttrib(window, GLFW_FOCUSED))
	{
		double mouse_x, mouse_y;
		glfwGetCursorPos(window, &mouse_x, &mouse_y);
		io.MousePos = ImVec2(static_cast<float>(mouse_x), static_cast<float>(mouse_y));   // Mouse position in screen coordinates (set to -1,-1 if no mouse / on another screen, etc.)
	}
	else
	{
		io.MousePos = ImVec2(-1, -1);
	}

	for (int i = 0; i < 3; i++)
	{
		io.MouseDown[i] = m_MousePressed[i] || glfwGetMouseButton(window, i) != 0;    // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
		m_MousePressed[i] = false;
	}

	io.MouseWheel = m_MouseWheel;
	m_MouseWheel = 0.0f;

	// Hide OS mouse cursor if ImGui is drawing it
	if(mouseCursor != m_mouseCursor || io.MouseDrawCursor) {
		m_mouseCursor = mouseCursor;
		if(m_mouseCursor) glfwSetInputMode(window, GLFW_CURSOR, io.MouseDrawCursor ? GLFW_CURSOR_HIDDEN : GLFW_CURSOR_NORMAL);
	}
	
	
	// Start the frame
	ImGui::NewFrame();
}

void UIManager::SetFont(std::string  font, float size)
{
	ImGuiIO& io = ImGui::GetIO();
	
	if (m_FontTexture)
	{
		glDeleteTextures(1, &m_FontTexture);
		io.Fonts->TexID = 0;
		m_FontTexture = 0;
	}

	ImFontConfig config, config2;
	config.MergeMode = true;
	

	io.Fonts->AddFontFromFileTTF(font.c_str(), size, &config2, s_latin);
	io.Fonts->AddFontFromFileTTF("../resources/ui/FontAwesome.ttf", size -2, &config, s_FontAwesomeRange);
}

bool UIManager::MouseOverGUI()
{
	return m_mouseOverGui;
}

void UIManager::Redraw()
{
	if(m_instance != nullptr) {
		m_instance->Run(m_lastState);
		m_instance->m_wManager->Run();
	}
}


void UIManager::ImGuiScrollCallback(GLFWwindow*, double xoffset, double yoffset)
{
	m_MouseWheel += (float)yoffset; // Use fractional mouse wheel, 1.0 unit 5 lines.
}