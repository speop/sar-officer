// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-01-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <string>
#include <thread>
#include <future>
#include "imgui.h"
#include <glm/detail/type_vec3.hpp>

/// <summary>
/// UIElement interface. 
/// </summary>
class UIElement
{
protected:
	
	std::thread m_worker;
	std::future<bool> m_asyncThreadRunnig;
public:
	
	/// <summary>
	/// Initializes this instance. This function is called when creating ui elements.
	/// </summary>
	virtual void Init() = 0;

	/// <summary> 
	/// Wakes this instance. This function is called when UI switch to this element.
	/// </summary>
	virtual void Wake() = 0;

	/// <summary>
	/// Runs this instance.
	/// </summary>
	virtual void Run() = 0;

	/// <summary>
	/// Runs in the imgui context
	/// </summary>
	virtual void RunImgui() = 0;

	/// <summary>
	/// Stops this instance. This function is called when UI switch from this element.
	/// </summary>
	virtual void Stop() = 0;

	/// <summary>
	/// Burns this instance. 
	/// </summary>
	virtual void Burn() = 0;

	/// <summary>
	/// Finalizes an instance of the <see cref="UIElement"/> class.
	/// </summary>
	virtual ~UIElement();

	/// <summary>
	/// Colors the specified hexadecimal.
	/// </summary>
	/// <param name="hex">The hexadecimal.</param>
	/// <returns>ImColor.</returns>
	static ImColor Color(std::string hex);
	/// <summary>
	/// gs the color of the lm.
	/// </summary>
	/// <param name="hex">The hexadecimal.</param>
	/// <returns>glm.vec3.</returns>
	static glm::vec3 GLmColor(std::string hex);

	/// <summary>
	/// The c BTN blue
	/// </summary>
	const ImColor cBtnBlue = Color("2c3e50");
	/// <summary>
	/// The c BTN blue hover
	/// </summary>
	const ImColor cBtnBlueHover = Color("34495E");
	/// <summary>
	/// The c BTN blue active
	/// </summary>
	const ImColor cBtnBlueActive = Color("2980b9");
	/// <summary>
	/// The c BTN green
	/// </summary>
	const ImColor cBtnGreen = Color("008C31");
	/// <summary>
	/// The c BTN green hover
	/// </summary>
	const ImColor cBtnGreenHover = Color("27ae60");
	/// <summary>
	/// The c BTN green active
	/// </summary>
	const ImColor cBtnGreenActive = Color("2ecc71");
	/// <summary>
	/// The c BTN red
	/// </summary>
	const ImColor cBtnRed = Color("9A0038");
	/// <summary>
	/// The c BTN red hover
	/// </summary>
	const ImColor cBtnRedHover = Color("BA1C58");
	/// <summary>
	/// The c BTN red active
	/// </summary>
	const ImColor cBtnRedActive = Color("CA2C68");

	/// <summary>
	/// The c dropdown item
	/// </summary>
	const ImColor cDropdownItem = Color("008B8055");
	/// <summary>
	/// The c dropdown item hover
	/// </summary>
	const ImColor cDropdownItemHover = Color("27CBC055");
	/// <summary>
	/// The c dropdown item active
	/// </summary>
	const ImColor cDropdownItemActive = Color("37DBD055");
	/// <summary>
	/// The c input
	/// </summary>
	const ImColor cInput = Color("CCCCCC4D");
	/// <summary>
	/// The c input hover
	/// </summary>
	const ImColor cInputHover = Color("27CBC044");
	/// <summary>
	/// The c input active
	/// </summary>
	const ImColor cInputActive = Color("37DBD053");
	/// <summary>
	/// The c widget background
	/// </summary>
	const ImColor cWidgetBackground = Color("253536bf");

	
};

