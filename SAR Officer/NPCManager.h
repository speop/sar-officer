#pragma once
#include "GraphicElement.h"
#include "Animal.h"

class NPCManager :
	public GraphicElement
{
public:
	NPCManager();
	void Init() override;
	void Run() override{ Run(RenderPass::CLASSIC); };
	void Run(RenderPass pass);
	~NPCManager();

private:

	struct NPC {
		std::shared_ptr<GO::Animal> animal;
		std::shared_ptr<Model> model;
		std::vector<glm::mat4> positions;
	};

	std::vector<NPC> m_npcs;
	std::shared_ptr<Shader> m_shader;
	std::shared_ptr<Shader> m_shadowShader;

};

