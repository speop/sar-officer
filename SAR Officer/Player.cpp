#include "Player.h"
#include "CollisionManager.h"
#define DISTANCE(point1 ,point2)(sqrt(pow((point1.x - point2.x), 2) + pow((point1.y - point2.y), 2)))
using namespace GO;


Player::Player(::PerspectiveCamera* camera)
{
	m_camera = camera;
	if (m_camera != nullptr) {
		m_position = camera->GetPosition();
	}
	m_capsule = std::make_unique<Capsule>(m_height, m_width, m_position);
}


Player::~Player()
{
}

void Player::ProcessMove(glm::vec2 moveVec)
{	
	//m_camera->Move({ moveVec.x, 0, moveVec.y }); return;
	DoJumping();
	
	glm::vec2 oldPos = { m_position.x, m_position.z };
	glm::vec2 currSlope = CollisionManager::Instance().GetHeightInterpolatedAndSlope(oldPos, true).second;

	if(!m_jumping && ( abs(currSlope.x) > m_maxSlopeWithoutGravity || abs(currSlope.y)> m_maxSlopeWithoutGravity)) {
		moveVec += currSlope;
	}
	
	auto newPos = CollisionCheck(moveVec);
	
	//m_position = glm::vec3(newPos, newHeight);
	UpdatePosition({ newPos.x, newPos.z }, newPos.y);
}

void Player::Jump()
{
	if(!m_jumping && m_onGround) {
		m_jumping = true;
		m_velocityY = m_jumpinSpeed;
		m_startJump = true;
	}
}

void Player::UpdatePosition(glm::vec2 pos, float height)
{
	auto deltaTime = TimeManager::GetInstance()->GetDeltaTime();
	
	//height minus gravity
	if(!m_jumping) {
		m_velocityY -= m_gravitySpeed * deltaTime;
	}

	float currHeight = m_position.y + m_velocityY;

	if(height < currHeight) {
		height = currHeight;
		m_onGround = false;
	}
	else {
		m_onGround = true;
	}

	if(m_onGround) {
		m_velocityY = 0;
		m_jumping = false;
	}

	m_position = { pos.x, height, pos.y };
	if (m_camera != nullptr) {
		m_camera->SetPosition({ pos.x, height + m_height, pos.y });
	}
	
}

glm::vec3 Player::CollisionCheck(glm::vec2 moveVec)
{
	auto deltaTime = TimeManager::GetInstance()->GetDeltaTime();
	auto speed = m_movementSpeed;
	glm::vec2 oldPos = { m_position.x, m_position.z };
	auto colTime = m_capsule->CalculateColidingTime(deltaTime*m_movementSpeed * glm::vec3( moveVec.x, m_velocityY, moveVec.y ));
	colTime = colTime < 1 ? colTime : 1;
	
	glm::vec2 newPos = oldPos + deltaTime*m_movementSpeed * colTime*moveVec;

	

	//map border check
	if (newPos.x < -2000 || newPos.x > 2000 || newPos.y < -2000 || newPos.y > 2000) {
		return{ m_position.x, m_position.y, m_position.z };
	}
	auto newHeight = CollisionManager::Instance().GetHeightInterpolated(newPos, true);


	// Collisions against other objects
	m_capsule->UpdatePosition({ newPos.x , newHeight, newPos.y });

	do {
		colTime = m_capsule->CalculateColidingTime();
		if (colTime <= 1) {
			colTime = colTime * -1;
			newPos = newPos + deltaTime*m_movementSpeed * colTime  * moveVec;
			newHeight = CollisionManager::Instance().GetHeightInterpolated(newPos, true);
			m_capsule->UpdatePosition({ newPos.x , newHeight, newPos.y });

			/*m_capsule->UpdatePosition({m_position.x, m_position.y, m_position.z});
			return{ m_position.x, m_position.y, m_position.z };*/
		}
	

		// Collisions against terrain
		if (newHeight - m_position.y <= m_maxSlope) {
			return{ newPos.x , newHeight, newPos.y };
		}

		//we have to do bisection move
		float delta;
		auto speedHalf = m_movementSpeed / 2.f;
		auto sign = -1;
		glm::vec2 middlePos;

		//bisection move style, finding closest point where can player move
		do {
			speed = speed + sign*speedHalf;
			speedHalf /= 2.f;

			middlePos = oldPos + deltaTime*speed  * moveVec;
			newHeight = CollisionManager::Instance().GetHeightInterpolated(middlePos, true);
			sign = newHeight - m_position.y <= m_maxSlope ? 1 : -1;


			delta = sqrt((oldPos.x - middlePos.x) * (oldPos.x - middlePos.x) + (oldPos.y - middlePos.y) *(oldPos.y - middlePos.y));

			oldPos = middlePos;

			//runaway check
			if ((speed < 0 && speed > m_movementSpeed) || speedHalf > m_movementSpeedBias) {
				return{ m_position.x, m_position.y, m_position.z };
			}
		} while (delta > m_movementBias);

	} while (colTime < 1);
	return{ newPos.x, newHeight, newPos.y};
}

void Player::DoJumping()
{
	if (!m_jumping) return;

	auto deltaTime = TimeManager::GetInstance()->GetDeltaTime();

	if(m_startJump) {
		m_startJump = false;
		return;
	}

	

	m_velocityY -= m_gravitySpeed * deltaTime;
}
