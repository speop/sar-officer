#pragma once
#include <vector>
#include <memory>
#include "SceneManager.h"
#include "Ray.h"

class Editor
{
	class ModelTemplate {
	public:
		std::string m_name;
		SceneManager::SceneRecord m_template;
	};

	void PickObject(const SAR::Ray &ray);
	void AddObject(const SAR::Ray &ray);

public:
	enum Axis { X, Y, Z, XYZ };
	enum Alteration { RLeft, RRight, MLeft, MRight, SPlus, SMinus };

	/// <summary>
	/// Gets this instance.
	/// </summary>
	/// <returns>Editor *.</returns>
	static Editor* Get();

	/// <summary>
	/// Resets editor.
	/// </summary>
	/// <returns>Editor *.</returns>
	static Editor* Reset();

	/// <summary>
	/// Alters the object.
	/// </summary>
	/// <param name="alt">The alt.</param>
	/// <param name="axis">The axis.</param>
	/// <param name="speed">The speed.</param>
	void AlterObject(Alteration alt, Axis axis, float speed);

	/// <summary>
	/// Add the toggle.
	/// </summary>
	/// <param name="value">The value.</param>
	void AddToggle(bool value);

	/// <summary>
	/// Snap to terrain toggle.
	/// </summary>
	/// <param name="value">The value.</param>
	void SnapToTerrainToggle(bool value);

	/// <summary>
	/// Deletes the object.
	/// </summary>
	void DeleteObject();

	/// <summary>
	/// Sets the model.
	/// </summary>
	/// <param name="modelID">The model identifier.</param>
	void SetModel(int modelID);

	/// <summary>
	/// Processes the ray.
	/// </summary>
	/// <param name="ray">The ray.</param>
	/// 
	void ProcessRay(const SAR::Ray &ray);
	/// <summary>
	/// Adds the model template.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="tpl">The TPL.</param>
	void AddModelTemplate(const std::string &name, SceneManager::SceneRecord tpl);

	/// <summary>
	/// Saves scene.
	/// </summary>
	void Save();

	/// <summary>
	/// Retrurns current loaded models
	/// </summary>
	/// <returns>std.vector&lt;_Ty, _Alloc&gt;.</returns>
	std::vector<std::string> Models();

private:
	
	/// <summary>
	/// Prevents a default instance of the <see cref="Editor"/> class from being created.
	/// </summary>
	Editor();

	/// <summary>
	/// Finalizes an instance of the <see cref="Editor"/> class.
	/// </summary>
	~Editor();
	static Editor* instance;
	
	
	bool m_addMode = false;
	bool m_snapToTerrain = true;
	int m_selectedModel = 0;
	std::shared_ptr<SceneManager::SceneRecord> m_pickedObject;
	std::vector<ModelTemplate> m_models;
	glm::mat4 m_prevPosition = glm::mat4(1.0);
};

