// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 05-05-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <vector>
#include <string>
#include <GL\glew.h>
#include <set>
#include <map>
#include <FreeImagePlus.h>
#include "Exceptions.h"

/// <summary>
/// More advanced texture class. This class should be used if we want to create classic 2D_texture_array
/// </summary>
class TextureA
{
public:
	static const int ShaderNull = 0;

	/// <summary>
	/// Initializes a new instance of the <see cref="TextureA"/> class.
	/// </summary>
	TextureA() : m_baked(false), m_texture(0) {};

	/// <summary>
	/// Function stores textures name in internal variable. This method does not check if files exist or even if can be opened or processed by free image.
	/// </summary>
	/// <param name="textures">Texture filenames</param>
	/// <exception cref="TextureAlreadyBakedException">If texture array was already created in OpenGL what happened if there already was Bake(...) method call.</exception>
	void Load(const std::vector<std::string> &textures);

	/// <summary>
	/// Function stores texture name in internal variable. This method does not check if files exist or even if can be opened or processed by free image.
	/// </summary>
	/// <param name="texture">The texture.</param>
	/// <exception cref="TextureAlreadyBakedException">If texture array was already created in OpenGL what happened if there already was Bake(...) method call.</exception>
	void Load(const std::string &texture);

	/// <summary>
	/// Function loads textures image form files specified by Load(...) function. After it loads textures they are converted to 32bits (if they weren't 32 bit) and uploaded to GPU.
	/// </summary>
	/// <returns>this</returns>
	/// <exception cref="TextureException">If some error occurs during loading file.</exception>
	/// <exception cref="TextureRescaleException">If cannot rescale image to required dimensions.</exception>
	/// <exception cref="TextureAlreadyBakedException">If textures were already baked.</exception>
	TextureA* Bake();

	/// <summary>
	/// Function delete textures from OpenGL and set baked flag to false.
	/// </summary>
	/// <returns>this</returns>
	TextureA* Burn();

	/// <summary>
	/// Delete all loaded texture names.
	/// </summary>
	/// <returns>this</returns>
	TextureA* ClearTextures();

	/// <summary>
	/// Gets the index of the texture.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <returns>GLuint.</returns>
	GLuint GetTextureIndex(std::string &name);

	/// <summary>
	/// Draws the specified unit.
	/// </summary>
	/// <param name="unit">The unit.</param>
	void Draw(uint unit);

	//TextureA* SetShader

	/// <summary>
	/// Finalizes an instance of the <see cref="TextureA"/> class.
	/// </summary>
	~TextureA();

private:
	
	std::vector<std::string> m_textureNames;
	std::map<std::string, GLuint> m_textureIndex;
	bool m_baked;
	GLuint m_texture;
	

};

#pragma region exceptions

/// <summary>
/// Class TextureAException.
/// </summary>
/// <seealso cref="BasicException" />
class TextureAException : public BasicException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="TextureAException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	TextureAException(std::string msg) : BasicException(msg) {};
};

/// <summary>
/// Class TextureAlreadyBakedException.
/// </summary>
/// <seealso cref="TextureAException" />
class TextureAlreadyBakedException : public TextureAException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="TextureAlreadyBakedException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	TextureAlreadyBakedException(std::string msg) : TextureAException(msg) {};
};

/// <summary>
/// Class TextureRescaleException.
/// </summary>
/// <seealso cref="TextureAException" />
class TextureRescaleException : public TextureAException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="TextureRescaleException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	TextureRescaleException(std::string msg) : TextureAException(msg) {};
};

/// <summary>
/// Class TextureNotFound.
/// </summary>
/// <seealso cref="TextureAException" />
class TextureNotFound : public TextureAException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="TextureNotFound"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	TextureNotFound(std::string msg) : TextureAException(msg) {};

	/// <summary>
	/// Gets the info excetpion MSG.
	/// </summary>
	/// <returns>std.string.</returns>
	std::string GetMsg() { return "Texture not found /n" + message; }
};



#pragma endregion 

