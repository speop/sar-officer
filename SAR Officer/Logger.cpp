#include "Logger.h"
#include "MainGame.h"
#include <ios>
#include <io.h>
#include <fcntl.h>

/// <summary>
/// The logger instance{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
Logger* Logger::LoggerInstance = nullptr;
Logger::Logger()
{
}


Logger* Logger::GetLogger()
{
	if (Logger::LoggerInstance == nullptr) {
		Logger::LoggerInstance = new Logger();
	}

	return LoggerInstance;
}

Logger* Logger::HideConsole()
{
	m_console = false;
#ifdef _WIN32
	m_winpid = GetCurrentProcessId();
	FreeConsole();
#endif

	return this;
}

Logger* Logger::Log(Logger::Level level, std::string msg)
{
	bool succeed = m_console;
	if(!m_console) {
#ifdef _WIN32
		succeed = AllocConsole() !=0;
		if(succeed ) {
			RedirectIO();
			SetForegroundWindow(MainGame::Get()->GetHWND());
		}
#endif
	}
	
	if (succeed) {
		m_console = true;
		ChangeColor(level);
		std::cerr << msg << std::endl;
		ResetColor();
	}
	return this;
}


void Logger::ChangeColor(Logger::Level level)
{
	int color = level & 15;

#ifdef _WIN32
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
#endif
}

void Logger::ResetColor()
{
#ifdef _WIN32
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, 15);
#endif
}

void Logger::RedirectIO()
{
	CinBuffer = std::cin.rdbuf();
	CoutBuffer = std::cout.rdbuf();
	CerrBuffer = std::cerr.rdbuf();
	ConsoleInput.open("CONIN$", std::ios::in);
	ConsoleOutput.open("CONOUT$", std::ios::out);
	ConsoleError.open("CONOUT$", std::ios::out);
	std::cin.rdbuf(ConsoleInput.rdbuf());
	std::cout.rdbuf(ConsoleOutput.rdbuf());
	std::cerr.rdbuf(ConsoleError.rdbuf());
}

void Logger::ResetIO()
{
	ConsoleInput.close();
	ConsoleOutput.close();
	ConsoleError.close();
	std::cin.rdbuf(CinBuffer);
	std::cout.rdbuf(CoutBuffer);
	std::cerr.rdbuf(CerrBuffer);
	CinBuffer = NULL;
	CoutBuffer = NULL;
	CerrBuffer = NULL;
}


Logger::~Logger()
{
}

#ifdef _WIN32
#undef APIENTRY 
#endif