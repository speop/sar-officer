#include "EditorUI.h"
#include "Translator.h"
#include "Editor.h"
#include <string.h>
#include "IconsFontAwesome.h"
#include "MainGame.h"
#include <future>
#include <thread>
#include <chrono>


EditorUI::EditorUI()
{
	Init();
}


EditorUI::~EditorUI()
{
}




void EditorUI::Run() {}
void EditorUI::RunImgui()
{
	ToolsPanel();
	MainPanel();
	
	if (m_saving) {

		auto status = m_asyncThreadRunnig.wait_for(std::chrono::milliseconds(0));
		//std::cout << val << std::endl;
		if (status == std::future_status::ready) {			
			m_saving = false;
		}
		else {
			SavingModal();
		}
	}
	
}

void EditorUI::Wake()
{
	m_models = Editor::Get()->Models();
	m_modelsChars = new char*[m_models.size() + 1];
	m_modelsChars[0] = "---";

	for (auto i = 0; i < m_models.size(); i++) {
		char* str = new char[m_models[i].size() + 1];
		strcpy_s(str, m_models[i].size() +1, m_models[i].c_str());
		m_modelsChars[i + 1] = str;
	}
	MainGame::Get()->DrawLighting(m_lighting);
	m_addMode = false;
	m_selectedModel = 0;
	m_lighting = false;
	m_snapToY = true;
	
}

void EditorUI::ToolsPanel()
{
	auto TR = Translator::Get();
	auto width = 900.f;
	auto margin = 40.f;


	auto display = ImGui::GetIO().DisplaySize;
	auto wX = display.x * 0.5f - width *0.5f;
	auto height = 55.f;

	ImGui::SetNextWindowPos(ImVec2(wX, margin));
	ImGui::SetNextWindowSizeConstraints(ImVec2(width, height), ImVec2(width, height));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(10.f, 10.f));
	ImGui::PushStyleColor(ImGuiCol_WindowBg, cWidgetBackground);

	ImGui::Begin("toolsPanel", nullptr,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoCollapse
	);

	auto iconBtnSize = ImVec2(35, 35);
	ImGui::PushStyleColor(ImGuiCol_Button, cBtnGreen);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cBtnGreenHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cBtnGreenActive);

	if (ImGui::Button(ICON_FA_REPEAT "##rotateR", iconBtnSize)) AlterBtn_Click(Editor::RRight);
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_UNDO "##rotateL", iconBtnSize)) AlterBtn_Click(Editor::RLeft);
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_LONG_ARROW_LEFT "##moveL", iconBtnSize)) AlterBtn_Click(Editor::MLeft);
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_LONG_ARROW_RIGHT "##moveR", iconBtnSize)) AlterBtn_Click(Editor::MRight);
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_EXPAND "##scaleP", iconBtnSize)) AlterBtn_Click(Editor::SPlus);
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_COMPRESS "##scaleM", iconBtnSize)) AlterBtn_Click(Editor::SMinus);
	ImGui::SameLine();


	auto cursorPos = ImGui::GetCursorPos();
	cursorPos.x += 40;
	ImGui::SetCursorPos(cursorPos);

	ImGui::Text(TR->Trans("Speed", "Editor", 7).c_str());
	ImGui::SameLine();
	ImGui::PushItemWidth(150);
	cursorPos = ImGui::GetCursorPos();
	cursorPos.y += 2;
	ImGui::SetCursorPos(cursorPos);
	ImGui::PushStyleColor(ImGuiCol_SliderGrab, Color("C86400"));
	ImGui::PushStyleColor(ImGuiCol_SliderGrabActive, Color("E67E22"));
	ImGui::SliderFloat("##speeds", &m_speed, 0.1f, 10.0f, "%.1f", 4.0f);
	ImGui::PopItemWidth();
	cursorPos = ImGui::GetCursorPos();
	cursorPos.y -= 2;
	ImGui::SetCursorPos(cursorPos);
	ImGui::SameLine();



	cursorPos = ImGui::GetCursorPos();
	cursorPos.x += 20;
	ImGui::SetCursorPos(cursorPos);

	std::string axis = TR->Trans("Axis", "Editor", 5) + ":";
	ImGui::Text(axis.c_str()); ImGui::SameLine();
	cursorPos = ImGui::GetCursorPos();
	cursorPos.y += 2;

	ImGui::PushStyleColor(ImGuiCol_CheckMark, Color("C86400"));
	ImGui::PushStyleColor(ImGuiCol_FrameBg, Color("75858655"));
	ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, Color("75858699"));
	ImGui::PushStyleColor(ImGuiCol_FrameBgActive, Color("758586cc"));
	ImGui::SetCursorPos(cursorPos);
	
	int axisS = m_axis;
	ImGui::RadioButton("x", &axisS, Editor::X); ImGui::SameLine();
	ImGui::RadioButton("y", &axisS, Editor::Y); ImGui::SameLine();
	ImGui::RadioButton("z", &axisS, Editor::Z); ImGui::SameLine();
	ImGui::RadioButton("xyz", &axisS, Editor::XYZ);
	m_axis = static_cast<Editor::Axis>(axisS);
	
	ImGui::SameLine(ImGui::GetWindowSize().x - iconBtnSize.x - 10);

	cursorPos = ImGui::GetCursorPos();
	cursorPos.y -= 4;
	ImGui::SetCursorPos(cursorPos);

	auto pom = ImGui::GetWindowSize().x;
	ImGui::PushStyleColor(ImGuiCol_Button, cBtnRed);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cBtnRedHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cBtnRedActive);
	if (ImGui::Button(ICON_FA_TRASH "##delete", iconBtnSize)) DeleteBtn_Click();

	ImGui::PopStyleColor(12);
	ImGui::End();
	ImGui::PopStyleVar();
	ImGui::PopStyleColor();
}
void EditorUI::MainPanel()
{
	auto TR = Translator::Get();

	auto marginT = 300.f;
	auto marginR = 50.f;
	auto marginB = 50.f;
	auto width = 250.f;
	auto display = ImGui::GetIO().DisplaySize;
	auto wX = display.x - width - marginR;
	

	ImGui::SetNextWindowPos(ImVec2(wX, marginT));
	ImGui::SetNextWindowSizeConstraints(ImVec2(width, 100), ImVec2(width, display.y - marginB));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(10.f, 10.f));
	ImGui::PushStyleColor(ImGuiCol_WindowBg, cWidgetBackground);

	ImGui::Begin("mainPanel", nullptr,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		//ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoCollapse
	); 

	ImGui::PushStyleColor(ImGuiCol_CheckMark, Color("C86400"));
	ImGui::PushStyleColor(ImGuiCol_FrameBg, Color("75858655"));
	ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, Color("75858699"));
	ImGui::PushStyleColor(ImGuiCol_FrameBgActive, Color("758586cc"));	

	auto pomBool = m_lighting;
	ImGui::Checkbox(TR->Trans("Lighting","Editor").c_str(), &m_lighting);
	if (pomBool != m_lighting) LightingBtn_Click();

	auto cursorPos = ImGui::GetCursorPos();
	cursorPos.y += 40;
	ImGui::SetCursorPos(cursorPos);


	ImGui::PushItemWidth(width - 20);
	ImGui::PushStyleColor(ImGuiCol_Button, cBtnGreen);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cBtnGreenHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cBtnGreenActive);

	std::string save = std::string(ICON_FA_FLOPPY_O) + " " + TR->Trans("BtnSave", "Buttons") + "##Save";
	if (ImGui::Button(save.c_str(), ImVec2(width - 20, 40))) SaveBtn_Click();

	ImGui::PushStyleColor(ImGuiCol_Button, cBtnRed);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cBtnRedHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cBtnRedActive);

	cursorPos = ImGui::GetCursorPos();
	cursorPos.y += 6;
	ImGui::SetCursorPos(cursorPos);

	std::string exit = std::string(ICON_FA_SIGN_OUT) + " " + TR->Trans("ExitToMainMenu", "PauseMenu") + "##Exit";
	if (ImGui::Button(exit.c_str(), ImVec2(width - 20, 40))) ExitBtn_Click();
	ImGui::PopStyleColor(3);

	cursorPos = ImGui::GetCursorPos();
	cursorPos.y += 40;
	ImGui::SetCursorPos(cursorPos);

	ImGui::PushStyleColor(ImGuiCol_Header, cDropdownItem);
	ImGui::PushStyleColor(ImGuiCol_HeaderHovered, cDropdownItemHover);
	ImGui::PushStyleColor(ImGuiCol_HeaderActive, cDropdownItemActive);

	auto pomInt = m_selectedModel;
	ImGui::Text(TR->Trans("Model", "Editor").c_str());
	ImGui::Combo("##models", &m_selectedModel, m_modelsChars, static_cast<int>(m_models.size() +1));
	if (m_selectedModel != pomInt) ModelBtn_Click();

	cursorPos = ImGui::GetCursorPos();
	cursorPos.y += 20;
	ImGui::SetCursorPos(cursorPos);

	pomBool = m_addMode;
	ImGui::Checkbox(TR->Trans("Add", "Editor").c_str(), &m_addMode);
	if (pomBool != m_addMode) AddBtn_Click();

	pomBool = m_snapToY;
	ImGui::Checkbox((TR->Trans("SnapTo", "Editor") + " Y").c_str(), &m_snapToY);
	if (m_snapToY != pomBool) SnapToYBtn_Click();

	ImGui::PopStyleColor(10);
	ImGui::End();
	ImGui::PopStyleVar();
	ImGui::PopStyleColor();
}

void EditorUI::SavingModal()
{
	bool open;
	auto TR = Translator::Get();
	auto display = ImGui::GetIO().DisplaySize;

	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::SetNextWindowSize(ImVec2(display.x, display.y));
	ImGui::PushStyleColor(ImGuiCol_WindowBg, Color("#35454688"));

	ImGui::Begin("overlay", &open,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoScrollbar |
		ImGuiWindowFlags_NoCollapse 
	);

	auto t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - m_timerPoint).count() /500;

	auto text = TR->Trans("Saving", "Editor") +" " + std::string(duration % 4, '.');
	auto textSize = ImGui::CalcTextSize(text.c_str());

	
	
	ImGui::SetCursorPos(ImVec2(0.5f*(display.x - textSize.x), 0.5f*(display.y - textSize.y)));
	ImGui::Text(text.c_str());

	ImGui::End();
	ImGui::PopStyleColor();

	
}


void EditorUI::Stop()
{
	
	for (auto i = 1; i < m_models.size() + 1; i++) {
		delete[] m_modelsChars[i];
	}
	delete[] m_modelsChars;
}
void EditorUI::Burn() {}
void EditorUI::Init()
{
	m_models = Editor::Get()->Models();
}

#pragma region buttons callbacks
void EditorUI::AlterBtn_Click(Editor::Alteration alt)
{
	Editor::Get()->AlterObject(alt, m_axis, m_speed);
}

void EditorUI::DeleteBtn_Click()
{
	Editor::Get()->DeleteObject();
}
void EditorUI::SaveBtn_Click()
{
	m_saving = true;
	m_timerPoint = std::chrono::high_resolution_clock::now();
	m_asyncThreadRunnig = std::async(std::launch::async, [] {
		Editor::Get()->Save();
		return false;
	});

	//m_worker.detach();
}
void EditorUI::ExitBtn_Click()
{
	MainGame::Get()->Menu();
}
void EditorUI::LightingBtn_Click()
{
	MainGame::Get()->DrawLighting(m_lighting);
}
void EditorUI::AddBtn_Click()
{
	Editor::Get()->AddToggle(m_addMode);
}
void EditorUI::SnapToYBtn_Click()
{
	Editor::Get()->SnapToTerrainToggle(m_snapToY);
}
void EditorUI::ModelBtn_Click()
{
	Editor::Get()->SetModel(m_selectedModel);
}

#pragma endregion 
