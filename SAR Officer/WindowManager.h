// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-15-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once

// Include standard headers
#include <stdio.h>
#include <stdlib.h>
// Include GLEW. Always include it before gl.h and glfw.h, since it's a bit magic.
#include <GL/glew.h>
// Include GLFW
#include <GLFW\glfw3.h>
// Include GLM
#include <glm/glm.hpp>
#include "TimeManager.h"
#include "ConfigStore.h"


/// <summary>
/// Class WindowManager.
/// </summary>
class WindowManager
{
#pragma region Methods


private:
	
	/// <summary>
	/// Framebuffers size callback.
	/// </summary>
	/// <param name="window">The window.</param>
	/// <param name="width">The width.</param>
	/// <param name="height">The height.</param>	
	static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
	
	/// <summary>
	/// Key press callback.
	/// </summary>
	/// <param name="windows">The windows.</param>
	/// <param name="key">The key.</param>
	/// <param name="scancode">The scancode.</param>
	/// <param name="action">The action.</param>
	/// <param name="mode">The mode.</param>
	static void key_press_callback(GLFWwindow* windows, int key, int scancode, int action, int mode);


public:
	/// <summary>
	/// Initializes a new instance of the <see cref="WindowManager"/> class.
	/// </summary>
	/// <param name="w">The w.</param>
	/// <param name="h">The h.</param>
	/// <param name="fs">The fs.</param>
	WindowManager(int w, int h, bool fs = false);
	/// <summary>
	/// Initializes this instance.
	/// </summary>
	void Init();
	/// <summary>
	/// Runs this instance.
	/// </summary>
	void Run();
	/// <summary>
	/// Check if specified key is pressed.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <returns>bool.</returns>
	bool KeyPressed(int key);

	/// <summary>
	///  Check if specified key is pressed - multiple press.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <returns>bool.</returns>
	bool KeyPressedMulitple(int key);

	/// <summary>
	/// Checks if user closed the window.
	/// </summary>
	/// <returns>bool.</returns>
	bool WindowClosed();

	/// <summary>
	/// Checks if window has focus.
	/// </summary>
	/// <returns>bool.</returns>
	bool Focus();

	/// <summary>
	/// Gets the width.
	/// </summary>
	/// <returns>int.</returns>
	int GetWidth();

	/// <summary>
	/// Gets the height.
	/// </summary>
	/// <returns>int.</returns>
	int GetHeight();

	/// <summary>
	/// Gets the window.
	/// </summary>
	/// <returns>GLFWwindow *.</returns>
	GLFWwindow* GetWindow();

	/// <summary>
	/// Sets the resolution.
	/// </summary>
	/// <param name="res">The resource.</param>
	void SetResolution(ConfigStore::ResolutionRec res);

	/// <summary>
	/// Gets the last pressed key.
	/// </summary>
	/// <returns>int.</returns>
	int GetLastKey();

	/// <summary>
	/// Finalizes an instance of the <see cref="WindowManager"/> class.
	/// </summary>
	~WindowManager();

#pragma endregion

#pragma region Properties

private:
	GLFWwindow* m_window;
	int m_width;
	int m_height;
	bool m_fullscreen;
	static int m_lastKey;

	//if we would have multiple windows we would have to do some window id mapping instead of using simple static array
	static bool m_keys[1024];

#pragma endregion


};

