﻿#include "Ray.h"
#include "CollisionManager.h"
using namespace SAR;

Ray::Ray(PerspectiveCamera* camera, float mouseX, float mouseY)
{
	m_mouseX = mouseX;
	m_mouseY = mouseY;

	auto view = camera->GetViewport();
	m_screenW = static_cast<float>(view.x);
	m_screenH = static_cast<float>(view.y);

	m_projectionMatrix = camera->GetProjectionMatrix();
	m_inverseProjectionMatrix = camera->GetInverseProjectionMatrix();
	m_viewMatrix = camera->GetViewMatrix();

	CalculateRay();
}

std::pair<bool, float> Ray::IntersectAABB(const glm::vec3& minCorner, const glm::vec3& maxCorner) const
{
	float t1 = (minCorner.x - m_origin.x) * m_invRay.x;
	float t2 = (maxCorner.x - m_origin.x) * m_invRay.x;
	float t3 = (minCorner.y - m_origin.y) * m_invRay.y;
	float t4 = (maxCorner.y - m_origin.y) * m_invRay.y;
	float t5 = (minCorner.z - m_origin.z) * m_invRay.z;
	float t6 = (maxCorner.z - m_origin.z) * m_invRay.z;

	float tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
	float tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));

	// if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behing us
	if (tmax < 0) {

		return std::make_pair(false, 0.f);
	}

	// if tmin > tmax, ray doesn't intersect AABB
	if (tmin > tmax) {
		return std::make_pair(false, 0.f);
	}

	return std::make_pair(true, tmin);

	/*
	float t, tmin, tmax, tymin, tymax, tzmin, tzmax;
	glm::vec3 bounds[2];

	bounds[0] = minCorner;
	bounds[1] = maxCorner;

	tmin = (bounds[m_sign[0]].x - m_origin.x) * m_invRay.x;
	tmax = (bounds[1 - m_sign[0]].x - m_origin.x) *  m_invRay.x;
	tymin = (bounds[m_sign[1]].y - m_origin.y) * m_invRay.y;
	tymax = (bounds[1 - m_sign[1]].y - m_origin.y) *  m_invRay.y;

	if (tmin > tymax || tmax < tymin) return std::make_pair(false, 0.f);
	
	if (tymin > tmin) tmin = tymin;
	if (tymax < tmax) tmax = tymax;

	tzmin = (bounds[m_sign[2]].z - m_origin.z) * m_invRay.z;
	tzmax = (bounds[1 - m_sign[2]].z - m_origin.z) *  m_invRay.z;

	if (tmin > tzmax || tmax < tzmin) return std::make_pair(false, 0.f);

	if (tzmin > tmin) tmin = tzmin;
	if (tzmax < tmax) tmax = tzmax;

	t = tmin;

	if (t < 0) {
		t = tmax;
		if (t < 0) return std::make_pair(false, 0.f);;
	}
	*/
}

std::pair<bool, glm::vec3> Ray::IntersectTerrain(float maxRayLength) const
{
	auto ym = CollisionManager::Instance().GetHeightInterpolated(glm::vec2(m_origin.x, m_origin.z), true);
	//std::cout << "Origin y: " << m_origin.y << " Y: " << ym << std::endl;


	for (float i = 0; i < maxRayLength; i++) {
		auto p = m_origin + m_ray * i;
		auto y = CollisionManager::Instance().GetHeightInterpolated(glm::vec2(m_origin.x, m_origin.z), true);
		auto d = p.y - y;

		if (m_origin.y >= 0 && d <= 0 || m_origin.y <= 0 && d >= 0) {
			p = i > 1 ? m_origin + m_ray * (i - 1) : p;
			return std::make_pair(true, p);
		}
	}

	return std::make_pair(true, glm::vec3(0));
}

void Ray::Print()
{
	std::cout << "Origin: X: " << m_origin.x << " Y: " << m_origin.y << " Z: " << m_origin.z << std::endl;
	std::cout << "X: " << m_ray.x << " Y: " << m_ray.y << " Z: " << m_ray.z << std::endl << std::endl;
}


void Ray::CalculateRay()
{
	float x = (2.0f * m_mouseX) / m_screenW - 1.0f;
	float y = 1.0f - (2.0f * m_mouseY) / m_screenH;
	auto normalizedCoords = glm::vec2(x, y);

	auto clipCoords = glm::vec4(normalizedCoords, -1.f, 1.f);

	auto eyeCoords = m_inverseProjectionMatrix * clipCoords;
	eyeCoords = glm::vec4(eyeCoords.x, eyeCoords.y, -1.f, 0.f);

	auto inverseView = glm::inverse(m_viewMatrix);
	auto rayWorld = inverseView * eyeCoords;
	m_ray = glm::normalize(glm::vec3(rayWorld.x, rayWorld.y, rayWorld.z));
	auto origin = inverseView * glm::vec4(0, 0, 0, 1);
	m_origin = glm::vec3(origin.x, origin.y, origin.z);

	m_invRay = 1.f / m_ray;
	m_sign[0] = (m_invRay.x < 0);
	m_sign[1] = (m_invRay.y < 0);
	m_sign[2] = (m_invRay.z < 0);
}
