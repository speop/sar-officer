#include "ForrestManager.h"
#include "MainGame.h"
#include "SceneManager.h"
#include "Editor.h"
#include <random>




ForrestManager::ForrestManager(std::shared_ptr<GBuffer> gBuffer)
{
	m_GBuffer = gBuffer;
}


ForrestManager::~ForrestManager()
{
	
}


void ForrestManager::Init()
{
	MainGame::Get()->LoadingProgressStackPushP(1.f / 2.f);
	

	std::vector<glm::mat4> positions;	
	
	//define trees files
	std::vector<std::pair<std::string, std::vector<Model::ModelSaveRecord>>> forrestModels;
	forrestModels.push_back({ "Trees", ConfigStore::Instance()->GetModels("Trees") });
	forrestModels.push_back({ "Rocks", ConfigStore::Instance()->GetModels("Rocks") });
	forrestModels.push_back({ "Forrest obj", ConfigStore::Instance()->GetModels("ForrestObjects") });
	

	uint nameUID = 101;
	MainGame::Get()->LoadingProgressStackPushP(1.f / (forrestModels[0].second.size() + forrestModels[1].second.size() + forrestModels[2].second.size()));
	MainGame::Get()->LoadingProgressStackPushP(1.f / 2.f);
	auto sManager = SceneManager::Get();

	for (auto &models : forrestModels) {
		//loading all tree models
		for (auto &it : models.second) {
			Tree tree;

			try {

				try { LoadPositions(it.positions, positions, it.hasY); }
				catch (ModelParamsFileErrorException &ex) { throw BasicException("Model " + it.name + " positions file error, please reinstall game"); }

				auto model = std::make_shared<Model>(it.path);
				std::string objName = it.name + std::to_string(nameUID++);

				tree.model = model;
				tree.objName = objName;
				tree.direct = !it.indirect;
				tree.positions = positions;
				m_trees.push_back(tree);
				SceneManager::SceneRecord objectTemplate;

				MainGame::Get()->LoadingProgressStackInc();

				if (it.indirect) {
					auto aabb = tree.model->SetupForGBuffer(m_GBuffer, objName, it.alphaDiscard);
					std::vector<ModelLocation> objectsLocation;
					ModelLocation::Create(tree.positions, objectsLocation, aabb.first, aabb.second);
					auto matrixPosPtrs = m_GBuffer->AddInstances(objectsLocation, objName);
					objectTemplate = sManager->AddObject(objName, models.first, it.positions, aabb.first, aabb.second, matrixPosPtrs.get(), m_GBuffer);

				}
				else {___
					auto aabb = tree.model->SetupForMesh(it.alphaDiscard);___
					std::vector<std::shared_ptr<DataPtr>> instancePtrs;___
					tree.model->SetupInstancesTransformation(tree.positions, instancePtrs);___
					objectTemplate = sManager->AddObject(objName, models.first, it.positions, aabb.first, aabb.second, &instancePtrs, model);___
				}

				Editor::Get()->AddModelTemplate(it.name, objectTemplate);
				MainGame::Get()->LoadingProgressStackInc();
				positions.clear();
			}
			catch (ModelBasicException &ex) {
				Logger::GetLogger()->Log(Logger::Warning, ex.GetMsg());
			}


		}
	}

	MainGame::Get()->LoadingProgressStackPop();
	MainGame::Get()->LoadingProgressStackPop();

	___

	m_shader = std::make_shared<Shader>();
	m_shader->BuildShaders({ ShaUN(GL_VERTEX_SHADER, "Shaders/Imodel.vert"), ShaUN(GL_FRAGMENT_SHADER, "Shaders/Imodel.frag") });
	
	std::string additionalCode = "#define SHADER_SHADOW \n";
	m_shadowShader = std::make_shared<Shader>();	
	m_shadowShader->BuildShaders({
		ShaUN(GL_VERTEX_SHADER, "Shaders/Imodel.vert",additionalCode),
		ShaUN(GL_GEOMETRY_SHADER, "Shaders/CSM.geom", " #define DRAWID \n"),
		ShaUN(GL_FRAGMENT_SHADER, "Shaders/Imodel.frag",additionalCode) });

	MainGame::Get()->LoadingProgressStackInc();
	MainGame::Get()->LoadingProgressStackPop();
}

// Life is like a box of chocolates. It doesn't last long if you're fat.. so.. Run Forrest run!!!

void ForrestManager::Run(RenderPass pass)
{
	auto shader = true ? m_shader : m_shadowShader;
	


	for (auto &tree : m_trees) {
		if (!tree.direct) continue;
		tree.model->Draw(shader,0, true);___
	}

}
