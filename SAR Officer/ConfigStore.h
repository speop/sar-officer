// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 01-03-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-16-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <string>
#include <stack>
#include <vector>
#include "Exceptions.h"
#include "Model.h"
#include "pugixml.hpp"


/// <summary>
/// Class ConfigStore.
/// </summary>
class ConfigStore
{
public:
	
	struct LangRec {
		std::string path;
		std::string name;
		std::string key;
	};

	
	struct ResolutionRec {
		int x;
		int y;
	};

	
	struct SoundRec {
		std::string path;
		std::string name;
		float volume;
	};


	/// <summary>
	/// Get singleton instance.
	/// </summary>
	/// <returns>ConfigStore *.</returns>
	static ConfigStore*  Instance() {
		static ConfigStore myInstance;
		return &myInstance;
	}

	/// <summary>
	/// Pushes the configuration to stack.
	/// </summary>
	/// <param name="data">The data.</param>
	/// <returns>ConfigStore *.</returns>
	ConfigStore* PushSetConfig(const std::string &data);

	/// <summary>
	/// Pops the configuration from stack.
	/// </summary>
	/// <returns>ConfigStore *.</returns>
	ConfigStore* PopSetConfig();

	/// <summary>
	/// Saves configuration.
	/// </summary>
	/// <returns>ConfigStore *.</returns>
	ConfigStore* Save();

	// delete copy and move constructors and assign operators
	ConfigStore(ConfigStore const&) = delete;             // Copy construct
	ConfigStore(ConfigStore&&) = delete;                  // Move construct
	ConfigStore& operator=(ConfigStore const&) = delete;  // Copy assign
	ConfigStore& operator=(ConfigStore &&) = delete;      // Move assign


	//public methods
#pragma region Config getters and setters
	//ApplicationData region
	std::vector<std::string> GetTerrainKeyPath(const std::string &key, uint maxChildren = 0);
	std::vector<Model::ModelSaveRecord> GetModels(const std::string &key);
	std::vector<std::string> GetModels();
	std::string GetMainMenuBackground();
	std::string GetMainMenuSound();
	std::string GetLanguage();
	ConfigStore* SetLanguage(std::string key);
	std::vector<LangRec> GetLanguages();
	bool EditorEnabled();
	std::vector<ResolutionRec> GetResolutions();
	glm::vec3 GetSunColor(const std::string &name);
	float GetSunFloat(const std::string &nodeName, const std::string &name);
	std::vector<SoundRec> GetSounds(const std::string &category, const std::string &section);


	//UserData region
	int GetXRes();
	ConfigStore* SetXres(int xRes);
	int GetYRes();
	ConfigStore* SetYres(int yRes);
	bool GetFullscreen();
	ConfigStore* SetFullscreen(bool fs);
	float GetMusicLevel();
	ConfigStore * SetMusicLevel(float level);
	float GetAudioFXlevel();
	ConfigStore * SetAudioFXlevel(float level);

#pragma endregion
		

protected:

	/// <summary>
	/// Initializes a new instance of the <see cref="ConfigStore"/> class.
	/// </summary>
	ConfigStore();
	/// <summary>
	/// Finalizes an instance of the <see cref="ConfigStore"/> class.
	/// </summary>
	~ConfigStore();

	/// <summary>
	/// Loads the configuration.
	/// </summary>
	/// <param name="filename">The filename.</param>
	void LoadConfig(const std::string &filename);

protected:
	
	const std::string defaultConfig = "../resources/SAR.cfg";
	std::string m_currentConfig;
	std::stack<std::string> m_configFilesStack;
	pugi::xml_document  m_configData;
	pugi::xml_node m_rootNode;
	pugi::xml_node m_UserData;
	pugi::xml_node m_ApplicationData;
};

#pragma region Exceptions 

/// <summary>
/// Class ConfigStoreException.
/// </summary>
/// <seealso cref="BasicException" />
class ConfigStoreException : public BasicException {
public:
	ConfigStoreException(std::string msg) : BasicException(msg) {};
};

/// <summary>
/// Class ConfigCoruptedException.
/// </summary>
/// <seealso cref="ConfigStoreException" />
class ConfigCoruptedException : public ConfigStoreException {
public:
	
	ConfigCoruptedException(std::string msg) : ConfigStoreException(msg) {};
};

#pragma endregion

