#include "Camera.h"
#include <iostream>
#include "SceneManager.h"
// TransformedVector = TranslationMatrix * RotationMatrix * ScaleMatrix * OriginalVector;
// mvp = Projection * View * Model;


glm::mat4 Camera::GetMVP()
{
	return MVP;
}

glm::mat4 Camera::GetProjectionMatrix()
{
	return this->projectionMatrix;
}

glm::mat4 Camera::GetViewMatrix()
{
	return this->viewMatrix;
}

glm::ivec2 Camera::GetViewport()
{
	int xres, yres;
	glfwGetWindowSize(this->m_wManager->GetWindow(), &xres, &yres);

	return glm::ivec2(xres, yres);
}

/// <summary>
/// The instances{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
std::map<std::string, PerspectiveCamera*> PerspectiveCamera::_instances;

PerspectiveCamera::PerspectiveCamera(WindowManager* wm)
{
	m_wManager = wm;
	CountDirection();
	Recalc();
}

void PerspectiveCamera::CountDirection()
{
	this->m_directionVector = glm::vec3(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
	);
}

void PerspectiveCamera::CountRight()
{
	// Right vector
	this->rightVector = glm::vec3(
		sin(horizontalAngle - 3.14f / 2.0f),
		0,
		cos(horizontalAngle - 3.14f / 2.0f)
	);
}

void PerspectiveCamera::CountUp()
{
	// Up vector : perpendicular to both direction and right
	this->upVector = glm::cross(this->rightVector, this->m_directionVector);
}

void PerspectiveCamera::CountVectors()
{
	CountDirection();
	CountRight();
	CountUp();
}

void PerspectiveCamera::CalculateFrustrum()
{
	//auto mat = glm::transpose(MVP);
	auto mvp = glm::value_ptr(MVP);
	
	int sign;
	int indexOffset = -1;
	
	for(auto i =0; i <6; i++ ) {
		if(!(i % 2)) {
			sign = 1;
			indexOffset++;
		}
		else {
			sign = -1;
		}

		m_frustrum[i].x = mvp[3] + sign * mvp[indexOffset + 0];
		m_frustrum[i].y = mvp[7] + sign * mvp[indexOffset + 4];
		m_frustrum[i].z = mvp[11] + sign * mvp[indexOffset + 8];
		m_frustrum[i].w = mvp[15] + sign * mvp[indexOffset + 12];
	}
	

}



PerspectiveCamera * PerspectiveCamera::GetCamera(std::string cameraName, WindowManager* wm)
{
	PerspectiveCamera *cam;
	if (_instances.find(cameraName) == _instances.end()) {
		cam = new PerspectiveCamera(wm);
		_instances[cameraName] = cam;
	}
	else {
		cam = _instances[cameraName];
	}

	return cam;
}

PerspectiveCamera * PerspectiveCamera::GetCameraInstance(std::string cameraName)
{
	PerspectiveCamera *cam = nullptr;
	if (_instances.find(cameraName) != _instances.end()) {
		cam = _instances[cameraName];
	}

	return cam;
}

void PerspectiveCamera::Recalc()
{
	
	// Projection matrix : 45&deg; Field of View, ratio, display range : 0.1 unit <-> 10000 units
	projectionMatrix = glm::perspective(this->initialFoV, float(this->m_wManager->GetWidth()) / float(this->m_wManager->GetHeight()), 0.1f, 10000.0f);

	//PerspectiveCamera matrix
	viewMatrix = glm::lookAt(
		this->position,           // PerspectiveCamera is here
		this->position + this->m_directionVector, // and looks here : at the same position, plus "direction"
		this->upVector                // Head is up (set to 0,-1,0 to look upside-down)
	);


	//MVP
	MVP = projectionMatrix * viewMatrix;
	CalculateFrustrum();

	//std::cout << "PerspectiveCamera pos: " << this->position.x << ", " << this->position.y << ", " << this->position.z << ", " << std::endl;

}

glm::vec3 PerspectiveCamera::ProcessMove(PerspectiveCamera::MoveEnum dir)
{
	
	glm::vec3 dirVec;	


	switch (dir)
	{
	case PerspectiveCamera::UP:
		dirVec = m_directionVector;		
		break;
	case PerspectiveCamera::LEFT:
		dirVec = - this->rightVector;
		break;
	case PerspectiveCamera::DOWN:
		dirVec = -m_directionVector;
		break;
	case PerspectiveCamera::RIGHT:
		dirVec = this->rightVector;
		break;
	case PerspectiveCamera::NONE:
		break;
	default:
		break;
	}

	
	return dirVec;
}

void PerspectiveCamera::Move(const glm::vec3& movementDirection)
{
	auto deltaTime = TimeManager::GetInstance()->GetDeltaTime();
	position += movementDirection * deltaTime * speed;
}

void PerspectiveCamera::ProcessMove(float hAngle, float vAngle)
{
	auto deltaTime = TimeManager::GetInstance()->GetDeltaTime();
	horizontalAngle += mouseSpeed * deltaTime * hAngle;
	verticalAngle += mouseSpeed * deltaTime * vAngle;
	CountVectors();
}



glm::mat4 PerspectiveCamera::GetMVP(glm::mat4 model)
{
	return  this->projectionMatrix * this->viewMatrix * model;
}


glm::vec3 PerspectiveCamera::GetPosition()
{
	return this->position;
}

void PerspectiveCamera::SetPosition(const glm::vec3& pos)
{
	this->position = pos;
}


glm::mat4 PerspectiveCamera::GetInverseProjectionMatrix()
{
	return Matrix::perspectiveInverse(this->initialFoV, float(this->m_wManager->GetWidth())/float(this->m_wManager->GetHeight()), 0.1f, 10000.0f);
}

glm::vec4* PerspectiveCamera::GetFrustrum()
{
	return m_frustrum;
}

PerspectiveCamera::~PerspectiveCamera()
{
}

/// <summary>
/// The m instances{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
std::map<std::string, DirectCamera*> DirectCamera::m_instances;
DirectCamera* DirectCamera::GetCamera(std::string cameraName, WindowManager* wm, float nearPlane, float farPlane, float fov)
{
	DirectCamera *cam;
	if (m_instances.find(cameraName) == m_instances.end()) {
		cam = new DirectCamera(wm, nearPlane, farPlane, fov);
		m_instances[cameraName] = cam;
	}
	else {
		cam = m_instances[cameraName];
	}

	return cam;
}

DirectCamera* DirectCamera::GetCameraInstance(std::string cameraName)
{
	DirectCamera *cam = nullptr;
	if (m_instances.find(cameraName) != m_instances.end()) {
		cam = m_instances[cameraName];
	}

	return cam;
}

DirectCamera::DirectCamera(WindowManager* wm, float nearPlane, float farPlane, float fov)
{
	m_wManager = wm;
	m_near = nearPlane;
	m_far = farPlane;	
	
	float ratio =  m_wManager->GetWidth()/ static_cast<float>(m_wManager->GetHeight());
	for (int i = 0; i < 8; ++i) {
		auto iP = glm::inverse(glm::perspective(fov, ratio, nearPlane, farPlane));
		glm::vec4 v;
		for (int j = 0; j<3; ++j)
			v[j] = 1.f*(-1.f + 2.f*((i >> j) & 1));
		v[3] = 1.f;
		v = iP*v;
		v.x /= v.w;
		v.y /= v.w;
		v.z /= v.w;
		v.w = 1.f;
		m_frustrumCorners[i] = v;
	}
}

void DirectCamera::Recalc(const glm::vec3 &lightPos, const glm::mat4 &inverseView, glm::vec2 margin)
{
	viewMatrix = glm::lookAt(lightPos, glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.0f, 1.0f, 0.0f));
	
	float minX, minY, minZ, maxX, maxY, maxZ;
	minZ =  minY = minX = (std::numeric_limits<float>::max)();
	maxZ = maxY = maxX = -minZ;

	//calculating frustrum BB
	for(auto i = 0; i < 8; i++) {
		glm::vec4 worldSpaceFrustrum = inverseView * m_frustrumCorners[i];
		glm::vec4 lightSpaceFrustrum = viewMatrix * worldSpaceFrustrum;

		minX = (std::min)(minX, lightSpaceFrustrum.x);
		minY = (std::min)(minY, lightSpaceFrustrum.y);
		minZ = (std::min)(minZ, lightSpaceFrustrum.z);

		maxX = (std::max)(maxX, lightSpaceFrustrum.x);
		maxY = (std::max)(maxY, lightSpaceFrustrum.y);
		maxZ = (std::max)(maxZ, lightSpaceFrustrum.z);
	}


	
	auto planes = ComputeNearAndFar(minX, maxX, minY, maxY);
	projectionMatrix = glm::ortho<float>(minX - margin.x, maxX + margin.x, minY - margin.y, maxY + margin.y, -std::get<1>(planes), -std::get<0>(planes));

	MVP = projectionMatrix * viewMatrix;
}


//adopted from DirectX 11 example
std::tuple<float, float> DirectCamera::ComputeNearAndFar(float xmin, float xmax, float ymin, float ymax)
{
	auto sceneAABB = SceneManager::Get()->GetSceneAABB();
	glm::vec3 minLightSceneABBA = sceneAABB.first;
	glm::vec3 maxLightSceneABBA = sceneAABB.second;

	
	//calculating light space positions of all scene AABB vertexes
	ModelLocation::RecalcAABB(minLightSceneABBA, maxLightSceneABBA, viewMatrix);
	glm::vec3 SceneAABBPointsLightSpace[8];

	bool fx = true;
	bool fy = true;
	bool fz = true;

	for (uint i = 0; i < 8; i++) {

		fz = !fz;
		if (i % 2 == 0) fy = !fy;
		if (i % 4 == 0) fx = !fx;

		auto z = fz ? minLightSceneABBA.z : maxLightSceneABBA.z;
		auto y = fy ? minLightSceneABBA.y : maxLightSceneABBA.y;
		auto x = fx ? minLightSceneABBA.x : maxLightSceneABBA.x;

		SceneAABBPointsLightSpace[i] = { x,y,z };
	}


	static const int iAABBTriIndexes[] =
	{
		0,2,1,	1,2,3,
		5,0,1,	5,4,0,
		0,4,2,	4,6,2,
		5,4,7,	4,6,7,
		3,7,2,	7,6,2,
		1,7,3,	1,5,7,


		/*0,1,2,  1,2,3,
		4,5,6,  5,6,7,
		0,2,4,  2,4,6,
		1,3,5,  3,5,7,
		0,1,4,  1,4,5,
		2,3,6,  3,6,7*/
	};

	int pointPassesCollision[3];

	auto nearPlane = std::numeric_limits<float>::infinity();
	auto farPlane = -nearPlane;

	//array for triangles generated by clipping from frustrum trianlge. Index = 0 is basic triangle
	Triangle triangles[16];
	int triangleCnt = 1;

	float frustrumLines[4] = { xmin, xmax, ymin, ymax };

	//iterate over all frustrum triangles
	for (int AABBtriIter = 0; AABBtriIter < 12; ++AABBtriIter) {
		triangles[0].vertices[0] = SceneAABBPointsLightSpace[iAABBTriIndexes[AABBtriIter * 3 + 0]];
		triangles[0].vertices[1] = SceneAABBPointsLightSpace[iAABBTriIndexes[AABBtriIter * 3 + 1]];
		triangles[0].vertices[2] = SceneAABBPointsLightSpace[iAABBTriIndexes[AABBtriIter * 3 + 2]];
		triangles[0].culled = false;
		triangleCnt = 1;


		// Clip each invidual triangle against the 4 frustums.  When ever a triangle is clipped into new triangles, 
		//add them to the list.
		for (auto frustrumPlaneIter = 0; frustrumPlaneIter < 4; frustrumPlaneIter++) {
			float edge;
			int component;

			edge = frustrumLines[frustrumPlaneIter];
			component = frustrumPlaneIter < 2 ? 0 : 1;



			for (auto triIter = 0; triIter < triangleCnt; triIter++) {
				int verticesInside = 0;
				glm::vec3 tempVertex;

				//because there is no triangle deletion we have to skip culled triangles
				if (!triangles[triIter].culled) {


					for (auto triVertexIt = 0; triVertexIt < 3; triVertexIt++) {
						auto point = frustrumPlaneIter < 2 ? triangles[triIter].vertices[triVertexIt].x : triangles[triIter].vertices[triVertexIt].y;
						auto left = frustrumPlaneIter % 2 == 0 ? frustrumLines[frustrumPlaneIter] : point;
						auto right = frustrumPlaneIter % 2 == 0? point : frustrumLines[frustrumPlaneIter];
						
						pointPassesCollision[triVertexIt] = left < right ? 1 : 0;
						
						verticesInside += pointPassesCollision[triVertexIt];
					}

				
					// Move the points that pass the frustum test to the begining of the array.
					for (int i = 0; i < 3; i++) {						
						auto s = i%2;

						if (pointPassesCollision[s+1] && !pointPassesCollision[s]) {
							tempVertex = triangles[triIter].vertices[s+1];
							triangles[triIter].vertices[s+1] = triangles[triIter].vertices[s];
							triangles[triIter].vertices[s] = tempVertex;
							pointPassesCollision[s + 1] = false;
							pointPassesCollision[s] = true;
						}

					}


					if (verticesInside == 0) {
						//all vertices culled
						triangles[triIter].culled = true;
					}
					else if (verticesInside == 1) {
						// One point passed. Clip the triangle against the Frustum plane
						triangles[triIter].culled = false;

						glm::vec3 vert0ToVert1 = triangles[triIter].vertices[1] - triangles[triIter].vertices[0];
						glm::vec3 vert0ToVert2 = triangles[triIter].vertices[2] - triangles[triIter].vertices[0];

						float hitPointTimeRatio = edge - triangles[triIter].vertices[0][component];
						float distanceAlongVector01 = hitPointTimeRatio / vert0ToVert1[component];
						float distanceAlongVector02 = hitPointTimeRatio / vert0ToVert2[component];

						vert0ToVert1 *= distanceAlongVector01;
						vert0ToVert1 += triangles[triIter].vertices[0];

						vert0ToVert2 *= distanceAlongVector02;
						vert0ToVert2 += triangles[triIter].vertices[0];

						triangles[triIter].vertices[1] = vert0ToVert1;
						triangles[triIter].vertices[2] = vert0ToVert2;

					}


					else if (verticesInside == 2) {
						// 2 in  tesselate into 2 triangles
						triangles[triangleCnt] = triangles[triIter + 1];
						triangles[triIter].culled = false;
						triangles[triIter + 1].culled = false;

						glm::vec3 vert2ToVert0 = triangles[triIter].vertices[0] - triangles[triIter].vertices[2];
						glm::vec3 vert2ToVert1 = triangles[triIter].vertices[1] - triangles[triIter].vertices[2];

						float hitPointTimeRatio2_0 = edge - triangles[triIter].vertices[2][component];
						float distanceAlongVector2_0 = hitPointTimeRatio2_0 / vert2ToVert0[component];

						// Calcaulte the new vert by adding the percentage of the vector plus point 2.
						vert2ToVert0 *= distanceAlongVector2_0;
						vert2ToVert0 += triangles[triIter].vertices[2];

						// Add a new triangle.
						triangles[triIter + 1].vertices[0] = triangles[triIter].vertices[0];
						triangles[triIter + 1].vertices[1] = triangles[triIter].vertices[1];
						triangles[triIter + 1].vertices[2] = vert2ToVert0;



						float hitPointTimeRatio2_1 = edge - triangles[triIter].vertices[2][component];
						float distanceAlongVector2_1 = hitPointTimeRatio2_1 / vert2ToVert1[component];

						vert2ToVert1 *= distanceAlongVector2_1;
						vert2ToVert1 += triangles[triIter].vertices[2];

						triangles[triIter].vertices[0] = triangles[triIter + 1].vertices[1];
						triangles[triIter].vertices[1] = triangles[triIter + 1].vertices[2];
						triangles[triIter].vertices[2] = vert2ToVert1;

						// increment triangle count and skip the triangle we just inserted.
						triangleCnt++;
						triIter++;
					}
					else {
						//all in
						triangles[triIter].culled = false;
					}
				}
			}

		}
		for (int index = 0; index < triangleCnt; index++) {
			if (triangles[index].culled) continue;

			for (int vertexIndex = 0; vertexIndex < 3; vertexIndex++) {
				nearPlane = (std::min)(nearPlane, triangles[index].vertices[vertexIndex].z);
				farPlane = (std::max)(farPlane, triangles[index].vertices[vertexIndex].z);
			}
		}

	}

	return{ nearPlane, farPlane };



}

std::pair<glm::vec4, glm::vec4> DirectCamera::WorldBB()
{
	auto invView = glm::inverse(viewMatrix);
	auto min = invView * m_viewMinCorner;
	auto max = invView * m_viewMaxCorner;
	return{ min,max };
}

void DirectCamera::Recalc() {}

namespace Matrix {

	template <typename T>
	/// <summary>
	/// Create inversion of perspective matrix. If you supply same arguments as you supplied for perpsective matrix you get its inversion. 
	/// But this doesn't work every time. Some nasty perspective matrix are inreversible.
	/// 
	/// </summary>
	/// <param name="fovy"></param>
	/// <param name="aspect"></param>
	/// <param name="zNear"></param>
	/// <param name="zFar"></param>
	/// <returns></returns>
	inline glm::tmat4x4<T, glm::defaultp> perspectiveInverse(
		T const & fovy,
		T const & aspect,
		T const & zNear,
		T const & zFar)
	{
		glm::tmat4x4<T, glm::defaultp> m(static_cast<T>(0));

		const T tanHalfFovy = tan(fovy / static_cast<T>(2));
		m[0][0] = tanHalfFovy * aspect;
		m[1][1] = tanHalfFovy;
		m[3][2] = static_cast<T>(-1);

		const T d = static_cast<T>(2) * zFar * zNear;
		m[2][3] = (zNear - zFar) / d;
		m[3][3] = (zFar + zNear) / d;

		return m;
	}
};