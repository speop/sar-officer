// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-02-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-16-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include "Exceptions.h"
#include "pugixml.hpp"

/// <summary>
/// Class Translator.
/// </summary>
class Translator
{

public:
	/// <summary>
	/// Gets this instance.
	/// </summary>
	/// <returns>Translator *.</returns>
	static Translator* Get();
	/// <summary>
	/// Finalizes an instance of the <see cref="Translator"/> class.
	/// </summary>
	~Translator();

	/// <summary>
	/// Transes the specified key.
	/// </summary>
	/// <param name="key">The key.</param>
	/// <param name="category">The category.</param>
	/// <param name="maxLength">The maximum length.</param>
	/// <returns>std.string.</returns>
	std::string Trans(std::string key, std::string category, int maxLength = 0);
	
	/// <summary>
	/// Reloads this instance.
	/// </summary>
	void Reload();

private:
	/// <summary>
	/// Prevents a default instance of the <see cref="Translator"/> class from being created.
	/// </summary>
	Translator();
	/// <summary>
	/// Loads this instance.
	/// </summary>
	void Load();
	static Translator* instance;
	
	pugi::xml_document*  m_langData;
	pugi::xml_node m_rootNode;
};

