// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 12-09-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-05-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <string>
#include <vector>
#include <memory>
#include <map>
#include <glm/gtc/matrix_transform.hpp>
#include "assimp\Importer.hpp"
#include "assimp\scene.h"
#include "assimp\postprocess.h"
#include "Mesh.h"
#include "Shader.h"
#include "GBuffer.h"
#include "Exceptions.h"
#include "Logger.h"

#define NUM_BONES_PER_VERTEX 4
/// <summary>
/// The maximum bones
/// </summary>
const int MAX_BONES = 64;

/// <summary>
/// Class Model which represents one 3D model
/// </summary>
class Model
{
private:
	
	struct TextMapRec {
		uint Lod;
		uint Index;
		
		friend inline bool operator<(TextMapRec const& left, TextMapRec const& right) {
			if (left.Lod < right.Lod) { return true; }
			if (left.Lod > right.Lod) { return false; }
			return left.Index < right.Index;
		}
	};	

	

	using TextMap = std::map<Mesh::TextTypes, std::map<std::string, std::vector<TextMapRec>>>;
	
	/// <summary>
	/// Calculates the aabb of model.
	/// </summary>
	/// <param name="aabb">The aabb.</param>
	/// <param name="point">The point.</param>
	void CalculateAABB(std::pair<glm::vec3, glm::vec3> &aabb, const glm::vec3 &point);

public:
	
	struct ModelSaveRecord {
		std::string name;
		std::string path;
		std::string positions;
		bool hasY;
		bool indirect;
		float alphaDiscard;
	};

	/// <summary>
	/// Creates new instance of model.
	/// </summary>
	/// <param name="path">Path to model file.</param>
	/// <exception cref="ModelBasicException">If model file doesn't exists or if model is corrupted.</exception>
	Model(std::string path);

	/// <summary>
	/// Model destructor.
	/// </summary>
	~Model();
	
	/// <summary>
	/// Directly draws all models meshes. This function can also be used for direct instance drawing.
	/// </summary>
	/// <param name="shader">Id of shader program used for drawing</param>
	/// <param name="LOD">LOD which we wants to use</param>
	/// <param name="instanced">Tells if we want use instance drawing</param>
	/// <exception cref="ModelNotSetupForMesh">If model is not setup for mesh</exception>
	void Draw(std::shared_ptr<Shader> shader, int LOD = 0, bool instanced = false);

	/// <summary>
	/// Setups the instances transformation for modela loaded for mesh
	/// </summary>
	/// <param name="trans">Vector of transformation matrices.</param>
	/// <param name="dataPtrVec">Vector of data PTRs.</param> 
	/// <exception cref="ModelNotSetupForMesh">If model is not setup for mesh</exception>
	void SetupInstancesTransformation(const std::vector<glm::mat4> &trans, std::vector<std::shared_ptr<DataPtr>> &dataPtrVec);

	/// <summary>
	/// Updates the instance transformation.
	/// </summary>
	/// <param name="dataPtrVec">Vector of data PTRs to objects which should be updated.</param>
	/// <param name="trans">The trans.</param>
	void UpdateInstanceTransformation(std::shared_ptr<DataPtr> dataPtrVec, const glm::mat4 &trans);

	/// <summary>
	/// Deletes the instance transformation.
	/// </summary>
	/// <param name="dataPtr">The data PTR of object which should be delete.</param>
	/// <exception cref="ModelNotSetupForMesh">If model is not setup for mesh</exception>
	void DeleteInstanceTransformation(std::shared_ptr<DataPtr> dataPtr);

	/// <summary>
	/// Gets the instance transformation.
	/// </summary>
	/// <param name="dataPtr">The data PTR.</param>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetInstanceTransformation(std::shared_ptr<DataPtr> dataPtr);

	/// <summary>
	/// Sets the instance additional data.
	/// </summary>
	/// <param name="dataPtr">The data PTR of object.</param>
	/// <param name="data">The data.</param>
	/// <exception cref="ModelNotSetupForMesh">If model is not setup for mesh</exception>
	void SetInstanceAdditionalData(std::shared_ptr<DataPtr> dataPtr, uint data);
	

	/// <summary>
	/// This one from two functions (<see cref="SetupForGBuffer(std::shared_ptr{GBuffer})" />) for setup. This function should be used if want to use direct drawing. Calling this function should be done just after calling constructor. Function can be called only once. Animations are supported only if we use this method.
	/// </summary>
	/// <param name="alphaDiscard">The alpha discard.</param>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	/// <exception cref="ModelHadAlreadyBeenSetup">If we have already called any SetupFor* function.</exception>
	std::pair<glm::vec3, glm::vec3> SetupForMesh(float alphaDiscard);

	/// <summary>
	/// This is one from two functions (<see cref="SetupForMesh()" />) for setup. This function should be used if want to use indirect drawing. Calling this function should be done just after calling constructor. Function can be called only once.
	/// </summary>
	/// <param name="gBuffer">The g buffer.</param>
	/// <param name="modelIdentifier">The model identifier.</param>
	/// <param name="alphaDiscard">The alpha discard.</param>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	/// <exception cref="ModelHadAlreadyBeenSetup">If we have already called any SetupFor* function.</exception>
	std::pair<glm::vec3, glm::vec3> SetupForGBuffer(std::shared_ptr<GBuffer> gBuffer, std::string &modelIdentifier, float alphaDiscard);

	/// <summary>
	/// Frees the scene.
	/// </summary>
	void FreeScene();
	
	/// <summary>
	/// Finds element in map and returns its pointer. If element doesn't exist, new record is created.
	/// </summary>
	/// <param name="map">The map.</param>
	/// <param name="key">The key.</param>
	/// <returns>T *.</returns>
	template<typename T, typename K>  static inline T* MapFind(std::map<K, T>&map, K key) {
		typedef typename  std::map<K, T>::iterator MapIterator;

		MapIterator it = map.find(key);
		if (it != map.end())
		{
			// the key exists,
			return &(it->second);
		}
		else
		{
			// the key does not exist in the map
			// add it to the map
			T obj;
			std::pair<MapIterator, bool> pair = map.insert(std::make_pair(key, obj));

			//pair is <iterator, bool>
			return &(pair.first->second);
		}
	}
private:
	/// <summary>
	/// Processes the model and creates all needed structures and data.
	/// </summary>
	/// <param name="texturesMap">The textures map.</param>
	/// <param name="alphaDiscard">The alpha discard.</param>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	std::pair<glm::vec3, glm::vec3> ProcessModel(TextMap &texturesMap, float alphaDiscard);

	/// <summary>
	/// Processes the node of Assimp model.
	/// </summary>
	/// <param name="node">The node.</param>
	/// <param name="meshes">The meshes.</param>
	/// <param name="transform">The transform.</param>
	/// <param name="texturesMap">The textures map.</param>
	/// <param name="aabb">The aabb.</param>
	/// <param name="alphaDiscard">The alpha discard.</param>
	/// <param name="Lod">The lod.</param>
	void ProcessNode(aiNode* node, std::shared_ptr<std::vector<Mesh>>meshes, glm::mat4 transform, TextMap &texturesMap, std::pair<glm::vec3, glm::vec3> &aabb, float alphaDiscard, uint Lod = 0);
	
	/// <summary>
	/// Processes the one mesh.
	/// </summary>
	/// <param name="mesh">The mesh.</param>
	/// <param name="texturesMap">The textures map.</param>
	/// <param name="rec">The record.</param>
	/// <param name="aabb">The aabb.</param>
	/// <returns>Mesh.</returns>
	Mesh ProcessMesh(aiMesh* mesh, TextMap &texturesMap, TextMapRec &rec, std::pair<glm::vec3, glm::vec3> &aabb);
	
	/// <summary>
	/// Processes the materials.
	/// </summary>
	/// <param name="mat">The mat.</param>
	/// <param name="texturesMap">The textures map.</param>
	/// <param name="rec">The record.</param>
	void ProcessMaterials(aiMaterial* mat, TextMap &texturesMap, TextMapRec &rec);
	
	/// <summary>
	/// Gets the material.
	/// </summary>
	/// <param name="mat">The mat.</param>
	/// <param name="type">The type.</param>
	/// <returns>std.string.</returns>
	inline std::string GetMaterial(aiMaterial* mat, const aiTextureType type);
	
private:

  void InitMesh();
	
	std::vector<std::shared_ptr<std::vector<Mesh>>> m_meshes;
	std::string m_fileName;
	std::string m_directory;
	bool m_setForMesh = false;
	bool m_setForGBuffer = false;
	Assimp::Importer* m_assimpImporter;
	std::vector<GLuint> m_texturesFreeingInfo;


public:
	
	/// <summary>
	/// Cinvert GLMs the mat4 to ai matrix.
	/// </summary>
	/// <param name="mat">The mat.</param>
	/// <returns>aiMatrix4x4.</returns>
	static aiMatrix4x4 GLMMat4ToAi(glm::mat4 mat)
	{
		return aiMatrix4x4(mat[0][0], mat[0][1], mat[0][2], mat[0][3],
			mat[1][0], mat[1][1], mat[1][2], mat[1][3],
			mat[2][0], mat[2][1], mat[2][2], mat[2][3],
			mat[3][0], mat[3][1], mat[3][2], mat[3][3]);
	}

	/// <summary>
	/// Ais matrix to GLM mat4.
	/// </summary>
	/// <param name="in_mat">The in mat.</param>
	/// <returns>glm.mat4.</returns>
	static glm::mat4 AiToGLMMat4(aiMatrix4x4& in_mat)
	{
		glm::mat4 tmp;
		tmp[0][0] = in_mat.a1;
		tmp[1][0] = in_mat.b1;
		tmp[2][0] = in_mat.c1;
		tmp[3][0] = in_mat.d1;

		tmp[0][1] = in_mat.a2;
		tmp[1][1] = in_mat.b2;
		tmp[2][1] = in_mat.c2;
		tmp[3][1] = in_mat.d2;

		tmp[0][2] = in_mat.a3;
		tmp[1][2] = in_mat.b3;
		tmp[2][2] = in_mat.c3;
		tmp[3][2] = in_mat.d3;

		tmp[0][3] = in_mat.a4;
		tmp[1][3] = in_mat.b4;
		tmp[2][3] = in_mat.c4;
		tmp[3][3] = in_mat.d4;
		return tmp;
	}
};


#pragma region Exceptions

/// <summary>
/// Class ModelBasicException.
/// </summary>
/// <seealso cref="BasicException" />
class ModelBasicException : public BasicException {
public:
	
	ModelBasicException(std::string msg) : BasicException(msg) {};
};

/// <summary>
/// Class ModelParamsFileErrorException.
/// </summary>
/// <seealso cref="ModelBasicException" />
class ModelParamsFileErrorException : public ModelBasicException {
public:
	
	ModelParamsFileErrorException(std::string msg) : ModelBasicException(msg) {};
};

/// <summary>
/// Class ModelCannotDrawGBuffered.
/// </summary>
/// <seealso cref="ModelBasicException" />
class ModelCannotDrawGBuffered : public ModelBasicException {
public:
	
	ModelCannotDrawGBuffered(std::string msg) : ModelBasicException(msg) {};
};

/// <summary>
/// Class ModelHadAlreadyBeenSetup.
/// </summary>
/// <seealso cref="ModelBasicException" />
class ModelHadAlreadyBeenSetup : public ModelBasicException {
public:
	
	ModelHadAlreadyBeenSetup(std::string msg) : ModelBasicException(msg) {};
};

/// <summary>
/// Class ModelNotSetupForMesh.
/// </summary>
/// <seealso cref="ModelBasicException" />
class ModelNotSetupForMesh : public ModelBasicException {
public:
	
	ModelNotSetupForMesh(std::string msg) : ModelBasicException(msg) {};
};

#pragma endregion 