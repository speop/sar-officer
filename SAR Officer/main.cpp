/*
	This is our stargate. This is where everything start and finish. Are you ready for the journey? Are you ready to leave Tauri and travel via Stargate to new dimensions?
	So if you are, lets go!!
*/

#include "MainGame.h"
#include "Exceptions.h"
#include <iostream>

int main(int argc, char ** argv) {
	MainGame* SG1 = MainGame::Get(); 
	Logger::GetLogger()->HideConsole();

	try
	{		
		SG1->Start();
	}
	catch (FatalErrorException &ex) {
		std::string str = "Something went terribly wrong and program must be terminated";
		std::cerr << str << std::endl;
		Logger::GetLogger()->Log(Logger::Critical, ex.GetMsg());
		//display error box
#ifdef _WIN32		
		LPSTR s = const_cast<char *>(str.c_str());
		int msgboxID = MessageBox(NULL, s, "Runtime Error", MB_ICONERROR | MB_OK);
#endif
	}
	catch (BasicException &ex)
	{
		std::cerr << ex.GetMsg() << std::endl;

		//display error box
#ifdef _WIN32
		std::string str = ex.GetMsg();
		str = str == "" ? "Something went wrong. Application will be closed." : str;
		LPSTR s = const_cast<char *>(str.c_str());
		int msgboxID = MessageBox(NULL,s,"Runtime Error",	MB_ICONERROR | MB_OK );
#endif
	}

	return 0;
}