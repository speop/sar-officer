#include "Translator.h"
#include "ConfigStore.h"

/// <summary>
/// The instance{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
Translator* Translator::instance = nullptr;

Translator * Translator::Get()
{
	if(instance == nullptr) instance = new Translator();
	return instance;
}

Translator::Translator()
{
	Load();
}

void Translator::Load()
{
	auto lng = ConfigStore::Instance()->GetLanguage();
	auto lngs = ConfigStore::Instance()->GetLanguages();
	std::string file;

	for (auto &lang : lngs) {
		if (lang.key != lng) continue;

		file = lang.path;
		break;
	}

	m_langData = new pugi::xml_document();
	pugi::xml_parse_result result = m_langData->load_file(file.c_str());

	if (result.status != pugi::xml_parse_status::status_ok) {
		throw ConfigCoruptedException("Error code: " + std::to_string(result.status) + ", message: " + result.description());
	}


	m_rootNode = m_langData->document_element();
}


Translator::~Translator()
{
	delete m_langData;
}


std::string Translator::Trans(std::string key, std::string category, int maxLength)
{
	if (!m_rootNode) return "";
	std::string str = m_rootNode.child(category.c_str()).child(key.c_str()).child_value();

	if(maxLength > 0 ) {
		str = str.substr(0,maxLength);
	}
	return str;
}

void Translator::Reload()
{
	instance->Load();
}
