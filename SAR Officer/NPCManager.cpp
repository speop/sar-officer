#include "NPCManager.h"
#include "MainGame.h"
#include "SceneManager.h"
#include "Editor.h"


NPCManager::NPCManager()
{
}

void NPCManager::Init()
{
	MainGame::Get()->LoadingProgressStackPushP(1.f / 2.f);


	std::vector<glm::mat4> positions;

	//define trees files
	std::vector<Model::ModelSaveRecord> trees = ConfigStore::Instance()->GetModels("Animals");
	uint nameUID = 101;
	MainGame::Get()->LoadingProgressStackPushP(1.f / trees.size());
	MainGame::Get()->LoadingProgressStackPushP(1.f / 2.f);
	auto sManager = SceneManager::Get();

	//loading all tree models
	for (auto &it : trees) {
		NPC npc;

		try {

			try { LoadPositions(it.positions, positions, it.hasY); }
			catch (ModelParamsFileErrorException &ex) { throw BasicException("Model " + it.name + " positions file error, please reinstall game"); }

			auto model = std::make_shared<Model>(it.path);
			std::string objName = it.name + std::to_string(nameUID++);

			npc.animal = std::make_shared<GO::Animal>();
			npc.model = model;
			npc.positions = positions;
			m_npcs.push_back(npc);

			MainGame::Get()->LoadingProgressStackInc();

			auto aabb = npc.model->SetupForMesh(it.alphaDiscard);
			std::vector<std::shared_ptr<DataPtr>> instancePtrs;
			npc.model->SetupInstancesTransformation(npc.positions, instancePtrs);
			
			
			MainGame::Get()->LoadingProgressStackInc();
			positions.clear();
		}
		catch (ModelBasicException &ex) {
			Logger::GetLogger()->Log(Logger::Warning, ex.GetMsg());
		}


	}

	MainGame::Get()->LoadingProgressStackPop();
	MainGame::Get()->LoadingProgressStackPop();



	m_shader = std::make_shared<Shader>();
	m_shader->BuildShaders({ ShaUN(GL_VERTEX_SHADER, "Shaders/Imodel.vert"), ShaUN(GL_FRAGMENT_SHADER, "Shaders/Imodel.frag") });

	std::string additionalCode = "#define SHADER_SHADOW \n";
	m_shadowShader = std::make_shared<Shader>();
	m_shadowShader->BuildShaders({
		ShaUN(GL_VERTEX_SHADER, "Shaders/Imodel.vert",additionalCode),
		ShaUN(GL_GEOMETRY_SHADER, "Shaders/CSM.geom", " #define DRAWID \n"),
		ShaUN(GL_FRAGMENT_SHADER, "Shaders/Imodel.frag",additionalCode) });

	MainGame::Get()->LoadingProgressStackInc();
	MainGame::Get()->LoadingProgressStackPop();
}

void NPCManager::Run(RenderPass pass)
{
	auto shader = pass == CLASSIC ? m_shader : m_shadowShader;



	for (auto &npc : m_npcs) {		
		npc.model->Draw(shader, 0, true);
	}

}

NPCManager::~NPCManager()
{
}
