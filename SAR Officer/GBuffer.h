// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 01-20-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <memory>
#include <map>
#include "TextureA.h"
#include "Exceptions.h"
#include "Mesh.h"
#include "ModelLocation.h"

//class Model;
/// <summary>
/// Class DataPtr.
/// </summary>
class DataPtr {
	friend class GBuffer;
	friend class Model;

	std::vector<std::shared_ptr<int>> m_dataPositionsPtrs;
};


#pragma region Exceptions

/// <summary>
/// Class GBufferException.
/// </summary>
/// <seealso cref="BasicException" />
class GBufferException : public BasicException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="GBufferException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	GBufferException(std::string msg) : BasicException(msg) {};
};

/// <summary>
/// Class GBufferAlreadyBakedException.
/// </summary>
/// <seealso cref="GBufferException" />
class GBufferAlreadyBakedException : public GBufferException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="GBufferAlreadyBakedException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	GBufferAlreadyBakedException(std::string msg) : GBufferException(msg) {};

};

/// <summary>
/// Class GBufferInvalidDataPtrException.
/// </summary>
/// <seealso cref="GBufferException" />
class GBufferInvalidDataPtrException : public GBufferException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="GBufferInvalidDataPtrException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	GBufferInvalidDataPtrException(std::string msg) : GBufferException(msg) {};

};

/// <summary>
/// Class GBufferModelInvalidNameException.
/// </summary>
/// <seealso cref="GBufferException" />
class GBufferModelInvalidNameException : public GBufferException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="GBufferModelInvalidNameException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	GBufferModelInvalidNameException(std::string msg) : GBufferException(msg) {};

};

/// <summary>
/// Class GBufferNotBakedException.
/// </summary>
/// <seealso cref="GBufferException" />
class GBufferNotBakedException : public GBufferException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="GBufferNotBakedException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	GBufferNotBakedException(std::string msg) : GBufferException(msg) {};

};
#pragma endregion

/// <summary>
/// Buffer class. This class should be used if we want to indirectly render multiple objects. This class merge all supplied vertices, indices and textures so state changes will not be needed for rendering.
/// </summary>
class GBuffer {
public:

	


	/// <summary>
	/// Struct GBufferStruct
	/// </summary>
	struct GBufferStruct {
		friend class GBuffer;

		std::shared_ptr<const std::vector<Mesh::Vertex>> vertices;
		std::shared_ptr<const std::vector<GLuint>> indices;
		glm::mat4 transformation;
		std::map<Mesh::TextTypes, std::string> textures;
		GLfloat alphaDiscard;

	private:
		GLuint firstIndex = 0;
		GLint baseVertex = 0;
	};


private:

	
	typedef struct {
		GLuint vertexCount;
		GLuint instanceCount;
		GLuint firstIndex;
		GLint baseVertex;
		GLuint baseInstance;
	} DrawElementsIndirectCommand;

	
	typedef struct alignas(16) {
		glm::mat4 transformation;
		glm::mat4 position;
		glm::vec4 minAABB;
		glm::vec4 maxAABB;
		GLint diffuseTextureIndex;
		GLint specularTextureIndex;
		GLint normalTextureIndex;
		GLfloat alphaDiscard;
		GLuint extraData = 0x009b0011;
		GLuint padding[3];
	} SSBOStruct;

	
	struct ModelRecord {
		std::string modelName;
		std::vector<GBufferStruct> meshes;
		std::vector<std::shared_ptr<DataPtr>> meshDataPointers;
	};


public:
	/// <summary>
	/// Initializes a new instance of the <see cref="GBuffer"/> class.
	/// </summary>
	GBuffer();
	/// <summary>
	/// Finalizes an instance of the <see cref="GBuffer"/> class.
	/// </summary>
	~GBuffer();

	/// <summary>
	/// Stores meshes in internal structures.
	/// </summary>
	/// <param name="data">mesh data</param>
	/// <param name="name">The name.</param>
	/// <exception cref="GBufferAlreadyBakedException">If all buffers was already created in OpenGL what happend if there already was Bake(...) method call.</exception>
	void Load(const GBufferStruct& data, const std::string& name);

	/// <summary>
	/// Adds the texture.
	/// </summary>
	/// <param name="names">The names.</param>
	/// <param name="TextType">Type of the text.</param>
	/// <returns>GBuffer *.</returns>
	template <typename T, typename = std::enable_if_t<std::is_same<T, std::string>::value || std::is_same<T, std::vector<std::string>>::value>>
	GBuffer* AddTexture(const T& names, Mesh::TextTypes TextType)
	{
		if (m_baked) throw GBufferAlreadyBakedException("You cannot add textures file if GBuffer was already baked.");

		switch (TextType) {
		case Mesh::TextTypes::Diffuse:
			m_textureDiffuse->Load(names);
			break;

		case Mesh::TextTypes::Specular:
			m_textureSpecular->Load(names);
			break;

		case Mesh::TextTypes::Normal:
			m_textureNormal->Load(names);
			break;
		}

		return this;
	}

	/// <summary>
	/// Adds the instances.
	/// </summary>
	/// <param name="modelLocations">The model locations.</param>
	/// <param name="objectName">Name of the object.</param>
	/// <returns>std.shared_ptr&lt;_Ty&gt;.</returns>
	std::shared_ptr<std::vector<std::shared_ptr<DataPtr>>> AddInstances(std::vector<ModelLocation>& modelLocations, std::string& objectName);
	
	/// <summary>
	/// Gets the transformation matrix.
	/// </summary>
	/// <param name="ptr">The PTR.</param>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetMatrix(std::shared_ptr<DataPtr> ptr);

	/// <summary>
	/// Updates the transformation matrix.
	/// </summary>
	/// <param name="dator">The dator.</param>
	/// <param name="position">The position.</param>
	void UpdateMatrix(std::shared_ptr<DataPtr> dator, const glm::mat4 &position);

	/// <summary>
	/// Updates the data.
	/// </summary>
	/// <param name="dator">The dator.</param>
	/// <param name="data">The data.</param>
	void UpdateData(std::shared_ptr<DataPtr> dator, uint data);

	/// <summary>
	/// Deletes from baked.
	/// </summary>
	/// <param name="dator">The dator.</param>
	void DeleteFromBaked(std::shared_ptr<DataPtr> dator);

	/// <summary>
	/// Adds to baked.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="modelLocations">The model locations.</param>
	/// <returns>std.shared_ptr&lt;_Ty&gt;.</returns>
	std::shared_ptr<std::vector<std::shared_ptr<DataPtr>>> AddToBaked(const std::string &name, const std::vector<ModelLocation>& modelLocations);

	/// <summary>
	/// Bakes this instance.
	/// </summary>
	/// <returns>GBuffer *.</returns>
	GBuffer* Bake();

	/// <summary>
	/// Draws the specified render pass.
	/// </summary>
	/// <param name="renderPass">The render pass.</param>
	void Draw(RenderPass renderPass = CLASSIC) const;

private:
	
	std::unique_ptr<TextureA> m_textureDiffuse;
	std::unique_ptr<TextureA> m_textureSpecular;
	std::unique_ptr<TextureA> m_textureNormal;
	std::map<std::string, ModelRecord> m_objects;
	std::vector<DrawElementsIndirectCommand> m_commands;
	std::vector<GLuint> m_drawIds;
	std::shared_ptr<Shader> m_indirectShader;
	std::shared_ptr<Shader> m_shadowShader;
	std::shared_ptr<Shader> m_cullingShader;

	//free space means amount of elements for that buffer type which can be added to container
	int m_commandsBufferFreeSpace = 0;
	std::vector<int> m_bufferFreeSpace;

	
	int m_bufferPlusGrow = 8;
	const int m_bufferPlusGrowLimit = 1024;


	//stores matrices for individual objects. Individual means every one instance for every one mesh;
	std::vector<glm::mat4> m_objectsMatrices;
	std::vector<SSBOStruct> m_ssboData;


	//stores pointers for matrices/ssbo. Pointer at N position points to matrix on N position.
	std::vector<std::shared_ptr<int>> m_objectsDataPointer;



	bool m_baked;
	GLuint m_VAO = 0, m_VBO = 0, m_IBO = 0, m_SSBO = 0, m_commandBuffer = 0, m_drawIdBO = 0, m_AABBVAO = 0;
};


