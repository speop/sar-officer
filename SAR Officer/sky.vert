#version 420

void main()
{
    float x = -1.0 + float((gl_VertexID & 1) << 1);
    float y = -1.0 + float(gl_VertexID & 2);
    gl_Position = vec4(x, y, 0, 1);
}
