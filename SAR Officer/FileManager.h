// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-22-2016
//
// Last Modified By : David Buchta
// Last Modified On : 03-09-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <string>
#include <fstream>
#include <vector>
#include "Exceptions.h"

/// <summary>
/// Class FileManager.
/// </summary>
class FileManager
{
public:

	/// <summary>
	/// Initializes a new instance of the <see cref="FileManager"/> class.
	/// </summary>
	/// <param name="filePath">The file path.</param>
	FileManager(const std::string &filePath);

	/// <summary>
	/// Finalizes an instance of the <see cref="FileManager"/> class.
	/// </summary>
	~FileManager() {};

	/// <summary>
	/// Reads the file strings vector.
	/// </summary>
	/// <param name="filePath">The file path.</param>
	/// <returns>std.vector&lt;_Ty, _Alloc&gt;.</returns>
	
	static std::vector<std::string> ReadFile(const std::string &filePath);
	/// <summary>
	/// Gets the line from file.
	/// </summary>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	std::pair<bool, std::string> GetLine();

private:
	
	std::string m_filename;
	std::ifstream m_file;


};

