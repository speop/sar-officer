#include "Model.h"
#include <map>
#include <limits>



Model::Model(std::string path)
{
	auto pos = path.find_last_of('/');
	m_directory = path.substr(0, pos );
	m_fileName = path.substr(pos + 1);
	
	// loading model from file
	m_assimpImporter = new Assimp::Importer();
	const aiScene* scene = m_assimpImporter->ReadFile(path, aiProcess_Triangulate | aiProcess_CalcTangentSpace);
	
	//checking if model is ok
	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		throw ModelBasicException("ERROR::ASSIMP::" + std::string(m_assimpImporter->GetErrorString()));
	}
	else if (scene->mRootNode->mNumChildren <= 0) {
		throw ModelBasicException("Model " + path + " does not contain any meshes.");
	}
}



Model::~Model()
{
	if (m_assimpImporter != nullptr) delete m_assimpImporter;
	if(m_texturesFreeingInfo.size() > 0) {
		glDeleteTextures(m_texturesFreeingInfo.size(), &m_texturesFreeingInfo[0]);
	}
}



std::pair<glm::vec3, glm::vec3> Model::SetupForMesh(float alphaDiscard)
{
	if (m_setForMesh || m_setForGBuffer) throw ModelHadAlreadyBeenSetup("You cannot setup one model twice");
	m_setForMesh = true;

	TextMap texturesMap;
	auto retVal = ProcessModel(texturesMap, alphaDiscard);
  InitMesh();
  

	//loading textures and storing them in OpenGL
	for(auto &typesIt :texturesMap) {
		for (auto &namesIt : typesIt.second) {
			try {
				Texture tex;
				tex.Load(0, { this->m_directory + "/" + namesIt.first }, false);
				m_texturesFreeingInfo.push_back(tex.GetID());
        
				//and get mesh which wants texture
				for (auto &recIt : namesIt.second) {
					(*m_meshes[recIt.Lod])[recIt.Index].SetTexture(tex.GetID(), typesIt.first);
				}
			}
			catch (TextureException &ex) {
				Logger::GetLogger()->Log(Logger::Level::Warning, ex.GetMsg());
			}
		}		
	}	
	return retVal;
}



std::pair<glm::vec3, glm::vec3> Model::SetupForGBuffer(std::shared_ptr<GBuffer> gBuffer, std::string &modelIdentifier, float alphaDiscard)
{
	if (m_setForMesh || m_setForGBuffer) throw ModelHadAlreadyBeenSetup("You cannot setup one model twice");
	m_setForGBuffer = true;

	TextMap texturesMap;
	auto retVal = ProcessModel(texturesMap, alphaDiscard);

	//proccess textures
	std::map<TextMapRec, std::map<Mesh::TextTypes, std::string>> MeshTextMap;

	
	for(auto &it: texturesMap) {
		auto textType = it.first;

		for (auto &textIt: it.second) {
			std::string textName = m_directory + "/" + textIt.first;
			gBuffer->AddTexture(textName, textType);

			//and making map for meshes
			for( auto &recIt : textIt.second) {
				auto map = Model::MapFind(MeshTextMap, recIt);
				auto pointer = Model::MapFind(*map, textType);
				*pointer = textName;
			}
		}
		
	}

	//proccess  meshes
	
	//for now only LOD1
	auto meshes = m_meshes[0];
	TextMapRec rec;
	rec.Lod = 0;

	for (auto i = 0; i <meshes->size(); i++) {
		GBuffer::GBufferStruct meshRec;

		meshRec.indices = (*meshes)[i].m_indices;
		meshRec.vertices = (*meshes)[i].m_vertices;
		meshRec.transformation = (*meshes)[i].GetTransformation();
		meshRec.alphaDiscard = alphaDiscard;

		rec.Index = i;
		auto tex = MeshTextMap.find(rec);
		for(auto &it : tex->second) {
			Mesh::TextTypes textType = it.first;
			std::string textName = it.second;

			meshRec.textures.insert(std::make_pair(textType, textName));
		}

		gBuffer->Load(meshRec, modelIdentifier);
		
	}
	
	return retVal;
}


//todo: fix LOD

std::pair<glm::vec3, glm::vec3> Model::ProcessModel(TextMap &texturesMap, float alphaDiscard)
{
	
	auto scene = m_assimpImporter->GetScene();
  
	//LOD loading
	int LODs = scene->mRootNode->mNumChildren;
	auto maxFloat = (std::numeric_limits<float>::max)();
	auto aabb = std::make_pair(glm::vec3(maxFloat), glm::vec3(glm::vec3(-maxFloat)));
	
	//check if model contains LODs
	std::string name = scene->mRootNode->mChildren[0]->mName.C_Str();

	//because for assimp name means nothing and he has no problem to mess with names so we have to prepare our name
	name = name.substr(0, name.find_first_of('_'));
  
	//there is at least one LOD group
	if (name == "LOD0") {
		m_meshes.reserve(LODs);
		
		for (auto i = 0; i < LODs; i++) {
			auto mesh = std::make_shared<std::vector<Mesh>>();
			this->ProcessNode(
				scene->mRootNode->mChildren[i], 
				mesh,
				Model::AiToGLMMat4(scene->mRootNode->mChildren[i]->mTransformation)*
				Model::AiToGLMMat4(scene->mRootNode->mTransformation),
				texturesMap,
				aabb,
				alphaDiscard,
				i);
			m_meshes.push_back(mesh);
			
		}
	}
	else {
		auto mesh = std::make_shared<std::vector<Mesh>>();
		this->ProcessNode(
			scene->mRootNode,
			mesh, 
			Model::AiToGLMMat4(scene->mRootNode->mTransformation),
			texturesMap,
			aabb,
			alphaDiscard); 
		m_meshes.push_back(mesh);
	}
 
	return aabb;
}
	


void Model::ProcessNode(aiNode * node, std::shared_ptr<std::vector<Mesh>>meshes, glm::mat4 transform, TextMap &texturesMap, std::pair<glm::vec3, glm::vec3> &aabb, float alphaDiscard, uint Lod)
{
	
	transform = Model::AiToGLMMat4(node->mTransformation) * transform;
	auto scene = m_assimpImporter->GetScene();
  

	//process all node meshes (if any of course..)
	for (uint i = 0; i < node->mNumMeshes; i++) {
		auto maxFloat = (std::numeric_limits<float>::max)();
		auto pomAABB = std::make_pair(glm::vec3(maxFloat), glm::vec3(glm::vec3(-maxFloat)));

		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		TextMapRec rec;
		rec.Index = static_cast<uint>(meshes->size());
		rec.Lod = Lod;

		auto transTransform = glm::transpose(transform);

		auto temp = this->ProcessMesh(mesh, texturesMap, rec, pomAABB);
		auto min = transTransform * glm::vec4(pomAABB.first, 1);
		auto max = transTransform * glm::vec4(pomAABB.second, 1);
		CalculateAABB(aabb, glm::vec3(min));
		CalculateAABB(aabb, glm::vec3(max));

		temp.SetupBaseMatrix(transTransform);
		temp.SetAlphaDiscard(alphaDiscard);
		meshes->push_back(temp);

		
	}
	

	//and call recursively itself on node children
	for (uint i = 0; i < node->mNumChildren; i++) {
		std::pair<glm::vec3, glm::vec3> pomAABB;		
		this->ProcessNode(node->mChildren[i], meshes, transform, texturesMap, aabb, Lod);
		
		
	}
}


Mesh Model::ProcessMesh(aiMesh * mesh, TextMap &texturesMap, TextMapRec &rec, std::pair<glm::vec3, glm::vec3> &aabb)
{
	std::vector<Mesh::Vertex> verticies;
	std::vector<GLuint> indicies;
	
	for (uint i = 0; i < mesh->mNumVertices; i++) {
		//glm::vec3 point;
		Mesh::Vertex vertex;

		//coords
		vertex.Postion.x = mesh->mVertices[i].x;
		vertex.Postion.y = mesh->mVertices[i].y;
		vertex.Postion.z = mesh->mVertices[i].z;
		//vertex.Postion = point;

		//aabb
		CalculateAABB(aabb, vertex.Postion);
		
		//normals
		vertex.Normal.x = mesh->mNormals[i].x;
		vertex.Normal.y = mesh->mNormals[i].y;
		vertex.Normal.z = mesh->mNormals[i].z;
		//vertex.Normal = point;

		//tangents
		vertex.Tangent.x = mesh->mTangents[i].x;
		vertex.Tangent.y = mesh->mTangents[i].y;
		vertex.Tangent.z = mesh->mTangents[i].z;
		//vertex.Tangent = point;


		//texture coords
		glm::vec2 vec;

		if (mesh->mTextureCoords[0]) {
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
		}
		else {
			vec = glm::vec2(0.0f);
		}
		vertex.TexCoords = vec;


		verticies.push_back(vertex);
	}

	//process indicies
	for (uint i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];

		for (uint j = 0; j < face.mNumIndices; j++) {
			indicies.push_back(face.mIndices[j]);
		}

	}

	//process textures
	if (mesh->mMaterialIndex >= 0) {
		aiMaterial *material = m_assimpImporter->GetScene()->mMaterials[mesh->mMaterialIndex];			
		this->ProcessMaterials(material, texturesMap, rec);
	}
  
	return Mesh(verticies, indicies);
}

void Model::CalculateAABB(std::pair<glm::vec3, glm::vec3>& aabb, const glm::vec3 & point)
{
	//aabb
	aabb.first.x = point.x < aabb.first.x ? point.x : aabb.first.x;
	aabb.first.y = point.y < aabb.first.y ? point.y : aabb.first.y;
	aabb.first.z = point.z < aabb.first.z ? point.z : aabb.first.z;

	aabb.second.x = point.x > aabb.second.x ? point.x : aabb.second.x;
	aabb.second.y = point.y > aabb.second.y ? point.y : aabb.second.y;
	aabb.second.z = point.z > aabb.second.z ? point.z : aabb.second.z;
}


void Model::ProcessMaterials(aiMaterial* mat, TextMap &texturesMap, TextMapRec &rec)
{

	std::string textName;
	std::map<std::string, std::vector<TextMapRec>>* mapTextName;
	std::vector<TextMapRec> *recVector;

	// mapping up textures
	if((textName = GetMaterial(mat, aiTextureType_DIFFUSE)) != "") {
		mapTextName = Model::MapFind(texturesMap, Mesh::TextTypes::Diffuse);
		textName = textName.substr(textName.find_last_of("\\/")+1);
		recVector = Model::MapFind(*mapTextName, textName);
		recVector->push_back(rec);
	}

	if ((textName = GetMaterial(mat, aiTextureType_SPECULAR)) != "") {
		mapTextName = Model::MapFind(texturesMap, Mesh::TextTypes::Specular);
		textName = textName.substr(textName.find_last_of("\\/")+1);
		recVector = Model::MapFind(*mapTextName, textName);
		recVector->push_back(rec);
	}

	if ((textName = GetMaterial(mat, aiTextureType_HEIGHT)) != "") {
		mapTextName = Model::MapFind(texturesMap, Mesh::TextTypes::Normal);
		textName = textName.substr(textName.find_last_of("\\/") + 1);
		recVector = Model::MapFind(*mapTextName, textName);
		recVector->push_back(rec);
	}

}


inline std::string Model::GetMaterial(aiMaterial * mat, const aiTextureType type)
{
	std::string str = "";

	if(mat->GetTextureCount(type) >= 1 ) {
		aiString path;
		mat->GetTexture(type, 0, &path);
		str = std::string(path.C_Str());
	}
	
	return str;
}


void Model::SetupInstancesTransformation(const std::vector<glm::mat4>& trans, std::vector<std::shared_ptr<DataPtr>> &dataPtrVec)
{
	if (!m_setForMesh) throw ModelNotSetupForMesh("");

	bool dataPtrsProcessed = false;
	std::vector<std::vector<std::shared_ptr<int>>> meshPtrs;

	//m_meshes LOD
	for (auto &model : m_meshes)
	 {
		//one model
		for (auto &mesh : *model) {
			auto ptrs = mesh.AddInstances(trans);
			if(!dataPtrsProcessed) {
				meshPtrs.push_back(ptrs);
			}
		}
		dataPtrsProcessed = true;
	}

	auto count = static_cast<int> (trans.size());
	

	//copyin mesh pointers
	for(auto i = 0 ; i<count; i++) {
		auto ptrRec = std::make_shared<DataPtr>();

		for(const auto &meshPtr : meshPtrs) {
			ptrRec->m_dataPositionsPtrs.push_back(meshPtr[i]);
		}

		dataPtrVec.push_back(ptrRec);
	}


}


void Model::UpdateInstanceTransformation(std::shared_ptr<DataPtr> dataPtr, const glm::mat4& trans)
{
	if (!m_setForMesh) throw ModelNotSetupForMesh("");

	//m_meshes LOD
	for (auto &model : m_meshes) {

		//one model
		for (auto i = 0; i < model->size(); ++i) {
			auto ptr = dataPtr->m_dataPositionsPtrs[i];
			if (*ptr < 0) continue;

			model->at(i).UpdatePosition(ptr, trans);
		}
		
	}

}


void Model::DeleteInstanceTransformation(std::shared_ptr<DataPtr> dataPtr)
{
	if (!m_setForMesh) throw ModelNotSetupForMesh("");

	//m_meshes LOD
	for (auto &model : m_meshes) {

		//one model
		for (auto i = 0; i < model->size(); ++i) {
			auto ptr = dataPtr->m_dataPositionsPtrs[i];
			if (*ptr < 0) continue;

			model->at(i).DeletePosition(ptr);
		}

	}
}


glm::mat4 Model::GetInstanceTransformation(std::shared_ptr<DataPtr> dataPtr)
{
	return (*m_meshes.begin())->begin()->GetPosition(dataPtr->m_dataPositionsPtrs[0]);
}


void Model::SetInstanceAdditionalData(std::shared_ptr<DataPtr> dataPtr, uint data)
{
	if (!m_setForMesh) throw ModelNotSetupForMesh("");

	//m_meshes LOD
	for (auto &model : m_meshes) {

		//one model
		for (auto i = 0; i < model->size(); ++i) {
			auto ptr = dataPtr->m_dataPositionsPtrs[i];
			if (*ptr < 0) continue;

			model->at(i).SetAditionalData(ptr, data);
		}

	}
	
}

void Model::InitMesh()
{
  if (!m_setForMesh) throw ModelNotSetupForMesh("");

	//m_meshes LOD
	for (auto &model : m_meshes) {

		//one model
		for (auto &mesh : *model) {
		  mesh.SetupMesh();
	  }

	}
}



void Model::Draw(std::shared_ptr<Shader> shader, int LOD, bool instanced)
{
	if (!m_setForMesh || m_setForGBuffer) throw ModelNotSetupForMesh("Model for direct drawing has to be setup for mesh.");

	int loadedLods = static_cast<int>(m_meshes.size());
	if (loadedLods == 0) return;

	LOD = loadedLods > LOD ? LOD : loadedLods - 1;

	shader->Use();
	auto drawing = m_meshes[LOD];
	for (auto &mesh : *drawing) {
    
		mesh.Draw(shader, instanced);
    
	}
	shader->Unuse();
}

void Model::FreeScene()
{
	if(m_assimpImporter != nullptr) {
		delete m_assimpImporter;
		m_assimpImporter = nullptr;
	}
}
