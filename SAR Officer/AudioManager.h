// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-01-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-16-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <memory>
#include <irrKlang.h>
#include <map>
#include <vector>
#include "ConfigStore.h"
#include <random>

/// <summary>
/// Class AudioManager.
/// </summary>
class AudioManager
{
	
public:
	
	/// <summary>
	/// Gets this instance.
	/// </summary>
	/// <returns>AudioManager *.</returns>
	static AudioManager* Get();
	/// <summary>
	/// Finalizes an instance of the <see cref="AudioManager"/> class.
	/// </summary>
	~AudioManager();
	
	/// <summary>
	/// Plays main manu sound.
	/// </summary>
	/// <param name="play">The play.</param>
	void MainMenu(bool play);

	/// <summary>
	/// Plays game sound.
	/// </summary>
	/// <param name="play">The play.</param>
	void Game(bool play);

	/// <summary>
	/// Updates the player position.
	/// </summary>
	/// <param name="pos">The position.</param>
	/// <param name="lookDir">The look dir.</param>
	/// <returns>AudioManager *.</returns>
	AudioManager* UpdatePlayerPos(const glm::vec3 &pos, const glm::vec3 &lookDir);

private:
	
	/// <summary>
	/// Prevents a default instance of the <see cref="AudioManager"/> class from being created.
	/// </summary>
	AudioManager();
	
	/// <summary>
	/// Plays game ambient sound.
	/// </summary>
	/// <param name="play">The play.</param>
	void GameAmbient(bool play);

	/// <summary>
	/// Plays game FX sound.
	/// </summary>
	void GameFX();

	
	std::uniform_real_distribution<float> m_timeDist = std::uniform_real_distribution<float>(0.5f, 10.f);
	std::uniform_int_distribution<int> m_fxDist;
	bool m_scheduledPlay = false;

	float m_nextFxTime;
	std::mt19937 m_random;
	std::vector<ConfigStore::SoundRec> m_gameFxs;

	 
	static AudioManager* instance;
	std::map<std::string, bool> m_playingMAP;

	
	irrklang::ISoundEngine* m_sEngine;
	bool m_playingMainMenu = false;
	irrklang::ISound* m_mainMenuSound = nullptr;
	std::vector<irrklang::ISound*> m_ambient;
	
};
