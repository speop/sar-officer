#include "UIElement.h"
#include <sstream>
#include "Defines.h"


UIElement::~UIElement()
{
}

ImColor UIElement::Color( std::string hex)
{
	if(hex[0] == '#') {
		hex.erase(0, 1);
	}

	auto size = hex.size();
	std::stringstream ss;
	std::string val;
	int r = 0, g = 0, b = 0, a = 255;

	switch (size) {

	case 4:
		val = hex.substr(3, 1);
		ss << std::hex << val << val;
		ss >> a;
		ss = std::stringstream();

	case 3:
		val =hex.substr(0, 1);
		ss << std::hex << val << val;
		ss >> r;
		ss = std::stringstream();

		val = hex.substr(1, 1);
		ss << std::hex << val << val;
		ss >> g;
		ss = std::stringstream();

		val = hex.substr(2, 1);
		ss << std::hex << val << val;
		ss >> b;
		ss = std::stringstream();
		break;

	case 8:
		val = hex.substr(6, 2);
		ss << std::hex << val;
		ss >> a;
		ss = std::stringstream();

	case 6:
		val = hex.substr(0, 2);
		ss << std::hex << val;
		ss >> r;
		ss = std::stringstream();

		val = hex.substr(2, 2);
		ss << std::hex << val;
		ss >> g;
		ss = std::stringstream();

		val = hex.substr(4, 2);
		ss << std::hex << val;
		ss >> b;
		ss = std::stringstream();
		
	}
	return ImColor(r, g, b, a);
}

glm::vec3 UIElement::GLmColor(std::string hex)
{
	auto color = Color(hex);
	return {color.Value.x, color.Value.y, color.Value.z };
}
