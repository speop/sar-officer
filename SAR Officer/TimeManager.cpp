#include "TimeManager.h"
TimeManager* TimeManager::_instance = nullptr;


TimeManager::TimeManager()
{
	m_fpsLastTime = startTime =  lastTime = currentTime = float(glfwGetTime());	
	deltaTime = 0;
	m_dayTime = 0;
	m_gameTime = 0;
}


TimeManager* TimeManager::GetInstance()
{
	if (!_instance) {
		_instance = new TimeManager();
	}
	return  _instance;
}

void TimeManager::Run()
{
	currentTime = float(glfwGetTime());
	deltaTime = currentTime - lastTime;
	lastTime = currentTime;
	m_gameTime += deltaTime;
	m_dayTime += deltaTime*480;
	if(m_dayTime > 57600) {
		m_dayTime -= 57600;
	}

	
	numFrames++;
	if (glfwGetTime() - m_fpsLastTime >= 1.0)
	{
		//std::cout << 1000.0 / (double)numFrames << "ms/frame (" << numFrames << " fps)" << std::endl;
		
		m_fpsLastTime += 1.0;

		if (m_measuring) {
			m_minFps = (std::min)(m_minFps, static_cast<float>(numFrames));
			m_maxFps = (std::max)(m_maxFps, static_cast<float>(numFrames));

			m_Fps++;
			m_avgFps += (numFrames - m_avgFps)/m_Fps;
		}
		numFrames = 0;
	}
}

void TimeManager::StartMeasurement()
{
	if (m_measuring) return;

	m_measuring = true;
	m_minFps = (std::numeric_limits<float>::max)();
	m_maxFps = -m_minFps;
	m_avgFps = 0;
	m_Fps = 0;
}

void TimeManager::StopMeasurement()
{
	if (!m_measuring) return;
	m_measuring = false;
	std::cout << "Min: " << m_minFps << ", Max: " << m_maxFps << ", Avg: " << m_avgFps << std::endl;
}

float TimeManager::GetDeltaTime()
{
	return deltaTime;
}

float TimeManager::GetDayTime()
{
	return m_dayTime + 18000;
}

float TimeManager::GetGameTime()
{
	return m_gameTime;
}

TimeManager::~TimeManager()
{
	
}
