// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-16-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <string>
#include <memory>
#include "GBuffer.h"
#include "Model.h"
#include "AABBQuadTree.h"
#include "ModelLocation.h"


/// <summary>
/// Class SceneManager.
/// </summary>
class SceneManager
{
public:
	/// <summary>
	/// Class SceneRecord.
	/// </summary>
	class SceneRecord {
		std::string ObjectName = "";
		std::string CategoryName = "";
		std::string FilePath = "";
		std::shared_ptr<Model> Model;
		glm::vec3 MinCorner;
		glm::vec3 MaxCorner;
		std::shared_ptr<DataPtr> DataPtr;
		bool Indirect = false;
		std::shared_ptr<GBuffer> GBuffer;
		std::shared_ptr<SAR::AABBQuadTree<std::shared_ptr<SceneRecord>>::Record> AABBTreeRec;
		friend SceneManager;
	};

	
private:
	//SceneManager(const SceneManager& other) = delete; // non construction-copyable
	//SceneManager& operator=(const SceneManager&) = delete; // non copyable
	/// <summary>
	/// Prevents a default instance of the <see cref="SceneManager"/> class from being created.
	/// </summary>
	SceneManager();
	
	using DataPtrPtr = std::vector<std::shared_ptr<DataPtr>>*;
	std::string m_world;

	static SceneManager* m_instance;
	std::shared_ptr<SAR::AABBQuadTree<std::shared_ptr<SceneRecord>>> m_staticTree;

	/// <summary>
	/// Adds the object to scene.
	/// </summary>
	/// <param name="templ">The templ.</param>
	/// <param name="name">The name.</param>
	/// <param name="DataPath">The data path.</param>
	/// <param name="MinCorner">The minimum corner.</param>
	/// <param name="MaxCorner">The maximum corner.</param>
	/// <param name="dataPointer">The data pointer.</param>
	/// <returns>SceneManager.SceneRecord.</returns>
	SceneRecord AddObject(std::shared_ptr<SceneRecord> templ, const std::string &name, const std::string &DataPath, const glm::vec3 &MinCorner, const glm::vec3 &MaxCorner, DataPtrPtr dataPointer);
	
	/// <summary>
	/// Finalizes an instance of the <see cref="SceneManager"/> class.
	/// </summary>
	~SceneManager();

public:
	
	/// <summary>
	/// Gets this instance.
	/// </summary>
	/// <returns>SceneManager *.</returns>
	static SceneManager* Get();

	/// <summary>
	/// Makes the scene.
	/// </summary>
	/// <returns>SceneManager *.</returns>
	SceneManager* MakeScene();

	/// <summary>
	/// Deletes the scene.
	/// </summary>
	/// <returns>SceneManager *.</returns>
	SceneManager* DeleteScene();

	/// <summary>
	/// Adds the object.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="category">The category.</param>
	/// <param name="DataPath">The data path.</param>
	/// <param name="MinCorner">The minimum corner.</param>
	/// <param name="MaxCorner">The maximum corner.</param>
	/// <param name="dataPointer">The data pointer.</param>
	/// <param name="model">The model.</param>
	/// <returns>SceneManager.SceneRecord.</returns>
	SceneRecord AddObject(const std::string &name, const std::string &category, const std::string &DataPath, const glm::vec3 &MinCorner, const glm::vec3 &MaxCorner, DataPtrPtr dataPointer, std::shared_ptr<Model> model);
	
	/// <summary>
	/// Adds the object.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="category">The category.</param>
	/// <param name="DataPath">The data path.</param>
	/// <param name="MinCorner">The minimum corner.</param>
	/// <param name="MaxCorner">The maximum corner.</param>
	/// <param name="dataPointer">The data pointer.</param>
	/// <param name="gBuffer">The g buffer.</param>
	/// <returns>SceneManager.SceneRecord.</returns>
	SceneRecord AddObject(const std::string &name, const std::string &category, const std::string &DataPath, const glm::vec3 &MinCorner, const glm::vec3 &MaxCorner, DataPtrPtr dataPointer, std::shared_ptr<GBuffer> gBuffer);
	
	/// <summary>
	/// Adds the object.
	/// </summary>
	/// <param name="templ">The templ.</param>
	/// <param name="position">The position.</param>
	void AddObject(SceneRecord templ, glm::mat4 position);

	/// <summary>
	/// Sets the world.
	/// </summary>
	/// <param name="world">The world.</param>
	/// <returns>SceneManager *.</returns>
	SceneManager* SetWorld(std::string world);
	

	/// <summary>
	/// Updates the object.
	/// </summary>
	/// <param name="object">The object.</param>
	/// <param name="transform">The transform.</param>
	/// <returns>SceneManager *.</returns>
	SceneManager* UpdateObject(std::shared_ptr<SceneRecord> object, const glm::mat4 &transform);

	/// <summary>
	/// Updates the object data.
	/// </summary>
	/// <param name="object">The object.</param>
	/// <param name="data">The data.</param>
	/// <returns>SceneManager *.</returns>
	SceneManager* UpdateObjectData(std::shared_ptr<SceneRecord> object, uint data);

	/// <summary>
	/// Deletes the object.
	/// </summary>
	/// <param name="object">The object.</param>
	/// <returns>SceneManager *.</returns>
	SceneManager* DeleteObject(std::shared_ptr<SceneRecord> &object);

	/// <summary>
	/// Saves the scene.
	/// </summary>
	void SaveScene();

	/// <summary>
	/// Collision chekc with specified lambda.
	/// </summary>
	/// <param name="lambda">The lambda.</param>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	std::pair<bool, float> Collision(std::function<std::pair<bool, float>(glm::vec3, glm::vec3)> lambda);

	/// <summary>
	/// Finds the object by ray intersetion.
	/// </summary>
	/// <param name="ray">The ray.</param>
	/// <returns>std.shared_ptr&lt;_Ty&gt;.</returns>
	std::shared_ptr<SceneRecord> FindObject(const SAR::Ray &ray);

	/// <summary>
	/// Gets the object constrains.
	/// </summary>
	/// <param name="object">The object.</param>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	std::pair<glm::vec3, glm::vec3> GetObjectConstrains(const SceneRecord &object) const;

	/// <summary>
	/// Gets the scene aabb.
	/// </summary>
	/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
	std::pair<glm::dvec3, glm::dvec3>  GetSceneAABB();
};

