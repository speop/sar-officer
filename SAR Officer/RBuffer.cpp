#include "RBuffer.h"



RBuffer::RBuffer()
{
}


RBuffer::~RBuffer()
{
	glDeleteTextures(1, &m_depthTexture);
	glDeleteTextures(1, &m_basicTexture);

	if(!m_lite) {
		glDeleteTextures(1, &m_normalTexture);
		glDeleteTextures(1, &m_positionTexture);
		glDeleteTextures(1, &m_idTexture);
	}

	glDeleteFramebuffers(1, &m_fbo);

}

void RBuffer::Init(uint width, uint height, bool lite)
{
	m_lite = lite;
	// Create the FBO
	glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	GenerateTextures(width, height);
	FinalizeFBO(width, height);

}

void RBuffer::InitLayered(uint texture, uint width, uint height, uint layers)
{
	m_layered = true;
	m_layers = layers;
	m_basicTexture = texture;
	// Create the FBO
	glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	if (m_basicTexture != 0) {
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_basicTexture, 0);
	}
	// - Depth buffer
	glGenTextures(1, &m_depthTexture);
	FinalizeFBO(width, height);

	//making views - artefacts on AMD480
	/*m_depthTextureViews = new GLuint[layers];
	glGenTextures(layers, m_depthTextureViews);

	for(auto i =0; i < layers; i++) {
		glTextureView(*(m_depthTextureViews + i), GL_TEXTURE_2D, m_depthTexture, GL_DEPTH_COMPONENT32F, 0, 1, i, 1);
		glBindTexture(GL_TEXTURE_2D, *(m_depthTextureViews + i));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
	*/
}

void RBuffer::Init(GLuint textureID, uint width, uint height)
{
	m_lite = true;
	m_basicTexture = textureID;
	// Create the FBO
	glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_basicTexture, 0);	
	FinalizeFBO(width, height);
}

void RBuffer::BindForWriting()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
}

void RBuffer::BindForReading(uint firstTextureUnit)
{
	if (!m_layered)
	{
		glActiveTexture(GL_TEXTURE0 + firstTextureUnit);
		glBindTexture(GL_TEXTURE_2D, m_basicTexture);

		if (!m_lite) {
			glActiveTexture(GL_TEXTURE1 + firstTextureUnit);
			glBindTexture(GL_TEXTURE_2D, m_normalTexture);

			glActiveTexture(GL_TEXTURE2 + firstTextureUnit);
			glBindTexture(GL_TEXTURE_2D, m_positionTexture);

			glActiveTexture(GL_TEXTURE3 + firstTextureUnit);
			glBindTexture(GL_TEXTURE_2D, m_idTexture);
		}
	}else if(m_basicTexture != 0){
		glActiveTexture(GL_TEXTURE0 + firstTextureUnit);
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_basicTexture);
	}
}

void RBuffer::BindDepthForReading(uint firstTextureUnit)
{
	if (!m_layered) {
		glActiveTexture(GL_TEXTURE0 + firstTextureUnit);
		glBindTexture(GL_TEXTURE_RECTANGLE, m_depthTexture);
	}else {
		
		glActiveTexture(GL_TEXTURE0 + firstTextureUnit);
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_depthTexture);	
	}
}

void RBuffer::FinalizeFBO(uint width, uint height)
{
	// - Depth buffer
	glGenTextures(1, &m_depthTexture);

	if(m_layered) {
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_depthTexture);
		glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT32F, width, height, m_layers);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_depthTexture, 0);
	}
	else {		
		glBindTexture(GL_TEXTURE_RECTANGLE, m_depthTexture);
		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_DEPTH_COMPONENT32F, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_RECTANGLE, m_depthTexture, 0);
	}

	if (!m_lite && !m_layered) {
		GLuint attachments[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 , GL_COLOR_ATTACHMENT3 };
		glDrawBuffers(4, attachments);
	}
	else if(m_basicTexture != 0) {
		GLuint attachments[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, attachments);
	}
	else {
		glDrawBuffer(GL_NONE);
	}

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		throw RBufferException("FB error, status: 0x" + std::to_string(status));
	}

	// restore default FBO
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void RBuffer::GenerateTextures(uint width, uint height)
{
	// - Color + Specular color buffer
	glGenTextures(1, &m_basicTexture);
	glBindTexture(GL_TEXTURE_2D, m_basicTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_basicTexture, 0);

	if (!m_lite) {
		// - Normal color buffer
		glGenTextures(1, &m_normalTexture);
		glBindTexture(GL_TEXTURE_2D, m_normalTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_normalTexture, 0);

		// - Position color buffer
		glGenTextures(1, &m_positionTexture);
		glBindTexture(GL_TEXTURE_2D, m_positionTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_positionTexture, 0);


		// - Object ids texture
		glGenTextures(1, &m_idTexture);
		glBindTexture(GL_TEXTURE_2D, m_idTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R8UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_BYTE, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_idTexture, 0);

	}
}

