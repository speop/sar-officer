#include "Editor.h"
#include "ConfigStore.h"
#include "SceneManager.h"
#include "MainGame.h"

Editor* Editor::instance = nullptr;

Editor::Editor()
{
	//m_models = ConfigStore::Instance()->GetModels();
}


Editor::~Editor()
{
	instance = nullptr;
}

Editor * Editor::Get()
{
	if (instance == nullptr) instance = new Editor();
	return instance;
}

Editor* Editor::Reset()
{
	delete instance;
	instance = new Editor();
	return instance;
}

void Editor::AlterObject(Alteration alt, Axis axis, float speed) {
	
	if (m_pickedObject == nullptr) return;
	glm::vec3 axisV;

	switch (axis) {
	case X:
		axisV = glm::vec3(1, 0, 0);
		break;
	case Y:
		axisV = glm::vec3(0, 1, 0);
		break;
	case Z:
		axisV = glm::vec3(0, 0, 1);
		break;
	case XYZ:
		axisV = glm::vec3(1, 1, 1);
		break;
	}

	auto mat = glm::mat4();
	auto ang = glm::pi<float>() / -speed;
	switch (alt) {
	case RLeft:
		speed = -speed;
	case RRight:		
		mat = glm::rotate(glm::mat4(1.f), (glm::pi<float>() / 50.f) * -speed, axisV);
		break;
	case MLeft:
		speed = -speed;
	case MRight:
		mat = glm::translate(glm::mat4(1.f), speed * axisV);
		break;
	case SMinus:
		speed = -speed *0.5f;
	case SPlus:
		auto scale = 0.1f *speed + 1;
		axisV.x = axisV.x ? scale : 1.f;
		axisV.y = axisV.y ? scale : 1.f;
		axisV.z = axisV.z ? scale : 1.f;

		mat = glm::scale(glm::mat4(1.f), axisV);
		break;

	}

	SceneManager::Get()->UpdateObject(m_pickedObject, mat);
}

void Editor::PickObject(const SAR::Ray &ray)
{
	if (m_pickedObject != nullptr) SceneManager::Get()->UpdateObjectData(m_pickedObject, 0x009b59b6);
	m_pickedObject = SceneManager::Get()->FindObject(ray);
	if (m_pickedObject == nullptr) return;

	SceneManager::Get()->UpdateObjectData(m_pickedObject, 0x559b59b6);
}
void Editor::AddObject(const SAR::Ray & ray)
{
	glm::mat4 position;
	auto model = m_models[m_selectedModel - 1];

	if (!m_snapToTerrain) {
		auto camPos = PerspectiveCamera::GetCameraInstance("Basic")->GetPosition();
		auto constrains = SceneManager::Get()->GetObjectConstrains(model.m_template);
		auto transVec = camPos - (constrains.second - constrains.first) / 2.f;

		position = glm::translate(glm::mat4(1.f), transVec);
	}
	else {
		auto intersect = ray.IntersectTerrain(2048);
		if (!intersect.first) return;
		position = glm::translate(glm::mat4(1.f), intersect.second);
		//return;
	}

	if (position == m_prevPosition) return;
	m_prevPosition = position;

	SceneManager::Get()->AddObject(model.m_template, position);
}
void Editor::ProcessRay(const SAR::Ray &ray)
{
	if(!m_addMode) {
		PickObject(ray);
	}
	else if(m_selectedModel > 0) {	
		AddObject(ray);
	}
}

void Editor::AddModelTemplate(const std::string& name, SceneManager::SceneRecord tpl)
{
	ModelTemplate model;
	model.m_name = name;
	model.m_template = tpl;

	m_models.push_back(model);
}

void Editor::AddToggle(bool value)
{
	m_addMode = value;
}
void Editor::SnapToTerrainToggle(bool value)
{
	m_snapToTerrain = value;
}

void Editor::DeleteObject()
{
	if (m_pickedObject == nullptr) return;
	SceneManager::Get()->DeleteObject(m_pickedObject);
	m_pickedObject = nullptr;
}
void Editor::SetModel(int modelID) 
{
	m_selectedModel = modelID;
}
void Editor::Save() 
{
	SceneManager::Get()->SaveScene();
}

std::vector<std::string> Editor::Models()
{
	std::vector<std::string> models;
	for(auto &it: m_models) {
		models.push_back(it.m_name);
	}
	return models;
}
