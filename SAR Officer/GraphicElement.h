// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 11-11-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-05-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <memory>
#include <glm\gtc\type_ptr.hpp>
#include <glm\glm.hpp>
#include <fstream>     
#include <sstream>
#include "Shader.h"
#include "CollisionManager.h"

/// <summary>
/// Class GraphicElement.
/// </summary>
class GraphicElement
{
public:
	/// <summary>
	/// Initializes this instance.
	/// </summary>
	virtual void Init() = 0;
	/// <summary>
	/// Runs this instance.
	/// </summary>
	virtual void Run() = 0;
	/// <summary>
	/// Finalizes an instance of the <see cref="GraphicElement"/> class.
	/// </summary>
	virtual ~GraphicElement();
	

protected:
	
	/// <summary>
	/// Loads the basic object world transform matrix from given file.
	/// </summary>
	/// <param name="filename">The filename.</param>
	/// <param name="positions">The positions.</param>
	/// <param name="hasY">The has y.</param>
	void LoadPositions(std::string filename, std::vector<glm::mat4> &positions, bool hasY = true);



protected: 
	
	std::shared_ptr<Shader> m_program;
	GLuint m_VAO = 0;
	GLuint m_VBO = 0;
};

