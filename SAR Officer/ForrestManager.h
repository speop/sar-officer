// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 12-09-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-05-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include "GraphicElement.h"
#include "Model.h"
#include "Camera.h"
#include "ConfigStore.h"
#include "Exceptions.h"
#include "GBuffer.h"

/// <summary>
/// Class for maintaining base game models.
/// </summary>
/// <seealso cref="GraphicElement" />
class ForrestManager :
	public GraphicElement
{
public:

	/// <summary>
	/// Struct Tree
	/// </summary>
	struct Tree {
		bool direct;
		std::string objName;
		std::shared_ptr<Model> model;
		std::vector<glm::mat4> positions;

	};

	/// <summary>
	/// Creates new instance of Forrest manager.
	/// </summary>
	/// <param name="gBuffer">GBuffer object in which will be stored forest meshes.</param>
	ForrestManager(std::shared_ptr<GBuffer> gBuffer);
	/// <summary>
	/// Finalizes an instance of the <see cref="ForrestManager"/> class.
	/// </summary>
	~ForrestManager();
	
	/// <summary>
	/// initialization method which will load all forrest objects. This method creates Model instances and prepare all data to be rendered.
	/// </summary>
	virtual void Init();


	/// <summary>
	/// Calls run with default parameter CLASSIS
	/// </summary>
	void Run() { Run(RenderPass::CLASSIC); };

	/// <summary>
	/// Called in every frame. Updates all values and calls render function for all objects (if object isnt't in Gbuffer).
	/// </summary>
	/// <param name="pass">The pass.</param>
	void Run(RenderPass pass);



protected:
	std::vector<Tree> m_trees;
	std::shared_ptr<Shader> m_shader;
	std::shared_ptr<Shader> m_shadowShader;
	std::shared_ptr<GBuffer> m_GBuffer;

};

