#include "Sky.h"
#include <glm/gtx/rotate_vector.hpp>


Sky::Sky()
{
}

void Sky::Init() {
	this->m_program = std::make_shared<Shader>();
	m_program->BuildShaders({
		 ShaUN(GL_VERTEX_SHADER, "Shaders/sky.vert"),
		 ShaUN(GL_FRAGMENT_SHADER, "Shaders/sky.frag")
	});

	this->m_clouds = std::make_shared<Shader>();
	m_clouds->BuildShaders({
		ShaUN(GL_VERTEX_SHADER, "Shaders/clouds.vert"),
		ShaUN(GL_FRAGMENT_SHADER, "Shaders/clouds.frag")
	});

	
	m_cloudsShadow = std::make_unique<Shader>();
	m_cloudsShadow->BuildShaders({
		ShaUN(GL_VERTEX_SHADER, "Shaders/clouds.vert", "#define SHADER_SHADOW \n"),
    ShaUN(GL_GEOMETRY_SHADER, "Shaders/CSM.geom", "#define CAMERAS csmCloudsVP \n #define LAYERS 1\n"),
		ShaUN(GL_FRAGMENT_SHADER, "Shaders/clouds.frag","#define SHADER_SHADOW \n")
	});

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

	static const GLfloat g_vertex_buffer_data[] = {
		-8000.0f,1000.0f,-8000.0f, // triangle 1 : begin
		-8000.0f,1000.0f, 8000.0f,
		8000.0f, 1000.0f, -8000.0f, // triangle 1 : end
		8000.0f, 1000.0f, 8000.0f,
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindVertexArray(0);
	
	
	
}


void Sky::Run(RenderPass renderPass) {
	glBindVertexArray(m_VAO);
	auto trns = glm::translate(glm::mat4(1), glm::vec3(0.f, -100, 0.0f));
  
	if (renderPass == CLASSIC) {
		UpdateSun();

		m_program->Use();				
		m_program
			->SetVec3f("sunVector", 1, glm::value_ptr(m_sunPosition))
			->Set1f("time", m_sunAngle);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		m_program->Unuse();

		m_clouds->Use();
		m_clouds
			->SetVec3f(0, 1, glm::value_ptr(m_sunPosition))
			->Set1f(1, 0)
			->SetMat4f(2, 1, GL_FALSE, glm::value_ptr(glm::mat4(1)));
		glEnable(GL_BLEND); glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		m_clouds
			->Set1f(1, (42 / 316) + 21)
			->SetMat4f(2, 1, GL_FALSE, glm::value_ptr(trns));		
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		m_clouds->Unuse();
		glDisable(GL_BLEND);
	}

	else if(renderPass == SHADOW) {
		m_cloudsShadow->Use();
			m_clouds
        ->Set1f(1, 0)
			  ->SetMat4f(2, 1, GL_FALSE, glm::value_ptr(glm::mat4(1)));
		glEnable(GL_BLEND); glBlendFunc(GL_ONE, GL_ONE);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		m_clouds
			->Set1f(1, (42 / 316) + 21)
			->SetMat4f(2, 1, GL_FALSE, glm::value_ptr(trns));
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		m_clouds->Unuse();
	}
    
	glBindVertexArray(0);
}

void Sky::SunPositon(glm::vec3 position)
{
	m_sunPosition = position;
}

glm::vec3 Sky::SunPosition()
{
	return m_sunPosition;
}

void Sky::UpdateSun()
{
	auto dayTime = TimeManager::GetInstance()->GetDayTime();
	if(dayTime < c_sunRise) {
		m_sunAngle = -2;
	}
	else if(dayTime> c_sunset) {
		m_sunAngle = 182;
	}
	else {
		dayTime -= c_sunRise;
		m_sunAngle = c_sunIncPerDaySec *dayTime;
	}
	//m_sunAngle = 40;
	m_sunPosition = glm::normalize(glm::rotate<float>(glm::vec3(1.f, 0.f, 0.f), glm::radians(m_sunAngle), glm::vec3(0.f, 0.f, 1.f)));
}


Sky::~Sky()
{
}

