﻿// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-17-2017
//
// Last Modified By : David
// Last Modified On : 03-26-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include "Camera.h"

/// <summary>
/// The SAR namespace.
/// </summary>
namespace SAR {

	/// <summary>
	/// Class Ray.
	/// </summary>
	class Ray {
	public:
		/// <summary>
		/// Initializes a new instance of the <see cref="Ray"/> class.
		/// </summary>
		/// <param name="camera">The camera.</param>
		/// <param name="mouseX">The mouse x device space position.</param>
		/// <param name="mouseY">The mouse ydevice space position.</param>
		Ray(PerspectiveCamera* camera, float mouseX, float mouseY);
		/// <summary>
		/// Check if ray intersects with the given aabb.
		/// </summary>
		/// <param name="minCorner">The minimum corner.</param>
		/// <param name="maxCorner">The maximum corner.</param>
		/// <returns>std.pair&lt;bool, float&gt; ray length before intersection.</returns>
		std::pair<bool, float> IntersectAABB(const glm::vec3 &minCorner, const glm::vec3 &maxCorner) const;
		/// <summary>
		/// Check if ray intersects the terrain.
		/// </summary>
		/// <param name="maxRayLength">Distance limit of checking. After this distance intersect is false.</param>
		/// <returns>std.pair&lt;bool, glm.vec32&gt; coords of intersection..</returns>
		std::pair<bool, glm::vec3> IntersectTerrain(float maxRayLength) const;
		/// <summary>
		/// Prints ray debug info.
		/// </summary>
		void Print();

	private:
		/// <summary>
		/// Calculates the ray.
		/// </summary>
		void CalculateRay();		

		
		glm::mat4 m_projectionMatrix;
		glm::mat4 m_inverseProjectionMatrix;
		glm::mat4 m_viewMatrix;
		float m_mouseX;
		float m_mouseY;
		float m_screenW;
		float m_screenH;

		glm::vec3 m_ray;
		glm::vec3 m_invRay;
		glm::vec3 m_origin;

		int m_sign[3];
	};
}