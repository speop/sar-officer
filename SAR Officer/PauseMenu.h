// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-02-2017
//
// Last Modified By : David Buchta
// Last Modified On : 02-05-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include "UIElement.h"
/// <summary>
/// Class PauseMenu.
/// </summary>
/// <seealso cref="UIElement" />
class PauseMenu :
	public UIElement
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="PauseMenu"/> class.
	/// </summary>
	PauseMenu();
	/// <summary>
	/// Finalizes an instance of the <see cref="PauseMenu"/> class.
	/// </summary>
	~PauseMenu();

	/// <summary>
	/// Wake up method <see cref="UIElement.Wake" />.
	/// </summary>
	void Wake() override;

	/// <summary>
	/// Run method <see cref="UIElement.Run" />. 
	/// </summary>
	void Run() override;

	/// <summary>
	/// ImGui run method <see cref="UIElement.RunImgui" />. Draw UI elements.
	/// </summary>
	void RunImgui() override;

	/// <summary>
	/// Stops method <see cref="UIElement.Stop" />. Releases alocated memory.
	/// </summary>
	void Stop() override;

	/// <summary>
	/// Burns this instance <see cref="UIElement.Burn" />.
	/// </summary>
	void Burn() override;

private:
	/// <summary>
	/// Initializes this instance.
	/// </summary>
	void Init() override;

#pragma region Buttons events
	
	/// <summary>
	/// Continue BTN click.
	/// </summary>
	void ContinueBtn_click();
	/// <summary>
	/// Main menu BTN click.
	/// </summary>
	void MainMenuBtn_click();
	/// <summary>
	/// Exit BTN click.
	/// </summary>
	void ExitBtn_click();
#pragma endregion 
};

