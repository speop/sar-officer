#include "TextureA.h"
#include "Texture.h"


TextureA::~TextureA()
{
	glDeleteTextures(1, &m_texture);
}



void TextureA::Load(const std::vector<std::string> &textures)
{
	for(auto &it : textures) {
		Load(it);
	}
}


void TextureA::Load(const std::string & texture)
{
	if (m_baked) throw TextureAlreadyBakedException("You cannot add texture file if textures was already baked.");

	// duplicity entries check
	auto fit = m_textureNames.begin();

	while (fit != m_textureNames.end()) {
		if (*fit == texture) break;
		++fit;
	}

	if (fit == m_textureNames.end()) {
		m_textureNames.push_back(texture);
	}

}


TextureA* TextureA::Bake()
{
	if (m_baked) throw TextureAlreadyBakedException("Textures was already baked");
	m_baked = true;

	int count = static_cast<int>(m_textureNames.size());

	//creating texture array for 1024x1024 textures...
	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D_ARRAY, m_texture);
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, 1024, 1024, count);

	//loading textures 
	auto fip = new fipImage();

	for (auto i = 0; i < count; i++) {
		bool loaded = fip->load(m_textureNames[i].c_str()) != 0;
		if (!loaded) {
			throw TextureException("Texture \""+ m_textureNames[i]+"\" cannot be loaded, file not found or unknown format.", m_textureNames[i]);
		}

		/*** normalizing image ***/
		
		if (fip->getBitsPerPixel() != 32) {
			fip->convertTo32Bits();
		}

		//upscaling  - it i'll be beeter to make some mipmap optimiyation for this but i dont know how
		if (fip->getHeight() != 1024 || fip->getWidth() != 1024) {
			if (!(fip->rescale(1024, 1024, FILTER_BICUBIC))) { 
				delete fip;
				throw TextureRescaleException("Rescaling error file: "+ m_textureNames[i]);
			}			
		}

		//uploading texture to GPU
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, 1024, 1024, 1, GL_BGRA, GL_UNSIGNED_BYTE, fip->accessPixels());

		//storing mapping texture name -> array index
		m_textureIndex.insert(std::make_pair(m_textureNames[i], static_cast<GLuint>(i)));
	}
	delete fip;

	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	return this;
}


TextureA* TextureA::Burn()
{
	if(m_baked) glDeleteTextures(1, &m_texture);
	m_baked = false;
	return this;
}


TextureA * TextureA::ClearTextures()
{
	m_textureNames.clear();
	return this;
}

GLuint TextureA::GetTextureIndex(std::string & name)
{
	
	if (!m_baked) throw TextureAException("Textures were not baked");

	auto rec = m_textureIndex.find(name);
	if (rec == m_textureIndex.end()) throw TextureNotFound(name);

	return rec->second;
}

void TextureA::Draw(uint unit)
{

	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D_ARRAY, m_texture);
}