#include "GraphicsEngine.h"
#include "MainGame.h"
#include <random>
#include "SceneManager.h"


GraphicsEngine::GraphicsEngine()
{
}


GraphicsEngine::~GraphicsEngine()
{
	Release();
}

void GraphicsEngine::Init(uint xres, uint yres)
{

	MainGame::Get()->LoadingProgressStackPushP(1.f / 6.f);
	//terrain will be 4096x4096m it's size is similiar to skyrim land
	m_terrainManager = std::make_unique<TerrainManager>(4096, 4096, -300);	
	m_terrainManager->Init();
	MainGame::Get()->LoadingProgressStackInc();
	 
	m_skyManager = std::make_unique<Sky>();
	m_skyManager->Init();
	MainGame::Get()->LoadingProgressStackInc();
	
	_program = new Shader();
	_program->BuildShaders({ ShaUN(GL_VERTEX_SHADER, "Shaders/basic.vert"), ShaUN(GL_FRAGMENT_SHADER, "Shaders/basic.frag") });
	m_lightingShader = std::make_unique<Shader>();
	m_lightingShader->BuildShaders({ ShaUN(GL_VERTEX_SHADER, "Shaders/lighting.vert"), ShaUN(GL_FRAGMENT_SHADER, "Shaders/lighting.frag") });
	m_ssaoShader = std::make_unique<Shader>();
	m_ssaoShader->BuildShaders({ ShaUN(GL_VERTEX_SHADER, "Shaders/lighting.vert"), ShaUN(GL_FRAGMENT_SHADER, "Shaders/ssao.frag") });
	m_ssaoBlurShader = std::make_unique<Shader>();
	m_ssaoBlurShader->BuildShaders({ ShaUN(GL_VERTEX_SHADER, "Shaders/lighting.vert"), ShaUN(GL_FRAGMENT_SHADER, "Shaders/ssaoBlur.frag") });



	glGenVertexArrays(1, &m_lightingVAO);

	m_GBuffer = std::make_shared<GBuffer>();
	
	MainGame::Get()->LoadingProgressStackInc();

	m_forrestManager = std::make_unique<ForrestManager>(m_GBuffer);
	m_forrestManager->Init();

	
	glGenBuffers(1, &m_ubo);
	glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(UBOstruct), nullptr, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 5, m_ubo);

	glGenBuffers(1, &m_csmUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, m_csmUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(UBOcsm), nullptr, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 6, m_csmUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	
	m_GBuffer->Bake();
	MainGame::Get()->LoadingProgressStackInc();

	m_sunMinColor = ConfigStore::Instance()->GetSunColor("Dawn");
	m_sunColorMultplier = ConfigStore::Instance()->GetSunColor("Noon") - m_sunMinColor;
	
	m_sunMinAmbient = ConfigStore::Instance()->GetSunFloat("Ambient", "Dawn");
	m_sunAmbientMultiplier = ConfigStore::Instance()->GetSunFloat("Ambient", "Noon") - m_sunMinAmbient;
	m_fogIntensityMapper = 1.0f * (1 - 0.4f) / m_sunAmbientMultiplier;
	
	m_deferredFBO = std::make_unique<RBuffer>();
	m_deferredFBO->Init(xres, yres);
	m_cloudsShaddowFBO.reset();
	SSAOInit(xres,yres);
	
	CSMInit(xres, yres, 4);

	CreateCSMCameras(0.1f, 3500, 4, 5.5);
	CreateCSMCameras(0.1f, 3500, 1, 3, "cloudsCSM");
	

	m_initialized = true;

	MainGame::Get()->LoadingProgressStackInc();
	MainGame::Get()->LoadingProgressStackPop();

	
}

void GraphicsEngine::Release()
{
	if (!m_initialized) return;
	
	m_initialized = false;
	m_terrainManager.reset();
	m_skyManager.reset();
	delete _program;
	m_lightingShader.reset();
	m_ssaoShader.reset();
	m_ssaoBlurShader.reset();

	glDeleteVertexArrays(1, &m_lightingVAO);
	m_GBuffer.reset();
	m_forrestManager.reset();
	glDeleteBuffers(1, &m_ubo);
	glDeleteBuffers(1, &m_csmUBO);
	m_deferredFBO.reset();
	m_shadowFBO.reset();
	m_cloudsShaddowFBO.reset();
	m_ssaoFBO.reset();
	m_ssaoBlurFBO.reset();

	glDeleteTextures(1, &m_noiseTexture);	
}

void GraphicsEngine::Run()
{
	if (!m_initialized) throw NotInitializedException("GEdngine not initialized!");

	UBOstruct record;
	UBOcsm csmRec;
	auto camera = PerspectiveCamera::GetCameraInstance("Basic");
	auto invView = glm::inverse(camera->GetViewMatrix());
	auto sunPos = m_skyManager->SunPosition();

	for(auto i = 0; i < 4; i++) {
		auto csm = DirectCamera::GetCameraInstance("CSM"+std::to_string(i));
		csm->Recalc(sunPos, invView);
		csmRec.csmVP[i] = csm->GetMVP();
	}
	
	SceneManager::Get()->SetWorld("Clouds");
	for (auto i = 0; i < 1; i++) {
		auto csm = DirectCamera::GetCameraInstance("cloudsCSM" + std::to_string(i));
		csm->Recalc(sunPos, invView,{20,20});
		csmRec.csmCLOUDS[i] = csm->GetMVP();
	}
	SceneManager::Get()->SetWorld("");

	glBindBuffer(GL_UNIFORM_BUFFER, m_csmUBO);
	GLvoid* pointer = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
	memcpy_s(pointer, sizeof(UBOcsm), &csmRec, sizeof(UBOcsm));
	glUnmapBuffer(GL_UNIFORM_BUFFER);
	
	record.BasicVP = record.CurrentVP = camera->GetMVP();
	record.BasicP = record.CurrentP = camera->GetProjectionMatrix();
	record.BasicV = record.CurrentV = camera->GetViewMatrix();
	record.BasicInvP = camera->GetInverseProjectionMatrix();
	record.BasicInvV = invView;
	record.BasicInvVP = glm::inverse(camera->GetProjectionMatrix() * camera->GetViewMatrix());
	record.Time = TimeManager::GetInstance()->GetGameTime();
	record.CloudsTime = record.Time /800;//0.126531184f;
	std::mt19937 rnd(record.CloudsTime);
	std::exponential_distribution<float> dist(1.5f);
	record.CloudsIntensity = 2 / glm::pi<float>() * atan(dist(rnd));

	auto ScreenDim = camera->GetViewport();
	record.ScreenDim = glm::ivec4(ScreenDim, 1, 1);

	glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
	pointer = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
	memcpy_s(pointer, sizeof(UBOstruct), &record, sizeof(UBOstruct));
	glUnmapBuffer(GL_UNIFORM_BUFFER);
	
	
	
	
	Render();

	_program->Use();
	//Render();
	_program->Unuse();

	
}

void GraphicsEngine::Render()
{
	auto camera = PerspectiveCamera::GetCameraInstance("Basic");

	//shadow pass 
  //*/
	m_cloudsShaddowFBO->BindForWriting();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	m_skyManager->Run(SHADOW);
	glDisable(GL_BLEND);
	
	
	m_shadowFBO->BindForWriting();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(4.5f, 1000);
	m_terrainManager->Run(SHADOW);
	m_GBuffer->Draw(SHADOW);
	m_forrestManager->Run(SHADOW);
	glDisable(GL_POLYGON_OFFSET_FILL);


	//geometry pass
	m_deferredFBO->BindForWriting();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	m_terrainManager->Run();	
	m_GBuffer->Draw();
	m_skyManager->Run();	
	
	
	
	
	glBindVertexArray(m_lightingVAO);
	
	m_ssaoFBO->BindForWriting();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_ssaoShader->Use();
	m_deferredFBO->BindForReading();
	m_deferredFBO->BindDepthForReading(5);
	

	m_ssaoShader->SetVec3f(0, m_ssaoKernel.size(), glm::value_ptr(m_ssaoKernel[0]));
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_noiseTexture);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	m_ssaoShader->Unuse();
	
	m_ssaoBlurShader->Use();
	m_ssaoBlurFBO->BindForWriting();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	m_ssaoFBO->BindForReading();
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	m_ssaoBlurShader->Unuse();

	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_lightingShader->Use();
	m_deferredFBO->BindForReading();
	m_ssaoBlurFBO->BindForReading(4);
	m_cloudsShaddowFBO->BindForReading(5);
	m_shadowFBO->BindDepthForReading(6);

	auto sunPos = m_skyManager->SunPosition();
	auto mult = -(std::powf(std::abs(sunPos.x), 7) - 1);
	auto sunColor = m_sunMinColor + m_sunColorMultplier*mult;
	auto sunAmbient = m_sunMinAmbient + m_sunAmbientMultiplier*mult;
	auto fogIntesity = 0.4 + m_fogIntensityMapper *(sunAmbient- m_sunMinAmbient);
	
	auto SSlightPos = camera->GetMVP() * glm::vec4(sunPos, 1.f);
	auto SSlight = glm::vec2(SSlightPos.x, SSlightPos.y);
	//SSlight = SSlight * glm::vec2(0.5, 0.5) + glm::vec2(0.5, 0.5);	
	//SSlight = (SSlight / SSlightPos.w);

	m_lightingShader
		->Set1f(2, sunAmbient)
		->Set1f(6, fogIntesity)
		->SetVec3f(3, 1, glm::value_ptr(sunColor))
		->SetVec3f(4, 1, glm::value_ptr(sunPos))
		->SetVec3f(5, 1, glm::value_ptr(PerspectiveCamera::GetCameraInstance("Basic")->GetPosition()))
		->SetBool(8,m_ligthing)
		//->SetVec2f(7, 1, glm::value_ptr(SSlight))
    ;


	glDrawArrays(GL_TRIANGLE_STRIP, 0, 10);

	m_lightingShader->Unuse();
	glBindVertexArray(0);

  	MainGame::Get()->SaveScreenshot();
	
}

void GraphicsEngine::SSAOInit(uint xres, uint yres)
{
	GLuint texture;

	m_ssaoBlurFBO = std::make_unique<RBuffer>();
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, xres, yres, 0, GL_RGB, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	m_ssaoBlurFBO->Init(texture, xres, yres);
	

	m_ssaoFBO = std::make_unique<RBuffer>();
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, xres, yres, 0, GL_RGB, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	m_ssaoFBO->Init(texture, xres, yres);



	std::mt19937 rnd(42);
	std::uniform_real_distribution<float> dist1(-1.f, 1.f);
	std::uniform_real_distribution<float> dist0(0.f, 1.f);

	//creating random seed texture
	for (auto i = 0; i < 16; ++i)
	{
		glm::vec3 sample(
			dist1(rnd),
			dist1(rnd),
			dist0(rnd)
		);

		sample = glm::normalize(sample);
		sample *= dist0(rnd);
		auto scale = i / 16.f;
		if (fabs(sample.x) < 0.2f && fabs(sample.y) < 0.3f) {
			i--;
			continue;
		}
		
		scale = 0.1f + scale*scale*0.9f; 			
		sample *= scale;
		auto pom = sample.x * sample.x + sample.z*sample.z + sample.y*sample.y;
		m_ssaoKernel.push_back(sample);
	}

	std::vector<glm::vec3> ssaoNoise;
	for (auto i = 0; i < 16; i++)
	{
		glm::vec3 noise(
			dist1(rnd),
			dist1(rnd),
			0.0f);
		ssaoNoise.push_back(noise);
	}

	glGenTextures(1, &m_noiseTexture);
	glBindTexture(GL_TEXTURE_2D, m_noiseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void GraphicsEngine::CSMInit(uint xres, uint yres, int parts)
{
	//classic shadows
	m_shadowFBO = std::make_unique<RBuffer>();
	m_shadowFBO->InitLayered(0, xres, yres, static_cast<int>(parts));
	
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D_ARRAY, texture);
	
	//clouds shaddows texture
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_R8, xres, yres, parts);
	
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	m_cloudsShaddowFBO = std::make_unique<RBuffer>();
	m_cloudsShaddowFBO->InitLayered(texture, xres, yres, static_cast<int>(parts));
	
}

void GraphicsEngine::CreateCSMCameras(float nearPlane, float farPlane, int parts, int base, const std::string &name)
{
	auto camera = PerspectiveCamera::GetCameraInstance("Basic");
	auto wm = camera->GetWM();
	auto fov = camera->initialFoV;

	auto lNear = nearPlane;
	auto lFar = nearPlane;

	int powBase = 1;
	int denominator = 0;
	for(int i = 0; i < parts; i++) {
		denominator += powBase;
		powBase *= base;
	}

	auto frac = (farPlane - nearPlane) / denominator;
	powBase = 1;
	for(int i =0; i< parts; i++) {
		lNear = lFar;
		lFar = lNear + frac * powBase;
		powBase *= base;

		DirectCamera::GetCamera(name + std::to_string(i), wm, lNear, lFar, fov);
	}

}
