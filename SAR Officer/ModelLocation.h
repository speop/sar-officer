// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 03-04-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-05-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <glm/glm.hpp>
#include <vector>

/// <summary>
/// Class whitch respresents model spatial attributes.
/// </summary>
class ModelLocation
{
	
	glm::vec3 m_minCorner;
	glm::vec3 m_maxCorner;
	glm::mat4 m_position;

public:
	/// <summary>
	/// Initializes a new instance of the <see cref="ModelLocation"/> class.
	/// </summary>
	ModelLocation();

	/// <summary>
	/// Initializes a new instance of the <see cref="ModelLocation"/> class.
	/// </summary>
	/// <param name="minCorner">The minimum corner.</param>
	/// <param name="maxCorner">The maximum corner.</param>
	/// <param name="position">The position.</param>
	ModelLocation(glm::vec3 minCorner, glm::vec3 maxCorner, glm::mat4 position);

	/// <summary>
	/// Recalcs the aabb.
	/// </summary>
	/// <param name="min">The minimum.</param>
	/// <param name="max">The maximum.</param>
	/// <param name="transform">The transform.</param>
	void static RecalcAABB(glm::vec3 &min, glm::vec3 &max, const glm::mat4 &transform);

	/// <summary>
	/// Creates the specified transforms.
	/// </summary>
	/// <param name="transforms">The transforms.</param>
	/// <param name="modelLocations">The model locations.</param>
	/// <param name="minCorner">The minimum corner.</param>
	/// <param name="maxCorner">The maximum corner.</param>
	void static Create(const std::vector<glm::mat4> &transforms, std::vector<ModelLocation> &modelLocations, glm::vec3 minCorner, glm::vec3 maxCorner);
	
	/// <summary>
	/// Creates the specified transforms.
	/// </summary>
	/// <param name="transforms">The transforms.</param>
	/// <param name="modelLocations">The model locations.</param>
	/// <param name="minCorner">The minimum corner.</param>
	/// <param name="maxCorner">The maximum corner.</param>
	void static Create(const std::vector<glm::mat4> &transforms, std::vector<ModelLocation> &modelLocations, glm::vec4 minCorner, glm::vec4 maxCorner);
	
	/// <summary>
	/// Recalcs the aabb.
	/// </summary>
	/// <param name="transform">The transform.</param>
	void RecalcAABB(const glm::mat4 &transform);
	
	/// <summary>
	/// Gets the minimum corner.
	/// </summary>
	/// <returns>glm.vec3.</returns>
	glm::vec3 GetMinCorner();
	
	/// <summary>
	/// Gets the maximum corner.
	/// </summary>
	/// <returns>glm.vec3.</returns>
	glm::vec3 GetMaxCorner();
	
	/// <summary>
	/// Gets the position.
	/// </summary>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetPosition();
	
	/// <summary>
	/// Gets the minimum corner.
	/// </summary>
	/// <returns>glm.vec3.</returns>
	glm::vec3 GetMinCorner() const;
	
	/// <summary>
	/// Gets the maximum corner.
	/// </summary>
	/// <returns>glm.vec3.</returns>
	glm::vec3 GetMaxCorner() const;
	
	/// <summary>
	/// Gets the position.
	/// </summary>
	/// <returns>glm.mat4.</returns>
	glm::mat4 GetPosition() const;
	
	/// <summary>
	/// Finalizes an instance of the <see cref="ModelLocation"/> class.
	/// </summary>
	~ModelLocation();
};

