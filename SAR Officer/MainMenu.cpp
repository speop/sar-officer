#include "MainMenu.h"
#include "ConfigStore.h"
#include "AudioManager.h"
#include "Translator.h"
#include "MainGame.h"
#include "KeyboardLayoutManager.h"
#include <string.h>


MainMenu::MainMenu()
{
	m_backgroundShader =  std::make_shared<Shader>();
	Init();
}


MainMenu::~MainMenu()
{
	Burn();
}

void MainMenu::Wake()
{
	AudioManager::Get()->MainMenu(true);

	std::mt19937 engine;
	auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	engine.seed((unsigned long)seed);
	auto dist = std::uniform_int_distribution<>(0, m_backgroundS.size() - 1);
	m_bgIndex = dist(engine);
}

void MainMenu::Init()
{
	m_backgroundShader->BuildShaders({ ShaUN(GL_VERTEX_SHADER, "Shaders/Mmenu.vert"), ShaUN(GL_FRAGMENT_SHADER, "Shaders/Mmenu.frag") });
	auto backgrounds = ConfigStore::Instance()->GetMainMenuBackground();
	std::size_t pos;
	std::string bg;

	do {
		pos = backgrounds.find_first_of(';');		
		if(pos != std::string::npos) {
			bg = backgrounds.substr(0, pos);
			backgrounds = backgrounds.substr(pos+1);
		}else {
			bg = backgrounds;
		}

		auto ptr = std::make_unique<Texture>();
		ptr->Load(0, { bg });
		ptr->SetShader(m_backgroundShader);
		m_backgroundS.push_back(std::move(ptr));

	} while (pos != std::string::npos);
	

	glGenVertexArrays(1, &m_VAO);

	m_Screens[Screen::ScrMain] = &MainMenu::Main;
	m_Screens[ScrLoading] = &MainMenu::Loading;
	m_Screens[ScrSettings] = &MainMenu::Settings;
	m_Screens[ScrKeyboard] = &MainMenu::Keyboard;

	m_languages = ConfigStore::Instance()->GetLanguages();
	auto gameRes = ConfigStore::Instance()->GetResolutions();
	auto supportedRes = MainGame::Get()->GetVideoModes();

	for (auto &res : gameRes) {
		for(auto &sRes : supportedRes) {
			if(res.x == sRes.x && res.y == sRes.y) {
				m_resolutions.push_back(res);
				break;
			}
		}
	}
	
	RefrehSettingValues();
	RefrehKeyboardValues();

}


void MainMenu::Run()
{
	

	glBindVertexArray(m_VAO);
	m_backgroundShader->Use();
	m_backgroundS[m_bgIndex]->Use("Background");
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	m_backgroundShader->Unuse();
	glBindVertexArray(0);

}

void MainMenu::RunImgui()
{
	Screen scr = m_currentScreen;
	
	auto func = m_Screens[scr];
	if(func != nullptr) (this->*func)();

}

void MainMenu::Stop()
{
	AudioManager::Get()->MainMenu(false);
	m_currentScreen = ScrMain;
}

void MainMenu::Burn()
{
	if (m_keys != nullptr) delete[] m_keys;
	m_keys = nullptr;
}

void MainMenu::RefrehSettingValues()
{
	auto conf = ConfigStore::Instance();
	m_Swindowed = !conf->GetFullscreen();

	auto xRes = conf->GetXRes();
	auto yRes = conf->GetYRes();
	
	m_SselectedRes = 0;
	for (auto i = 0; i<m_resolutions.size(); i++) {	
		if (xRes == m_resolutions[i].x && yRes == m_resolutions[i].y) {
			m_SselectedRes = i;
			break;
		}
	}

	m_SselectedLang = 0;
	auto lang = conf->GetLanguage();
	for (auto i = 0; i<m_languages.size(); i++) {
		if (lang == m_languages[i].key) {
			m_SselectedLang = i;
			break;
		}
	}
	
	m_SaudioFXLevel = conf->GetAudioFXlevel();
	m_SmusicLevel = conf->GetMusicLevel();

}

void MainMenu::RefrehKeyboardValues()
{
	if (m_keys != nullptr) delete[] m_keys;
	m_keys = KeyboardLayoutManager::Instance()->GetKeys();
}

#pragma region Screens
void MainMenu::Main()
{
	bool open;
	auto TR = Translator::Get();
	auto display = ImGui::GetIO().DisplaySize;
	auto wX = 300.f;
	auto wY = display.y - 400.f;
	ImGui::SetNextWindowPos(ImVec2(wX, wY));

	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImColor(0, 0, 0, 0));
	ImGui::Begin("Main menu", &open,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoScrollbar |
		ImGuiWindowFlags_NoCollapse
	);

	auto i = 2;
	auto pom = ImColor(0, 200, 12);
	ImGui::PushStyleColor(ImGuiCol_Button, cBtnBlue);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cBtnBlueHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cBtnBlueActive);

	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(17, 6));
	ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.f);
	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 11));

	if (ImGui::Button(TR->Trans("NewGame", "Menu").c_str())) NewGameBtn_click();
	if (ImGui::Button(TR->Trans("Settings", "Menu").c_str())) SettingsBtn_click();
	if (ImGui::Button(TR->Trans("Keyboard", "Menu").c_str())) KeyboardBtn_click();	
	if (ConfigStore::Instance()->EditorEnabled()) {
		if (ImGui::Button(TR->Trans("Editor", "Menu").c_str())) EditorBtn_click();
	}
	if (ImGui::Button(TR->Trans("ExitToWindows", "Menu").c_str())) ExitToWindowsBtn_click();


	ImGui::PopStyleColor(3);
	ImGui::PopStyleVar(3);
	ImGui::End();
	ImGui::PopStyleColor();
}

void MainMenu::Loading()
{
	bool open;

	auto display = ImGui::GetIO().DisplaySize;

	auto elemW = display.x * 0.8f;
	auto wX = display.x * 0.1f;
	auto wY = display.y  -220.f;
	
	ImGui::SetNextWindowSize(ImVec2(elemW, 50.f));
	ImGui::SetNextWindowPos(ImVec2(wX, wY));
	//ImGui::PushItemWidth(400);
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImColor(0, 0, 0, 0));
	ImGui::Begin("Loading", &open,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoScrollbar |
		ImGuiWindowFlags_NoCollapse
	);

	ImGui::PushStyleColor(ImGuiCol_PlotHistogram, Color("#FFCF4B"));
	ImGui::ProgressBar(MainGame::Get()->LoadingProgress(), ImVec2(elemW, 40.0f));
	ImGui::PopStyleColor();
	
	ImGui::End();
	ImGui::PopStyleColor();
	//ImGui::PopItemWidth();
}

void MainMenu::Settings()
{
	bool open;
	auto TR = Translator::Get();
	auto width = 900.f;
	auto margin = 50.f;


	auto display = ImGui::GetIO().DisplaySize;
	auto wX = display.x * 0.5f - width *0.5f;
	auto maxHeight = display.y - 2 * margin;

	ImGui::SetNextWindowPos(ImVec2(wX, margin));
	ImGui::SetNextWindowSizeConstraints(ImVec2(width, 0.f), ImVec2(width, maxHeight));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(50.f, 25.f));

	ImGui::Begin("settings", &open,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoCollapse
	);

	auto tSize = ImGui::CalcTextSize(TR->Trans("Settings", "Menu").c_str());
	ImGui::SetCursorPosX(0.5f * (width - tSize.x));
	ImGui::Text(TR->Trans("Settings", "Menu").c_str());

	auto rowH = 50;
	auto cursorY = tSize.y + margin;
	ImGui::SetCursorPosY(cursorY);

	ImGui::PushStyleColor(ImGuiCol_Button, cBtnBlue);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cBtnBlueHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cBtnBlueActive);

	ImGui::Columns(2, "MainGrid", false);

	ImGui::Text(TR->Trans("Resolution", "Settings").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::Text(TR->Trans("Windowed", "Settings").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::Text(TR->Trans("Music", "Settings").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::Text(TR->Trans("SoundFX", "Settings").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::Text(TR->Trans("Language", "Settings").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::NextColumn();
	ImGui::SetColumnOffset(-1, 350);
	cursorY = tSize.y + margin;


	char** resolutions = new char*[m_resolutions.size()];

	for (auto i = 0; i < m_resolutions.size(); i++) {
		std::string str = std::to_string(m_resolutions[i].x) + "x" + std::to_string(m_resolutions[i].y);
		char* c = new char[str.length() + 1];
		strcpy_s(c, str.length() + 1, str.c_str());
		resolutions[i] = c;
	}
	
	ImGui::PushStyleColor(ImGuiCol_Header, cDropdownItem);
	ImGui::PushStyleColor(ImGuiCol_HeaderHovered, cDropdownItemHover);
	ImGui::PushStyleColor(ImGuiCol_HeaderActive, cDropdownItemActive);

	ImGui::Combo("##resolutions", &m_SselectedRes, resolutions, static_cast<int>(m_resolutions.size()));
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);
	
	for (auto i = 0; i < m_resolutions.size(); i++) {
		delete[] resolutions[i];
	}
	delete[] resolutions;
	
	//ImGui::PushStyleColor(ImGuiCol_CheckMark, cBtnBlue);
	//
	ImGui::PushStyleColor(ImGuiCol_CheckMark, Color("008B80"));
	ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, cDropdownItemHover);
	ImGui::PushStyleColor(ImGuiCol_FrameBgActive, cDropdownItemActive);

	ImGui::Checkbox("##windowed", &m_Swindowed);
	ImGui::PopStyleColor(3);
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::PushStyleColor(ImGuiCol_SliderGrab, cDropdownItem);
	ImGui::PushStyleColor(ImGuiCol_SliderGrabActive, cDropdownItemActive);

	ImGui::SliderFloat("##musicVolume", &m_SmusicLevel, 0, 100,"%.0f %%");
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::SliderFloat("##audioVolume", &m_SaudioFXLevel, 0, 100, "%.0f %%");
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);
	 
	
	char** languages = new char*[m_languages.size()];

	for (auto i = 0; i<m_languages.size(); i++) {
		auto str = m_languages[i].name;
		char* c = new char[str.length() + 1];
		strcpy_s(c, str.length() + 1, str.c_str());
		languages[i] = c;
	}

	ImGui::Combo("##language", &m_SselectedLang, languages, static_cast<int>(m_languages.size()));

	for (auto i = 0; i < m_languages.size(); i++) {
		delete[] languages[i];
	}
	delete[] languages;

	cursorY += rowH + 80;
	ImGui::Columns();


	ImGui::SetCursorPos(ImVec2(width - 200, cursorY));
	if (ImGui::Button(TR->Trans("BtnSave", "Buttons").c_str())) {
		SettingsSaveBtn_click();
		TR = Translator::Get();
	}
	ImGui::SameLine();
	if (ImGui::Button(TR->Trans("BtnCancel", "Buttons").c_str())) SettingsCancelBtn_click();
	

	ImGui::PopStyleColor(8);
	ImGui::End();
	ImGui::PopStyleVar();

}

void MainMenu::Keyboard()
{


	bool open;
	auto TR = Translator::Get();
	auto width = 900.f;
	auto margin = 50.f;


	auto display = ImGui::GetIO().DisplaySize;
	auto wX = display.x * 0.5f - width *0.5f;
	auto maxHeight = display.y - 2 * margin;

	auto key = MainGame::Get()->GetSafeKey();
	
	if(m_modalW && key > 0) {				
		m_modalW = false;
		m_keys[m_key] = key;
	}

	ImGui::SetNextWindowPos(ImVec2(wX, margin));
	ImGui::SetNextWindowSizeConstraints(ImVec2(width, 0.f), ImVec2(width, maxHeight));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(50.f, 25.f));

	ImGui::Begin("settings", &open,
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoCollapse
	);

	auto tSize = ImGui::CalcTextSize(TR->Trans("Keyboard", "Menu").c_str());
	ImGui::SetCursorPosX(0.5f * (width - tSize.x));
	ImGui::Text(TR->Trans("Keyboard", "Menu").c_str());

	auto rowH = 50;
	auto cursorY = tSize.y + margin;
	ImGui::SetCursorPosY(cursorY);

	

	ImGui::Columns(2, "MainGrid", false);

	ImGui::Text(TR->Trans("Forward", "Keyboard").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::Text(TR->Trans("Backward", "Keyboard").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::Text(TR->Trans("Left", "Keyboard").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::Text(TR->Trans("Right", "Keyboard").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	ImGui::Text(TR->Trans("Jump", "Keyboard").c_str());
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);



	ImGui::NextColumn();
	ImGui::SetColumnOffset(-1, 350);
	cursorY = tSize.y + margin;

	//just pretend this buttons are inputs
	ImGui::PushStyleColor(ImGuiCol_Button, cInput);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cInputHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cInputActive);

	auto btnSize = ImVec2(200, 30);
	int tag = 0;

		
	if (ImGui::Button(KeyboardBtnName(m_keys[KeyboardLayoutManager::FORWARD], tag++).c_str(), btnSize)) {
		m_key = KeyboardLayoutManager::FORWARD;
		m_modalW = true;
	};	
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	if (ImGui::Button(KeyboardBtnName(m_keys[KeyboardLayoutManager::BACKWARD], tag++).c_str(), btnSize)) {
		m_key = KeyboardLayoutManager::BACKWARD;
		m_modalW = true;
	};	
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	if (ImGui::Button(KeyboardBtnName(m_keys[KeyboardLayoutManager::LEFT], tag++).c_str(), btnSize)) {
		m_key = KeyboardLayoutManager::LEFT;
		m_modalW = true;
	};
	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	
	if (ImGui::Button(KeyboardBtnName(m_keys[KeyboardLayoutManager::RIGHT], tag++).c_str(), btnSize)) {
		m_key = KeyboardLayoutManager::RIGHT;
		m_modalW = true;
	};

	cursorY += rowH;
	ImGui::SetCursorPosY(cursorY);

	if (ImGui::Button(KeyboardBtnName(m_keys[KeyboardLayoutManager::JUMP], tag++).c_str(), btnSize)) {
		m_key = KeyboardLayoutManager::JUMP;
		m_modalW = true;
	};



	cursorY += rowH + 80;
	ImGui::PopStyleColor(3);

	ImGui::Columns();

	ImGui::PushStyleColor(ImGuiCol_Button, cBtnBlue);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, cBtnBlueHover);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, cBtnBlueActive);
	ImGui::SetCursorPos(ImVec2(width - 200, cursorY));
	if (ImGui::Button(TR->Trans("BtnSave", "Buttons").c_str())) KeyboardSaveBtn_click() ;
	ImGui::SameLine();
	if (ImGui::Button(TR->Trans("BtnCancel", "Buttons").c_str())) KeyboardCancelBtn_click();


	ImGui::PopStyleColor(3);
	ImGui::End();
	ImGui::PopStyleVar();

	if (m_modalW) {
		ImGui::OpenPopup("Delete?");
		
		auto modalDim = ImVec2(300.f, 150.f);

		ImGui::SetNextWindowSize(modalDim);
		ImGui::BeginPopupModal("Delete?", nullptr, ImGuiWindowFlags_AlwaysAutoResize |
			ImGuiWindowFlags_NoTitleBar |
			ImGuiWindowFlags_NoResize |
			ImGuiWindowFlags_NoMove |
			ImGuiWindowFlags_NoCollapse);
		
		auto textSize = ImGui::CalcTextSize(TR->Trans("PressKey", "Keyboard").c_str());
		
		ImGui::SetCursorPos(ImVec2(0.5f*(modalDim.x - textSize.x), 0.5f*(modalDim.y - textSize.y)));
		ImGui::Text(TR->Trans("PressKey", "Keyboard").c_str());
		
			
		ImGui::EndPopup();
		
	}

	

}
#pragma endregion

#pragma region Buttons events
void MainMenu::NewGameBtn_click()
{
	MainGame::Get()->LoadLevel("");
	m_currentScreen = Screen::ScrLoading;
}

void MainMenu::SettingsBtn_click()
{
	m_currentScreen = ScrSettings;
}

void MainMenu::KeyboardBtn_click()
{
	m_currentScreen = ScrKeyboard;
}

void MainMenu::EditorBtn_click()
{
	MainGame::Get()->LoadEditor();
	m_currentScreen = Screen::ScrLoading;
}

void MainMenu::ExitToWindowsBtn_click()
{
	MainGame::Get()->Exit();
}

void MainMenu::SettingsSaveBtn_click()
{
	m_currentScreen = ScrMain;
	auto xRes = ConfigStore::Instance()->GetXRes();
	auto yRes = ConfigStore::Instance()->GetYRes();
	auto lng = ConfigStore::Instance()->GetLanguage();

	auto res = m_resolutions[m_SselectedRes];
	bool resChanged = xRes != res.x || yRes != res.y;

	ConfigStore::Instance()
		->SetAudioFXlevel(m_SaudioFXLevel)
		->SetFullscreen(!m_Swindowed)
		->SetLanguage(m_languages[m_SselectedLang].key)
		->SetMusicLevel(m_SmusicLevel)
		->SetXres(res.x)
		->SetYres(res.y)
		->Save();

	if (resChanged) MainGame::Get()->SetResolution(res);
	if (lng != m_languages[m_SselectedLang].key) Translator::Get()->Reload();
}

void MainMenu::SettingsCancelBtn_click()
{
	m_currentScreen = ScrMain;
	RefrehSettingValues();	
}

void MainMenu::KeyboardSaveBtn_click()
{
	m_currentScreen = ScrMain;
	KeyboardLayoutManager::Instance()->SaveKeys(m_keys);
}

void MainMenu::KeyboardCancelBtn_click()
{
	m_currentScreen = ScrMain;
	RefrehKeyboardValues();
}
#pragma endregion 


#pragma region Helpers
std::string MainMenu::KeyboardBtnName(int GLFW_code, int tag)
{
	std::string name;
	auto namePtr = glfwGetKeyName(GLFW_code, 0);
	
	if(namePtr != nullptr) {
		name = namePtr;
	}else {
		name = Translator::Get()->Trans("C" + std::to_string(GLFW_code), "KeyNames");
		name = name == "" ? "???" : name;
	}

	return name + "##" + std::to_string(tag);
}

#pragma endregion 