// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-15-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once

#include <string>
#include "Defines.h"


/// <summary>
/// Class BasicException.
/// </summary>
class BasicException
{
protected:
	
	std::string message;

public:
	/// <summary>
	/// Initializes a new instance of the <see cref="BasicException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	BasicException(std::string msg) : message(msg) {};
	/// <summary>
	/// Finalizes an instance of the <see cref="BasicException"/> class.
	/// </summary>
	virtual ~BasicException() {};

	/// <summary>
	/// Gets the exception MSG.
	/// </summary>
	/// <returns>std.string.</returns>
	virtual std::string GetMsg() { return message; }
};

/// <summary>
/// Class cannot create OGL buffer Exception.
/// </summary>
/// <seealso cref="BasicException" />
class CannotCreateOGLBufferException : public BasicException {
public: 
	/// <summary>
	/// Initializes a new instance of the <see cref="CannotCreateOGLBufferException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	CannotCreateOGLBufferException(std::string msg) : BasicException(msg) {};
};

/// <summary>
/// Class Fatal error Exception.
/// </summary>
/// <seealso cref="BasicException" />
class FatalErrorException : public BasicException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="FatalErrorException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	FatalErrorException(std::string msg) : BasicException(msg) {};
};


/// <summary>
/// Class NotInitializedException.
/// </summary>
/// <seealso cref="BasicException" />
class NotInitializedException : public BasicException {
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="NotInitializedException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	NotInitializedException(std::string msg) : BasicException(msg) {};
};

