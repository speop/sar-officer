#include "FileManager.h"


FileManager::FileManager(const std::string& filePath)
{
	m_filename = filePath;
	m_file = std::ifstream(filePath);
	if (m_file.fail()) {
		throw BasicException("Failed to load: " + filePath);
	}
}

std::vector<std::string> FileManager::ReadFile(const std::string & filePath)
{
	std::string fileContent = "";
	std::string line;
	std::ifstream file(filePath);
	std::vector<std::string> shader;

	if (file.fail()){
		perror(filePath.c_str());
		throw new BasicException("Failed to load: " + filePath);
	}

	while (std::getline(file, line)){
		shader.push_back(line + "\n");
	}
	return shader;
}

std::pair<bool, std::string> FileManager::GetLine()
{
	std::string line;
	if (std::getline(m_file, line)) {
		return{ true, line + "\n" };
	}
	else {
		return{ false, line};
	}
}
