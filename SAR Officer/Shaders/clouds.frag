#version 440 
//uniform float time;
#pragma sar include("Shaders/common.include")

#ifdef SHADER_SHADOW
in GS_OUT {
    vec2 TexCoords;
} vertexData;

layout (location = 0) out float shadow;

#else
#pragma sar include("Shaders/commonGeometryFrag.include")
in VS_OUT { vec2 vertexCoords; } frag_in; 
#endif

layout (location = 0) uniform vec3 sunPos;
layout (location = 1)  uniform float TimeOffset;


//  Simplex 3D Noise 
//  by Ian McEwan, Ashima Arts
//
vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}
vec4 taylorInvSqrt(vec4 r){return 1.79284291400159 - 0.85373472095314 * r;}

float Snoise(vec3 v){ 
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //  x0 = x0 - 0. + 0.0 * C 
  vec3 x1 = x0 - i1 + 1.0 * C.xxx;
  vec3 x2 = x0 - i2 + 2.0 * C.xxx;
  vec3 x3 = x0 - 1. + 3.0 * C.xxx;

// Permutations
  i = mod(i, 289.0 ); 
  vec4 p = permute( permute( permute( 
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients
// ( N*N points uniformly over a square, mapped onto an octahedron.)
  float n_ = 1.0/7.0; // N=7
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z *ns.z);  //  mod(p,N*N)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                                dot(p2,x2), dot(p3,x3) ) );
}

float OctavePerlin(vec3 P, int octaves, float persistence) {
    float total = 0;
    float frequency = 1;
    float amplitude = 1;
    float maxValue = 0;  // Used for normalizing result to 0.0 - 1.0
    for(int i=0;i<octaves;i++) {
        total += Snoise(P * frequency) * amplitude;
        
        maxValue += amplitude;
        
        amplitude *= persistence;
        frequency *= 2;
    }
    
    return total/maxValue;
}

float FBM(vec3 P, int octaves){
	float total = 0;
    float frequency = 1;
    float amplitude = 1;
	float lacunarity = 1.8715 ;
	float gain= 0.5;
	 

	for (int i = 0; i < octaves; ++i) 
	{ 
		total += Snoise(P * frequency) * amplitude; 
		frequency *= lacunarity; 
		amplitude *= gain; 
	}

	return total;
}


//  Function from I�igo Quiles 
//  www.iquilezles.org/www/articles/functions/functions.htm
float cubicPulse(float x, float c, float w){
    x = abs(x - c);
    if( x>w ) return 0.0;
    x /= w;
    return 1.0 - x*x*(3.0-2.0*x);
}

void main(){
	vec2 coords;
	#ifdef SHADER_SHADOW
		coords = vertexData.TexCoords;
	#else
		coords = frag_in.vertexCoords;
	#endif
	
	float noise = FBM(vec3(coords/8000 +(CloudsTime+TimeOffset)*5, (CloudsTime+TimeOffset)*2),16)/7.5f;
	vec2 fade = vec2(6000, 6000) - abs(coords);
	float fadeVal = min(fade.x, fade.y);
	
	fadeVal = fadeVal <  6000 ? 1 + fadeVal/2000.f : 1.f;


	float sharpness = 0.3;
	float aNoise = noise;

	const float gamma = mix(1.2, 4.6f,CloudsIntensity);  //5.6
    // Reinhard tone mapping
     noise = noise / (noise + 1.0);
    // Gamma correction 
    noise = pow(noise, 1.0 / gamma);
	noise = 1- noise;
	
	
	aNoise = cubicPulse(noise,0.5, 0.3);
	aNoise = aNoise * fadeVal;
	//noise *= 1.15;
	#ifdef SHADER_SHADOW
	shadow = noise * aNoise;
	#else

	vec3 cloudColor;
	if(sunPos.y <= 0.3f){
		float color = smoothstep(0.f, 0.3f, sunPos.y);
		cloudColor = mix(vec3(0.529, 0.318, 0.008), vec3(1,1,1), vec3(color,color,color));
	}else{
		cloudColor = vec3(1,1,1);
	}
	o_diffuseAndSpecular = vec4(noise*cloudColor,aNoise);
	o_normal = vec3(0,0,0);
	o_position = vec3(1,1,1);
	o_ID = aNoise < 0.2f ? SKY_ID : CLOUDS_ID;
	#endif
 }
