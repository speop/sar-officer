// shadertype=glsl

#version 440

#pragma sar include("Shaders/common.include")

uniform float TerrainHeightOffset;
layout(binding=0) uniform sampler2D TexTerrainHeight;



//
// Inputs
//
layout(quads, fractional_even_spacing) in;


in vec2 tcs_terrainTexCoord[];
in vec2 tcs_coordsTriangle[];
in float tcs_tessLevel[];

//
// Outputs
//

in TCS_OUT { vec2 coordsTriangle; flat float tileSize; } tes_in[];
  
#ifdef SHADER_SHADOW
out GS_IN{
	vec2 TexCoords;
	} tes_out;
#else
out TES_OUT { 
	vec2 coordsTriangle; 
	vec2 terrainTexCoord;
	vec3 position;
	vec3 viewPos;
	flat float tileSize;
	float depth;
	} tes_out; 
#endif


vec4 interpolate(in vec4 v0, in vec4 v1, in vec4 v2, in vec4 v3)
{
	vec4 a = mix(v0, v1, gl_TessCoord.x);
	vec4 b = mix(v3, v2, gl_TessCoord.x);
	return mix(a, b, gl_TessCoord.y);
}

vec2 interpolate2(in vec2 v0, in vec2 v1, in vec2 v2, in vec2 v3)
{
	vec2 a = mix(v0, v1, gl_TessCoord.x);
	vec2 b = mix(v3, v2, gl_TessCoord.x);
	return mix(a, b, gl_TessCoord.y);
}

void main()
{
	
	// Calculate the vertex position using the four original points and interpolate depending on the tessellation coordinates.	
	vec4 position = interpolate(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_in[2].gl_Position, gl_in[3].gl_Position);
	
	// Terrain heightmap coords
	vec2 terrainTexCoord = interpolate2(tcs_terrainTexCoord[0], tcs_terrainTexCoord[1], tcs_terrainTexCoord[2], tcs_terrainTexCoord[3]);

	// Sample the heightmap and offset y position of vertex
	//vec4 samp = texture(TexTerrainHeight, terrainTexCoord);
	float temp = texture(TexTerrainHeight, terrainTexCoord).r;
	position.y = temp*3000 + TerrainHeightOffset;
	
	

	// Project the vertex to clip space and send it along
	//vec4 pom =  gl_Position*modelMatrix;
	//gl_Position =  perDraw[tes_in[0].DrawID].modelViewMatrix * gl_Position;

	
	
#ifdef SHADER_SHADOW
	gl_Position = position;
	tes_out.TexCoords = interpolate2(tes_in[0].coordsTriangle, tes_in[1].coordsTriangle,tes_in[2].coordsTriangle, tes_in[3].coordsTriangle);
#else
	tes_out.position = position.xyz;
	position = BasicV *position;
	tes_out.viewPos = position.xyz;
	position = BasicP * position;
	tes_out.depth = position.z;
	tes_out.tileSize = tes_in[0].tileSize;
	
	gl_Position = position;

	// quad texture position
	tes_out.coordsTriangle = interpolate2(tes_in[0].coordsTriangle, tes_in[1].coordsTriangle,tes_in[2].coordsTriangle, tes_in[3].coordsTriangle);

	//heightmap coordination
	tes_out.terrainTexCoord = terrainTexCoord;
#endif
	
	
}