#version 450

out vec2 TexCoord;

void main()
{
	vec2 vCoord;
    vCoord.x = -1.0 + float((gl_VertexID & 1) << 1);
    vCoord.y = -1.0 + float(gl_VertexID & 2);
	gl_Position = vec4(vCoord, 1, 1);

	TexCoord = (vCoord +1)/2.0;
}
