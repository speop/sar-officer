#version 440

#pragma sar include("Shaders/common.include")

layout(location =0 ) uniform float ElemHeight = 0.6f;
layout(location =1 ) uniform float ElemWidth = 0.05f;

layout (points) in;
layout (triangle_strip) out;
layout (max_vertices = 7) out;

in vertex{
	uint drawID;
} vertex_i[];

out geometry{
	vec3 position;
	vec3 normal;
	vec3 color;
	flat uint drawID;
} geom_o;

const vec3 colors[10] = vec3[](
	vec3(0.376, 0.514, 0.114),
	vec3(0.376, 0.557, 0.102),
	vec3(0.396, 0.545, 0.114),
	vec3(0.408, 0.545, 0.114),
	vec3(0.427, 0.49, 0.02),
	vec3(0.376, 0.514, 0.114),
	vec3(0.376, 0.557, 0.102),
	vec3(0.396, 0.545, 0.114),
	vec3(0.408, 0.545, 0.114),
	vec3(0.427, 0.49, 0.02)


	/*
	vec3(0.431, 0.682, 0.173),
	vec3(0.498, 0.765, 0.173),
	vec3(0.514, 0.678, 0.125),
	vec3(0.435, 0.729, 0.141),
	vec3(0.608, 0.925, 0.188),
	vec3(0.431, 0.682, 0.173),
	vec3(0.498, 0.765, 0.173),
	vec3(0.514, 0.678, 0.125),
	vec3(0.435, 0.729, 0.141),
	vec3(0.608, 0.925, 0.188)*/
	/*
	vec3(0.325, 0.6, 0.376),
	vec3(0.247, 0.639, 0.306),
	vec3(0.043, 0.843, 0.251), 
	vec3(0.482, 0.729, 0.157),
	vec3(0.439, 0.741, 0.153),
	vec3(0.922, 0.91, 0.345),
	vec3(0.71, 0.839, 0.353),
	vec3(0.094, 0.416, 0.275),
	vec3(0.365, 0.671, 0.349),
	vec3(0.616, 0.651, 0.463)*/
);

void main(){
	vec4 pos = gl_in[0].gl_Position;

	float rand = Random(vec2(pos.x,pos.z));

	//rotating base in 0 - pi/2 and moving center to origin
	float alpha = floor(rand*100)/100.f;
	float beta =  sqrt(1 - rand*rand);


	//getting random numbers from -0.05 to 0.05 from generated random number
	int firstIndex = int(fract(rand)*10.f);
	int secondIndex = int(fract(rand*10)*10.f);
	int thirdIndex = int(fract(rand*100)*10.f);

	float secondDec =  floor(fract(rand*10.f)*10.f)/10.f;
	float thirdDec = floor(fract(rand*100.f)*10.f)/10.f;	
	
	float offsetSecondDec = secondDec/5.f -0.1f;
	float offsetThirdDec = thirdDec/5.f -0.1f;
	float offsetFourthdDec = floor(fract(rand*1000.f)*10.f)/50.f -0.1f;

	//randomize grass height
	float height = mix(ElemHeight, ElemHeight/2.f, rand);
	//float height = ElemHeight;
	//random move
	pos.x += offsetSecondDec;
	pos.z += offsetThirdDec;

	vec3 A,B,C,D,E,F,G;
	
	A = vec3(pos);
	B = vec3(ElemWidth*alpha/pos.w +pos.x,pos.y, ElemWidth*beta/pos.w+pos.z);
	C = vec3(pos.x+offsetSecondDec, pos.y + height, pos.z+offsetThirdDec);
	D = vec3(B.x+offsetSecondDec, B.y +offsetFourthdDec+height, B.z+offsetThirdDec);
	E = vec3(C.x +2*offsetSecondDec, C.y +offsetSecondDec+height, C.z+2*offsetThirdDec);
	F = vec3(D.x +2*offsetSecondDec, D.y +offsetThirdDec+height, D.z+2*offsetThirdDec);	
	G = vec3((F.x +E.x)/2  +offsetSecondDec, F.y +offsetSecondDec+height, (E.z + F.z) /2 +offsetThirdDec);
	



	vec3 ABCnormal, CBDnormal, CDEnormal, EDFnormal, EFGnormal;

	ABCnormal = -1* normalize(cross((B-A), (C-A)));
	CBDnormal = -1* normalize(cross((D-C), (B-C)));
	CDEnormal = -1* normalize(cross((D-C), (E-C)));
	EDFnormal = -1* normalize(cross((F-E), (D-E)));
	EFGnormal = -1* normalize(cross((F-E), (G-E)));

	ABCnormal.y = abs(ABCnormal.y);
	CBDnormal.y = abs(CBDnormal.y);
	CDEnormal.y = abs(CDEnormal.y);
	EDFnormal.y = abs(EDFnormal.y);
	EFGnormal.y = abs(EFGnormal.y);

	vec3 color1, color2;

   
   /*
   
     gl_Position = vec4(-1,-1,0, 1.0);  
	geom_o.position =  vec3(-1,-1,0);
	geom_o.normal = vec3(-1,-1,0);
	geom_o.color = vec3(1,0, 1);
    EmitVertex();

	 gl_Position = vec4(1,-1,0, 1.0);  
	geom_o.position =  vec3(1,-1,0);
	geom_o.normal = vec3(-1,-1,0);
	geom_o.color = vec3(1,0, 1);
    EmitVertex();

	 gl_Position = vec4(1,1,0, 1.0);  
	geom_o.position =  vec3(1,1,0);
	geom_o.normal = vec3(-1,-1,0);
	geom_o.color = vec3(1,0, 1);
    EmitVertex();

	EndPrimitive();
	return;
   //*/

	color1 = colors[firstIndex];
    gl_Position = BasicVP * vec4(A, 1.0);  
	geom_o.position =  vec3(A);
	geom_o.normal = ABCnormal;
	geom_o.color = color1;
	geom_o.drawID = vertex_i[0].drawID;
    EmitVertex();


	color2 = colors[secondIndex];
	gl_Position = BasicVP * vec4(B, 1.0);
	geom_o.position =  vec3(B.z);
	geom_o.normal = normalize(ABCnormal + CBDnormal);
	geom_o.color = color2;
	geom_o.drawID = vertex_i[0].drawID;
    EmitVertex();

	
	color1=  mix(color1, colors[thirdIndex], secondDec);
    gl_Position = BasicVP * vec4(C, 1.0);
	geom_o.position =  vec3(C);
	geom_o.normal = normalize(ABCnormal + CBDnormal + CDEnormal);
	geom_o.color = color1;
	geom_o.drawID = vertex_i[0].drawID;
    EmitVertex();

	
	color2 =  mix(color2, colors[thirdIndex], thirdDec);
	gl_Position = BasicVP * vec4(D, 1.0);
	geom_o.position =  vec3(D);
	geom_o.normal = normalize(CBDnormal + CDEnormal + EDFnormal);
	geom_o.color = color2;
	geom_o.drawID = vertex_i[0].drawID;
    EmitVertex();

	color1=  mix(color1, colors[thirdIndex], secondDec);  
	gl_Position = BasicVP * vec4(E, 1.0);
	geom_o.position =  vec3(E);
	geom_o.normal = normalize(CDEnormal + EDFnormal + EFGnormal);
	geom_o.color = color1;
	geom_o.drawID = vertex_i[0].drawID;
    EmitVertex();

	color2 =  mix(color2, colors[thirdIndex], thirdDec);
	gl_Position = BasicVP * vec4(F, 1.0);
	geom_o.position =  vec3(F);
	geom_o.normal = normalize(EDFnormal + EFGnormal);
	geom_o.color = color2;
	geom_o.drawID = vertex_i[0].drawID;
    EmitVertex();
   
	
	color1=  mix(color1, colors[thirdIndex], secondDec);
	color2 = mix(color2, colors[thirdIndex], thirdDec);
	gl_Position = BasicVP * vec4(G, 1.0);
	geom_o.position =  vec3(G);
	geom_o.normal = EFGnormal;
	geom_o.color = mix(color1,color2,secondDec);
	geom_o.drawID = vertex_i[0].drawID;
    EmitVertex();
	
    EndPrimitive();

}