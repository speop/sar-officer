// shadertype=glsl

#version 440
uniform float TerrainHeightOffset;
layout(binding=0) uniform sampler2D TexTerrainHeight;


#pragma sar include("Shaders/common.include")

//
// Inputs
//
in vec2 terrainTextureCoords[];
in vec2 coordsTriangle[];

//
// Outputs
//
layout(vertices = 4) out;

patch out float gl_TessLevelOuter[4];
patch out float gl_TessLevelInner[2];


out vec2 tcs_terrainTexCoord[];
out vec2 tcs_coordsTriangle[];
out float tcs_tessLevel[];


in VS_OUT { 
	vec2 coordsTriangle;
	flat float tileSize;	
	flat int tscaleNegX;
	flat int tscaleNegZ;
	flat int tscalePosX;
	flat int tscalePosZ;
	 } tcs_in[];   
out TCS_OUT { vec2 coordsTriangle; flat float tileSize; } tcs_out[];



/**
* Dynamic level of detail using sphere algorithm.
* Source adapated from the DirectX 11 Terrain Tessellation example.
*/
float dlodSphere(vec4 p0, vec4 p1, vec2 t0, vec2 t1)
{

	float g_tessellatedTriWidth = 2.5f;

	p0.y = texture(TexTerrainHeight, t0).r * 3000 + TerrainHeightOffset; 
	p1.y = texture(TexTerrainHeight, t1).r * 3000 + TerrainHeightOffset;

	vec4 center = 0.5 * (p0 + p1);
	vec4 view0 =  BasicV *center;
	vec4 view1 = view0;
	view1.x += distance(p0, p1);
	

	vec4 clip0 = BasicP * view0;
	vec4 clip1 = BasicP * view1;

	clip0 /= clip0.w;
	clip1 /= clip1.w;


	vec2 screen0 = ((clip0.xy + 1.0) / 2.0) * ScreenDim.xy;
	vec2 screen1 = ((clip1.xy + 1.0) / 2.0) * ScreenDim.xy;
	float d = distance(screen0, screen1);

	// g_tessellatedTriWidth is desired pixels per tri edge
	float t = clamp(d / g_tessellatedTriWidth, 0,64);
	
	return 32;
	if (t <= 2.0)
	{ 
		return 2.0;
	}
	if (t <= 4.0)
	{ 
		return 4.0;
	}
	if (t <= 8.0)
	{ 
		return 8.0;
	}
	if (t <= 16.0)
	{ 
		return 16.0;
	}
	if (t <= 32.0)
	{ 
		return 32.0;
	}
	
	return 64.0;
}

void main()
{
	// Outer tessellation level
	gl_TessLevelOuter[0] = dlodSphere(gl_in[3].gl_Position, gl_in[0].gl_Position, terrainTextureCoords[3], terrainTextureCoords[0]);
	gl_TessLevelOuter[1] = dlodSphere(gl_in[0].gl_Position, gl_in[1].gl_Position, terrainTextureCoords[0], terrainTextureCoords[1]);
	gl_TessLevelOuter[2] = dlodSphere(gl_in[1].gl_Position, gl_in[2].gl_Position, terrainTextureCoords[1], terrainTextureCoords[2]);
	gl_TessLevelOuter[3] = dlodSphere(gl_in[2].gl_Position, gl_in[3].gl_Position, terrainTextureCoords[2], terrainTextureCoords[3]);
	
	if (tcs_in[gl_InvocationID].tscaleNegX == 2)
		gl_TessLevelOuter[0] = max(2.0, gl_TessLevelOuter[0] * 0.5);
	if (tcs_in[gl_InvocationID].tscaleNegZ == 2)
		gl_TessLevelOuter[1] = max(2.0, gl_TessLevelOuter[1] * 0.5);
	if (tcs_in[gl_InvocationID].tscalePosX == 2)
		gl_TessLevelOuter[2] = max(2.0, gl_TessLevelOuter[2] * 0.5);
	if (tcs_in[gl_InvocationID].tscalePosZ == 2)
		gl_TessLevelOuter[3] = max(2.0, gl_TessLevelOuter[3] * 0.5);

	// Inner tessellation level
	gl_TessLevelInner[0] = 0.5 * (gl_TessLevelOuter[0] + gl_TessLevelOuter[3]);
	gl_TessLevelInner[1] = 0.5 * (gl_TessLevelOuter[2] + gl_TessLevelOuter[1]);

	// Pass the patch verts along
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

	// Output heightmap coordinates
	tcs_terrainTexCoord[gl_InvocationID] = terrainTextureCoords[gl_InvocationID];

	// Output texture coordinates
	tcs_out[gl_InvocationID].coordsTriangle = tcs_in[gl_InvocationID].coordsTriangle;
	tcs_out[gl_InvocationID].tileSize = tcs_in[gl_InvocationID].tileSize;


	// Output tessellation level (used for wireframe coloring)
	tcs_tessLevel[gl_InvocationID] = gl_TessLevelOuter[0];
}
