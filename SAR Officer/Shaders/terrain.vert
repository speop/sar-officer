#version 440
///
/// UNIFORMS
///
uniform vec3 TerrainOrigin;

uniform float TerrainLength;
uniform float TerrainWidth;
uniform float TerrainHeightOffset;

struct PerDraw{
	mat4 modelMatrix;
	int tscaleNegX;
	int tscaleNegZ;
	int tscalePosX;
	int tscalePosZ;	
	float tileScale;
};


layout (binding = 3, std430 ) buffer Transformation{
	PerDraw perDraw[];
};



///
/// INPUTS
///
in vec3 a_vertex;


///
/// OUTPUS
///
out vec2 terrainTextureCoords;
out VS_OUT {
    vec2 coordsTriangle;
	flat float tileSize;	
	flat int tscaleNegX;
	flat int tscaleNegZ;
	flat int tscalePosX;
	flat int tscalePosZ;		
} vs_out;

vec2 calcTextureCoords(vec4 pos){
	return vec2(abs(pos.x - TerrainOrigin.x) / TerrainWidth, abs(pos.z - TerrainOrigin.z) / TerrainLength);
}


void main(){
	vs_out.tscaleNegX = perDraw[gl_InstanceID].tscaleNegX;
	vs_out.tscaleNegZ = perDraw[gl_InstanceID].tscaleNegZ;
	vs_out.tscalePosX = perDraw[gl_InstanceID].tscalePosX;
	vs_out.tscalePosZ = perDraw[gl_InstanceID].tscalePosZ;

	// Calcuate texture coordantes (u,v) relative to entire terrain
	vec4 pos = perDraw[gl_InstanceID].modelMatrix * vec4(a_vertex.xyz * perDraw[gl_InstanceID].tileScale, 1.0f);
	terrainTextureCoords = calcTextureCoords(pos);
	
	//calculate texture position relative to tile	
	vs_out.coordsTriangle = vec2((a_vertex.x + 0.5) *perDraw[gl_InstanceID].tileScale /32, (a_vertex.z + 0.5) * perDraw[gl_InstanceID].tileScale/32 );

	// Send vertex position along
	vs_out.tileSize = perDraw[gl_InstanceID].tileScale;
	gl_Position = perDraw[gl_InstanceID].modelMatrix * vec4(a_vertex.xyz * perDraw[gl_InstanceID].tileScale, 1.0);
	
}

 