//shadertype=glsl
#define DEFERRED_RENDERING 1
#define SHADOWS_RENDERING 2

#define SKY_ID 0xFF
#define CLOUDS_ID 0xF0
#define PLAYER_ID 0xbb
#define BASIC_OBJECT_ID 0x20
#define TERRAIN_ID 0x10
#define GRASS_ID 0x11

layout (binding=5, std140) uniform basicUBO
{ 
 		mat4 BasicVP;
		mat4 BasicP;
		mat4 BasicV;
		mat4 BasicInvVP;
		mat4 BasicInvP;
		mat4 BasicInvV;

		mat4 CurrentVP;
		mat4 CurrentP;
		mat4 CurrentV;

		vec4 Wind;
		ivec4 ScreenDim;
		float Time;
		float CloudsTime;
		float CloudsIntensity;
};


layout (binding=6, std140) uniform shadowUBO{
		mat4 csmVP[4];
		mat4 csmCloudsVP[4];
};

vec4 GetViewSpaceFromDepth(vec2 coords, float depth)
{
	float z =  depth * 2.0 - 1.0;
	 
	vec4 clipSpacePosition = vec4(coords * 2.0 - 1.0, z, 1.0);
	vec4 viewSpacePosition = BasicInvP * clipSpacePosition;
	
	viewSpacePosition /= viewSpacePosition.w;
	return viewSpacePosition;
}

vec4 GetWorldSpaceFromDepth(vec2 coords, float depth){
	vec4 worldSpacePosition = inverse(BasicV) * GetViewSpaceFromDepth(coords, depth);    
	return worldSpacePosition;
}




/************************************************
    static.frag
    by Spatial
    05 July 2013
*/



// A single iteration of Bob Jenkins' One-At-A-Time hashing algorithm.
uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}



// Compound versions of the hashing algorithm I whipped together.
uint hash( uvec2 v ) { return hash( v.x ^ hash(v.y)                         ); }
uint hash( uvec3 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z)             ); }
uint hash( uvec4 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z) ^ hash(v.w) ); }



// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct( uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat( m );       // Range [1:2]
    return f - 1.0;                        // Range [0:1]
}



// Pseudo-random value in half-open range [0:1].
float Random( float x ) { return floatConstruct(hash(floatBitsToUint(x))); }
float Random( vec2  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float Random( vec3  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float Random( vec4  v ) { return floatConstruct(hash(floatBitsToUint(v))); }