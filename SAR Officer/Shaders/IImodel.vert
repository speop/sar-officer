#version 440

layout (location = 4) in vec3 position;
layout (location = 5) in vec3 normal;
layout (location = 6) in vec3 tangent;
layout (location = 7) in vec2 texCoords;
layout (location = 8) in uint DrawID;


#pragma sar include("Shaders/IImodel.include")
#pragma sar include("Shaders/common.include")

#ifdef SHADER_SHADOW
out GS_IN{
	vec2 TexCoords;
	flat uint DrawID;
	} outData;
#else
out VertexData{
	vec2 TexCoords;
	flat uint DrawID;
	vec3 Normal;
	vec3 Tangent;
	vec3 Position; 
}outData;
#endif


uniform mat4 VP;
uniform mat4 BaseMat;

void main()
{

	vec4 pos = perDraw[DrawID].position
		* perDraw[DrawID].transformation 
		*  vec4(position, 1.0f);

#ifdef SHADER_SHADOW
	gl_Position = pos;
#else
    gl_Position = BasicVP * pos;
	outData.Position = pos.xyz;	
	outData.Normal = (perDraw[DrawID].transformation  * vec4(normal, 0.0)).xyz;
	outData.Tangent = (perDraw[DrawID].transformation  * vec4(tangent, 0.0)).xyz;
#endif	
	outData.DrawID = DrawID;	
    outData.TexCoords = texCoords;

}
