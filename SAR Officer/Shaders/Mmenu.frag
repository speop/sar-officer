#version 430

in vec2 TexCoord;
out vec4 color;

uniform sampler2D Background;

void main()
{    
	color = vec4(texture(Background, TexCoord));	
	//color = vec4(1,0,1,1);
}
