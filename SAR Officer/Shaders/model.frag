#version 430

in vec2 TexCoords;
out vec4 color;

uniform sampler2D Diffuse[4];

void main()
{    
	color = vec4(texture(Diffuse[0], TexCoords));
	if(color.a <= 0.2){
		//discard;
	}
	//color = vec4(color.a);
	//color = vec4(TexCoords,0,1);
	//color = vec4(1.0,0,1,1);
}
