#version 440
#pragma sar include("Shaders/common.include")
layout(binding=0) uniform sampler2D Diffuse;
layout(binding=1) uniform sampler2D Specular;
layout(binding=2) uniform sampler2D Normal;

layout (location = 7) uniform float alphaDrop;
layout (location = 8) uniform int instanceID;
layout (location = 9) uniform uint extraData;

#ifdef SHADER_SHADOW
in GS_OUT {
    vec2 TexCoords;
	//vec3 position;
	flat uint DrawID;
} vertexData;
#else
#pragma sar include("Shaders/commonGeometryFrag.include")
in VertexData{
	vec2 TexCoords;
	flat uint DrawID;
	vec3 Normal;
	vec3 Tangent;
	vec3 Position;
}vertexData;
#endif

#pragma sar include("Shaders/BumpedNormal.include");

void main()
{    
	vec4 color = vec4(texture(Diffuse, vertexData.TexCoords));
	if(color.a <= alphaDrop){
		discard;
	}
	
	//color = vec4(color.a);
	//color = vec4(TexCoords,0,1);
	//color = vec4(1.0,0,1,1);

	#ifdef SHADER_SHADOW
	#else
	
	float specular = texture(Specular,  vertexData.TexCoords).r;	
	vec4 normal = texture(Normal,  vertexData.TexCoords);

	//special color	
	uint val =  extraData & 0xFF000000;
	if(bool(val) && instanceID == vertexData.DrawID ){
		val >>= 24;
		float a = 1.f/255 * val;

		val = extraData & 0x00FF0000;
		val >>= 16;
		float r = 1.f/255 * val;

		val = extraData & 0x0000FF00;
		val >>= 8;
		float g = 1.f/255 * val;

		val = extraData & 0x000000FF;
		float b = 1.f/255 * val;

		vec3 pomColor = vec3(r,g,b);
		color =  vec4(mix(color.rgb,pomColor, a), color.a);
	}

	o_diffuseAndSpecular = vec4(color.r,color.g,color.b,specular);
	o_normal = CalcBumpedNormal(vertexData.Normal, vertexData.Tangent, normal.xyz);
	o_position = vertexData.Position;
	o_ID = BASIC_OBJECT_ID;
	
	#endif
}
