#version 440
#pragma sar include("Shaders/common.include")
layout(location = 0) in vec3 vertexPosition;
layout(location = 2) uniform mat4 Model;


#ifdef SHADER_SHADOW
out GS_IN{
	vec2 TexCoords;
} outData;
#else
out VS_OUT {
    vec2 vertexCoords;
} outData;
#endif




void main(){	
	
	
	#ifdef SHADER_SHADOW
	outData.TexCoords = vec2(vertexPosition.xz);
	gl_Position = Model*vec4(vertexPosition, 1);
	#else
	outData.vertexCoords = vec2(vertexPosition.xz);
	gl_Position = CurrentVP *Model* vec4(vertexPosition, 1);
	#endif
}

 