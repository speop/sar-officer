#version 450
/*
main		cliff			trail
0,55 - 1	0 - 0,27		0,30 - 0,35
			0,40 - 0,48		0.50 - 0.53

2			0				1
*/




	
#pragma sar include("Shaders/common.include")

///
/// UNIFORMS
///
layout(binding=1) uniform sampler2D baseColor;
layout(binding=2) uniform sampler2D normalMap;
layout(binding=3) uniform sampler2D materialsTextures[3];
layout(binding=7) uniform sampler2D  materialsNormal[3];

struct DrawElementsIndirectCommand {
	uint vertexCount;
	uint instanceCount;
	uint firstIndex;
	uint baseVertex;
};


//*
layout (binding = 11, std430 ) buffer Commands{
	DrawElementsIndirectCommand drawCommand;
};


struct Grass {
	vec4 normal;
	vec4 seed;
};

layout (binding = 10, std430 ) buffer GrassSeeds{
	Grass grassSeeds[];
};
//*/

#ifdef SHADER_SHADOW
in GS_OUT {
    vec2 TexCoords;
	vec3 position;
} frag_in;
#else
in TES_OUT { 
	vec2 coordsTriangle; 
	vec2 terrainTexCoord;
	vec3 position;
	vec3 viewPos;
	flat float tileSize;
	float depth;
	} frag_in; 

#pragma sar include("Shaders/commonGeometryFrag.include")
//layout(location=6) uniform float grassFar0;
//layout(location=7) uniform float grassFar1;
layout(location=8) uniform float grassFar2;
layout(location=9) uniform int maxGrassSeedsNo;
layout(location=10) uniform vec2 xAxisRatio;
layout(location=11) uniform vec2 zAxisRatio;
#endif

//
// Ouputs
//
//out vec4 fragColor;


const float c_grassSpacing = 0.250;

float alpha(float r1, float r2,float mat){
	float a = 1.0f / (r2 - r1);
	a = (mat - r1)  * a;

	return a;
}

float MyRound(float val){

	float valAsInt = floor(val);
	float f = val - valAsInt;
	float n = floor((f+c_grassSpacing/2.f)/c_grassSpacing);
	return valAsInt+n*c_grassSpacing;
}

void main(void)
{
	#ifdef SHADER_SHADOW
	#else

	ivec2 offset[4];
	offset[0] = ivec2(-1,-1);
	offset[1] = ivec2(-1,1);
	offset[2] = ivec2(1,1);
	offset[3] = ivec2(1,-1);

	vec4 color = vec4( 0.4 , 0.50196 , 0.6, 1.0);
	vec4 normal;
	bool grass = false;
	
	const float fRange1 = 0.25f;  
	const float fRange2 = 0.28f;  
	const float fRange3 = 0.35f;  
	const float fRange4 = 0.42f;  
	const float fRange5 = 0.48f;
	const float fRange6 = 0.50f;  
	const float fRange7 = 0.53f;    
	const float fRange8 = 0.55f; 

	float originalZ = gl_FragCoord.z / 1.9f;
	float texBaseAlpha = 0.25f;
	float normBaseAlpha = 0.25f;
	vec4 baseNormal = texture(normalMap, frag_in.terrainTexCoord);

	if(originalZ > 0.25 && originalZ < 0.9f){
		texBaseAlpha = mix(0.25f, 0.9f,originalZ);
	}
	else if(originalZ > 0.5){
		texBaseAlpha = 0.9f;
	}	
	

	float mat =  0.2989f*texture(baseColor, frag_in.terrainTexCoord).r
			+0.587f*texture(baseColor, frag_in.terrainTexCoord).g
			+0.114f* texture(baseColor, frag_in.terrainTexCoord).b;
	
	if(mat < fRange1){
		color = mix(texture(materialsTextures[0], frag_in.coordsTriangle), texture(baseColor, frag_in.terrainTexCoord), texBaseAlpha); //stone
		normal = mix(texture(materialsNormal[0], frag_in.coordsTriangle), baseNormal, normBaseAlpha); //stone
	}

	else if(mat < fRange2){
		float a = alpha(fRange1, fRange2, mat);
		color = mix(
						mix(							
							texture(materialsTextures[1], frag_in.coordsTriangle),  //moss
							texture(materialsTextures[0], frag_in.coordsTriangle), //stone
							a
						),
						texture(baseColor, frag_in.terrainTexCoord),
						texBaseAlpha
				);		
				normal = mix(
						mix(							
							texture(materialsNormal[1], frag_in.coordsTriangle),  //moss
							texture(materialsNormal[0], frag_in.coordsTriangle), //stone
							a
						),
						baseNormal,
						normBaseAlpha
				);		
	}

	else if(mat < fRange3){
		color = mix(texture(materialsTextures[1], frag_in.coordsTriangle), texture(baseColor, frag_in.terrainTexCoord), texBaseAlpha); //moss
		normal = mix(texture(materialsNormal[1], frag_in.coordsTriangle), baseNormal, normBaseAlpha); //moss
		//color = vec4(1,0,1,1);
	}

	else if(mat < fRange4){
		float a = alpha(fRange4, fRange3, mat);
		color = mix(
						mix(							
							texture(materialsTextures[0], frag_in.coordsTriangle), //stone
							texture(materialsTextures[1], frag_in.coordsTriangle), //moss
							a
						),
						texture(baseColor, frag_in.terrainTexCoord),
						texBaseAlpha
				);

				normal = mix(
						mix(							
							texture(materialsNormal[0], frag_in.coordsTriangle), //stone
							texture(materialsNormal[1], frag_in.coordsTriangle), //moss
							a
						),
						baseNormal,
						normBaseAlpha
				);
	}

	else if(mat < fRange5){
		color = mix(texture(materialsTextures[0], frag_in.coordsTriangle), texture(baseColor, frag_in.terrainTexCoord), texBaseAlpha); //stone
		normal = mix(texture(materialsNormal[0], frag_in.coordsTriangle), baseNormal, normBaseAlpha); //stone
	}

	else if(mat < fRange6){
		float a = alpha(fRange6, fRange5, mat);
		color = mix(
						mix(
							texture(materialsTextures[1], frag_in.coordsTriangle), //moss
							texture(materialsTextures[0], frag_in.coordsTriangle), //stone
							a
						),
						texture(baseColor, frag_in.terrainTexCoord),
						texBaseAlpha
				);

				normal = mix(
						mix(
							texture(materialsNormal[1], frag_in.coordsTriangle), //moss
							texture(materialsNormal[0], frag_in.coordsTriangle), //stone
							a
						),
						baseNormal,
						normBaseAlpha
				);
	}
		else if(mat < fRange7){
		color = mix(texture(materialsTextures[1], frag_in.coordsTriangle), texture(baseColor, frag_in.terrainTexCoord), texBaseAlpha); //moss
		normal = mix(texture(materialsNormal[1], frag_in.coordsTriangle), baseNormal, normBaseAlpha); //moss
	}

	else if(mat < fRange8){
		float a = alpha(fRange8, fRange7, mat);
		color = mix(
						mix(
							texture(materialsTextures[2], frag_in.coordsTriangle), //grass
							texture(materialsTextures[1], frag_in.coordsTriangle), //moss
							a
						),
						texture(baseColor, frag_in.terrainTexCoord),
						texBaseAlpha
				);
					normal = mix(
						mix(
							texture(materialsNormal[2], frag_in.coordsTriangle), //grass
							texture(materialsNormal[1], frag_in.coordsTriangle), //moss
							a
						),
						baseNormal,
						normBaseAlpha
				);
				grass = true;
	}
	else{
		color = mix(texture(materialsTextures[2], frag_in.coordsTriangle), texture(baseColor, frag_in.terrainTexCoord), texBaseAlpha); //grass
		normal = mix(texture(materialsNormal[2], frag_in.coordsTriangle), baseNormal, normBaseAlpha); //grass
		grass = true;
	}

	
	float dx = fwidth(frag_in.viewPos.x);
	float dz= fwidth(frag_in.viewPos.z);

	//current pixel contains grass. I am going to check if it is also seed for grass geometry
	if(grass)
	{
		float depth = MyRound(abs(frag_in.depth));
		if(depth <= grassFar2)
		{
			/*float derivX =dimX;
			float derivZ = dimZ;
			dimX *= frag_in.tileSize;
			dimZ *= frag_in.tileSize;
			float dx =  abs(dimX);//abs(xAxisRatio.x *dimX) + abs(dimX*xAxisRatio.y);
			float dz =  abs(zAxisRatio.x *dimZ) + abs(dimZ*zAxisRatio.y);*/
			//round to nearest c_grassSpacing
			//this will move grass seed to match regular grid
			float x = MyRound(frag_in.position.x);
			float y = MyRound(frag_in.position.y);
			float z = MyRound(frag_in.position.z);			

			//check if current pixel is a seed
			float cx =abs(abs(frag_in.position.x) - abs(x));
			float cz = abs(abs(frag_in.position.z) - abs(z)); 
			if(cx <= dx && cz <= dz)
			{

				//adding 1 or 4 seed, based on distance (LOD)
				
				//adding current rounded coords as seed
				uint index = atomicAdd(drawCommand.instanceCount, 1);
				if(index < maxGrassSeedsNo){
					grassSeeds[index].seed = vec4(x,y, z,1);
					grassSeeds[index].normal = vec4(normalize(baseNormal.xzy), 0);
				}
			}				
		}		
	}	

	normal = vec4(normalize(normal.xzy),0);
	o_diffuseAndSpecular = vec4(color.r,color.g,color.b,0);
	o_normal = vec3(normal.xyz);
	o_position = frag_in.position;
	o_ID = TERRAIN_ID;
#endif

	
}