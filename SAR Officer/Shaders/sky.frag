#version 420
#pragma sar include("Shaders/common.include")

uniform vec3 sunVector;
uniform mat4 invProjView;
//uniform float time;

const int radiusEarth = 6371000; //earth radius in m
const int radiusAtmosphere = 6431000; //radius of molecule atmosphere in m 
const int Hr = 8000; //rayleigh average density height
const int Hm = 1200; //mie average density height
const int stepCount = 16;
const int sunStepCount = 16;

const float rAS = 1;
const float rES = 0.9922;
const float HrS = 0.0023;
const float HmS = 0.00093298f;
const float scale =1.554968123153476e-07;
const float sunIntensity = 20.f; // 20

const float PI = 3.141592653589793238462643383;

const vec3 betaR = vec3(5.8e-6, 13.5e-6, 33.1e-6);
const vec3 betaM = vec3(21e-6); // 21e-6


in vec2 vCoord;
//out vec4 fragColor;
#pragma sar include("Shaders/commonGeometryFrag.include")



float PhaseRaleigh(float cosAlpha){
	float a = 3/(16*PI);
	float b = 1+cosAlpha *cosAlpha;
	return a*b;
}

float PhaseMie(float cosAlpha, float g){
	float g2 = g*g;

	float frac1 = 3/(8*PI);

	float a = 3.0*(1.0-g2);
    float b = 2.0*(2.0+g2);
	float frac2 = a/b;

	float c = 1.0+cosAlpha*cosAlpha;
	float d = 1.0+g2-2.0*g*cosAlpha;
	float frac3 = c/sqrt(d*d*d);

	return frac1 *frac2* frac3;
}

// calculate depth which ray traverses from a given point in a given direction inside a sphere. For simplicities sake the sphere is assumed to always be of radius 1
float AtmosphericDepth(vec3 position, vec3 dir){
	float r = rAS;
	//because we use normalized vector dot product will always be 1 so a will always be 1
	float b =  2.0*dot(dir, position);
	//raS = 1
	float c =  dot(position, position)- 1;
	float discriminant = b*b - 4.0 *c;
	float t1 = (-b+sqrt(discriminant))/(2.0);
	return t1;
}

void main(){
	vec3 sunVec = vec3(0, 1., 0.);

	// BasicInvVP = inverse (Projection * View)
	//we are drawing on far plane so the z coord should be 1. And because camera coords are [0,0,0] we automaticaly get camera to sky point vector
	vec3 viewSkyPointVector = normalize((BasicInvVP*vec4(vec2(vCoord.x, vCoord.y),1,1)).xyz);

	//cos of angle between vector from camera to sky point and vector from camera to sun
	float cosAlpha = dot(viewSkyPointVector, sunVector);

	//we can asume that x and z are 0 because we use realitevly small terain (4x4km) what is << than circumference of earth = 40 030 km and also we can scale everything down
	//and set the Atmoshpere radius to be 1 then Earth radius will be  0.9922
	vec3 eyePosition = vec3(0.0, rES, 0.0);
	float viewDepth = AtmosphericDepth(eyePosition, viewSkyPointVector);
	//if(viewDepth>1) viewDepth = viewDepth- 0.0078;
	

	float stepSize = viewDepth / float(stepCount);

	//vec3 skyColor = vec3(0.0);

	vec3 colorRayleigh = vec3(0.0);
	vec3 colorMie = vec3(0.0);

	float opticalDepthR = 0;
	float opticalDepthM = 0;

	float phaseR = PhaseRaleigh(cosAlpha);
	float phaseM  = PhaseMie(cosAlpha, .76);
	
	
	//sum from Pa to Pc
	for (int i = 0; i < stepCount; i++){
		float t = stepSize*float(i);
		// P = O + tD
		vec3 position = eyePosition + t* viewSkyPointVector;
		float pointHeight = abs(position.y - rES);
		
		// sum from Px to Pc
		float dR =  exp(-pointHeight/HrS) * stepSize/scale;
		float dM = exp(-pointHeight/HmS)*stepSize/scale;
		opticalDepthR += dR;
		opticalDepthM += dM;


		float sunDepth = AtmosphericDepth(position,sunVector);		
		float sunStepSize = sunDepth / sunStepCount;
		float opticalDepthSunR = 0;
		float opticalDepthSunM = 0;
				
		// T(X,Ps) - without beta(0) so it isnt vector
		for(int j = 0; j < sunStepCount; j++){
			float Xt = sunStepSize*float(j);
			vec3 Xpoint = position + Xt * sunVector;
			float Xheight = Xpoint.y - rES;

			//sum from Px to Pa
			opticalDepthSunR += exp(-Xheight/HrS);
			opticalDepthSunM += exp(-Xheight/HmS) ;
		}
		vec3 A = exp(-(betaR * (opticalDepthSunR*sunStepSize/scale + opticalDepthR) + betaM * (opticalDepthSunM*sunStepSize/scale + opticalDepthM)));
		colorRayleigh += A * dR;
		colorMie += A * dM;
		
		/*
		colorRayleigh += exp(
			-(4*PI*kR*sunStepSize*opticalDepthSunRayleigh)
			-(4*PI*kR*stepSize*opticalDepthRayleigh)
			-pointHeight/HrS
		);

		colorMie += exp(
			-(4*PI*kM*sunStepSize*opticalDepthSunMie)
			-(4*PI*kM*stepSize*opticalDepthMie)
			-pointHeight/HrS
		);
		*/
		
	}	
	vec3 skyColor = ( colorRayleigh * betaR * phaseR + colorMie * betaM * phaseM) * sunIntensity;
	


	const float gamma = 2.2; 
    // Reinhard tone mapping
    vec3 mapped = skyColor / (skyColor + vec3(1.0));
    // Gamma correction 
    mapped = pow(mapped, vec3(1.0 / gamma));

	vec3 sunColor = vec3(0.0);
	if(cosAlpha > 0.992){
		float st = (cosAlpha - 0.992) *100;
		float m = mix(0.0f,0.15,st);
		sunColor = vec3(m);
	}

	mapped += sunColor;		
	//fragColor = vec4(mapped, 1.);
	o_diffuseAndSpecular =  vec4(mapped,1);
	o_normal = vec3(0,0,0);
	o_position = vec3(1,1,1);
	o_ID = SKY_ID;
}

