#version 440
#pragma sar include("Shaders/common.include")
#define POINT_IN_CLIP(point) (all(greaterThan(point.xyz,-point.www))&&all(lessThan(point.xyz,point.www)))
layout(binding=0) uniform sampler2D DiffuseAndSpecular;
layout(binding=1) uniform sampler2D Normal;
layout(binding=3) uniform usampler2D ID;
layout(binding=2) uniform sampler2D Postion;
layout(binding=4) uniform sampler2D SSAO;
layout(binding=5) uniform sampler2DArray  VolumeShadows;
layout(binding=6) uniform sampler2DArrayShadow ShadowMaps;

layout(location = 2) uniform float SunAmbient;
layout(location = 3) uniform vec3 SunColor;
layout(location = 4) uniform vec3 SunPos;
layout(location = 5) uniform vec3 CameraPos;
layout(location = 6) uniform float FogBrightness;
//layout(location = 7) uniform vec2 SSLightPos;
layout(location = 8) uniform bool Lighting;


in vec2 vCoord;
out vec4 color;

const float zNear = 0.1f;
const float zFar = 10000.0f;
const float CSMBase = 5;
const int CSMLevels = 4;


float CalculateShadows( vec3 objPos, bool volumetric){
	
	
	int csmLevel = 0;
	vec4 projected;
	vec4 shadowCoord;
	vec4 shadowCoordBetterRes;
	vec4 shadowCoordWorseRes;
	vec2 texelSize;

	if(!volumetric){
		texelSize = vec2(textureSize(ShadowMaps, 0));
	}else{
		texelSize = vec2(textureSize(VolumeShadows, 0));
	}

	
	//get CSM level
	if(volumetric){
		projected=  csmCloudsVP[0] * vec4(objPos,1);
		if(!(all(greaterThan(projected.xyz,-projected.www))&&all(lessThan(projected.xyz,projected.www)))) return 1.f;
		shadowCoord = projected;
		shadowCoordBetterRes = shadowCoord;
		shadowCoordWorseRes = shadowCoordBetterRes;
	}
	else {
		for (int i =0; i < CSMLevels; i++)
		{	
			projected=  csmVP[i] * vec4(objPos,1); 
			shadowCoord = projected;
		
			if(all(greaterThan(projected.xyz,-projected.www))&&all(lessThan(projected.xyz,projected.www)))break;
				
			shadowCoordBetterRes = shadowCoord;
			csmLevel++;
		}
		if(csmLevel >= CSMLevels ) return 1.f;	

	

		if(csmLevel < CSMLevels - 1 && !volumetric){			
			shadowCoordWorseRes =  csmVP[csmLevel+ 1] * vec4(objPos,1); 				
		}
		else{
			shadowCoordWorseRes = shadowCoord;
		}
	}

	float shadowValue =0;
	int reads = 0;
	ivec2 offset[4];
	offset[0] = ivec2(-1,-1);
	offset[1] = ivec2(-1,1);
	offset[2] = ivec2(1,1);
	offset[3] = ivec2(1,-1);
	
	int fetchLevel = csmLevel;

	vec2 worseTexelSize = 2*vec2(shadowCoordWorseRes.ww) / texelSize;
	vec2 shadowTexelSize =  2*vec2(shadowCoord.ww) / texelSize;
	vec2 shadowBetterSize =  2*vec2(shadowCoordBetterRes.ww) / texelSize;


	//calculate PCF shadow
	for (int i = 0; i<4; i++){

		vec4 fetchCoords = vec4(shadowCoord.xy + offset[i]* shadowTexelSize, shadowCoord.z, shadowCoord.w);		
		
		if(POINT_IN_CLIP(fetchCoords) || volumetric){
			fetchLevel = csmLevel;
		}
		else  {
			fetchCoords = vec4(shadowCoordBetterRes.xy + offset[i]*shadowBetterSize, shadowCoordBetterRes.z, shadowCoordBetterRes.w);
			if(csmLevel > 0 && POINT_IN_CLIP(fetchCoords)){
				fetchLevel = csmLevel-1;
			}else {
				fetchCoords =  vec4(shadowCoordWorseRes.xy + offset[i]*worseTexelSize, shadowCoordWorseRes.z, shadowCoordWorseRes.w);
				if(csmLevel < CSMLevels - 1 && POINT_IN_CLIP(fetchCoords)){
					fetchLevel = csmLevel+1;
				}
				else{
					continue;
				}
			}	
		}

		fetchCoords = (fetchCoords /fetchCoords.w) *0.5 +0.5;
		
		if(!volumetric)
		{			
			vec4 coords =  vec4(fetchCoords.xy, fetchLevel, fetchCoords.z );
			shadowValue += texture(ShadowMaps, coords);
		}else {
			vec3 coords =  vec3(fetchCoords.xy, fetchLevel);
			shadowValue +=1- texture(VolumeShadows, coords).r;
		}
		
		reads++;
	}

	
	shadowValue = ( shadowValue / reads);
	return shadowValue;	
	
}



vec3 ApplyFog( in vec3  objColor, in vec3  objPos, in vec3 lighDir, in vec3 viewDir )  
{
	vec3 viewSpace = (BasicV * vec4(objPos, 1)).xyz;
	float depth = length(objPos-CameraPos);
	float dist = depth;
	float FogDensity = mix(0.001f, 0.0008f, lighDir.y);
	float fogFactor = 2.f /exp(dist * FogDensity);
    fogFactor = clamp( fogFactor, 0.0, 1.0 );

	vec3 rayDir = normalize(objPos - CameraPos);
	float sunAmount = max( dot( viewDir, SunPos ), 0.0 );
    float fogAmount = 1.0 * exp(-CameraPos.y*1.0) * (1.0-exp( -depth*rayDir.y*1.0 ))/rayDir.y;

    vec3  fogColor  = mix( vec3(0.36,0.44,0.49), // bluish
                           vec3(0.95,0.93,0.8), // yellowish
                           pow(sunAmount,8.0) );
				
	fogColor *= FogBrightness;
	return mix(fogColor, objColor, fogFactor);
    
}

vec3 Vignette(in vec3 objColor){
	float outerRadius = .80, innerRadius = .35, intensity = .5;
	vec2 relativePosition = gl_FragCoord.xy / ScreenDim.xy - .5;

	float len = length(relativePosition);
	float vignette = smoothstep(outerRadius, innerRadius, len);
	return  mix(objColor, objColor * vignette, intensity);
}

/*
vec3 GodRays(
    float density,
    float weight,
    float decay,
    float exposure,
    int numSamples,
    vec2 uv
    ) {

    vec3 fragColor = vec3(0.0,0.0,0.0);
	vec2 deltaTexCoords = vec2( uv - SSLightPos.xy );
	vec2 texCoords = uv;
	deltaTexCoords *= (1.0 /  float(numSamples)) * density;
	float illuminationDecay = 1.0;


	for(int i=0; i < numSamples ; i++){
	    
		texCoords -= deltaTexCoords;
		if(all(lessThan(texCoords, vec2(0,0))) && all(greaterThan(texCoords, vec2(1,1)))) continue;
		uint objId = texture(ID, texCoords).r;
		
		vec3 samp = vec3(0,0,0);
		if(objId >= SKY_ID){
			vec4 col= texture(DiffuseAndSpecular, texCoords);
			samp = col.rgb *col.a;
			//samp =  vec3(1);
		}
		
		samp *= illuminationDecay * weight;
		fragColor += samp;
		illuminationDecay *= decay;
	}

	fragColor *= exposure;

    return fragColor;
}
*/

void main(){


	vec2 coords = (vCoord +1) / 2.f;
	//color = vec4(texture(Postion, coords));

	uint objId = texture(ID, coords).r;
	vec3 objColor =  texture(DiffuseAndSpecular, coords).rgb;
	float objSpecular = texture(DiffuseAndSpecular, coords).a;
	vec3 objNormal = texture(Normal, coords).rgb;
	vec3 objPos = texture(Postion, coords).rgb;
	float objSSAO = texture(SSAO, coords).r;


	if(!Lighting || objId >= CLOUDS_ID ){
		objColor = Vignette(objColor);
		color = vec4(objColor, 1);
	}
	else{
		objNormal = normalize(objNormal *2.f - 1.f);
		vec3 lightDir = normalize(SunPos);
		
		// Then calculate lighting as usual
		vec3 ambient = objColor * SunAmbient;// *objSSAO;
		vec3 viewDir = normalize(CameraPos - objPos);
		vec3 halfwayDir = normalize(lightDir + viewDir);
		float spec = pow(max(dot(objNormal, halfwayDir), 0.0), 4);
		
		// Diffuse
        vec3 diffuse = max(dot(objNormal, lightDir), 0.0) * objColor *SunColor;

		//Specular
		vec3 specular = SunColor *spec * objSpecular;// *objSSAO;
       
		//shadows
		float shadows =  CalculateShadows(objPos, true);
		shadows *= CalculateShadows(objPos, false);
		shadows = mix(shadows, 1.f, 0.5f);

		

		//and combining all lights
		vec3 lighting = ambient*objSSAO + (diffuse +specular) *shadows;
		//lighting = ve3(objSSAO);
		lighting = ApplyFog(lighting, objPos, lightDir, viewDir);
		lighting = Vignette(lighting);
		//lighting += GodRays(2,0.01f,1,0.8,64,coords);
				
		

		color = vec4(lighting, 1);
	

		
	}
	
}

