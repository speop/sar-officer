// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 05-06-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-16-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include  "glm/glm.hpp"
#include <functional>

/// <summary>
/// Class Capsule.
/// </summary>
class Capsule
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="Capsule"/> class.
	/// </summary>
	/// <param name="height">The height.</param>
	/// <param name="width">The width.</param>
	/// <param name="bottomCenter">The bottom center.</param>
	Capsule(float height, float width, glm::vec3 bottomCenter);
	/// <summary>
	/// Finalizes an instance of the <see cref="Capsule"/> class.
	/// </summary>
	~Capsule();

	
	std::pair<glm::vec3, glm::vec3>m_body;

	//collision detection by minkowski differnce
	std::function<std::pair<bool, float>(glm::vec3, glm::vec3)> m_collisionLambda;

	/// <summary>
	/// Updates the position.
	/// </summary>
	/// <param name="bottomCenter">The bottom center.</param>
	void UpdatePosition(glm::vec3 bottomCenter);
	
	/// <summary>
	/// Calculates the coliding time.
	/// </summary>
	/// <param name="velocity">The velocity.</param>
	/// <returns>float.</returns>
	float CalculateColidingTime(glm::vec3 velocity);
	
	/// <summary>
	/// Calculates the coliding time.
	/// </summary>
	/// <returns>float.</returns>
	float CalculateColidingTime();

private:

	
	float m_width;
	float m_height;
	glm::vec3 m_velocity;
	bool m_staticOnlyCheck = false;

	/// <summary>
	/// Recalcs capsule position.
	/// </summary>
	/// <param name="bottomCenter">The bottom center.</param>
	void Recalc(glm::vec3 bottomCenter);

	/// <summary>
	/// Collisions the line to plane.
	/// </summary>
	/// <param name="originA">The origin a.</param>
	/// <param name="endA">The end a.</param>
	/// <param name="originB">The origin b.</param>
	/// <param name="endB">The end b.</param>
	/// <returns>float.</returns>
	float CollisionLineToPlane(glm::vec3 originA, glm::vec3 endA, glm::vec3 originB, glm::vec3 endB);

	/// <summary>
	/// Checks if X is in interval. 
	/// </summary>
	/// <param name="x">The x.</param>
	/// <param name="a">a.</param>
	/// <param name="b">The b.</param>
	/// <returns>bool.</returns>
	bool Between(float x, float a, float b);

	/// <summary>
	/// Calculate collision
	/// </summary>
	/// <returns>float.</returns>
	float CollisionCalc();
};

