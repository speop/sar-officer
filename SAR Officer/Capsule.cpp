#include "Capsule.h"
#include "SceneManager.h"


Capsule::Capsule(float height, float width, glm::vec3 bottomCenter)
{
	m_width = width;
	m_height = height;
	Recalc(bottomCenter);


	m_collisionLambda = [&](glm::vec3 minB, glm::vec3 maxB)
	{
		auto retVal = std::make_pair(false, (std::numeric_limits<float>::max)());

		auto MDleft = m_body.first.x - maxB.x;
		auto MDbottom = m_body.first.y - maxB.y;
		auto MDnear = m_body.first.z - maxB.z;

		auto dimB = maxB - minB;
		auto dimBody = m_body.second - m_body.first;

		auto MDwidth = dimB.x + dimBody.x;
		auto MDheight = dimB.y + dimBody.y;
		auto MDdepth = dimB.z + dimBody.z;


		
		bool collison =
			MDleft <= 0 && (MDleft + MDwidth) >= 0 &&
			MDbottom <= 0 && (MDbottom + MDheight) >= 0 &&
			MDnear <= 0 && (MDnear + MDdepth) >= 0;
		
		
		if (collison) {
			auto penetration = (std::min)({
				2 * (std::min)({abs(MDleft), abs(MDleft + MDwidth)}) / MDwidth,
				2 * (std::min)({abs(MDbottom), abs(MDbottom + MDheight)}) / MDheight,
				2 * (std::min)({abs(MDnear), abs(MDnear + MDdepth)}) / MDdepth
			});

			return std::make_pair(true, 1 -penetration);

		}

		
		if(m_staticOnlyCheck) return retVal;


		MDleft = minB.x - (m_body.second.x );
		MDbottom = minB.y - (m_body.second.y );
		MDnear = minB.z - (m_body.second.z );  
		float h = 1, s;
		

		// X min
		s = CollisionLineToPlane({ 0, 0, 0 }, m_velocity, { MDleft, MDbottom, MDnear }, { -1, 0, 0 });
		if (m_velocity.x > 0 && s < h && Between(s*m_velocity.y, MDbottom, MDbottom + MDheight) && Between(s*m_velocity.z, MDnear, MDnear + MDdepth))
		{
			h = s;
		}

		// X max
		s = CollisionLineToPlane({ 0, 0, 0 }, m_velocity, { MDleft + MDwidth, MDbottom, MDnear }, { 1, 0, 0 });
		if (m_velocity.x < 0 && s < h && Between(s*m_velocity.y, MDbottom, MDbottom + MDheight) && Between(s*m_velocity.z, MDnear, MDnear + MDdepth))
		{
			h = s ;
		}
		
		
		// Y min
		s = CollisionLineToPlane({ 0, 0, 0 }, m_velocity, { MDleft, MDbottom, MDnear }, { 0, -1, 0 });
		if (m_velocity.y > 0 && s < h && Between(s*m_velocity.x, MDleft, MDleft + MDwidth) && Between(s*m_velocity.z, MDnear, MDnear + MDdepth))
		{
			h = s;
		}

		// Y max
		s = CollisionLineToPlane({ 0, 0, 0 }, m_velocity, { MDleft, MDbottom + MDheight, MDnear }, { 0, 1, 0 });
		if (m_velocity.y < 0 && s < h && Between(s*m_velocity.x, MDleft, MDleft + MDwidth) && Between(s*m_velocity.z, MDnear, MDnear + MDdepth))
		{
			h = s;
		}
		

		// Z min
		s = CollisionLineToPlane({ 0, 0, 0 }, m_velocity, { MDleft, MDbottom, MDnear }, { 0, 0, -1 });
		if (m_velocity.z >0 && s < h && Between(s*m_velocity.x, MDleft, MDleft + MDwidth) && Between(s*m_velocity.y, MDbottom, MDbottom + MDheight))
		{
			h = s;
		}

		
		// Z max
		s = CollisionLineToPlane({ 0, 0, 0 }, m_velocity, { MDleft, MDbottom, MDnear + MDdepth }, { 0, 0, 1 });
		if (m_velocity.z < 0 && s < h && Between(s*m_velocity.x, MDleft, MDleft + MDwidth) && Between(s*m_velocity.y, MDbottom, MDbottom + MDheight))
		{
			h = s;
		}
		
		if (h < 1) {
			retVal.first = true;
			retVal.second = h;
		}
	

	
		return retVal;
	};

	
}


Capsule::~Capsule()
{
}

void Capsule::UpdatePosition(glm::vec3 bottomCenter)
{
	Recalc(bottomCenter);
}

float Capsule::CalculateColidingTime(glm::vec3 velocity)
{
	m_velocity = velocity;
	m_staticOnlyCheck = false;
	return CollisionCalc();	
}

float Capsule::CalculateColidingTime()
{
	m_staticOnlyCheck = true;
	return CollisionCalc();
}

float Capsule::CollisionCalc()
{
	auto col = SceneManager::Get()->Collision(m_collisionLambda);

	if (col.first) {
		std::cout << "Collision" << std::endl;
		return col.second;
	}
	else return 42;
}

void Capsule::Recalc(glm::vec3 bottomCenter)
{
	m_body.first = { bottomCenter.x - m_width / 2.f, bottomCenter.y, bottomCenter.z - m_width / 2.f };
	m_body.second = { bottomCenter.x + m_width / 2.f, bottomCenter.y + m_height, bottomCenter.z + m_width / 2.f };
	
}

float Capsule::CollisionLineToPlane(glm::vec3 originA, glm::vec3 endA, glm::vec3 originB, glm::vec3 endB)
{ 
	auto BdotA = glm::dot(endB, endA);
	if (BdotA == 0) return std::numeric_limits<float>::infinity();

	return abs((
		endB.x*(originB.x - originA.x) +
		endB.y*(originB.y - originA.y) +
		endB.z*(originB.z - originA.z)
		) / BdotA);
	
}

bool Capsule::Between(float x, float a, float b)
{
	return x >= a && x <= b;
}
