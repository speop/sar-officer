// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 02-22-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-16-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <memory>
#include <vector>
#include <stack>
#include <limits>
#include  <functional>
#include "glm\glm.hpp"
#include "Defines.h"
#include "Ray.h"


/// <summary>
/// The SAR namespace.
/// </summary>
namespace SAR {

	/// <summary>
	/// Class AABBQuadTree.
	/// </summary>
	template <typename T>
	class AABBQuadTree
	{

	public: class Record;

	private:
		/// <summary>
		///  Internal class representing tree node.
		/// </summary>
		/// <seealso cref="std::enable_shared_from_this{Node}" />
		class Node : std::enable_shared_from_this<Node> {
		public:
			
			Node* m_parrent;
			std::vector<std::shared_ptr<typename AABBQuadTree<T>::Node::Record>> m_data;
			/// <summary>
			/// Stores data which doesn't fit to any child.
			/// </summary>
			std::vector<std::shared_ptr<Record>> m_notMovableData;
			bool m_hasChildren = false;
			glm::dvec3 m_minCorner;
			glm::dvec3 m_maxCorner;
			std::shared_ptr<Node> m_children[4];

			/// <summary>
			/// Incs the x.
			/// </summary>
			/// <param name="index">The index.</param>
			/// <returns>int.</returns>
			static int IncX(int index = 0) { return index + 1; }

			/// <summary>
			/// Incs the z.
			/// </summary>
			/// <param name="index">The index.</param>
			/// <returns>int.</returns>
			static int IncZ(int index = 0) { return index + 2; }

		public:


			/// <summary>
			/// Initializes a new instance of the <see cref="AABBQuadTree"/> class.
			/// </summary>
			/// <param name="minCorner">The minimum corner.</param>
			/// <param name="maxCorner">The maximum corner.</param>
			/// <param name="parent">The parent.</param>
			Node(const glm::dvec3& minCorner, const glm::dvec3& maxCorner, Node* parent)
			{
				m_parrent = parent;
				m_minCorner = minCorner;
				m_maxCorner = maxCorner;
			}

			/// <summary>
			/// Makes the node children.
			/// </summary>
			void MakeChildren()
			{
				glm::dvec3 min;
				glm::dvec3 max;

				m_hasChildren = true;
				min.y = 0;
				max.y = 0;

				for (uint i = 0; i < 4; i++) {
					min.x = i & IncX() ? (m_minCorner.x + m_maxCorner.x) / 2.0 : m_minCorner.x;
					min.z = i & IncZ() ? (m_minCorner.z + m_maxCorner.z) / 2.0 : m_minCorner.z;					
					max.x = i & IncX() ? m_maxCorner.x : (m_minCorner.x + m_maxCorner.x) / 2.0;
					max.z = i & IncZ() ? m_maxCorner.z : (m_minCorner.z + m_maxCorner.z) / 2.0;
					
					m_children[i] = std::make_shared<Node>(min, max, this);
				}
			}

			/// <summary>
			/// Recalc Y dimension and propagate changes to all parents.
			/// </summary>
			/// <param name="ymin">The ymin.</param>
			/// <param name="ymax">The ymax.</param>
			void RecalcY(double ymin, double ymax) {
				Node* node = this;

				while (node != nullptr) {
					node->m_minCorner.y =  (std::min)(ymin, node->m_minCorner.y);
					node->m_maxCorner.y = (std::max)(ymax, node->m_maxCorner.y);
					node = node->m_parrent;
				}
			}

			/// <summary>
			/// Finds the data slot where should be stored current data.
			/// </summary>
			/// <param name="minCorner">The minimum corner.</param>
			/// <param name="maxCorner">The maximum corner.</param>
			/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
			std::pair<int, int> FindDataSlot(const glm::dvec3& minCorner, const glm::dvec3& maxCorner)
			{
				int notFitAxisFlag = 0;
				int fitIndex = 0;

				// x fit
				if (minCorner.x >= this->m_children[0]->m_minCorner.x && maxCorner.x <= this->m_children[0]->m_maxCorner.x) {
					fitIndex |= 0;
				}
				else if (minCorner.x >= this->m_children[IncX(0)]->m_minCorner.x && maxCorner.x <= this->m_children[IncX()]->m_maxCorner.x) {
					fitIndex |= IncX();
				}
				else {
					notFitAxisFlag |= IncX();
				}

				//z fit
				if (minCorner.z >= this->m_children[0]->m_minCorner.z && maxCorner.z <= this->m_children[0]->m_maxCorner.z) {
					fitIndex |= 0;
				}
				else if (minCorner.z >= this->m_children[IncZ()]->m_minCorner.z && maxCorner.z <= this->m_children[IncZ()]->m_maxCorner.z) {
					fitIndex |= IncZ();
				}
				else {
					notFitAxisFlag |= IncZ();
				}

				return std::make_pair(notFitAxisFlag, fitIndex);
			}

			/// <summary>
			/// Checks if new item overlaps with already stored items.
			/// </summary>
			/// <param name="newItem">The new item.</param>
			/// <returns>bool.</returns>
			bool Overlap(std::shared_ptr<Record> newItem) {
				for (const auto &original : this->m_data) {
					if (
						newItem->minCorner.x < original->minCorner.x && newItem->maxCorner.x >= original->minCorner.x ||
						newItem->maxCorner.x >  original->maxCorner.x && newItem->minCorner.x <= original->maxCorner.x ||
						newItem->minCorner.z < original->minCorner.z && newItem->maxCorner.z >= original->minCorner.z ||
						newItem->maxCorner.z >  original->maxCorner.z && newItem->minCorner.z <= original->maxCorner.z 
						) {
						return true;
					}
				}
				return false;
			}

			/// <summary>
			/// Loop recursion. Recuresivly calls method for finding data slot and on bottomest level adds data to returned slot. If no slot is returned data does not fit to anyone and will be stored in current recursion node.
			/// </summary>
			/// <param name="rootNode">The root node.</param>
			/// <param name="data">The data.</param>
			/// <param name="maxNodeItem">The maximum node item.</param>
			void AddData(std::shared_ptr<Node> rootNode, std::shared_ptr<Record> data, uint maxNodeItem)
			{
				auto node = rootNode;

				while (node != nullptr) {
					
					//we can store data in current node
					if (!node->m_hasChildren && (node->m_data.size() < maxNodeItem || node->Overlap(data))) {
						data->node = node.get();
						data->notMovable = false;
						node->m_data.push_back(data);
						node->RecalcY(data->minCorner.y, data->maxCorner.y);
						return;
					}


					else if (!node->m_hasChildren) {
						//we can't store data in current node so we must make children and move data from this node to children
						node->MakeChildren();

						//moving data to child nodes
						for (auto& item : node->m_data) {
							AddData(node, item, maxNodeItem);
						}

						//emptying 
						std::vector<std::shared_ptr<Record>> dat;
						node->m_data.swap(dat);

					}


					//tree traversing check where we can store our data
					auto searchResult = node->FindDataSlot(data->minCorner, data->maxCorner);
					if (searchResult.first == 0) {
						node = node->m_children[searchResult.second];
						continue;
					}
				

					//we can't store data in any children (data node is bigger or intersecting witx axis) so we are storing data in closest fitting node which is obviously current one
					data->notMovable = true;
					node->m_notMovableData.push_back(data);
					node->RecalcY(data->minCorner.y, data->maxCorner.y);
					return;
					
				}
				
			}

			/// <summary>
			/// Finds the element which is intersected by given ray.
			/// </summary>
			/// <param name="rootNode">The root node.</param>
			/// <param name="ray">The ray.</param>
			/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
			std::pair<float, std::shared_ptr<Record>> FindElement(std::shared_ptr<Node> rootNode, const Ray &ray)
			{
				std::pair<float, std::shared_ptr<Record>> object;
				std::shared_ptr<Node> node;
				std::stack<std::shared_ptr<Node>> nodeStack;
				nodeStack.push(rootNode);

				while (!nodeStack.empty()) {
					node = nodeStack.top();
					nodeStack.pop();

					if (node == nullptr) continue;

					//check if user clicked on some not movable data
					for (auto &nmData : node->m_notMovableData) {
						auto intersect = ray.IntersectAABB(nmData->minCorner, nmData->maxCorner);
						StoreFoundElement(object, intersect, nmData);
					}

					//now check all children.. we are performing DFS with leftiest way
					if (node->m_hasChildren) {
						for (int i = 3; i >= 0; i--) {
							auto pomNode = node->m_children[i];
							auto intersect = ray.IntersectAABB(pomNode->m_minCorner, pomNode->m_maxCorner);

							//we would work with node only if there is intersection or all objects are in front of currently found object
							if (!intersect.first || object.second != nullptr && object.first <= intersect.second) continue;
							nodeStack.push(pomNode);
						}
					}
					//node doesn't have any children -> we check its data
					else {
						for (auto &data : node->m_data) {
							auto intersect = ray.IntersectAABB(data->minCorner, data->maxCorner);
							StoreFoundElement(object, intersect, data);
						}
					}
				}

				return object;
			}

			

			/// <summary>
			/// Elem finding helper function.
			/// </summary>
			/// <param name="object">The object.</param>
			/// <param name="findResult">The find result.</param>
			/// <param name="rec">The record.</param>
			void StoreFoundElement(std::pair<float, std::shared_ptr<Record>> &object, const std::pair<bool, float> &findResult, std::shared_ptr<Record> rec)
			{
				if (findResult.first == false) return;

				if (object.second == nullptr || object.first > findResult.second) {
					object.first = findResult.second;
					object.second = rec;
				}

			}


			/// <summary>
			/// Checks if the data fits to current node.
			/// </summary>
			/// <param name="minCorner">The minimum corner.</param>
			/// <param name="maxCorner">The maximum corner.</param>
			/// <returns>bool.</returns>
			bool CheckDataFits(const glm::dvec3 &minCorner, const glm::dvec3 &maxCorner)
			{
				return  m_minCorner.x < minCorner.x  && m_minCorner.z < minCorner.z
					&& m_maxCorner.x > maxCorner.x  && m_maxCorner.z > maxCorner.z;
			}

			/// <summary>
			/// Deletes the element.
			/// </summary>
			/// <param name="rec">The record.</param>
			void DeleteElement(std::shared_ptr<Record> rec)
			{
				auto  ourDataVector = rec->notMovable ? &(m_notMovableData) : &(m_data);

				for (uint i = 0; i < ourDataVector->size(); i++) {
					auto pomRec = ourDataVector->at(i);

					if (pomRec == rec) {
						ourDataVector->erase(ourDataVector->begin() + i);
						break;
					}
				}
			}


			/// <summary>
			/// Make preorder on tree.
			/// </summary>
			/// <param name="lambda">The lambda.</param>
			void PreOrder(std::function<void(T)> lambda) {
				std::stack<Node*> nodeStack;
				nodeStack.push(this);

				while (!nodeStack.empty()) {
					auto node = nodeStack.top();
					nodeStack.pop();
					if (node == nullptr) continue;

					for (const auto& record : node->m_notMovableData) {
						lambda(record->data);
					}

					for (const auto& record : node->m_data) {
						lambda(record->data);
					}

					for (int i = 0; i < 4; i++) {
						nodeStack.push(node->m_children[i].get());
					}



				}
			}

			/// <summary>
			/// Collision detection. Collision function is specified by lambda and obtains node's data.
			/// </summary>
			/// <param name="lambda">The lambda.</param>
			/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
			std::pair<bool, float> Collision(std::function<std::pair<bool, float>(glm::vec3, glm::vec3)> lambda)
			{
				
				std::stack<Node*> nodeStack;
				std::pair<bool, float> collisionResult;
				std::pair<bool, float> retVal({false, (std::numeric_limits<float>::max)() });
				nodeStack.push(this);
				float penetration = retVal.second;  

				while (!nodeStack.empty()) {
					auto node = nodeStack.top();
					nodeStack.pop();
					if (node == nullptr) continue;

					//check if user clicked on some not movable data
					for (auto &nmData : node->m_notMovableData) {
						collisionResult = lambda(nmData->minCorner, nmData->maxCorner);
						if (collisionResult.first && abs(collisionResult.second) < abs(penetration)) {
							penetration = collisionResult.second;
							retVal = collisionResult;
						}
					}

					//now check all children.. we are performing DFS with leftiest way
					if (node->m_hasChildren) {
						for (int i = 3; i >= 0; i--) {
							auto pomNode = node->m_children[i];
							 collisionResult = lambda(pomNode->m_minCorner, pomNode->m_maxCorner);

							//we would work with node only if there is intersection or all objects are in front of currently found object
							if (!collisionResult.first) continue;
							nodeStack.push(pomNode.get());
						}
					}
					//node doesn't have any children -> we check its data
					else {
						for (auto &data : node->m_data) {
							collisionResult = lambda(data->minCorner, data->maxCorner);
							if (collisionResult.first && abs(collisionResult.second) < abs(penetration)) {
								penetration = collisionResult.second;
								retVal = collisionResult;
							}
						}
					}
				}

				return retVal;
			}



		};


		
		uint m_MaxDataInNode = 1;
		uint m_MinDataInNode = 1;
		std::shared_ptr<Node> m_rootNode;


	public:
		/// <summary>
		/// Class Record.
		/// </summary>
		class Record {
			
			friend AABBQuadTree;
			friend Node;
			Node* node;
			bool notMovable = false;
			glm::dvec3 minCorner;
			glm::dvec3 maxCorner;

		public:
			T data;
		};


		/// <summary>
		/// Initializes a new instance of the <see cref="AABBQuadTree"/> class.
		/// </summary>
		/// <param name="max">The maximum.</param>
		AABBQuadTree(double max)
		{
			Construct(0, max,  0, max);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AABBQuadTree"/> class.
		/// </summary>
		/// <param name="min">The minimum.</param>
		/// <param name="max">The maximum.</param>
		AABBQuadTree(double min, double max)
		{
			Construct(min, max, min, max);
		}

	
		/// <summary>
		/// Initializes a new instance of the <see cref="AABBQuadTree"/> class.
		/// </summary>
		/// <param name="minX">The minimum x.</param>
		/// <param name="maxX">The maximum x.</param>
		/// <param name="minZ">The minimum z.</param>
		/// <param name="maxZ">The maximum z.</param>
		AABBQuadTree(double minX, double maxX, double minZ, double maxZ)
		{
			Construct(minX, maxX, minZ, maxZ);
		}



		/// <summary>
		/// Add data to tree
		/// </summary>
		/// <param name="data">Our data</param>
		/// <param name="minCorner">Min corner of aabb</param>
		/// <param name="maxCorner">Max corner of aabb</param>
		/// <returns>std.shared_ptr&lt;_Ty&gt;.</returns>
		std::shared_ptr<Record> AddData(T data, glm::dvec3 minCorner, glm::dvec3 maxCorner)
		{
			auto rec = std::make_shared<Record>();
			rec->minCorner = minCorner;
			rec->maxCorner = maxCorner;
			rec->data = data;
			//if it ended up in root node  it would have nullptr as node ptr..in other cases node ptr will be overwritten by current node ptr
			rec->node = m_rootNode.get();

			m_rootNode->AddData(m_rootNode, rec, m_MaxDataInNode);
			return rec;
		}

		/// <summary>
		/// Updates the data position.
		/// </summary>
		/// <param name="rec">The record.</param>
		/// <param name="minCorner">The minimum corner.</param>
		/// <param name="maxCorner">The maximum corner.</param>
		void UpdateDataPos(std::shared_ptr<Record> rec, const glm::dvec3 &minCorner, const glm::dvec3 &maxCorner)
		{
			bool fits = rec->node->CheckDataFits(minCorner, maxCorner);

			//update values
			rec->minCorner = minCorner;
			rec->maxCorner = maxCorner;

			//if data still fits to their node we don't have to do anything
			if (fits) {
				rec->node->RecalcY(minCorner.y, maxCorner.y);
				return;
			}

			//data not fits we are removing them 
			DeleteElement(rec);

			//and insert them as new data
			m_rootNode->AddData(m_rootNode, rec, m_MaxDataInNode);
		}

		/// <summary>
		/// Finds the element.
		/// </summary>
		/// <param name="ray">The ray.</param>
		/// <returns>std.shared_ptr&lt;_Ty&gt;.</returns>
		std::shared_ptr<Record> FindElement(const Ray &ray)
		{
			auto object = m_rootNode->FindElement(m_rootNode, ray);
			return object.second;
		}

		/// <summary>
		/// Colllisions the specified lambda.
		/// </summary>
		/// <param name="lambda">The lambda.</param>
		/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
		std::pair<bool, float> Colllision(std::function<std::pair<bool, float>(glm::vec3, glm::vec3)> lambda)
		{
			return m_rootNode->Collision(lambda);
		}

	
		/// <summary>
		/// Deletes the element.
		/// </summary>
		/// <param name="rec">The record.</param>
		void DeleteElement(std::shared_ptr<Record> rec)
		{
			rec->node->DeleteElement(rec);
		}

		/// <summary>
		/// Does preorder on tree.
		/// </summary>
		/// <param name="lambda">The lambda.</param>
		void PreOrder(std::function<void(T)> lambda) {
			m_rootNode->PreOrder(lambda);
		}

		/// <summary>
		/// Gets the aabb.
		/// </summary>
		/// <returns>std.pair&lt;_Ty1, _Ty2&gt;.</returns>
		std::pair<glm::dvec3, glm::dvec3> GetAABB() {
			return{ m_rootNode->m_minCorner, m_rootNode->m_maxCorner };
		}

		
		/// <summary>
		/// Finalizes an instance of the <see cref="AABBQuadTree"/> class.
		/// </summary>
		~AABBQuadTree() {};

		private:

			/// <summary>
			/// Something like constructor. This function is called from all constructors and creates tree.
			/// </summary>
			/// <param name="minX">Scene min X corner</param>
			/// <param name="maxX">Scene max X corner</param>
			/// <param name="minZ">Scene min Z corner</param>
			/// <param name="maxZ">Scene max Z corner</param>
			void Construct(double minX, double maxX, double minZ, double maxZ)
			{
				auto minCorner = glm::dvec3(minX, 0, minZ);
				auto maxCorner = glm::dvec3(maxX, 0, maxZ);

				m_rootNode = std::make_shared<Node>(minCorner, maxCorner, nullptr);
				m_rootNode->MakeChildren();
			}
	};

}
