#include "CollisionManager.h"
#include <algorithm>
#include "Texture.h"

CollisionManager & CollisionManager::SetHeightMap(std::string path)
{
	m_HeightMap = std::make_unique<fipImage>();
	bool result = m_HeightMap->load(path.c_str());
	
	if (!result) {
		throw TextureException("Cannot load Heightmap!", path);
	}

	m_HeightMap->convertToFloat();
	float* ptr = (float*)m_HeightMap->accessPixels();
	auto w = m_HeightMap->getWidth();
	auto h = m_HeightMap->getHeight();

	m_minHeight = std::numeric_limits<float>::infinity();
	m_maxHeight = -m_minHeight;

	for(auto i = 0; i < h*w; i++) {
		m_minHeight = (std::min)(m_minHeight, *(ptr + i));
		m_maxHeight = (std::max)(m_maxHeight, *(ptr + i));
	}
	

	return *this;
}

float CollisionManager::GetHeight(glm::vec2 coords, bool centered)
{
	if (m_HeightMap.get() == nullptr) throw CollisionNoDataException("No heightmpa data. Did you load some heightmap?");

	float* ptr = (float*)m_HeightMap->accessPixels();
	auto w = m_HeightMap->getWidth();
	auto h = m_HeightMap->getHeight();
	if (centered) {
		coords.x += w / 2.0f;
		coords.y += h / 2.0f;
	}

	coords.x = coords.x < 0 || coords.x > w - 1 ? 0 : coords.x;
	coords.y = coords.y < 0 || coords.y > h - 1 ? 0 : coords.y;

	
	// coord = width * y + x 
	// coord = 4096 *y + x, x e [0 .. 4095], y e [0 .. 4095]
	int coord = static_cast<int>( w * coords.y + coords.x);

	
	float value = *(ptr + coord);
	return value*m_hMultiplier + m_hOffset;
}
float CollisionManager::GetHeightInterpolated(glm::vec2 coords, bool centered)
{
	return GetHeightInterpolatedAndSlope(coords, centered).first;
}

std::pair<glm::vec3, glm::vec3> CollisionManager::GetAABB(bool centered)
{
	float* ptr = (float*)m_HeightMap->accessPixels();
	auto w = m_HeightMap->getWidth();
	auto h = m_HeightMap->getHeight();
	auto wBorder = centered? w / 2.f  : w;
	auto hBorder = centered ? h / 2.f : h;

	return {{wBorder - w, m_minHeight * m_hMultiplier + m_hOffset,hBorder-h}, {wBorder,m_maxHeight* m_hMultiplier + m_hOffset,hBorder }};
}


std::pair<float, glm::vec2> CollisionManager::GetHeightInterpolatedAndSlope(glm::vec2 coords, bool centered)
{
	
	if (m_HeightMap.get() == nullptr) throw CollisionNoDataException("No heightmpa data. Did you load some heightmap?");

	float* ptr = (float*)m_HeightMap->accessPixels();
	auto w = m_HeightMap->getWidth();
	auto h = m_HeightMap->getHeight();
	if (centered) {
		coords.x += w / 2.0f;
		coords.y += h / 2.0f;
	}

	if (coords.x < 1 || coords.x > w - 2 || coords.y < 1 || coords.y > h - 2) return{ GetHeight(coords, false),{0,0} };

	float a,b;
	modf(coords.x, &a);
	a = coords.x - a;
	modf(coords.y, &b);
	b = coords.y - b;

	int p1Coord = static_cast<int>( w * floor(coords.y) + floor(coords.x));
	int p2Coord = static_cast<int>( w * floor(coords.y) + ceil(coords.x));
	int p3Coord = static_cast<int>( w * ceil(coords.y) + floor(coords.x));
	int p4Coord = static_cast<int>( w * ceil(coords.y) + ceil(coords.x));
	
	float p1 = *(ptr + p1Coord);
	float p2 = *(ptr + p2Coord);
	float p3 = *(ptr + p3Coord);
	float p4 = *(ptr + p4Coord);


	auto dxTop = a*p4 + (1 - a)*p3;
	auto dxBottom = a*p2 + (1 - a)*p1;
	
	auto dyLeft = b*p3 + (1 - b)*p1;
	auto dyRight = b*p4 + (1 - b)*p2;
	
	glm::vec2 slope = { dyLeft - dyRight,	dxBottom - dxTop };


	float val = b*(dxTop) + (1 - b)*(dxBottom);
	return{ val*m_hMultiplier + m_hOffset, slope*m_hMultiplier };
}

