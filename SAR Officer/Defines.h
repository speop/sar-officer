#pragma once
#define VERSION "1.0.0"

typedef unsigned int uint;
enum class GameState { PLAY, EXIT, LOADING, PAUSE, DONE, FOCUS_OUT, MENU , EDITOR};
enum RenderPass{CLASSIC, SHADOW, GRASS};

//OGL debug macro
#define ___\
		for(auto err = glGetError();err != GL_NO_ERROR;){\
			std::cerr << __FILE__<<" "<<__LINE__<<" OpenGL error: " << err << std::endl;break;}