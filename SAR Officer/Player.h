// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 05-06-2017
//
// Last Modified By : David Buchta
// Last Modified On : 05-06-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <glm/glm.hpp>
#include "Camera.h"
#include "Capsule.h"

/// <summary>
/// The GO namespace.
/// </summary>
namespace GO {

	/// <summary>
	/// Class Player.
	/// </summary>
	class Player
	{
	public:
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Player"/> class.
		/// </summary>
		/// <param name="camera">The camera.</param>
		Player(::PerspectiveCamera* camera);
		/// <summary>
		/// Finalizes an instance of the <see cref="Player"/> class.
		/// </summary>
		~Player();
		
		/// <summary>
		/// Processes the move.
		/// </summary>
		/// <param name="moveVec">The move vec.</param>
		void ProcessMove(glm::vec2 moveVec);
		/// <summary>
		/// Process jump.
		/// </summary>
		void Jump();

	private:
		
		/// <summary>
		/// Updates the position.
		/// </summary>
		/// <param name="pos">The position.</param>
		/// <param name="height">The height.</param>
		void UpdatePosition(glm::vec2 pos, float height);
		
		/// <summary>
		/// Check if there is collision.
		/// </summary>
		/// <param name="moveVec">The move vec.</param>
		/// <returns>glm.vec3.</returns>
		glm::vec3 CollisionCheck(glm::vec2 moveVec);

		/// <summary>
		/// Does the jumping.
		/// </summary>
		void DoJumping();

		
		::PerspectiveCamera* m_camera;
		float m_height = 5.f;
		float m_width = 1.f;
		float m_movementBias = 0.2f;
		float m_movementSpeedBias = 0.02f;
		float m_maxSlope = 0.5f;
		float m_maxSlopeWithoutGravity = 0.6f;
		float m_radius;
		float m_movementSpeed = 16.33f;
		float m_gravitySpeed = 4.96f;
		float m_jumpinSpeed = 0.88f;
		float m_velocityY = 0.f;
		bool m_onGround = true;
		bool m_jumping = false;
		bool m_startJump = false;
		glm::vec3 m_position;
		std::unique_ptr<Capsule> m_capsule;
	};
}

