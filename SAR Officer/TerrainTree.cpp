#include "TerrainTree.h"
/*
	-----------------
	|       |       |				N
	|   4   |   3   |
	|       |       |
	-----------------		W				E
	|       |       |
	|   1   |   2   |
	|       |       |
	-----------------			   S

*/


bool TerrainNode::HasChildren()
{
	return child1 != nullptr || child2 != nullptr || child3 != nullptr || child4 != nullptr;
}


TerrainTree::TerrainTree()
{
}


TerrainTree::~TerrainTree()
{
}

void TerrainTree::CreateTree(Coords origin, float width, float height, bool deleteOld)
{
	if (deleteOld) { ClearTree(); }
	
	treeRoot = new TerrainNode(nullptr, 0, origin, width, height);
	treeRoot->DivideNode();
}

void TerrainTree::ShutdownTree()
{
	ClearTree();
}

TerrainNode * TerrainTree::GetRoot()
{
	return treeRoot;
}

TerrainNode* TerrainTree::FindNode(float x, float z)
{
	return treeRoot != nullptr ? FindNode(treeRoot,x,z) : nullptr;
}

TerrainNode * TerrainTree::FindNode(TerrainNode* root, float x, float z)
{
	if (root == nullptr) return nullptr;

	if (root->origin.x == x && root->origin.z == z || !root->HasChildren()) return root;

	if (root->origin.x >= x && root->origin.z >= z && root->child1 != nullptr) return FindNode(root->child1, x, z);
	else if (root->origin.x <= x && root->origin.z >= z && root->child2 != nullptr) return FindNode(root->child2, x, z);
	else if (root->origin.x <= x && root->origin.z <= z && root->child3 != nullptr) return FindNode(root->child3, x, z);
	else if (root->origin.x >= x && root->origin.z <= z && root->child4 != nullptr) return FindNode(root->child4, x, z);

	return root;
}


void TerrainTree::ClearTree()
{
	if (treeRoot == nullptr) return;
	ClearTree(treeRoot);
	delete treeRoot;
	treeRoot = nullptr;
}

void TerrainTree::ClearTree(TerrainNode * root)
{
	if(root->child1 != nullptr){
		ClearTree(root->child1);
		delete root->child1;
		root->child1 = nullptr;
	}

	if (root->child2 != nullptr) {
		ClearTree(root->child2);
		delete root->child2;
		root->child2 = nullptr;
	}
	
	if (root->child3 != nullptr) {
		ClearTree(root->child3);
		delete root->child3;
		root->child3 = nullptr;
	}
	if (root->child4 != nullptr) {
		ClearTree(root->child4);
		delete root->child4;
		root->child4 = nullptr;
	}
}



bool TerrainNode::DivideNode()
{
	// Subdivide
	float wNew = 0.5f * this->width;
	float hNew = 0.5f * this->height;

	// Create the child nodes
	this->child1 = new TerrainNode(this, 1, Coords(origin.x - 0.5f * wNew, origin.y, origin.z - 0.5f * hNew), wNew, hNew);
	this->child2 = new TerrainNode(this, 2, Coords(origin.x + 0.5f * wNew, origin.y, origin.z - 0.5f * hNew), wNew, hNew);
	this->child3 = new TerrainNode(this, 3, Coords(origin.x + 0.5f * wNew, origin.y, origin.z + 0.5f * hNew), wNew, hNew);
	this->child4 = new TerrainNode(this, 4, Coords(origin.x - 0.5f * wNew, origin.y, origin.z + 0.5f * hNew), wNew, hNew);

	switch (this->type)
	{
	case 1:
		this->neihbourEast = this->parent->child2;
		this->neihbourNorth = this->parent->child4;
		break;
	case 2:
		neihbourNorth = parent->child3;
		neihbourWest = parent->child1;
	case 3:
		neihbourWest = parent->child4;
		neihbourSouth = parent->child2;
	case 4:
		neihbourEast = parent->child3;
		neihbourSouth = parent->child1;
	default:
		break;
	}

	if (child1->CheckDivideNode()) child1->DivideNode();
	if (child2->CheckDivideNode()) child2->DivideNode();
	if (child3->CheckDivideNode()) child3->DivideNode();
	if (child4->CheckDivideNode()) child4->DivideNode();

	return true;
}

bool TerrainNode::CheckDivideNode()
{
	auto camera = PerspectiveCamera::GetCameraInstance("Basic");
	auto cameraPos = camera->GetPosition();

	// Distance from current origin to camera
	float d = abs(
		sqrt(
			powf((cameraPos.x - this->origin.x), 2.0f) +
			powf((cameraPos.z - this->origin.z), 2.0f)
		));

	// Check base case:
	// Distance to camera is greater than twice the length of the diagonal
	// from current origin to corner of current square.
	// OR
	// Max recursion level has been hit
	if (d > 2.5 * sqrt(powf(0.5f * this->width, 2.0f) + powf(0.5f * this->height, 2.0f)) || this->width < TERRAIN_REC_CUTOFF)
	{
		return false;
	}

	return true;
}


void TerrainTree::CalcTessScale()
{
	CalcTessScale(treeRoot);
}



void TerrainTree::CalcTessScale(TerrainNode * node)
{
	/* 

	--------------|--------------
	|             |             | 
	|             |             |
	|             |             |
	|             |             |
	|             |             |
	--------------|--------------
	|             |  |          |
	|             |  |          |
	|      o      |+1|          |
	|             |  |          |
	|             |  |          |
	--------------|--------------


	 */
	// Positive Z (north)
	auto t = FindNode(treeRoot, node->origin.x, node->origin.z + 1 + node->width / 2.0f);		
	if (t->width > node->width)	node->tscalePosZ = 2;

	// Positive X (east)
	t = FindNode(treeRoot, node->origin.x + 1 + node->width / 2.0f, node->origin.z);
	if (t->width > node->width)	node->tscalePosX = 2;

	// Negative Z (south)
	t = FindNode(treeRoot, node->origin.x, node->origin.z - 1 - node->width / 2.0f);
	if (t->width > node->width)	node->tscaleNegZ = 2;

	// Negative X (west)
	t = FindNode(treeRoot, node->origin.x - 1 - node->width / 2.0f, node->origin.z);
	if (t->width > node->width)	node->tscaleNegX = 2;
}