// ***********************************************************************
// Assembly         : 
// Author           : David Buchta
// Created          : 10-22-2016
//
// Last Modified By : David Buchta
// Last Modified On : 05-14-2017
// ***********************************************************************
// <summary></summary>
// ***********************************************************************
#pragma once
#include <GL\glew.h>
#include <string>
#include <vector>
#include <FreeImagePlus.h>
#include <memory>
#include "Exceptions.h"
#include "Shader.h"

/// <summary>
/// Class for simple texture or uniform array textures. This class cannot create classic texture Array.
/// </summary>
class Texture
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="Texture"/> class.
	/// </summary>
	/// <param name="program">The program.</param>
	Texture(std::shared_ptr<Shader> program);
	/// <summary>
	/// Initializes a new instance of the <see cref="Texture"/> class.
	/// </summary>
	Texture() : m_shader(ShaderNull) {};
	/// <summary>
	/// Finalizes an instance of the <see cref="Texture"/> class.
	/// </summary>
	~Texture();

	/// <summary>
	/// Loads all specified textures and bind them to OpenGL texture units.
	/// </summary>
	/// <param name="textureUnit">The texture unit.</param>
	/// <param name="filePaths">The file paths.</param>
	/// <param name="managed">The managed.</param>
	/// <returns>Texture *.</returns>
	Texture* Load(uint textureUnit, const std::vector<std::string>& filePaths, bool managed = true);

	/// <summary>
	/// Sets the shader.
	/// </summary>
	/// <param name="program">The program.</param>
	void SetShader(std::shared_ptr<Shader> program);

	/// <summary>
	/// Binds texture to sampler.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="textures">The textures.</param>
	/// <param name="textureUnit">The texture unit.</param>
	/// <param name="asArray">As array.</param>
	void Use(std::string name, const std::vector<GLuint> &textures, uint textureUnit, bool asArray = false);

	/// <summary>
	/// Binds texture to sampler.
	/// </summary>
	/// <param name="name">The name.</param>
	void Use(std::string name);
	
	/// <summary>
	/// Binds texture to sampler.
	/// </summary>
	/// <param name="binding">The binding.</param>
	void Use(GLuint binding);

	/// <summary>
	/// Binds texture to sampler..
	/// </summary>
	/// <param name="binding">The binding.</param>
	/// <param name="textureId">The texture identifier.</param>
	void Use(GLuint binding, GLuint textureId);

	/// <summary>
	/// Binds texture to sampler.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <param name="shader">The shader.</param>
	void Use(std::string name, std::shared_ptr<Shader> shader);

	/// <summary>
	/// Returns starting ID of textures
	/// </summary>
	/// <param name="index">The index.</param>
	/// <returns>GLuint.</returns>
	GLuint GetID(int index = 0);

	
	static const int ShaderNull = 0;

private:
	
	bool m_managed = false;
	std::vector<GLuint> m_textureIDs;
	uint m_textureUnit = 0;
	std::shared_ptr<Shader> m_shader;
};


#pragma region Exceptions
/// <summary>
/// Class TextureException.
/// </summary>
/// <seealso cref="BasicException" />
class TextureException : public BasicException
{
private:
	
	std::string texFile;

public:
	/// <summary>
	/// Initializes a new instance of the <see cref="TextureException"/> class.
	/// </summary>
	/// <param name="msg">The MSG.</param>
	/// <param name="file">The file.</param>
	TextureException(std::string msg, std::string file) : BasicException(msg), texFile(file) {};
	
	/// <summary>
	/// Return serialized esception MSG.
	/// </summary>
	/// <returns>std.string.</returns>
	std::string GetMsg() { return this->message + ": \"" + this->texFile + "\""; }
};


#pragma endregion 
