#include "SceneManager.h"
#include <limits>
#include <thread>
#include <fstream>
#include <iomanip>
#include <limits>
#include "CollisionManager.h"

/// <summary>
/// The m instance{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
SceneManager* SceneManager::m_instance = nullptr;


SceneManager::SceneManager()
{
	MakeScene();
	
}

SceneManager* SceneManager::Get()
{
	if (m_instance == nullptr) m_instance = new SceneManager();
	return  m_instance;
}

SceneManager* SceneManager::MakeScene()
{
	if (m_staticTree != nullptr) return this;
	
	m_staticTree = std::make_shared<SAR::AABBQuadTree<std::shared_ptr<SceneRecord>>>(-2047, 2048,  -2047, 2048);
	return this;
}

SceneManager* SceneManager::DeleteScene()
{
	m_staticTree = nullptr;	
	return this;
}

SceneManager::SceneRecord SceneManager::AddObject(const std::string& name, const std::string &category, const std::string &DataPath, const glm::vec3& MinCorner, const glm::vec3& MaxCorner, DataPtrPtr mPointers, std::shared_ptr<Model> model)
{
	auto rec = std::make_shared<SceneRecord>();
	rec->Indirect = false;
	rec->Model = model;
	rec->CategoryName = category;
	return AddObject(rec, name, DataPath, MinCorner,MaxCorner, mPointers);
}
SceneManager::SceneRecord SceneManager::AddObject(const std::string& name, const std::string &category, const std::string &DataPath, const glm::vec3& MinCorner, const glm::vec3& MaxCorner, DataPtrPtr mPointers, std::shared_ptr<GBuffer> gBuffer)
{
	auto rec = std::make_shared<SceneRecord>();	
	rec->Indirect = true;
	rec->GBuffer = gBuffer;	
	rec->CategoryName = category;
	return AddObject(rec, name, DataPath, MinCorner, MaxCorner, mPointers);
}

SceneManager::SceneRecord SceneManager::AddObject(std::shared_ptr<SceneRecord> templ, const std::string &name, const std::string &DataPath, const glm::vec3 &MinCorner, const glm::vec3 &MaxCorner, DataPtrPtr dataPointer)
{
	if (dataPointer == nullptr) return SceneRecord();
	templ->ObjectName = name;
	templ->FilePath = DataPath;
	templ->MinCorner = MinCorner;
	templ->MaxCorner = MaxCorner;

	for (auto& ptr : *dataPointer) {
		auto rec = std::make_shared<SceneRecord>(*templ);
		rec->DataPtr = ptr;

		glm::mat4 trans;
		if(rec->Indirect) {
			trans = rec->GBuffer->GetMatrix(ptr);
		}
		else {
			trans = rec->Model->GetInstanceTransformation(ptr);
		}

		ModelLocation::RecalcAABB(rec->MinCorner, rec->MaxCorner, trans);
		rec->AABBTreeRec = m_staticTree->AddData(rec, rec->MinCorner, rec->MaxCorner);
	}

	return SceneRecord(*templ);
}

void SceneManager::AddObject(SceneRecord templ, glm::mat4 position)
{
	auto object = std::make_shared<SceneRecord>(templ);
	ModelLocation::RecalcAABB(object->MinCorner, object->MaxCorner, position);


	std::vector<glm::mat4> pos;
	pos.push_back(position);

	if (object->Indirect) {
		auto ptrs = object->GBuffer->AddToBaked(object->ObjectName, { { object->MinCorner, object->MaxCorner, position }});
		object->DataPtr = ptrs->at(0);
	}
	else {
		std::vector<std::shared_ptr<DataPtr>> ptrs;
		object->Model->SetupInstancesTransformation({position}, ptrs);
		object->DataPtr = ptrs.at(0);
	}

		object->AABBTreeRec = m_staticTree->AddData(object, object->MinCorner, object->MaxCorner);

}

SceneManager * SceneManager::UpdateObject(std::shared_ptr<SceneRecord> object, const glm::mat4 & transform)
{
	if (object == nullptr) return this;

	std::thread worker([&]()
	{
		ModelLocation::RecalcAABB(object->MinCorner, object->MaxCorner, transform);
		m_staticTree->UpdateDataPos(object->AABBTreeRec, object->MinCorner, object->MaxCorner);
	});

	if (object->Indirect) {
		object->GBuffer->UpdateMatrix(object->DataPtr, transform);
	}else {
		object->Model->UpdateInstanceTransformation(object->DataPtr, transform);
	}

	worker.join();
	return this;
}

SceneManager* SceneManager::UpdateObjectData(std::shared_ptr<SceneRecord> object, uint data)
{
	if (object == nullptr) return this;
	if (object->Indirect) {
		object->GBuffer->UpdateData(object->DataPtr, data);
	}
	else {
		object->Model->SetInstanceAdditionalData(object->DataPtr, data);
	}
	return this;
}

SceneManager* SceneManager::DeleteObject(std::shared_ptr<SceneRecord> &object)
{
	if (object == nullptr) return this;
	if (object->Indirect) {
		object->GBuffer->DeleteFromBaked(object->DataPtr);
	}else {
		object->Model->DeleteInstanceTransformation(object->DataPtr);
	}

	m_staticTree->DeleteElement(object->AABBTreeRec);
	object == nullptr;
	return this;
}



std::shared_ptr<SceneManager::SceneRecord> SceneManager::FindObject(const SAR::Ray &ray)
{
	auto obj =  m_staticTree->FindElement(ray);
	return obj != nullptr ? obj->data : nullptr;
}

std::pair<glm::vec3, glm::vec3> SceneManager::GetObjectConstrains(const SceneRecord &object) const
{
	return std::pair<glm::vec3, glm::vec3>(object.MinCorner, object.MaxCorner);
}

void SceneManager::SaveScene()
{
	std::map<std::string, std::vector<glm::mat4>> serializedObjects;
	m_staticTree->PreOrder([&](std::shared_ptr<SceneRecord> record) {
		glm::mat4 transformation;
		if (record->Indirect) {
			transformation = record->GBuffer->GetMatrix(record->DataPtr);
		}
		else {
			transformation = record->Model->GetInstanceTransformation(record->DataPtr);
		}
		
		auto vec = Model::MapFind(serializedObjects, record->FilePath);
		vec->push_back(transformation);
	
	});

	

	for (const auto &model : serializedObjects) {
		std::ofstream myfile(model.first, std::ios::out);

		for (const auto& trans : model.second) {

			auto matPtr = glm::value_ptr(trans);

			for (auto i = 0; i < 16; i++) {
				myfile <<  std::setprecision(std::numeric_limits<float>::digits10 + 1) << matPtr[i];	
				myfile << " ";
			}

			myfile << std::endl;
		}

		myfile.close();
	}
}

std::pair<bool, float> SceneManager::Collision(std::function<std::pair<bool, float>(glm::vec3, glm::vec3)> lambda)
{
	return m_staticTree->Colllision(lambda);
}

std::pair<glm::dvec3, glm::dvec3> SceneManager::GetSceneAABB()
{
	glm::vec3 minb, maxb;

	if (m_world == "" || m_world == "Basic") {
		//Super Trouper beams are gonna blind me, But I won't feel blue, Like I always do....
		auto sceneeABBA = m_staticTree->GetAABB();
		auto terrainABBA = CollisionManager::Instance().GetAABB(true);

		
		for (auto i = 0; i < 3; i++) {
			minb[i] = (std::min)(glm::vec3(sceneeABBA.first)[i], terrainABBA.first[i]);
			maxb[i] = (std::max)(glm::vec3(sceneeABBA.second)[i], terrainABBA.second[i]);
		}

	}
	else if(m_world == "Clouds") {
		minb = {-7000,-100,-7000};
		maxb = { 7000,1220,7000 };
	}

	return{ minb, maxb };
}


SceneManager * SceneManager::SetWorld(std::string world)
{
	m_world = world;
	return this;
}
SceneManager::~SceneManager()
{
}
